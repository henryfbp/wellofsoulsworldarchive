# README

Hey,

This is a WoS world archive.

Please support the developers by buying their game at <http://www.synthetic-reality.com/wosHome.htm>! It's a gem!

Install by copying the WORDLNAME/ folder into C:/WOS/worlds/.

If you need me to add something, please open a pull request.

## Zip/unzip

If you're using Linux or MinGW, you can execute:

`cd scripts`

- `bash uncompress-worlds.sh`  
  to uncompress all worlds.

- `bash compress-worlds.sh`  
  to create `tar.gz` archives of all worlds in the `uncompressed-worlds` folder.

## Other Scripts

Check out `scripts` for other useful scripts.

Try `./list_worlds_urls.py --help`.