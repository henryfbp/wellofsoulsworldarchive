#!/usr/bin/env bash

. util.sh || echo "You must run this from within the scripts folder."

pushd ..

ensure_uncompressed_folder_exists
ensure_at_root_of_git_repo

# Make the compressed worlds folder if it doesn't exist.
mkdir -p compressed-worlds/

# Move into uncompressed-worlds folder.
pushd uncompressed-worlds/

# For all uncompressed worlds,
for dir in */ ; do
    name=`basename "${dir}"`
#    echo ${dir}
#    echo ${name}

    # Copy all files from one world dir into a .tar.gz file.
    # Do our work inside the uncompressed-worlds folder.
    GZIP=-9 tar cvzf "${name}.tar.gz" "${dir}" > /tmp/woswaCWLog.log 2>&1

    # Move the compressed file from the uncompressed folder to the compressed folder.
    mv "${name}.tar.gz" ../compressed-worlds/
    echo "Compressed '${name}'."

done

popd # return to root of git repo

popd # return to scripts folder