#!/usr/bin/env bash

. util.sh || echo "You must run this from within the linux-scripts folder."

pushd ..

ensure_at_root_of_git_repo
ensure_uncompressed_folder_exists

pushd uncompressed-worlds


# For all uncompressed worlds,
for dir in */ ; do
    basename=`basename "${dir}"`
    world_ini_path="${dir}/world.ini"

    world_name=$(grep "name=" "${world_ini_path}" | head -1)
    world_version=$(grep "version=" -m1 "${world_ini_path}" | head -1 )

    echo "${world_name}"
    echo "${world_version}"
    echo ""

done

popd # return to root of git directory

popd # return to scripts folder