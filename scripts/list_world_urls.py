#!/usr/bin/env python3
import argparse
import os
from typing import List

from util import WosWorld

parser = argparse.ArgumentParser()
parser.add_argument('--no-resolve-urls', help='Do not attempt to resolve URLs in WoS World INI files.',
                    action='store_true')

args = parser.parse_args()

WORLDS: List[WosWorld] = []

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

WORLDS_PATH = os.path.join(SCRIPT_DIR, '../uncompressed-worlds')
'''Path to WOS world folder.'''

ABS_WORLDS_PATH = os.path.abspath(WORLDS_PATH)

if os.path.basename(SCRIPT_DIR) != 'scripts':
	print("This script must be in the 'scripts' folder.")
	print(SCRIPT_DIR)
	exit(1)

if not os.path.exists(ABS_WORLDS_PATH):
	print("Could not find path " + ABS_WORLDS_PATH + " from " + os.getcwd() + " !")
	exit(1)

for worldFolderName in os.listdir(ABS_WORLDS_PATH):
	worldpath = os.path.join(ABS_WORLDS_PATH, worldFolderName)
	# print(worldpath)
	worldinipath = os.path.join(worldpath, 'world.ini')

	if os.path.exists(worldinipath):
		print('[x] ' + worldFolderName + " has a world.ini")

		WORLDS.append(
			WosWorld.from_ini_file(
				worldinipath, worldFolderName,
				should_resolve_urls=not args.no_resolve_urls))

	else:
		print('ERROR! ' + worldFolderName + " does not have a world.ini!")
		exit(1)

# sort and print
world: WosWorld
WORLDS = sorted(WORLDS, key=lambda world: world.folder_name)

for wosWorld in WORLDS:
	print(wosWorld)
	print()
