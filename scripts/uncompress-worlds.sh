#!/usr/bin/env bash

. util.sh || echo "You must run this from within the scripts folder."

pushd ..

ensure_compressed_folder_exists
ensure_at_root_of_git_repo

# Make the uncompressed worlds folder if it doesn't exist.
mkdir -p uncompressed-worlds/

# Move into compressed-worlds folder.
pushd compressed-worlds/

# For all compressed worlds,
for dir in *.tar.gz ; do
    name=`basename "${dir}"`

    tar xvzf "$dir" -C ../uncompressed-worlds/ > /tmp/woswaUCWLog.log 2>&1

    echo "Uncompressed '${name}'."

done

popd # return to root of git repo

popd # return to scripts folder
