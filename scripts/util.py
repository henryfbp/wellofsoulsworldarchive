# !/usr/bin/env python3
import socket
import urllib
from configparser import RawConfigParser
from urllib import request
from urllib.error import HTTPError
from urllib.parse import urlparse


def website_response_code(url: str) -> int:
	if url is None:
		return 0

	if url is '':
		return 0

	# ensure scheme is set
	url = urlparse(url)
	if url.scheme == '':
		url = urlparse('http://' + url.geturl())

	url = url.geturl()

	try:
		result = request.urlopen(url, timeout=5).getcode()

	except HTTPError as e:
		result = e.code
	except urllib.error.URLError as e:
		print(url, e)
		result = e.errno
	except ConnectionResetError as e:
		print('connection reset', url, e)
		result = e.errno
	except socket.timeout as e:
		print("socket timed out", url, e)
		result = e.errno

	if result is None:
		result = 0

	print(str(result) + ":" + url)

	return result


def bool_to_deadalive(b: bool) -> str:
	if b:
		return "ALIVE"
	else:
		return "--x--"


class WosWorld:

	def __init__(self, name, folder_name,
	             version=None, wos_version=None,
	             home_url=None, zip_url=None, ini_url=None,
	             should_resolve_urls=True):

		self.name = name
		self.folder_name = folder_name

		self.wos_version = wos_version
		self.version = version

		self.home_url = home_url
		self._home_url_response = None

		self.zip_url = zip_url
		self._zip_url_response = None

		self.ini_url = ini_url
		self._ini_url_response = None

		self.should_resolve_urls = should_resolve_urls

	def __str__(self):
		return f"""[{self.name}] at '{self.folder_name}/':" 
WoS v{self.wos_version}
World version v{self.version}
[{self.home_url_response:^5d}] Home URL is '{self.home_url}'
[{self.zip_url_response:^5d}] Zip URL is '{self.zip_url}'
[{self.ini_url_response:^5d}] Ini URL is '{self.ini_url}'"""

	# This caches the response and is a lazy way to initialize the website response.
	@property
	def home_url_response(self):

		if not self.should_resolve_urls:
			return 0

		if not self._home_url_response:
			self._home_url_response = website_response_code(self.home_url)

		return self._home_url_response

	# This caches the response and is a lazy way to initialize the website response.
	@property
	def zip_url_response(self):

		if not self.should_resolve_urls:
			return 0

		if not self._zip_url_response:
			self._zip_url_response = website_response_code(self.zip_url)

		return self._zip_url_response

	# This caches the response and is a lazy way to initialize the website response.
	@property
	def ini_url_response(self):

		if not self.should_resolve_urls:
			return 0

		if not self._ini_url_response:
			self._ini_url_response = website_response_code(self.ini_url)

		return self._ini_url_response

	@classmethod
	def from_ini_file(cls, ini_path: str, folder_name: str,
	                  should_resolve_urls=True):
		parser = RawConfigParser()
		parser.read(ini_path)

		return WosWorld(
			folder_name=folder_name,
			name=parser.get('world', 'name', fallback=None),
			version=parser.get('world', 'version', fallback=None),
			wos_version=parser.get('world', 'wos', fallback=None),
			home_url=parser.get('world', 'home_url', fallback=None),
			zip_url=parser.get('world', 'zip_url', fallback=None),
			ini_url=parser.get('world', 'ini_url', fallback=None),
			should_resolve_urls=should_resolve_urls,
		)
