#!/usr/bin/env bash

function ensure_uncompressed_folder_exists() {

if [[ ! -d  uncompressed-worlds ]]; then
    echo "No 'uncompressed-worlds' folder found."
    echo "First uncompress some worlds."
    echo "Also, you must run this in the root of the Git repository!"
    exit 1
fi
}

function ensure_compressed_folder_exists() {

if [[ ! -d  compressed-worlds ]]; then
    echo "No 'compressed-worlds' folder found."
    echo "First compress some worlds."
    echo "Also, you must run this in the root of the Git repository!"
    exit 1
fi

}

function ensure_at_root_of_git_repo() {

git_path=`git rev-parse --show-toplevel`

if [[ "$git_path" != "`pwd`" ]]; then
    echo "We are not at the root of the git repository, but we should be!"
    exit 1
fi

}