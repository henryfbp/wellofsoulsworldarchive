Hiya from the Cuzzy!! Hope you enjoy these skins!! ^_^

I made these rips from SFFs used for Mugen Character Maker and animated GIFs I find here and 
there. Many of the SFFs I got via StreetMugen [http://www.streetmugen.com], go there for links 
to hundreds of MUGEN character sites; Random Select [http://randomselect.i-xcell.com/], (same 
thing but in English); and MugenBR [http://www.mgbr.net], an active database for any of your 
MUGEN needs!!

Thanks to: NeoSonic (PPeak@aol.com) for the Amy Rose sprites, Betatronic (www.readyfyght.com) 
for the Big The Cat sprites, Reptile (second_dimensia@hotmail.com) for the Captain Falcon 
sprites, Daniel Sidney for the Cream sprites, Kevin Huff (stupidsonic@yahoo.com) for the 
Grizzo sprites, Scott (GordonBlazin@aol.com) for the Cackletta sprites, Justin Bailey 
(semijuggalo@yahoo.com) for the Samus Aran sprites, and Tommy Lee (SuperiorLordTommy@hotmail.com) 
for the Gravity Suit Samus sprites...

Special thanks goes out to Puar, Egoraptor and ShadowSonic of Orochinagi forums 
[http://www.orochinagi.com] for the awesome "King of Fighters 2003" aniGIFs I took some 
Benimaru rips from!!

Extra special thanks to Puar for the "SvC:Chaos" rips of Ken and Red Arrimer's "Midnight 
Bliss" female forms!! Got a sprite-ripping request? Go to Puar's cool beans site and email 'im!! 
[http://www.confusticated.com]

Also, MAJOR thanks go out to VX, Shouh and Kunio of Inner Circle for the sprite edits I used of 
Carnage and Geki!! Also, MAJOR thanks to Zweifuss [http://www.newwavemugen.com/~zweifuss/] for 
the cool beans Street Fighter III rips!! ^_^

Have any requests for a special skin? Email it to me at autumndacuzzy@yahoo.com, and I'll be 
glad to make one for you!!