this is the ROOT\SKINS folder
------------------------------

This folder is common to ALL worlds.  It contains the 'hero skin' files.

This is a common folder (rather than a wolrd folder) because I want people to collect skins and re-use them in new worlds.  You can think of skins as being associated with real human players, where as heroes are their characters within a single world.

Anyway, I just thought it would be nice.

---------

WHAT IS A SKIN?

Well, a skin file is used by a hero and defines their appearance on screen in scenes and on the map.  It is a BMP file (a RLE-encoded BMP file using only the colors in the 'souls.pal' palette file.  You can use a tool such as PaintShop Pro to coerce a 24 bit BMP file into a 256 color bitmap using this palette)  You can find the "souls.pal" file in the ROOT\ART folder.

The internal layout of a skin file is a horizontal strip of square images.  However many pixels tall the BMP file is determines the size of the square.

Check out the file heroSkinForm.jpg for a general layout guideline.  There are five basic squares required in a skin file.  They are (left to right)

    [MAP]  [READY]  [ATTACK]  [WEAK]   [CHAT]

The MAP square is divided into nine sub-squares and is used to supply the artwork of your hero while walking on the map in the 8 possible directions.  The center image is used when the character is 'camping' (or fighting)  Do not include a shadow in these map images as a simple oval shadow will be provided automatically.

The remaining four images are large views of the hero, and are used while in a scene.  The lower 1/6th of each square is your character's shadow (characters are generally assumed to be floating).

   READY

   This is used to indicate your character is ready for battle.  Your character should be facing away from the camera and toward the upper left, as though facing an enemy.

   ATTACK

   This is used during your character's attack.  Again, you character should be attacking a creature above and to the left (farther from the viewer than the character itself).  Images will be flipped left/right as necessary, so obey the standard rules and it will almost look great.  For magic users, the character will be displayed in the ATTACK pose for the entire spell duration.

   WEAK

   This is your character doubled over in pain.  It is used (flashing red) while you are being hit.  It is also used (black) when the character is dead.  It might be used when the character is close to death, poisoned, or otherwise in a state of emergency

   CHAT

   Your character should turn to face the camera for this one.  It will provide the clearest view of your character's face.  it is used when your character is chatting.