;
; change sandals back
SCENE 230 Cave1, SCENE, "Switch One"

	IF T238, @switch
	ACTOR	1,	"Thief", Pic055a, 1, 25, 90
	HOST_GIVE T239
	1: Greetings.
	1: I bet you're here to rescue the old man, aren't you?
	1: Well, I've got a proposition you might want to consider.
	1: I've been assigned at the very front of our base, you see.
	1: I'm supposed to deal with anything who tries to enter.
	1: It's not very fun, and I wouldn't have to stand here if the old man was rescued.
	1: So, just give me ten thousand gold and I'll explain how you can get to the dungeon.
	1: How about it? Are you smart enough to see a good deal when it's presented to you?
	IF R50, @skip
	SET switchtest "+"
	GOTO @ask1
@skip
	SET switchtest "-"
@ask1
	ASK 9999
	IF YES, @yes
	1: That figures. Well then, wander around here forever, for all I care!
	MOVE 1, -20, -20
	GOTO @switch
	END
@yes
	IF -G10000, @poor
	TAKE G10000
	1: That's a good fellow.
	1: Now, there's a door leading to the dungeon in the northeast corner of this floor.
	1: It's controlled by eight switches scattered around this floor.
	1: Some of the switches need to be up, and some need to be down.
	1: The door'll open if all eight switches are set correctly.
	1: In case your math is bad, I'll tell you that there's two hundred and fifty-six different switch combinations.
	1: I can tell you what the first and second switches need to be set at. For a price.
	1: That'll drop it to only sixty-four combinations.
	1: So, hand over one hundred-thousand gold.
	ASK 999999
	IF YES, @yes2
	1: Good luck with the switches, then... You'll need it! Hahahahaha!
	MOVE 1, -20, -20
	GOTO @switch
	END
@yes2
	IF -G100000, @poor
	TAKE G100000
	1: You are a smart one, aren't you?
	1: The first switch should be up.
	1: The second switch should also be up.
	1: I hope you're taking notes, because I won't tell you again!
	MOVE 1, -20, -20

@switch
	IF T230, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T230
	N: The switch is now up.
	END
@down
	TAKE T230
	N: The switch is now down.
	END
@end
END
@poor

	1: Come back when you get some money.
	END



SCENE 231 Cave1, SCENE, "Switch Two"


IF T238, @switch
	FIGHT 121
	IF WIN, @switch
	END
@switch
	IF T231, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T231
	N: The switch is now up.
	END
@down
	TAKE T231
	N: The switch is now down.
	END
SCENE 232 Cave1, SCENE, "Switch Three"

	IF T238, @switch
	ACTOR	1,	"Guard", Pic047b, 1, 25, 90
	1: You're trying to rescue the old man, aren't you?
	H: Yes.
	1: Hah! Try all you want, but you'll never figure out this switch is supposed to be down!
	WAIT 3
	MOVE 1, -20, -20
@switch	
	IF T232, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T232
	N: The switch is now up.
	END
@down
	TAKE T232
	N: The switch is now down.
	END
SCENE 233 Cave1, SCENE, "Switch Four"

	IF T238, @switch
	FIGHT 180
	IF WIN, @won
	END
@won
	ACTOR	1,	"Guard", Pic048b, 1, -20, -20
	MOVE 1, 25, 90
	WAIT 3
	1: Well, that was the best fight I've had in a while!
	1: This switch should be up, so you know.
@switch	
	WAIT 3
	IF T233, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T233
	N: The switch is now up.
	END
@down
	TAKE T233
	N: The switch is now down.
	END
SCENE 234 Cave1, SCENE, "Switch Five"

	IF T234, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T234
	N: The switch is now up.
	END
@down
	TAKE T234
	N: The switch is now down.
	END
SCENE 235 Cave1, SCENE, "Switch Six"

	IF T235, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T235
	N: The switch is now up.
	END
@down
	TAKE T235
	N: The switch is now down.
	END
SCENE 236 Cave1, SCENE, "Switch Seven"

	IF T236, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T236
	N: The switch is now up.
	END
@down
	TAKE T236
	N: The switch is now down.
	END
SCENE 237 Cave1, SCENE, "Switch Eight"

	IF T238, @switch
	FIGHT 184
	IF LOSE, @switch
	N: "Switch eight, up" is written on the back of his hand.
@switch
	WAIT 3
	IF T237, @up1
	N: The switch is currently down.
	GOTO @ask
	END
@up1
	N: The switch is currently up.
@ask	
	WAIT 1
	N: Would you like to change the switch?
	MENU "Up=@up", "Down=@down"
	END
	
@up
	GIVE T237
	N: The switch is now up.
	END
@down
	TAKE T237
	N: The switch is now down.
	END

SCENE 238 Vault, SCENE, "Dungeons"

	IF -T239, @shut
	IF T230+T231-T232+T233+T234-T235#<switchtest>T236+T237, @open

	IF T238, @shut
	FIGHT 206
	IF LOSE, @end
@shut
	N: The door remains shut.
	END
	
@open
	IF T238, @open1
	FIGHT 206
	IF LOSE, @end
@open1
	N: The door opens.
	WAIT 3
	GOTO LINK  17,  0
@end
	END
SCENE 239 Cave1, SCENE, "Jail Cell"


	ACTOR	1,	"Old man", joshTownsfolk, 21, 25, 90
	
	IF T370+C11, @spell
	IF T370,@back
	GOTO @dir
@back
	1: Welcome back!
	1: How is your quest going?
	END
@dir

	1: Greetings Adventurer, Listen well for I have much to tell you.
	1: Have you ever noticed how everyone you meet here in Aerianell seems to be an adventurer?
	1: And how there are no farmers to feed all these adventurers?
	1: Or schools to train them?
	1: The land of Aerianell wasn't always like it is now.
	1: Sure there were warriors, but most of the land was peaceful.
	1: This all changed with the coming of the Terrible Dark God.
	1: His Demigod army poured over the land destroying everything in their path.
	1: Fourty Eight of the elders of Aerianell gathered to try to stop the Dark God.
	1: A secret ritual was created to combine our power.
	1: Eight circles of six elders were formed during the ritual.
	1: An artifact of great power was created that allowed us to drive away the TDG.
	1: How ever the fundamental properties of Aerianell were changed at the time.
	1: Before there were no elements in Aerianell, magic was just magic.
	1: Each of those eight circles took control of a portion of the magic and shaped it after the elders in that circle.
	1: Six of the elders best at casting illusions chose to form a circle and the element of Illusion was born.
	1: But these changes did not just affect magic, Every creature in Aerianell was changed to line up with an element.
	1: After the TDG was defeated the artifact was divided into Seven pieces and each piece was kept by one of the masters of the elements.
	1: I can sense great things in you, I shall give you my piece of the artifact and you must find the other six pieces.
	1: I was captured by the thieves of this castle and I've stayed here in order to avoid the minions of the TDG.
	1: My Brethren might not have been so lucky.
	1: You must seek them out but be careful, They might have been captured by the TDG.
	1: Even worse for you they might be using their powers to protect themselves from any outsiders.
	1: I have lost track of the healing elder, but I know that the Chaos Elder is hiding in the Ruins of Luthmoor.
	1: Go now, the fate of the world rests on you.
	1: Any Elementalists should come back alone to visit me, I have a few tricks I can teach them.
	GIVE T370
	GIVE T238
	END
@spell
	COMPARE #<num.peopleInScene>, "1"
	IF> @end
	1: What's this? I see you're a fellow Elementalist.
	1: I have something here that you might find useful.
	HOST_GIVE I234
	1: Take this Prismatic Robe. I won't need it anymore.
	1: I also have this spell to teach you.
	HOST_GIVE S218
	1: Farewell.
@end
	END
;end comment

