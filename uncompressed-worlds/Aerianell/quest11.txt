SCENE 110 hangar, SCENE, "Science Institute"
	ACTOR	1,	"Bobo", joshAnimals, 15, 25, 90

	1: Ah, another rat for my maze!
	1: I wonder if this one can make it through?
	WAIT 2
	COUNTDOWN 50
	GOTO LINK 27, 0
	END

SCENE 111 hangar, SCENE, "Science Institute"
	ACTOR	1,	"Bobo", joshAnimals, 15, 25, 90

	IF XP, @lost
	COUNTDOWN 0
	1: Hmm... Very interesting.
	1: This one seems unusually intelligent. Maybe it will make it through the next one?
	WAIT 2
	COUNTDOWN 80
	GOTO LINK 28, 0
	END
@lost
	1: Note to self--humans are slow.
	HOST_TAKE T120
	HOST_TAKE T119
	WAIT 2
	GOTO LINK 0, 0
END

SCENE 112 hangar, SCENE, "Science Institute"
	ACTOR	1,	"Bobo", joshAnimals, 15, 25, 90

	IF XP, @lost
	COUNTDOWN 0
	1: It hasn't even broken a sweat!
	1: There's no way it will make it through the last one, though.
	WAIT 2
	COUNTDOWN 100
	GOTO LINK 29, 0
	END
@lost
	1: Note to self--humans are slow.
	HOST_TAKE T120
	HOST_TAKE T119
	WAIT 2
	GOTO LINK 0, 0
END
SCENE 113 hangar, SCENE, "Science Institute"
	ACTOR	1,	"Bobo", joshAnimals, 15, 25, 90

	IF XP, @lost
	1: Amazing!! Fantastic!! Incredible!!
	1: Now I have two humans to race through mazes!
	COUNTDOWN 0
	WAIT 2
	GOTO SCENE 114
	END
@lost
	1: Note to self--humans are slow.
	HOST_TAKE T120
	HOST_TAKE T119
	WAIT 2
	GOTO LINK 0, 0
END
SCENE 114 cave1, SCENE, "Dungeon"
	ACTOR	1,	"Scientist", joshTownsfolk, 8, 25, 90
	ACTOR	2,	"Mary Ann", joshTownsfolk, 32, 45, 45

	1: It looks as though someone else made it through the mazes.
	2: Hello. I'm Mary Ann, and this is the Professor.
	H: Hello...
	1: I imagine you're wondering what's going on around this place.
	H: I guess you could say the talking monkey freaked me out just a little, yes.
	1: Well now, I'll start at the beginning.
	1: I was the head scientist of this institute, and Mary Ann here was one of my assistants.
	1: We were studying animal intelligence, and working on ways to improve their thought processes.
	1: Bobo, the monkey you saw earlier, was one of the test animals.
	1: Unfortunately, one of our experiments worked too well.
	1: Bobo was endowed with a genius' IQ instantaneously.
	2: He's viewed humans as an inferior species ever since then.
	1: Yes. He was able to conceal his intelligence until we released him for a later experiment.
	1: He then wasted no time in taking over the entire institute.
	2: It was horrible...
	1: Yes it was. So many lost lives...
	2: So much blood...
	1: Bobo imprisoned me down here and began running my assistants through his mazes.
	1: Mary Ann was the only one to survive.
	H: That's amazing!
	H: But how did a monkey defeat all of you?
	1: Well, he constructed an amazing weapon.
	1: I'm really quite astounded that he managed to accomplish it.
	2: It looked like he was just tossing money at us.
	1: But in reality, it was an attack.
	1: Most of us were down before anyone even realized what was happening.
	2: So much blood...
	H: I see. Well, are there any ways out of this dungeon?
	1: No, but I've come up with a plan. The secret to victory lies within Bobo's hat.
	H: His hat??
	1: Yes. The hat is what gives him his intelligence.
	1: If he can be distracted long enough for someone to grab the hat, he'll be turned back into a normal monkey.
	H: How are we even supposed to get to him from here?
	2: Bobo's mentioned that he's wanted two humans to race through his mazes at the same time.
	H: Hey, yeah, I remember him saying something like that.
	2: Well, when he takes us to the entrance of the mazes, you distract him and I'll grab his hat.
	H: How should I distract him if he has this amazing weapon you mentioned?
	1: Simple. You'll use this banana that I've been hiding.
	1: He's already eaten every other banana in the institute, so he'll be very excited when he sees this one.
	H: It's worth a try.
	2: Here he comes!
	GOTO SCENE 115
	END	

SCENE 115 hangar, SCENE, "Science Institute"
	ACTOR	1,	"Bobo", joshAnimals, 15, 25, 90
	ACTOR	2,	"Mary Ann", joshTownsfolk, 32, 45, 45
	
	1: Ah yes, this race will be very interesting.
	1: Wait a minute! What do you have there?...
	1: Egads!! It's a banana! Give it to me!!
	MOVE  1  60, 50
	MOVE 2, 50, 60
	WAIT 4
	SELECT 1
	POSE 22
	SELECT 2
	POSE 31
	2: Got it!
	MOVE 1 25, 90
	MOVE 2, 45, 45
	1: <monkey noises>
	H: It worked!
	2: Let's go free the Professor!
	WAIT 2
	ACTOR	3,	"Scientist", joshTownsfolk, 8, 45, 90
	3: Well, that was fast.
	3: Hm. It seems as though Bobo released several other monkies with hats such as those.
	3: I'll hire some men to track them down.
	3: Hopefully no one notices the monkies, or I could lose my job...
	2: Thank you so much, %1!
	1: **eats the banana happily**
	2: We couldn't have done it without you!
	3: The institution will have to be closed down for a while, but I'm sure we'll meet again someday.
	HOST_TAKE T120
	HOST_TAKE T119
	HOST_GIVE T294
	2: Goodbye!
	1: <monkey noises>
	WAIT 2
	GOTO LINK 0,79
	END
; Comment