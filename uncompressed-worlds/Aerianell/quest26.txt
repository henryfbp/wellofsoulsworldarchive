
; T60 and T264 taken
;innleach quest
SCENE 260,  cave1,  SCENE, 

	IF T265+T266+T267+T268+T269, @fight
	END
@fight
	FIGHT 330, 330, 330
	IF LOSE, @end
	HOST_GIVE T420
	GOTO LINK 31, 0
	END
@end
END


SCENE 261, bgshore1, SCENE

	FIGHT 331
	IF WON, @dive
	END
@dive
	IF I132, @underwater
	END
@underwater
	BKGND	"bgWaterCave3.jpg"
	FX 1
	N: You dive underwater, and the Lake Monster attacks again!
	FIGHT2 333
	IF WON, @shield
	END
@shield
	N: You notice a shield sitting on the floor of the lake, and take it.
	GIVE I832
	END


SCENE 262, aztec2 , SCENE

	N: A small box sits on a pedestal before you.
	N: Do you wish to take it?
	ASK 99999999
	IF YES, @take
	End
@take
	N: You grab the box and nothing happens.
	HOST_GIVE I110
	N: A small grey object was resting under the box.
	N: Do you wish to take it?
	ASK 99999999
	IF YES, @take2
	END
@take2
	N: You grab the object, which is a Whetstone, and a small switch is set off.
	FIGHT2 338
	IF LOST, @deadtake
	HOST_GIVE I131
	END
@deadtake
	N: The Whetstone falls from your grasp as your dead body slumps to the ground.
	END

SCENE 265, bazaar, SCENE
	
	ACTOR	1,	"Blacksmith",	joshAnimals,	7,	25,	90

	1: Welcome to my smithy!
	IF I170, @quest
	IF I165, @quest2
	END
@quest
	1: I notice you've got a Dull Axe there.
	1: If you bring me a Whetstone, I could sharpen that axe for you.
	IF I131, @newaxe
	END
@newaxe
	H: I've got a Whetstone.
	1: Great! Give me the stuff. I'll get right to work.
	HOST_TAKE I131
	HOST_TAKE I170
	WAIT 5
	1: I'm done! Here you go--an Axe of Sharpness!
	HOST_GIVE I173
	END
@quest2
	1: I notice you've got a Dull Sword there.
	1: If you bring me a Whetstone, I could sharpen that sword for you.
	IF I131, @newsword
	END
@newsword
	H: I've got a Whetstone.
	1: Great! Give me the stuff. I'll get right to work.
	HOST_TAKE I131
	HOST_TAKE I165
	WAIT 5
	1: I'm done! Here you go--a Sword of Sharpness!
	HOST_GIVE I174
	END
SCENE 269, cave1 , SCENE


	N: The switch is currently set at #<Monsterbox>.
	N: Where do you want to set the switch?
	MENU "Large=@high", "Medium=@mid", "Small=@low"
	END
@high
	SET Monsterbox, "Large"
	SET Monsterbox1, "1"
	END
@mid
	SET Monsterbox, "Medium"
	SET Monsterbox1, "2"
	END
@low
	SET Monsterbox, "Small"
	SET Monsterbox1, "3"
	END


@large


;end comment