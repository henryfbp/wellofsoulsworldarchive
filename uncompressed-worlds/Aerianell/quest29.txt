


SCENE 291 ForestBKG, SCENE, 

        ACTOR	1,	"Begger",	joshtownsfolk, 21,	25,	90

	1: Spare a gold piece?
	END
@eventActorGive1
	COMPARE #<item.goldgiven>, "0"
	IF>, @thanks
	END
@thanks
	1: Thank you for the #<item.goldgiven> gold, kind traveller!
	IF T299, @end
	HOST_GIVE I56
	GIVE T299
	SET givemonth, #<num.timemonth>
	SET giveday, #<num.timeday>
	@end	
END



SCENE 292

        ACTOR	1,	"Palinus",	joshRoyalty, 4,	25,	90

	IF T298, @back
	1: Hey there! I'm looking for a Ring of Strength.
	1: I'll give you 3 Pieces of Steel for one.
	IF I1010, @gotone
	END
@gotone
	1: How about it? Want to trade?
	ASK 9999999
	IF YES, @take
	END
@take
	TAKE I1010
	GIVE I137
	GIVE I137
	GIVE I137
	GIVE T298
	1: All right, thanks! Here you go.
	END
@back
	1: Hey there!
	END
	

SCENE 290

        ACTOR	1,	"Piran",	joshRoyalty, 4,	25,	90

	IF T297, @back
	1: Hm. I need a Nature Crystal.
	1: I'll part with 3 Pieces of Steel in exhange.
	IF I18, @gotone
	END
@gotone
	1: Well?
	ASK 9999999
	IF YES, @take
	END
@take
	TAKE I18
	GIVE I137
	GIVE I137
	GIVE I137
	GIVE T297
	1: Okay! Here you are.
	END
@back
	1: Hm...
	END
	
SCENE 295,  rome1, SCENE, "Mountaintop"

	IF T295, @end
	FIGHT 343, 344, 344, 344, 344
	IF WON, @reward
	END
@reward
	N: You found a Ring of Flight laying in the Roc's nest!
	GIVE I725
	GIVE T295
	END
@end
	END
SCENE 293

OFFER 138

END

SCENE 294

N: You found a Mallet.
GIVE I139

END

SCENE 299

N: You found a Chisel.
GIVE I140

END

SCENE 296

N: You found a Sledgehammer.
GIVE I141

END


;end

