; T325 and T326 used

SCENE 321
	MUSIC miracle.mid
	ACTOR	1,	"Nature Elder", joshTownsfolk, 21, -20, -20
	IF T375+C10, @spell	
	IF T375, @end
	N: The lieutenant howls with rage and charges towards you!
	IF I125, @fight2
	FIGHT2 373, 372, 371
	IF WON, @reward
	END
@reward
	N: You remove the Lieutenant's Bracelet from his wrist.
	GIVE I850
	GIVE T322
	MOVE 1, 15,85,1
	MOVE 1, 15, 85
	1: Thank you for rescueing me
	1: The cleric in the Tarcius Volcano holds the final piece.
	1: Take my piece and go to him.
	1: He will tell you what you need to do.
	GIVE T375
	1: Any Druids should return to me for extra training.
	END	

@spell
	MOVE 1, 15, 85
	1: This spell will help you.
	HOST_GIVE S219
	END

@fight2
	N: Your Faerie Stick drives away some of the monsters.
	FIGHT2 373
	IF Won, @reward
@end
	END

SCENE 322
	GIVE I113
	GIVE I113
	HTML "Nithean.htm"
	IF T322, @end
	GIVE T321
@end
	END
SCENE 323
	GIVE I116.2
	HTML "Innleach.htm"
	END

SCENE 324
	STRCMP  #<str.soul>, "Soul 2530"
	IF= @loric
	END
@loric
	GIVE T4007	
END

SCENE 325
	STRCMP  #<str.soul>, "Soul 2530"
	IF= @loric
	END
@loric
	GIVE T4008
	END


;comment