; Used Items:
; Scenes 1999-200/Tokens 3-11
;
SCENE 1999, gate1, SCENE, "Gate of Malros"
IF T101, @normal
IF T100, @goblinfight
@normal
N: The gate seems to be locked.
END
@goblinfight
N: Hey!
WAIT 1
N: Someone's here!
WAIT 1
N: Let's get them!
WAIT 1
FIGHT2 900,901,902,903,904
IF WON, @giveitem
N: Now.. back to business..
END
@giveitem
H: This must be what they were talking about..
GIVE T101
GIVE I121
H: Looks like a ring..
END

;----------------------------------------------------------------------------------

SCENE 2000, hall, SCENE, "Epitath of Cerus"
COMPARE #<sleepydragon>, 1
IF=, @end
ACTOR 1, "Sleepy Dragon", fatdraggy, 1, 25, 80, 30
	1: Zzz..
	1: Zzz..Zzz...Zzz
N: The dragon is sleeping...
N: Maybe if you could get behind him somehow..
END
@eventActorClick1
FIGHT 26
IF DEAD, @end
FIGHT 27
IF DEAD, @end
FIGHT 28
IF DEAD, @end
FIGHT 29
IF DEAD, @end
FIGHT 30
IF DEAD, @end
FIGHT 31
IF DEAD, @end
FIGHT 32
IF DEAD, @end
1: Arghh!!
1: Who dares wake my slumber?!
WAIT 2
MOVE 1,0,0,1
FIGHT2 61
IF WON, @number
MOVE 1,25,80,1
1: Puny mortals..
1: Zzz..
END
@number
N: After defeating the dragon, you find this %I134!
WAIT 2
GIVE I134
GIVE T3
SET sleepydragon, 1
@end
END

;----------------------------------------------------------------------------------

SCENE 2001, , SCENE, "Fire Temple"
FIGHT 26,26,26
IF DEAD, @end
FIGHT 26,26,44
IF DEAD, @end
FIGHT 44,44,26
IF DEAD, @end
FIGHT 44,44,44
IF DEAD, @end
FIGHT 44,44,2505
IF DEAD, @end
FIGHT 2505,2505,44
IF DEAD, @end
FIGHT 2505,2505,2505
IF DEAD, @end
FIGHT 2505,2505,2505,4073
IF DEAD, @end
FIGHT 4073,4074
N: You have overcome the Fire Temple.
N: Take this %I136.
SET firetemple, 1
GIVE I136
GIVE T4
@end
END

;----------------------------------------------------------------------------------

SCENE 2002, lightsphere, SCENE, "Akeldama - The Field of Blood"
COMPARE #<fieldofblood>, "2"
IF=, @end
FIGHT 2501
IF DEAD, @end
FIGHT 2502
IF DEAD, @end
FIGHT 2503
IF DEAD, @end
FIGHT 2504
IF DEAD, @end
FIGHT 2505
IF DEAD, @end
FIGHT 2506
IF DEAD, @end
FIGHT 2507
IF DEAD, @end
FIGHT 2501,2502
IF DEAD, @end
FIGHT 2502,2503
IF DEAD, @end
FIGHT 2504,2505
IF DEAD, @end
FIGHT 2506,2507
IF DEAD, @end
FIGHT 2501,2502,2503
IF DEAD, @end
FIGHT 2504,2505,2506
IF DEAD, @end
FIGHT 2500
IF DEAD, @end
FIGHT 2500,2500
IF DEAD, @end
FIGHT 2500,2500,2507
IF DEAD, @end
FIGHT 2500,2500,2500,2507
IF DEAD, @end
FIGHT 2508,2500,2500,2507
IF DEAD, @end
N: You have cleared the Field of Blood, Akeldama!
N: Take this %I133!
SET fieldofblood, 2
GIVE I133
GIVE T5
@end
END 

;----------------------------------------------------------------------------------

SCENE 2003, fireblank, SCENE, "House of the Damned"
COMPARE #<hod>, 2
If=, @end
FIGHT 2027,2027
IF DEAD, @end
FIGHT 2027,2028
IF DEAD, @end
FIGHT 2028,2029
IF DEAD, @end
FIGHT 2029,2030
IF DEAD, @end
FIGHT 2030,2031
IF DEAD, @end
FIGHT 2031,2032
IF DEAD, @end
FIGHT 2027,2031,2032
IF DEAD, @end
FIGHT 2028,2032,2033
IF DEAD, @end
FIGHT 2033,2032,2030
IF DEAD, @end
FIGHT 2034,2033
IF DEAD, @end
FIGHT 2034,2034
IF DEAD, @end
FIGHT 2035,2034
IF DEAD, @end
FIGHT 2035,2034,2033
IF DEAD, @end
FIGHT 2035,2034,2033,2035
IF DEAD, @end
FIGHT 2036,2035,2034
IF DEAD, @end
FIGHT 2037,2036,2035
IF DEAD, @end
FIGHT 2038,2037,2036
IF DEAD, @end
FIGHT 2039,2038,2037,2037,2037
IF DEAD, @end
N: You have slain the Master of the Abyss!
SET hod, 2
N: You find the %I135!
GIVE I135
GIVE T6
@end
END

;----------------------------------------------------------------------------------

SCENE 2004, cave1, SCENE, "Tomb of Belshazar"
COMPARE #<catacombbattle>, 2
IF=, @end
FIGHT 2015,2015,2016
IF DEAD, @end
FIGHT 2015,2016,2016
IF DEAD, @end
FIGHT 2016,2016,2017
IF DEAD, @end
FIGHT 2017,2017,2018
IF DEAD, @end
FIGHT 2018,2018,2019
IF DEAD, @end
FIGHT 2018,2019,2019
IF DEAD, @end
FIGHT 2019,2019,2020
IF DEAD, @end
FIGHT 2020,2019,2020
IF DEAD, @end
FIGHT 2020,2020,2021
IF DEAD, @end
FIGHT 2021,2021,2022
IF DEAD, @end
FIGHT 2022,2022,2023
IF DEAD, @end
FIGHT 2023,2022,2023
IF DEAD, @end
FIGHT 2023,2023,2024
IF DEAD, @end
FIGHT 2023,2024,2024
IF DEAD, @end
FIGHT 2024,2025,2024
IF DEAD, @end
FIGHT 2024,2025,2025
IF DEAD, @end
FIGHT 2026,2025,2024,2023,2021,2020
IF DEAD, @end
SET catacombbattle, 2
N: You have slain the Master of the Catacombs!
N: Claim your prize, the %I137!
GIVE T7
GIVE I137
@end
END

;----------------------------------------------------------------------------------

SCENE 2005, waterorb, SCENE, "The Iris Core"
COMPARE #<waterrune>, 2
IF=, @end
FIGHT 28,28,28,28,28
IF DEAD, @end
FIGHT 28,28,43,43
IF DEAD, @end
FIGHT 43,43,39,39
IF DEAD, @end
FIGHT 19,19,43,39
IF DEAD, @end
FIGHT 19,43,43,43,39
IF DEAD, @end
FIGHT 43,43,39,39,19,11,12
IF DEAD, @end
FIGHT 28,28,50,19,43,43
IF DEAD, @end
FIGHT 12,11,39,50
IF DEAD, @end
FIGHT 50,50
IF DEAD, @end
FIGHT 43,50,50
IF DEAD, @end
FIGHT 58,50,50
IF DEAD, @end
FIGHT 50,58,58
IF DEAD, @end
FIGHT 2501
IF DEAD, @end
FIGHT 2501,50
IF DEAD, @end
FIGHT 2501,50,58
IF DEAD, @end
FIGHT 2501,2501,58
IF DEAD, @end
FIGHT 2501,2501,2501
IF DEAD, @end
FIGHT 2501,2501,2501,2501
IF DEAD, @end
N: You find the %I132 among the rubble.
SET waterrune, 2
GIVE I132
GIVE T8
@END
END

;----------------------------------------------------------------------------------

SCENE 2006, throne, SCENE, "Temple of Tiamat"
COMPARE #<airrune>, 2
IF=, @end
FIGHT 32,32,32
IF DEAD, @end
FIGHT 32,32,20
IF DEAD, @end
FIGHT 32,20,32,46
IF DEAD, @end
FIGHT 32,32,46
IF DEAD, @end
FIGHT 46,46,32
IF DEAD, @end
FIGHT 48,46,46
IF DEAD, @end
FIGHT 48,48,46
IF DEAD, @end
FIGHT 48,48,2009
IF DEAD, @end
FIGHT 48,2009,2009
IF DEAD, @end
FIGHT 2009,2009,2009
IF DEAD, @end
FIGHT 2009,2009,2015
IF DEAD, @end
FIGHT 2009,2015,2015
IF DEAD, @end
FIGHT 2015,2015,2015
IF DEAD, @end
FIGHT 2015,2015,2506
IF DEAD, @end
FIGHT 2506,2506,2015
IF DEAD, @end
FIGHT 2015,2015,2506,2506
IF DEAD, @end
FIGHT 2506,2506,2506
IF DEAD, @end
FIGHT 2506,2506,2506,2506
IF DEAD, @end
FIGHT2, 2506,2506,2506,73
IF DEAD, @end
N: You find the %I138 among the rubble..
SET airrune, 2
GIVE I138
GIVE T9
GIVE T101
@END
END

;----------------------------------------------------------------------------------

SCENE 2007, throne, SCENE, "Holy Tomb of Melchizadek"
COMPARE #<holyrune>, 1
IF=, @end
FIGHT 27,27,27
IF DEAD, @end
FIGHT 27,27,27,45
IF DEAD, @end
FIGHT 45,45,27,27
IF DEAD, @end
FIGHT 45,45,45
IF DEAD, @end
FIGHT 45,45,54
IF DEAD, @end
FIGHT 54,54,45
IF DEAD, @end
FIGHT 54,54,54
IF DEAD, @end
FIGHT 2006,54,54
IF DEAD, @end
FIGHT 2006,2006,54
IF DEAD, @end
FIGHT 2006,2006,2006
IF DEAD, @end
FIGHT 2502,2006,2006
IF DEAD, @end
FIGHT 2502,2502,2006
IF DEAD, @end
FIGHT 2502,2502,2502
IF DEAD, @end
FIGHT 60,2502,2502
IF DEAD, @end
SET holyrune, 1
N: You have slain the Master of the Sanctuary!
N: Take this %I131 as your reward!
GIVE I131
GIVE T10
@end
END

;----------------------------------------------------------------------------------

SCENE 2008, temple, SCENE, "Realm of Chaos"
COMPARE #<chaosrune>, 1
IF=, @end
N: This is the greatest challenge you will face.
WAIT 1
N: Do you still wish to continue?
ASK 10
IF YES, @goon
N: Farewell.
WAIT 1
GOTO EXIT
END
@goon
N: Prepare yourself!
WAIT 1
FIGHT2 4050,4051,4052
IF DEAD, @end
FIGHT2 4053,4054,4055
IF DEAD, @end
FIGHT2 4063,4064,4065
IF DEAD, @end
FIGHT2 4058,4067,4070
IF DEAD, @end
FIGHT2 4073,4075,4079
IF DEAD, @end
FIGHT2 4076,4074,4084
IF DEAD, @end
FIGHT2 4057,4077,4080
IF DEAD, @end
FIGHT2 4059,4073,4079
IF DEAD, @end
FIGHT2 4069
IF DEAD, @end
FIGHT2 4071
IF DEAD, @end
FIGHT2 4072
IF DEAD, @end
FIGHT2 4090
IF DEAD, @end
N: You have overcome the ultimate evil.
N: Take this %I95 as your prize.
GIVE I95
GIVE T11
SET chaosrune, 1
@end
END

; THE GREEN BUNNY HAS PINK EARS   