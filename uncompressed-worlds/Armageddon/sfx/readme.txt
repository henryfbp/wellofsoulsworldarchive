World SFX Folder

When you specify a sound effect file name, this folder will be checked first.  If the file doesn't exist in this folder, then the 'root' Well of Souls SFX folder will be used instead.

This lets you override monster sounds or other elements for your world.