;--------------
; Camp scene
;--------------
SCENE 1		sandpark, WELL, "The Park", 0, 0
STRCMP #<str.soul>, "519EDB43"
IF=, @specialmenu
STRCMP #<str.soul>, "71C3FFB1"
IF=, @specialmenu
IF M0, @cheater
IF M2, @cheater
WAIT 2
GIVE L1
N: Revived!
WAIT 0.2
GIVE H32000
N: Healed!
WAIT 0.2
GIVE M32000
N: Recovered!
END



@specialmenu
WAIT 2
GIVE L1
N: Revived!
WAIT 0.2
GIVE H32000
N: Healed!
WAIT 0.2
GIVE M32000
N: Recovered!
ACTOR	1, "~Kai~", kai, 1, 30, 90
1: Where do you want to warp?
MENU "Main City=@a1","Cheaters=@a2","Starting Point=@a3","Stay Here=@donem"
@donem
END

@a1
GOTO LINK 2,0
@a2
GOTO LINK 1,0
@a3
GOTO LINK 0,0

@cheater
GOTO LINK 1,0

;--------------

