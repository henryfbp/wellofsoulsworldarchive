SCENE 40
	IF T319 @empty
	IF T318 @youfoundme
END
@youfoundme
	ACTOR	1,	"Forest Elf King", mirror, 1, 5, 90
	ACTOR	2,	"Mountain Elf King", mirror, 2, 15, 90
	1: Ready, %1?
	H: Yes, sir!
	2: We are all ready now!
	1: To battle!
	FIGHT 245, -247, -248, -249, -249, -249, -249, -249, -249
	IF WIN @won
END
@won
	GIVE T319
@empty
END

SCENE 41
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 1, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 42
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 2, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 43
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 3, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 44
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 4, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 45
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 5, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 46
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven Warrior"
	ACTOR	2,	"Elven Warrior"
	ACTOR	9,	"Sign",	signs, 6, 80, 20
	1: Hi, %1.
	2: We haven't been called on to fight the demon yet.
END

SCENE 47
	MUSIC #nochange
	THEME 0
	ACTOR	1,	"Elven General", mirror, 1, 5, 90
	ACTOR	2,	"Elven General", mirror, 1, 5, 90
	ACTOR	9,	"Sign",	signs, 7, 80, 20
	1: And then they move north.
	2: Okay.
	2: But, when do we attack the Demon?
	1: We can't until we have enough people.
	2: If the Forest elves added their army?
	1: Yes, but it would help if the king could find a hero.
END

SCENE 48
	MUSIC #nochange
	THEME 0
IF T320 @alldone
IF T319 @rewardtime
IF T318 @noonehere
IF T317 @goodillgotoo
IF T316 @gotellthem
IF T315 @toldaboutproblem
IF T314 @toldaboutproblem
	ACTOR	1, "Mountain King", mirror, 1, 5, 90
	1: Hi, %1.
END
@toldaboutproblem
	ACTOR	1, "Mountain King", mirror, 1, 5, 90
	1: Yes, we have invaded their forests.
	1: But there is a good reason.
	1: The Mountain Demon has terrorized our folk, and we have been forced into the forests.
	GIVE T316
GOTO @nextpart
END
@gotellthem
	ACTOR	1, "Mountain King", mirror, 1, 5, 90
@nextpart
	1: Please tell the Forest king that.
END
@goodillgotoo
	1: We shall go forth and slay the Mountain Demon!
	GIVE T318
END
@noonehere
	ACTOR	2,	"Page", mirror, 1, 5, 90
	2: Please go help them fight!
END
@rewardtime
	ACTOR	1, "Mountain King", mirror, 1, 5, 90
	1: Here is your reward.
	GIVE I21
	GIVE T320
END
@alldone
	ACTOR	1, "Mountain King", mirror, 1, 5, 90
	1: Thank you again, %1.
END

;anti-smite comment