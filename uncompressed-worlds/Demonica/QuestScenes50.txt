;commentage
;Scene with boot-seller.
SCENE 50 rollinghills, SHOP, "Specialty Boots"
	ACTOR 1, "Jim", joshTownsfolk, 29, 5, 90
	OFFER 700,703,704
	1: BOOOOOOOOOOOTS.
STRCMP #<str.soul>, "SOUL 2044"
IF= @givething
STRCMP #<str.soul>, "SOUL 1969"
IF= @givething
STRCMP #<str.soul>, "SOUL 2363"
IF=@givething

	END
@givething
IF I737 @next
IF T69 @skipchange
SET num.hostClass, 10
@skipchange
HOST_GIVE I737
@next
HOST_GIVE T501
HOST_GIVE T502
HOST_GIVE T503
HOST_GIVE T504
HOST_GIVE T505
HOST_GIVE T506
HOST_GIVE T507
HOST_GIVE T508
HOST_GIVE T509
HOST_GIVE T510
HOST_GIVE T3
WAIT 0.1

END

SCENE 55	;scene with old dude who stole orb, hes a summonyr, the first we run into.
	THEME 0
	MUSIC #nochange
	IF T43 @end
	ACTOR 1, "Old Man", mirror, 1, 5, 90
	IF T42 @knowaboutorbandt42
	IF T41 @knowaboutorb
	1: Go away!
	WAIT 5.0
	GOTO EXIT
	END
@knowaboutorb
	GIVE T42
@knowaboutorbandt42
	1: So, you want my Orb, eh?
	1: Fight me for it!
	MOVE 1, -50, 90
	#include Determine.txt
	COMPARE nopartymembers, "1"
	IF= @nohelpers
	COMPARE just1, "1"
	IF= @justonehelper
	COMPARE just2, "1"
	IF= @justtwohelper
	COMPARE just3, "1"
	IF= @justthreehelper
	COMPARE just4, "1"
	IF= @justfourhelper
	FIGHT2 251, -#<member1>, -#<member2>, -#<member3>, -#<member4>, -#<member5>
	IF WON @iwon
END
@justonehelper
	FIGHT2 251, -#<member1>
	IF WON @iwon
END
@justtwohelper
	FIGHT2 251, -#<member1>, -#<member2>
	IF WON @iwon
END
@justthreehelper
	FIGHT2 251, -#<member1>, -#<member2>, -#<member3>
	IF WON @iwon
END
@justfourhelper
	FIGHT2 251, -#<member1>, -#<member2>, -#<member3>, -#<member4>
	IF WON @iwon
END
@iwon
GIVE T43
@end
END

SCENE 56
	THEME 0
	MUSIC #nochange
	ACTOR 1, "Mayor", joshRoyalty, 2, 5, 90	
	IF T44 @youhavealreadybeenrewarded
	IF T43 @yougotit
	IF T42 @youknowwhereitis
	IF T41 @remindtogetorb
	IF T40 @needtogivegerlaen
	1: Hello, young hero.
	1: Would you be willing to do a small quest for me?
	ASK 20
	IF YES @continuecuzhesaidyes
	1: *Sigh*
	1: I guess I'll just have to keep looking for a true hero.
	END
@continuecuzhesaidyes
	1: Good, I'm glad you said yes.
	1: I need you to retrieve the Orb of Warding.
	1: Kaelmir needs the Orb to keep the monsters away.
	1: The Orb was lost during the time of peace before Demonica.
	GIVE T40
@needtogivegerlaen
	1: When an old man left town, he stole the Orb.
	ACTOR 2, "Gerlaen", mirror, 1, -50, 70
	MOVE 2, 15, 70
	1: Some say he went to the abandoned temple in the south.
	1: Gerlaen here will aid you.
	GIVE T502
	SET helper2, "2"
	GIVE T41
@remindtogetorb
	1: Please hurry and get the Orb!
	END
@youknowwhereitis
	1: So, you know the location of the Orb?
	1: Hurry and get it!
	END
@yougotit
	GIVE T44
	1: Hallelujah!
	1: The Orb is returned to us!
	1: Thank you, %1!
	1: We are forever greatful!
	1: Take this iron and this jewel.
	1: I'm sure you can find some use for them.
	GIVE G2000
	N: You recieved 2000 IC.
	GIVE I8
	N: You recieved a Blue Gem.
END
@youhavealreadybeenrewarded
END

SCENE 57 ;demonica fight scene
	THEME 0
	MUSIC #nochange

	#include Determine.txt
	COMPARE nopartymembers, "1"
	IF= @nohelpers
	COMPARE just1, "1"
	IF= @justonehelper
	COMPARE just2, "1"
	IF= @justtwohelper
	COMPARE just3, "1"
	IF= @justthreehelper
	COMPARE just4, "1"
	IF= @justfourhelper
	FIGHT2 255, -#<member1>, -#<member2>, -#<member3>, -#<member4>, -#<member5>
END
@justonehelper
	FIGHT2 255, -#<member1>
END
@justonehelper
	FIGHT2 255, -#<member1>, -#<member2>
END
@justonehelper
	FIGHT2 255, -#<member1>, -#<member2>, -#<member3>
END
@justonehelper
	FIGHT2 255, -#<member1>, -#<member2>, -#<member3>, -#<member4>
END

SCENE 58
	THEME 0
	MUSIC #nochange
	ACTOR 1, "Flamelord", Flame, 1, 5, 90
	1: I'm sorry, this area is currently unavailable.
	1: Please check back next patch.
END

SCENE 59 bgWaterWall		;lake scene
	WEATHER 1
	FX 2
	THEME 2
	MUSIC #nochange
END

;anti-smiteness comment