;*************************************************************************************James's House***************************************************************
SCENE 53 Dam
THEME 1
ACTOR 1, "James", joshTownsfolk, 15, 25, 90
IF -T46, @nolilka22
IF T46, @lilka25
@lilka25
ACTOR 2, "Lilka", Lilka, 1, 88, 32
@nolilka22
IF T14, @help2
1: Something awful has happened!
H: What is it?
1: Aldehyde is being attacked!
H: What?!
H: Why?
1: I don't know.
1: But it's not good!
H: I have to get there!
1: They have sealed off the Underground Tunnel though.
1: And with the Giant Squid around, there isn't really many options to get to Aldehyde from here.
H: There has to be a way.
1: Good luck!
GIVE T14
END
@help2
1: Good luck!
END
;---------
;************************************************************************************Underground Cave 2***********************************************************
SCENE 58 plaza
THEME 0
IF T27, @empty5
IF T14, @toomanymonsters
IF -T14, @empty5
@toomanymonsters
ACTOR 1, "Tunnel Guard", joshRoyalty, 4, 25, 90
POSE 4, 11
1: I'm sorry, this area is too dangerous.
1: I can't let you pass.
WAIT 1.0sec
GOTO EXIT
END
@empty5
ACTOR 1, ""
GOTO LINK 3, 2, 0
END
;*************************************************************************************Entrance to Aldehyde*********************************************************
SCENE 59 grass3, SCENE, "Aldehyde Gate"
IF T14, @attack
ACTOR 1, ""
GOTO LINK 1, 0, 0
END
@attack
IF T18, @empty6
ACTOR 1, "Guard", josh33, 1, 25, 90
MUSIC luf1destroyed.mid
1: Ha ha ha!
H: What are you doing here?!
1: I'm here to stop people like you from entering this town.
H: Let me in!
1: I don't think so.
1: Now if you don't get lost I'm going to kill you.
H: I have to get into that town.
1: Then you will die!
WAIT 2.0sec
MOVE 1, 0, 0, 1
MUSIC luf2boss.mid
FIGHT 2010
IF WON, @ack
MOVE 1, 25, 90, 1
1: Ha ha ha!
1: Get lost!
H: Ugh...
WAIT 2.0sec
GOTO EXIT
END
@ack
WAIT 2.0sec
H: I have to find the King!
GIVE T18
END
@empty6
IF T27, @backtonormal
ACTOR 1, ""
MUSIC ""
GOTO LINK 15, 0, 0
END
@backtonormal
GOTO LINK 1, 0, 0
END
;--------
;******************************************************************************************Hideout*****************************************************************
SCENE 60 cave5, SCENE, "Hideout"
ACTOR 1, "Magic Specialist", joshMiscellaneous, 1, 25, 90
ACTOR 2, "Knife Man", joshTownsfolk, 1, 30, 95
ACTOR 3, "Sword Guy", joshTownsfolk, 9, 20, 85
ACTOR 4, "Ms. Items",  joshTownsfolk, 5, 35, 90
ACTOR 5, "Armor Extrodanaire",  joshTownsfolk, 24, 25, 70
ACTOR 6, "Topper",  joshTownsfolk, 2, 10, 90
IF T22, @savedthedog
IF T21, @savedthejewler
IF T20, @savedthepl
IF T19, @goodluckitems
1: Look!
2: It's %1!
H: Are you guys alright?
3: We're fine.
4: But we don't know about the king.
5: He's still in the castle!
6: Along with the Queen and Princess!
1: We're doomed.
H: I have to save them.
2: Wait!
2: Some of the townspeople are not safe yet.
3: They couldn't escape!
H: I will help them.
4: Be careful.
4: Guards are everywhere.
5: We have some items that you can use.
OFFER 1, 17, 2, 8, 4, 7, 10, 11, 12, 6, 14, 18, 23, 22
3: You can also come here to rest if things get too tough.
2: Good luck!
GIVE T19
END
@goodluckitems
1: Let me heal you.
OFFER 1, 17, 2, 8, 4, 7, 10, 11, 12, 6, 14, 18, 23, 22
GIVE L1
GIVE H5000
GIVE M5000
END
@savedthedog
IF T20+T21, @savedthemall
IF -T20+T21, @saved1
IF T20-T21, @saved2
ACTOR 7, "Dog", joshAnimals, 23, 90, 36
GOTO @saved
END
@savedthejewler
IF T20+T22, @savedthemall
IF -T20+T22, @saved1
IF T20-T22, @saved4
ACTOR 7, "Jewler", joshTownsfolk, 29, 90, 46
GOTO @saved
END
@savedthepl
IF T21+T22, @savedthemall
IF -T21+T22, @saved2
IF T21-T22, @saved4
ACTOR 7, "Pretty Lady", joshRoyalty, 19, 90, 56
GOTO @saved
END
@saved1
ACTOR 7, "Jewler", joshTownsfolk, 29, 90, 46
ACTOR 8, "Dog", joshAnimals, 23, 90, 56
GOTO @saved3
END
@saved2
ACTOR 7, "Dog", joshAnimals, 23, 90, 46
ACTOR 8, "Pretty Lady", joshRoyalty, 19, 90, 36
GOTO @saved3
END
@saved4
ACTOR 7, "Pretty Lady", joshRoyalty, 19, 90, 36
ACTOR 8, "Jewler", joshTownsfolk, 29, 90, 46
GOTO @saved3
END
@saved3
3: You've saved two of them.
4: Isn't there one more?
5: You better go check.
GOTO @goodluckitems
END
@saved
6: You saved one!
2: I think there are two more still out there.
4: Please save them.
GOTO @goodluckitems
END
@savedthemall
IF T23, @goodluckitems
ACTOR 7, "Pretty Lady", joshRoyalty, 19, 90, 36
ACTOR 8, "Jewler", joshTownsfolk, 29, 90, 46
ACTOR 9, "Dog", joshAnimals, 23, 90, 56
3: You did it!
2: Thank you so much.
4: The King is the only one left.
5: You have to save him!
H: I'm on my way!
6: You can't enter the castle.
6: Guards are blocking it.
1: There is a secret way though.
2: It's *west of here*.
3: Be careful!
GIVE T23
GOTO @goodluckitems
END
;***********************************************************************************Taken Over Castle*************************************************************
SCENE 61 soho, SCENE, "Outside of Aldehyde Castle"
THEME 0
ACTOR 1, "Castle Guard", josh37, 1, 25, 90
WAIT 2.0sec
1: You are not allowed to enter the castle!
WAIT 2.0sec
GOTO EXIT
END
;--------
;****************************************************************************************Sewer Boss***************************************************************
SCENE 62 cave6, SCENE, "Boss Chambers"
THEME 5
IF T24, @deaddragon
ACTOR 1, "Sewer Boss", josh44, 1, 25, 90
WAIT 1.0sec
SOUND "roar1.wav"
H: What was that sound?
H: Yikes!
MOVE 1, 0, 0, 1
MUSIC luf2boss.mid
FIGHT 2011
IF WON, @deaddragon
MOVE 1, 0, 0, 1
END
@deaddragon
H: I have to hurry!
GIVE T24
WAIT 2.0sec
GOTO LINK 18, 4, 0
END
;*****************************************************************************************Inner Gate*****************************************************************
SCENE 63 soho
THEME 0
IF T27, @exitcastle
WAIT 2.0sec
H: I can't leave yet!
H: I have to help the King!
END
@exitcastle
GOTO LINK 1, 11, 0
END
;-------
;*************************************************************************************Boat House******************************************************************
SCENE 55
THEME 1
ACTOR 1, "Boat Fish", joshRoyalty, 27, 25, 90
IF -T46, @nolilka24
IF T46, @lilka27
@lilka27
ACTOR 2, "Lilka", Lilka, 1, 88, 32
@nolilka24
POSE 27, 28
1: I can give you a ride to Port Timney.
1: It only costs 200G.
1: Would you like to go?
ASK 20
IF YES+G200, @gototimney
1: Alright then, just come back whenever you want to go.
END
@gototimney
TAKE G200
1: Thank you.
WAIT 2.0sec
GOTO LINK 11, 12, 0
END
;-------
;End of Save Aldehyde! quest! :)