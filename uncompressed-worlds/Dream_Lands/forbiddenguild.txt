;*****************************************************************************Forbidden Guild***********************************************************************
SCENE 500 blackhole
THEME 0
ACTOR 1, "Spell Binder", joshMiscellaneous, 19, 25, 90
IF C4, @fs
IF C6, @fs
IF C2, @fqd
IF C5, @fgu
1: You can't learn spells!
1: Good-bye.
END
@fs
1: Welcome to the Forbidden Magic guild.
1: This guild creates the most powerful spell for each element.
1: Because these spells are so strong, you need to bring me five Crest Graphs to bind them.
1: I also need 10 elemental coins from that element to bind the spell.
@fsme
IF -I23-I23-I23-I23-I23, @ncg
1: Please choose an element.
MENU "Cure=@fcs", "Wing=@fws", "Holy=@fhs", "Muse=@fms", "Geo=@fgs", "Evil=@fes", "Frey=@ffs"
1: Just come back when you're ready to bind the forbidden spells.
@fce
IF S240, @alreadyhaveit
IF -I203-I203-I203-I203-I203-I203-I203-I203-I203-I203, @nfs
1: I will bind the spell Regenerate.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
TAKE I203
GIVE S240
GOTO @fsme
@fws
IF S241, @alreadyhaveit
IF -I198-I198-I198-I198-I198-I198-I198-I198-I198-I198, @nfs
1: I will bind the spell Keen Silf..
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
TAKE I198
GIVE S241
GOTO @fsme
@fhs
IF S242, @alreadyhaveit
IF -I202-I202-I202-I202-I202-I202-I202-I202-I202-I202, @nfs
1: I will bind the spell Hi-Rainbow.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
TAKE I202
GIVE S242
GOTO @fsme
@fms
IF S243, @alreadyhaveit
IF -I200-I200-I200-I200-I200-I200-I200-I200-I200-I200, @nfs
1: I will bind the spell Ice Needle.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
TAKE I200
GIVE S243
GOTO @fsme
@fgs
IF S244, @alreadyhaveit
IF -I197-I197-I197-I197-I197-I197-I197-I197-I197-I197, @nfs
1: I will bind the spell Gaia Crash.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
TAKE I197
GIVE S244
GOTO @fsme
@fes
IF S245, @alreadyhaveit
IF -I201-I201-I201-I201-I201-I201-I201-I201-I201-I201, @nfs
1: I will bind the spell Plasma.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
TAKE I201
GIVE S245
GOTO @fsme
@ffs
IF S246, @alreadyhaveit
IF -I199-I199-I199-I199-I199-I199-I199-I199-I199-I199, @nfs
1: I will bind the spell Inferno.
1: Hokus Pokus....Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I23
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
TAKE I199
GIVE S246
GOTO @fsme
@nfs
1: You don't have enough items to bind that spell.
END
@ncg
1: You need at least five Crest Graphs to bind a spell.
END
@hta
1: You have all the Forbidden Spells!
END
;***********************************************************************Forbidden Quick Draw**********************************************************************
@fqd
1: I can write the Forbidden Technique with five Secret Signs.
IF -I195-I195-I195-I195-I195-I195-I195-I195-I195-I195, @nss
IF S61, @afqd
1: I will write the technique Void.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
TAKE I195
TAKE I195
TAKE I195
TAKE I195
TAKE I195
GIVE S61
@afqd
1: That's all of the Forbidden Quick Draw techniques.
END
@nss
1: You need at least five Secret Signs for me to write a technique.
END
;*************************************************************************Forbidden Guardians**********************************************************************
@fgu
1: I can extract the most powerful Guardians.
1: All you need to do is bring me the Rune of the Guardian you want me to extract.
1: Here is my list of Guardians.
@fgsm
MENU "Lucadia=@fluca", "Rigdobrite=@frig", "Solus Emsu=@fsol", "Duras Drum=@fdu", "Justine=@fjust", "Ge Ramtos=@fge", "Odoryuk=@fodo"
1: Just come back when you want to extract a Guardian.
END
@fluca
IF S73, @alreadyhaveit
1: I need the Triton Rune to extract this Guardian.
IF -I73, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S73
GOTO @fgsm
@frig
IF S74, @alreadyhaveit
1: I need the Star Rune to extract this Guardian.
IF -I74, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S74
GOTO @fgsm
@fsol
IF S75, @alreadyhaveit
1: I need the Heavens Rune to extract this Guardian.
IF -I75, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S75
GOTO @fgsm
@fdu
IF S76, @alreadyhaveit
1: I need the Hades Rune to extract this Guardian.
IF -I76, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S76
GOTO @fgsm
@fjust
IF S77, @alreadyhaveit
1: I need the Courage Rune to extract this Guardian.
IF -I77, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S77
GOTO @fgsm
@fge
IF S78, @alreadyhaveit
1: I need the Death Rune to extract this Guardian.
IF -I78, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S78
GOTO @fgsm
@fodo
IF S80, @alreadyhaveit
1: I need the Life Rune to extract this Guardian.
IF -I81, @nfgs
1: I will extract this Guardian.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S80
GOTO @fgsm
@nfgs
1: You don't have the Rune I need.
END
@alreadyhaveit
1: You already have that one!
1: Try something else!
END
:End of Forbidden Guild! :)