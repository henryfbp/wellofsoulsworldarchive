;****Magic Guild****
;Status Magic Guild
SCENE 57 forest
THEME 0
ACTOR 1, "Spell Binder", joshMiscellaneous, 19, 25, 90
POSE 19, 20
IF -C4-C6, @nostat
1: Welcome to the magic guild.
IF T17, @allgone
IF I23, @spells
1: I can make special spells for you if you bring me *Crest Graphs*.
1: I also need the item of the spell you want me to bind.
END
@spells
1: I see you have a *Crest Graph*.
IF S82, @spell2
1: Do you want the Heat Salve Spell?
ASK 20
IF YES, @heatsalve
IF -YES, @spell2
@spell2
IF S83, @spell3
1: Do you want the Violet Rose Spell?
ASK 20
IF YES, @violetrose
IF -YES, @spell3
@spell3
IF S85, @spell4
1: Do you want the Toy Hammer Spell?
ASK 20
IF YES, @toyhammer
IF -YES, @spell4
@spell4
IF S86, @spell5
1: Do you want the Holy Cross Spell?
ASK 20
IF YES, @holycross
IF -YES, @spell5
@spell5
IF S87, @spell6
1: Do you want the Light Shroom Spell?
ASK 20
IF YES, @lightshroom
IF -YES, @spell6
@spell6
IF S88, @spell7
1: Do you want the Medicine Spell?
ASK 20
IF YES, @medicine
IF -YES, @spell7
@spell7
IF S89, @allgone
1: Do you want the Pinwheel Spell?
ASK 20
IF YES, @pinwheel
IF -YES, @bye
@bye
1: Come again soon!
END
@heatsalve
IF -I7, @sorry
TAKE I7
TAKE I23
1: Hocus Pokus....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S82
IF I23, @spells
IF -23, @bye
@violetrose
IF -I22, @sorry
TAKE I23
TAKE I22
1: Hocus Pokus....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S83
IF I23, @spells
IF -I23, @bye
@toyhammer
IF -I21, @sorry
TAKE I21
TAKE I23
1: Hocus Pokus....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S85
IF I23, @spells
IF -23, @bye
@holycross
IF -I10, @sorry
TAKE I10
TAKE I23
1: Hocus Pokus.....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S86
IF I23, @spells
IF -I23, @bye
@lightshroom
IF -I11, @sorry
TAKE I11
TAKE I23
1: Hocus Pokus.....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S87
IF I23, @spells
IF -I23, @bye
@medicine
IF -I12, @sorry
TAKE I12
TAKE I23
1: Hocus Pokus.....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S88
IF I23, @spells
IF -I23, @bye
@pinwheel
IF -I16, @sorry
TAKE I16
TAKE I16
1: Hocus Pokus.....Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S89
IF I23, @spells
IF -I23, @bye
@sorry
1: I'm sorry, you don't have the proper materials to make that spell.
END
@allgone
1: That's all my spells.
1: Thanks!
GIVE T17
END
@nostat
1: I'm sorry.
1: Your class can't learn the Status spells.
END
;--------
;Simple Magic Guild
SCENE 121 blackHole
ACTOR 1, "Spell Binder", joshMiscellaneous, 19, 25, 90
POSE 19, 20
IF C1, @nospells
IF C2, @fastdrawspells1
IF C3, @nospells
IF C4, @spells1
IF C5, @gspells1
IF C6, @spells1
@spells1
IF S1+S2+S3+S4+S10+S11+S12+S13+S14+S20+S21+S22+S26+S27+S28+S32+S33+S34+S38+S39+S40+S90+S92+S94+S96+S98+S100+S102+S103, @havethemall1
1: Welcome to the Magic Guild.
1: I can bind Simple Magic for you.
1: All you need to bring me is a Crest Graph and two of the elemental coins for the spell you want.
@sspellmenu
1: Take a look.
MENU "Geo=@sgeo", "Wing=@swing", "Frey=@sfrey", "Muse=@smuse", "Holy=@sholy", "Evil=@sevil", "Cure=@scure"
1: Ok then.
1: Just come back when you want to bind a spell.
END
;****Geo Spells*****
@sgeo
IF S32+S33+S34, @geoall1
1: Here is my list of Geo Spells.
1: Please select one you want me to bind.
MENU "Break=@break", "Toxic=@toxic", "MP Drain=@mpdrain"
GOTO @sspellmenu
@break
IF S32, @alreadyhaveit
1: I can bind the Break spell with two Geo coins.
IF S32, @alreadyhaveit
IF I23+I197+I197, @bindbreak2
IF -I23, @nocrestgraph
IF -I197, @nocoin
@bindbreak2
1: I will bind the spell Geo.
1: Hocus Pokus Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you are!
GIVE S32
TAKE I197
TAKE I197
TAKE I23
1: Try another spell!
GOTO @sspellmenu
@toxic
IF S33, @alreadyhaveit
1: I can bind the Toxic spell with a Geo coin and a Muse coin.
IF I23+I197+I200, @bindtoxic2
IF -I23, @nocrestgraph
IF -I197, @nocoin
IF -I200, @nocoin
@bindtoxic2
1: I will bind the spell Toxic.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you are!
GIVE S33
TAKE I23
TAKE I197
TAKE I200
1: Try another spell!
GOTO @sspellmenu
@mpdrain
IF S34, @alreadyhaveit
1: I can bind the spell MP Drain with a Geo coin and a Wing coin.
IF I23+I197+I198, @bindmpdrain2
IF -I23, @nocrestgraph
IF -I197, @nocoin
IF -I198, @nocoin
@bindmpdrain2
1: I will bind the spell MP Drain.
1: Hokus Pokus Alaroo!!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S34
TAKE I23
TAKE I197
TAKE I198
1: Try another spell!
GOTO @sspellmenu
;*****Wing Spells*****
@swing
IF S20+S21+S22, @wingall1
1: Here is my list of Wing spells.
1: Please select one you would like me to bind.
MENU "Vortex=@vortex", "Prison=@prison", "Breeze=@breeze"
GOTO @sspellmenu
@vortex
IF S20, @alreadyhaveit
1: I can bind the Vortex spell with two Wing coins.
IF I23+I198+I198, @bindvortex2
IF -I23, @nocrestgraph
IF -I198, @nocoin
@bindvortex2
1: I will bind the spell Vortex.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S20
TAKE I23
TAKE I198
TAKE I198
1: Try another spell!
GOTO @sspellmenu
@prison
IF S21, @alreadyhaveit
1: I can bind the Prison spell with a Wing coin and a Holy coin.
IF I23+I198+I202, @bindprison2
IF -I23, @nocrestgraph
IF -I198, @nocoin
IF -I202, @nocoin
@bindprison2
1: I will bind the spell Prison.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S21
TAKE I23
TAKE I198
TAKE I202
1: Try another spell!
GOTO @sspellmenu
@breeze
IF S22, @alreadyhaveit
1: I can bind the Breeze spell with a Wing coin and a Frey coin.
IF I23+I198+I199, @bindbreeze2
IF -I23, @nocrestgraph
IF -I198, @nocoin
IF -I199, @nocoin
@bindbreeze2
1: I will bind the spell Breeze.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S22
TAKE I23
TAKE I198
TAKE I199
1: Try another spell!
GOTO @sspellmenu
;*****Frey Spells*****
@sfrey
IF S38+S39+S40, @freyall1
1: Here is my list of Frey spells.
MENU "Flame=@flame", "Silence=@silence", "Slow Down=@slowdown"
GOTO @sspellmenu
@flame
IF S38, @alreadyhaveit
1: I can bind the spell Flame with two Frey coins.
IF I23+I199+I199, @bindflame2
IF -I23, @nocrestgraph
IF -I199, @nocoin
@bindflame2
1: I will bind the spell Flame.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S38
TAKE I23
TAKE I199
TAKE I199
1: Try another spell!
GOTO @sspellmenu
@silence
IF S40, @alreadyhaveit
1: I can bind the spell Silence with a Frey coin and a Evil Coin.
IF I23+I199+I201, @bindsilence2
IF -I23, @nocrestgraph
IF -I199, @nocoin
IF -I201, @nocoin
@bindsilence2
1: I will bind the spell Silence.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S40
TAKE I23
TAKE I199
TAKE I201
1: Try another spell!
GOTO @sspellmenu
@slowdown
IF S39, @alreadyhaveit
1: I can bind the spell Slow Down with a Frey coin and a Geo coin.
IF I23+I199+I197, @bindslowdown2
IF -I23, @nocrestgraph
IF -I199, @nocoin
IF -I197, @nocoin
@bindslowdown2
1: I will bind the spell Slow Down.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S39
TAKE I23
TAKE I199
TAKE I197
1: Try another spell!
GOTO @sspellmenu
;*****Muse Spells*****
@smuse
IF S26+S27+S28, @museall1
1: Here is my list of Muse spells.
MENU "Freeze=@freeze", "Spark=@spark", "Blast=@blast"
GOTO @sspellmenu
@freeze
IF S26, @alreadyhaveit
1: I can bind the spell Freeze with two Muse coins.
IF I23+I200+I200, @bindfreeze2
IF -I23, @nocrestgraph
IF -I200, @nocoin
@bindfreeze2
1: I will bind the spell Freeze.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S26
TAKE I23
TAKE I200
TAKE I200
1: Try another spell!
GOTO @sspellmenu
@spark
IF S27, @alreadyhaveit
1: I can bind the spell Spark with a Muse coin and a Wing coin.
IF I23+I200+I198, @bindspark2
IF -I23, @nocrestgraph
IF -I200, @nocoin
IF -I198, @nocoin
@bindspark2
1: I will bind the spell Spark.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S27
TAKE I23
TAKE I200
TAKE I198
1: Try another spell!
GOTO @sspellmenu
@blast
IF S28, @alreadyhaveit
1: I can bind the spell Blast with a Muse coin and a Geo coin.
IF I23+I200+I197, @bindblast2
IF -I23, @nocrestgraph
IF -I200, @nocoin
IF -I197, @nocoin
@bindblast2
1: I will bind the spell Blast.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S28
TAKE I23
TAKE I200
TAKE I197
1: Try another spell!
GOTO @sspellmenu
;*****Holy Spells*****
@sholy
IF S90+S92+S94+S96, @holyall1
1: This is my list of Holy spells.
MENU "Saint=@saint", "Valkyrie=@valk", "Arm. Down=@armdown", "Flash=@flash"
GOTO @sspellmenu
@saint
IF S90, @alreadyhaveit
1: I can bind the spell Saint with two Holy coins.
IF I23+I202+I202, @bindsaint2
IF -I23, @nocrestgraph
IF -I202, @nocoin
@bindsaint2
1: I will bind the spell Saint.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S90
TAKE I23
TAKE I202
TAKE I199
1: Try another spell!
GOTO @sspellmenu
@valk
IF S92, @alreadyhaveit
1: I can bind the spell Valkyrie with a Holy coin and a Evil coin.
IF I23+I202+I201, @bindvalk2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I201, @nocoin
@bindvalk2
1: I will bind the spell Valkyrie.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S92
TAKE I23
TAKE I202
TAKE I201
1: Try another spell!
GOTO @sspellmenu
@armdown
IF S94, @alreadyhaveit
1: I can bind the spell Arm. Down with an Evil coin and a Geo coin.
IF I23+I201+I197, @bindarm2
IF -I23, @nocrestgraph
IF -I201, @nocoin
IF -I197, @nocoin
@bindarm2
1: I will bind the spell Arm. Down.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S94
TAKE I23
TAKE I201
TAKE I197
1: Try another spell!
GOTO @sspellmenu
@flash
IF S96, @alreadyhaveit
1: I can bind the spell Flash with an Evil coin and a Muse coin.
IF I23+I201+I200, @bindflash2
IF -I23, @nocrestgraph
IF -I201, @nocoin
IF -I200, @nocoin
@bindflash2
1: I will bind the spell Flash.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S96
TAKE I23
TAKE I201
TAKE I200
1: Try another spell!
GOTO @sspellmenu
;*****Evil Spells*****
@sevil
IF S98+S100+S102+S103, @evilall1
1: This is my list of Evil spells.
MENU "Darkness=@darkness", "Confuse=@confuse", "Dispell=@dispell", "Eraser=@eraser"
GOTO @sspellmenu
@darkness
IF S98, @alreadyhaveit
1: I can bind the spell Darkness with two Evil coins.
IF I23+I201+I201, @binddarkness2
IF -I23, @nocrestgraph
IF -I201, @nocoin
@binddarkness2
1: I will bind the spell Darkness.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S98
TAKE I23
TAKE I201
TAKE I201
1: Try another spell!
GOTO @sspellmenu
@confuse
IF S100, @alreadyhaveit
1: I can bind the spell Confuse with an Evil coin and a Wing coin.
IF I23+I201+I198, @bindconfuse2
IF -I23, @nocrestgraph
IF -I201, @nocoin
IF -I198, @nocoin
@bindconfuse2
1: I will bind the spell Confuse.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S100
TAKE I23
TAKE I201
TAKE I198
1: Try another spell!
GOTO @sspellmenu
@dispell
IF S102, @alreadyhaveit
1: I can bind the spell Dispell with an Evil coin and a Frey coin.
IF I23+I201+I199, @binddispell2
IF -I23, @nocrestgraph
IF -I201, @nocoin
IF -I199, @nocoin
@bindispell2
1: I will bind the spell Dispell.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S102
TAKE I23
TAKE I201
TAKE I199
1: Try another spell!
GOTO @sspellmenu
@eraser
IF S103, @alreadyhaveit
1: I can bind the spell Eraser with an Evil coin and a Holy coin.
IF I23+I201+I202, @binderaser2
IF -I23, @nocrestgraph
IF -I201, @nocoin
IF -I202, @nocoin
@binderaser2
1: I will bind the spell Eraser.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S103
TAKE I23
TAKE I201
TAKE I202
1: Try another spell!
GOTO @sspellmenu
;*****Cure Spells*****
@scure
IF S1+S7+S8+S13, @cureall1
1: Here is my list of Cure spells.
MENU "Heal=@heal", "Heal Wave=@healwave", "Med-Heal=@medheal", "Revive=@revive", "Protect=@protect", "Inspire=@inspire", "Quick=@quick", "Dodge=@dodge", "Power=@power"
GOTO @sspellmenu
@heal
IF S1, @alreadyhaveit
1: I can bind the spell Heal with a Cure coin.
IF I23+I203, @bindheal2
IF -I23, @nocrestgraph
IF -I203, @nocoin
@bindheal2
1: I will bind the spell Heal.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder.wav"
1: Here you go!
GIVE S1
TAKE I23
TAKE I203
1: Try another spell!
GOTO @sspellmenu
@healwave
IF S2, @alreadyhaveit
1: I can bind the spell Heal Wave with a Cure coin and a Muse coin.
IF I23+I203+I200, @bindhealwave2
IF -I23, @nocrestgraph
IF -I203, @nocoin
IF -I200, @nocoin
@bindhealwave2
1: I will bind the spell Heal Wave.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S2
TAKE I23
TAKE I203
TAKE I200
1: Try another spell!
GOTO @sspellmenu
@medheal
IF S3, @alreadyhaveit
1: I can bind the spell Med-Heal with a Cure coin and a Frey coin.
IF I23+I203+I199, @bindmedheal2
IF -I23, @nocrestgraph
IF -I203, @nocoin
IF -I199, @nocoin
@bindmedheal2
1: I will bind the spell Med-Heal.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S3
TAKE I23
TAKE I203
TAKE I199
1: Try another spell!
GOTO @sspellmenu
@revive
IF S4, @alreadyhaveit
1: I can bind the spell Revive with a Cure coin and a Holy coin.
IF I23+I203+I202, @bindrevive2
IF -I23, @nocrestgraph
IF -I203, @nocoin
IF -I202, @nocoin
@bindrevive2
1: I will bind the spell Revive.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S4
TAKE I23
TAKE I203
TAKE I202
1: Try another spell!
GOTO @sspellmenu
@protect
IF S10, @alreadyhaveit
1: I can bind the spell Protect with a Holy coin and a Geo coin.
IF I23+I202+I197, @bindprotect2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I197, @nocoin
@bindprotect2
1: I will bind the spell Protect.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S10
TAKE I23
TAKE I202
TAKE I197
1: Try another spell!
GOTO @sspellmenu
@inspire
IF S12, @alreadyhaveit
1: I can bind the spell Inspire with a Holy coin and a Wing coin.
IF I23+I202+I198, @bindinspire2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I198, @nocoin
@bindinspire2
1: I will bind the spell Inspire.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S12
TAKE I23
TAKE I202
TAKE I198
1: Try another spell!
GOTO @sspellmenu
@quick
IF S11, @alreadyhaveit
1: I can bind the spell Quick with a Holy and a Muse coin.
IF I23+I202+I200, @bindquick2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I200, @nocoin
@bindquick2
1: I will bind the spell Quick.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S11
TAKE I23
TAKE I202
TAKE I200
1: Try another spell!
GOTO @sspellmenu
@dodge
IF S13, @alreadyhaveit
1: I can bind the spell Dodge with a Holy coin and a Cure coin.
IF I23+I202+I203, @binddodge2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I203, @nocoin
@binddodge2
1: I will bind the spell Dodge.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S13
TAKE I23
TAKE I202
TAKE I203
1: Try another spell!
GOTO @sspellmenu
@power
IF S14, @alreadyhaveit
1: I can bind the spell Power with a Holy coin and a Frey coin.
IF I23+I202+I199, @bindpower2
IF -I23, @nocrestgraph
IF -I202, @nocoin
IF -I199, @nocoin
@bindpower2
1: I will bind the spell Power.
1: Hokus Pokus Alaroo!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here you go!
GIVE S14
TAKE I23
TAKE I202
TAKE I199
1: Try another spell!
GOTO @sspellmenu
@alreadyhaveit
1: You seem to already have that spell.
1: Try another one.
GOTO @sspellmenu
@nocrestgraph
1: I'm sorry, you don't seem to have a Crest Graph.
1: You need one to bind spells with.
1: Come back with a Crest Graph and you can bind spells.
END
@nocoin
1: I'm sorry, you don't seem to have all the required elemental coins.
1: Please come back when you find them.
END
@havethemall1
1: You already have all the Simple Crest spells.
1: Try to find an Advanced Guild.
END
@geoall1
1: You have all the Simple Geo spells.
GOTO @sspellmenu
@wingall1
1: You have all the Simple Wing spells.
GOTO @sspellmenu
@museall1
1: You have all the Simple Muse spells.
GOTO @sspellmenu
@freyall1
1: You have all the Simple Frey spells.
GOTO @sspellmenu
@cureall1
1: You have all the Simple Cure spells.
GOTO @sspellmenu
@holyall1
1: You have all the Simple Holy spells.
GOTO @sspellmenu
@evilall1
1: You have all the Simple Evil spells.
GOTO @sspellmenu
;*****Fast Draw*****
@fastdrawspells1
1: I see you are a Quick Knight.
IF S52+S53+S54+S55+S56, @allquick
1: I can write Simple Fast Draw techniques for you if you bring me a Secret Sign.
@fdrawmenu
IF -I195, @nosign
1: Here is my list of Fast Draw techniques.
MENU "Psycho Crack=@psy", "Sonic Buster=@buster", "Meteor Dive=@mdive", "Soul Breaker=@soul", "Blast Charge=@bcharge"
1: Just come back when you're ready to write a Quick Draw technique.
END
@psy
IF S52, @alreadyhaveit1
1: I will write the technique Psycho Crack.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
GIVE S52
TAKE I195
1: Would you like another technique?
ASK 2O
IF YES, @fdrawmenu
1: Alright then...
1: Just come back when you want me to write a new technique.
END
@buster
IF S53, @alreadyhaveit1
1: I will write the technique Sonic Buster.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
GIVE S53
TAKE I195
1: Would you like another technique?
ASK 20
IF YES, @fdrawmenu
1: Alright then...
1: Just come back when you want me to write a new technique.
END
@mdive
IF S54, @alreadyhaveit1
1: I will write the technique Meteor Dive.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
GIVE S54
TAKE I195
1: Would you like another technique?
ASK 20
IF YES, @fdrawmenu
1: Alright then...
1: Just come back when you want me to write a new technique.
END
@soul
IF S55, @alreadyhaveit1
1: I will write the spell Soul Breaker.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
GIVE S55
TAKE I195
1: Would you like to try another technique?
ASK 20
IF YES, @fdrawmenu
1: Alright then...
1: Just come back when you want me to write a new technique.
END
@bcharge
IF S56, @alreadyhaveit1
1: I will write the spell Blast Charge.
1: Shazoo Shazim Shaziee!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Here it is!
GIVE S56
TAKE I195
1: Would you like to try another technique?
ASK 20
IF YES, @fdrawmenu
1: Alright then...
1: Just come back when you want me to write a new technique.
END
@alreadyhaveit1
1: You seem to already have that technique.
GOTO @fdrawmenu
@allquick
1: You seem to have all my techniques.
END
@nosign
1: You don't seem to have a Secret Sign.
1: When you find one, come back!
END
;*****Guardian Spells*****
@gspells1
1: I can extract the Guardian spells from runes that you bring me.
IF S62+S63+S64+S65+S66+S67+S79, @haveallsguard
1: If you bring me a rune, I can extract the Guardian for you.
@gspellmenu
1: Here are the Guardians I can extract.
MENU "Moa Gault=@moa", "Stoldark=@stol", "Fengalon=@fen", "Guridjeff=@gurd", "Nua Shakks=@nua", "Ione Paua=@ione", "Raftina=@raf"
1: Just come back when you're ready for me to extract a Guardian.
END
@moa
IF S62, @alreadyhaveit2
1: I need the Fire Rune to extract the Guardian Moa Gault.
IF -I61, @norune
1: I see you have the Fire Rune.
1: I will extract the Guardian Moa Gault.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S62
1: Let's try another one!
GOTO @gspellmenu
@stol
IF S63, @alreadyhaveit2
1: I need the Water Rune to extract the Guardian Stoldark.
IF -I62, @norune
1: I see you have the Water Rune.
1: I will extract the Guardian Stoldark.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S63
1: Let's try another one!
GOTO @gspellmenu
@fen
IF S64, @alreadyhaveit2
1: I need the Wind Rune to extract the Guardian Fengalon.
IF -I63, @norune
1: I see you have the Wind Rune.
1: I will extract the Guardian Fengalon.
1: Exo Zam Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S64
1: Let's try another one!
GOTO @gspellmenu
@gurd
IF S65, @alreadyhaveit2
1: I need the Earth Rune to extract the Guardian Guridjeff.
IF -I64, @norune
1: I see you have the Earth Rune.
1: I will extract the Guardian Guridjeff.
1: Exo Zim Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S65
1: Let's try another one!
GOTO @gspellmenu
@nua
IF S66, @alreadyhaveit2
1: I need the Thunder Rune to extract the Guardian Nua Shakks.
IF -I65, @norune
1: I see you have the Thunder Rune.
1: I will extract the Guardian Nua Shakks.
1: Exo Zim Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S66
1: Let's try another one!
GOTO @gspellmenu
@ione
IF S67, @alreadyhaveit2
1: I need the Saint Rune to extract the Guardian Ione Paua.
IF -I66, @norune
1: I see you have the Saint Rune.
1: I will extract the Guardian Ione Paua.
1: Exo Zim Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S67
1: Let's try another one!
GOTO @gspellmenu
@raf
IF S79, @alreadyhaveit2
1: I need the Love Rune to extract the Guardian Raftina.
IF -I70, @norune
1: I see you have the Love Rune.
1: I will extract the Guardian Raftina.
1: Exo Zim Zing!
WAIT 3.0sec
SOUND "thunder1.wav"
1: Extraction complete!
GIVE S79
1: Let's try another one!
GOTO @gspellmenu
@alreadyhaveit2
1: You seem to already have that Guardian.
GOTO @gspellmenu
@norune
1: I'm sorry, you don't have the required rune.
GOTO @gspellmenu
@nospells
1: I'm sorry.
1: You cannot learn magic.
1: Please leave.
END
@haveallsguard
1: You have all the Simple Guardians.
END
;Whew!!