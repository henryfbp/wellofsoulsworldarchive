this is a WORLD\MAPS folder
---------------------------

Each WORLD folder has its own MAP folder containing maps unique to that world. The definitions
of the individual maps are actually controlled by the world's QUEST.TXT file in the "+MAPS"
section.  That section must have an entry for each map used in your world.  One of the
fields in each entry is the map 'mapName' (which is basically a file name for the JPG file
which contains that maps image).  Please see the quest file documentation for details.

Maps can be any size, but 512x512 (pixels) is a recommended maximum for the main world map.  Larger
maps should work, but use up lots of memory and disk space, and generally slow the game down.

Each map is then defined by two files (where 'mapName' is whatever name you chose):

    mapName.JPG		a JPEG file of map itself.  This will be shown in the upper right MAP window
			while playing the game.  It will also be stretched by a factor of four and
			used in the main walking around windows as well, unless you provide an
			optional 'high-resolution' map.  This file is MANDATORY for each map.

    mapName.OBL    	A file created by the LINK editor which defines where
                	links are placed on the base file, and special aspects
                	about each link (whether it links to a scene, another
                	map, etc.)

Optionally a map can also have a:

    mapName.TER    	a file created by the terrain editor which indicates areas which
			require special boots (or objects) to be crossed.

    mapName.MON		a file created by the monster placement editor (part of the LINK editor)
			which defines the placement of monsters on the map.

    mapNameX4.JPG  	an OPTIONAL, high-resolution, image of the map.  If provided, it must
			be EXACTLY four times as wide and four times as tall as the REQUIRED
			"mapName.jpg" image.  If you provide both map jpegs, the large one should
			be photorealistic and the small one can either be the same, or a more
			abstract version of the map (say, a piece of parchment with line drawings
			of the world on it) [requires version A30 or later]
			You might choose to only provide a high-resolution map of your main world
			map, and just provide the low-res maps for your city maps.  It's up to 
			you, but be warned that the high-res maps will be up to 16 times larger
			than the low res version, so it dramatically increases your world's download
			size.
			Just to be clear, you MUST ALWAYS include the low-res map image.  Only the
			Hi-res map image is optional.

-----------------------------------------
Each world also requires these two files.

 OBJECTS.JPG    a JPEG file containing images which are used as LINKs
                and pasted on top of the base map.  (trees, towns,
                cave entrances, etc.)

		Note, if you don't mind a larger download size, you can instead make a 256 color
		RLE BMP (Bitmap) file, and call it "OBJECTS.BMP"  This will look better since no
		noisy JPEG static will encroach on your creative vision.  If both files are
		present, only the BMP will be used.


 OBJECTS.OBR    A file created by the OBJECT editor which gives a
                name to selected rectangles of the MOB file

Objects from the object files can be placed on all maps.  That is to say, each map does NOT  have a unique ojbjects file, but each world *does*

