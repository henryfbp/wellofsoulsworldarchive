;**************************************************************************Foresta Shops***************************************************************************
;Weapon Shop
SCENE 305 bgLibrary
THEME 0
ACTOR 1, "Sword Guy", joshTownsfolk, 9, 25, 90
1: All the weapons are here!
OFFER 58, 79, 80, 34, 46
END
;---------
;Item Shop
SCENE 306 bgLibrary
THEME 0
ACTOR 1, "Ms. Items",  joshTownsfolk, 5, 25, 90
' We've got all your basic healing items.
OFFER 1, 17, 2, 8, 4, 7, 10, 11, 12, 6, 14, 18, 23, 22, 21, 13, 15, 20, 194, 195, 196, 5, 9
END
;--------
;Armor Shop
SCENE 307 bazaar
THEME 0
ACTOR 1, "Armor Extrodanaire",  joshTownsfolk, 24, 25, 90
POSE 24, 25
' All the protection for your body!
OFFER 103, 104, 127
END
;----------
;Accesory Shop
SCENE 308 bgLibrary
THEME 0
ACTOR 1, "Jewler", joshTownsfolk, 29, 25, 90
POSE 29
' All the accesories and rings you'll need for your adventure!
OFFER 169, 187, 136, 137, 138, 139, 140, 145, 150
END
;--------
;Rosetta Inn
SCENE 309 japan
THEME 0
ACTOR 1, "Inn Keeper", joshTownsfolk, 2, 25, 90
POSE 2, 3
ACTOR 2, "Bed", Misc, 4, 47, 78
1: Would you like to rest?
1: It's only 4000GP.
ASK 20
IF YES+G4000, @rest1
1: Alright then.
1: Goodluck on your adventures!
END
@rest1
TAKE G4000
MUSIC cc210.mid
SOUND CHIRP8.wav
WAIT 8.0sec
MUSIC ""
GIVE L1
GIVE H20000
GIVE M20000
1: I hope you had a nice rest.
1: Please come again.
END
:***********************************************************************Mountainside Town Shops******************************************************************
;Weapon Shop
SCENE 310 bgLibrary
THEME 0
ACTOR 1, "Sword Guy", joshTownsfolk, 9, 25, 90
1: All the weapons are here!
OFFER 59, 81, 82, 35, 47
END
;---------
;Item Shop
SCENE 311 bgLibrary
THEME 0
ACTOR 1, "Ms. Items",  joshTownsfolk, 5, 25, 90
' We've got all your basic healing items.
OFFER 1, 17, 2, 8, 4, 7, 10, 11, 12, 6, 14, 18, 23, 22, 21, 13, 15, 20, 194, 195, 196, 5, 9, 20
END
;--------
;Armor Shop
SCENE 312 bazaar
THEME 0
ACTOR 1, "Armor Extrodanaire",  joshTownsfolk, 24, 25, 90
POSE 24, 25
' All the protection for your body!
OFFER 105, 106, 128
END
;----------
;Accesory Shop
SCENE 313 bgLibrary
THEME 0
ACTOR 1, "Jewler", joshTownsfolk, 29, 25, 90
POSE 29
' All the accesories and rings you'll need for your adventure!
OFFER 169, 187, 136, 137, 138, 139, 140, 145
END
;--------
;Rosetta Inn
SCENE 314 japan
THEME 0
ACTOR 1, "Inn Keeper", joshTownsfolk, 2, 25, 90
POSE 2, 3
ACTOR 2, "Bed", Misc, 4, 47, 78
1: Would you like to rest?
1: It's only 5000GP.
ASK 20
IF YES+G5000, @rest1
1: Alright then.
1: Goodluck on your adventures!
END
@rest1
TAKE G5000
MUSIC cc210.mid
SOUND CHIRP8.wav
WAIT 8.0sec
MUSIC ""
GIVE L1
GIVE H20000
GIVE M20000
1: I hope you had a nice rest.
1: Please come again.
END
;*************************************************************************************Fireburg Shops****************************************************************
;Weapon Shop
SCENE 315 bgLibrary
THEME 0
ACTOR 1, "Sword Guy", joshTownsfolk, 9, 25, 90
1: All the weapons are here!
OFFER 60, 81, 83, 36, 48
END
;---------
;Item Shop
SCENE 316 bgLibrary
THEME 0
ACTOR 1, "Ms. Items",  joshTownsfolk, 5, 25, 90
' We've got all your basic healing items.
OFFER 1, 17, 2, 8, 4, 7, 10, 11, 12, 6, 14, 18, 23, 22, 21, 13, 15, 20, 194, 195, 196, 5, 9, 20, 3
END
;--------
;Armor Shop
SCENE 317 bazaar
THEME 0
ACTOR 1, "Armor Extrodanaire",  joshTownsfolk, 24, 25, 90
POSE 24, 25
' All the protection for your body!
OFFER 107, 108, 129
END
;----------
;Accesory Shop
SCENE 318 bgLibrary
THEME 0
ACTOR 1, "Jewler", joshTownsfolk, 29, 25, 90
POSE 29
' All the accesories and rings you'll need for your adventure!
OFFER 169, 187, 136, 137, 138, 139, 140, 145, 151
END
;--------
;Fireburg Inn
SCENE 319 japan
THEME 0
ACTOR 1, "Inn Keeper", joshTownsfolk, 2, 25, 90
POSE 2, 3
ACTOR 2, "Bed", Misc, 4, 47, 78
1: Would you like to rest?
1: It's only 8000GP.
ASK 20
IF YES+G8000, @rest1
1: Alright then.
1: Goodluck on your adventures!
END
@rest1
TAKE G8000
MUSIC cc210.mid
SOUND CHIRP8.wav
WAIT 8.0sec
MUSIC ""
GIVE L1
GIVE H20000
GIVE M20000
1: I hope you had a nice rest.
1: Please come again.
END
;************