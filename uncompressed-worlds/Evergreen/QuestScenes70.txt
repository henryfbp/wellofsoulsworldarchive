;-------------------------
; Quest 7, 

TOKEN 70, 	"The dolphins told you of a Sea Kingdom in the far east."
TOKEN 71,	"After feeding the dolphins, they told you of the water boots you will need."
TOKEN 72,	"You vanquished the electric eels."
TOKEN 73,	"The Sea Grotto jailor asked for help in re-capturing the escaped criminals."
TOKEN 74, 	"You were instrumental in the return of the escaped criminals to the Sea Grotto Jail."
TOKEN 75, 	"The Sea King told you the truth about his dolphin chefs."
TOKEN 76,	"You restored the dolphin chefs to the Sea King's kitchen."
TOKEN 77,	"The Sea King rewarded your faithful service with the mystical Soul Horn"

;---------
SCENE 70 lakeSide, SCENE, "Dolphin Pool"
	THEME	2
	MUSIC	""

;--
    IF  -T77,	@no77
	ACTOR	1,	"Flip",		joshAnimals,	32,		20,	100
	ACTOR	2,	"Flop",		joshAnimals,	2,		30,	90
	ACTOR	3,	"Mrs. Blarg",		josh76,	1,		20,	70

	1: Hi, %1!
	2: Thanks again for rescuing us!
	1: It's nice to have all that confusion behind us.
	3: Boys, time to get back to work!
	2: Yes, Mrs. Blarg.
	1: Yes, Mrs. Blarg.
	2: Mrs Blarg provided the venture capital for our restaurant.
	1: Said she'd been saving up for years.
	2: Now she's sort of our boss.
	1: Eeee-eeeeek!

	END

@no77
	IF	-T76,	@no76
	; dolphins are back with the king.

	END

@no76
	IF	-T75,	@no75
; you know the dolphins are innocent, but they are still in jail

	END

@no75
	; need to get dolphins out of prison
	IF	-T74,	@no74
; there is no one here to talk to (they are in jail)

	END

@no74
	; need to capture dolphins
	ACTOR	1,	"Flip",		joshAnimals,	32,		20,	100
	ACTOR	2,	"Flop",		joshAnimals,	2,		30,	90

	IF	-T73,	@no73

	1: Oh, hi %1
	2: Hi, %1
	1: How's it going?
	2: What's wrong?
	1: Um, what have you heard?
	2: Eeee-eeeeeek! Run!
	1: Get the kitchen staff to help!

	MOVE	1,-50,100
	MOVE	2,-50,90
	FIGHT	109, 115, 115
	MOVE	1,20,100
	MOVE	2,30,90
	
	WAIT 3

	IF	ALIVE	@won
	1: It's nothing personal, you understand
	2: We just can't go back.
	1: Not ever.
	END

@won
	1: OK, you caught us.
	2: But you don't know the whole story!
	1: He doesn't care.
	2: OK, we'll go with you.
	GIVE	T74

	WAIT	2

	; jump straight to jailor scene in sea grotto map
	GOTO	LINK	8, 19, 1

	END	

@no73
	; need to visit Sea Grotto jailor
	IF	-T72,	@no72

	1: Hey, great job with the eels!
	2: I never liked those eels.
	1: We go WAY back.  Had some trouble with them in the old days.
	2: Yep.
	1: Well, we're still designing our restaurant.
	2: We'll need a lot of fish.
	1: Sea Grotto fish, that is.
	2: We'd get them ourselves, but...
	1: Well, that's a long story.
	2: You wouldn't be interested.
	1: Um, come back when you have some more fish...
	2: Um, yes... eeee-eek

	END

@no72
	; need to kill eels
	IF	-T71,	@no71

	1: How's the fish business coming?
	2: Yes, how's your swimming?
	1: Oh, you still haven't done the eel thing, have you?
	2: You remember, the EELs on the Isle of Light?
	1: The ones with the special boots you need?

	END

@no71
	; need to feed dolphins
	IF	-T70,	@no70

	1: Hey, look!  It's %1!
	2: Eeee-eeeek!
	1: Did you bring us something from the Sea Kingdom?
	2: Mackerel?
	1: Halibut?

	IF I101,	@gotFish
	2: No?
	1: Please, none of that store-bought fish.
	2: Ewww, no thank you!
	1: We really have our hearts set on fish from the Sea Kingdom.
	2: You know, in the east?
	1: The FAR east.
	2: Let's not do that again, ok?
	1: All right.  Anyway, Walk the coast line in the far east and see if you can catch some fish.
	2: Yes, east coast is almost as good as sea kingdom itself.
	1: mmmmm, salmon!
	2: yessss, tuna!
	END

@gotFish
	2: YES!  Sea Grotto HALIBUT!
	TAKE	I101
	1: You mean Sea KINGDOM Halibut!
	2: I know what I mean.  Grotto is the PLACE.
	2: Kingdom is the PRINCIPALITY.
	1: Oh, don't remind me.
	1: I remember when we left there.
	2: SHHH!  ix-nay on the otto-gray!	
	1: Oh, right... um, so %1, think you might make regular deliveries of fish for us?
	2: Yes, that would be great!  We could open a restaurant maybe!
	1: Finally, someplace nice to eat in Westin!
	2: You know, it just occurred to me that %1 might not be able to swim!
	1: Oh you're right, how rude of us!
	2: You know, %1, what you could use are some magic boots.
	1: Yes, like these:
	GIVE I701
	GIVE	T71
	1: Only those are just rubber boots. 
	2: Good against electricity.
	1: You REALLY want some WATER boots.
	2: Good against water.
	1: I think %1 GOT the point.
	2: Sorry.
	1: Anyway, we know where you could find some water boots
	2: Do we?
	1: Yes, remember, the eeeels
	2: Ick, I was trying to forget them
	1: Yes, %1, The eels have what you need.
	2: They're in the south.
	1: The FAR south.
	2: Please, not again.  They're on the Isle of Light.
	1: to be specific.
	2: They're nasty.
	1: And icky.
	2: to be specific.
	1: eeee-eeek.

	END

@no70
	; first visit dolphins in Westin

	1: E-e-e-ek
	2: e-eek ee-eek
	1: I guess it doesn't speak Dolphin.
	2: I think you're right.  It looks kind of dumb.
	1: I wonder what it wants?
	2: Why don't you ask?
	1: All Right, I will.... um... person.. thing over there..
	2: I don't think it heard you.
	1: I spoke quite clearly!
	2: No you didn't!  Not with your mouth full!
	SOUND "petGulp.wav"
	1: M-mmgfg!  GULP!
	POSE	2
	1: There, are you happy?  I was savoring that!
	2: Oh you're such a big baby!
	1: Am not!
	2: Are too!

	GIVE	T70

	1: <sigh> That was my last one.
	2: Your last what?
	1: The last fish from the Sea Kingdom.
	2: WHAT! You still had one left!??
	1: Well... yes, sorry.
	2: I thought I was your friend!
	1: They're just so GOOD.  Not like the fish around here.
	2: Don't remind me.  The fish here are so... watery.
	1: And no crunch.
	2: Have you tried adding Basil?
	1: Where would we find Basil in WESTIN?
	2: You're right.  Lucky to find KETCHUP in Westin.
	1: Not like back in the Sea Kingdom.
	2: Not at all.
	1: Um... That person thing is still here
	2: Yes, so what?
	1: Well, it seems to have legs, more or less.
	1: I mean, it's not stuck here in Westin...
	2: Oh, I see your point.
	1: Say... %1 I believe your name is...
	2: Yes, %1.
	1: %1, do you think you might be visiting the Sea Kingdom area someday?
	2: Yes yes! Do!  It's really great!  You'll love it!
	1: And if you happen to come across any fish while you're there...
	2: Blackened Mackerel, preferably.
	1: Are you mad?  That's TRASH!  Halibut Filet is what we want!
	2: You %3, too many bones!
	1: Well, anyway, %1, we hope you'll remember us if you make the trek.
	2: It's in the east.
	1: far east.
	2: VERY far east.
	1: I SAID that!
	2: No, you said 'far east.'
	1: %3!
	2: I'm not speaking to you any more!
	1: fine!

END


;----------------------------------------------------------------------------------------------
;

SCENE 71 scene20, SCENE, "Sea Coast", 0, 1

;--
	IF	-T76,	@no76
	; dolphin's are back with the king.

	END

@no76
	IF	-T75,	@no75
	; need to get dolphins out of prison

	END

@no75
	IF	-T74,	@no74
	; need to capture dolphins
	END

@no74
	IF	-T73,	@no73
	; need to visit Sea Grotto jailor
	END

@no73
	IF	-T72,	@no72
	; need to kill eels
	END

@no72
	IF	-T71,	@no71
	; need to feed dolphins
	END

@no71
	ACTOR	1,	"Torch",		joshAnimals,	4,		9,	67
	POSE	4, 4, 21
; pose 21 has flamenco with fish
; pose 4 is flamenco without fish

	IF	-T70,	@no70
	; need to visit dolphins in Westin

	1: You smell familiar.
	1: You remind me of a couple guys I used to know.
	1: They used to live around here.
	1: In the grotto, I think.
	1: Just a second
	POSE	21
	1: Dwww Yww want thsss phsshhh?
	ASK	30
	POSE	4
	IF 	YES, @giveFish	
	GOTO 	@nofish
@giveFish
	1: Here you go
	GIVE I101
@nofish
	1: You know...
	1: I think this was their favorite fish!
	1: Nice guys, really.
	1: Shame, when you think about it.
	END

@no70
	

END




;---------
SCENE 72 bgUnderWater1, SCENE, "Eel Marsh", 1, 0
	ACTOR	1,	"Zing",		josh67,	1,		20,	90
	POSE	1,4,2
	ACTOR	2,	"Zap",		josh67,	4,		40,	80
	POSE	1,4,2

;--
	IF	-T76,	@no76
	1: Curses on yew!
	2: Dolfeen lover!
	1: Please return our %I700!
	2: Wizzout zem, wee are trapped here!
	1: As eef wee were in preeson!
	2: Ohhh.... zat's irony, izn't eet?
	1: Zoot! Ironee!
	END

@no76
	; dolphin's are back with the king.
	IF	-T75,	@no75
	1: Yew beleeef tew eeezily!
	2: We play wif yew!
	1: Yew beleef uz!
	2: Hee hee!
	1: WE are zee reel creeminals they seek!
	2: Now wee are FREE!
	1: Thanks to YEW!
	END

@no75
	; need to get dolphins out of prison
	IF	-T74,	@no74
	1: At leest zat dolfeeeen scum is where zey belongzz!
	2: Yezz!
	END

@no74
	; need to capture dolphins
	IF	-T73,	@no73
	1: so now yew see what scum ze dolfeeeenz are!
	2: hurry to Weztin!
	1: before zey eeeescape!
	END

@no73
	; need to visit Sea Grotto jailor
	IF	-T72,	@no72
	1: Oh look, it'z zee beeg bully.
	2: Whew took our booootz.
	END

@no72
	; need to kill eels
	IF	-T71,	@no71
	1: Zo, zee dolffeeenz haff told yew our zecret!
	2: Yew haff come for zee boots!
	1: Zee MAGIC boots!
	2: Zee Magic WATER boots!
	1: Yew weeeel not get them without a fight!
	GOTO @zap
@hasRubberBoots
	2: Zoot! Your RUBBER BOOTS make you immune to our electricity!
	1: Weee weeel keeel yew ze old fazhinned way!
	MOVE	1,-50,90
	MOVE	2,-50,80

	WAIT	3
	FIGHT	65, 65, 120
	
	MOVE	1, 20, 90
	MOVE	2, 40,80

	IF	ALIVE,	@won
	1: Zometimes, zee old wayz are ze best
	2: Yezz, ze VERY bezt

	END

@won
	GIVE	T72			; proof
	GIVE	I700		; and the water boots themselves
	1: Yew are tew strong for uzz
	2: Wee geev yew our bootz
	1: our MAGIC bootz
	2: our magic WATER boootz
	WAIT	40
	1: Now, how are we going to get home, Frank?
	2: I have no idea, Charley.  We really needed those boots.
	1: Dang  it.

	END

@no71
	; need to feed dolphins
	IF	-T70,	@no70
	1: Yew reeeek uf dolfeeen!
	2: Weee hayte dolfeeeen!
@zap
	1: Yew weeeel die in 30 seconds!
	WAIT	10
	2: Yew weel die in 20 seconds! Yew shewld leef!
	WAIT	10
	1: Yew weel die in 10 seconds! No keeedink!
	WAIT	10
	2: Now, you DIE!
	WAIT	2
	SOUND	"magic10.wav"

	IF I701, @hasRubberBoots
	
	TAKE	L10000			; take their life, is the idea
	1: Yew don' feel so smart NOW, dew yew?
	2: Our electric powers are not to be ignored!
	END

@no70
	; need to visit dolphins in Westin
	1: Geet out of here!
	2: Zees ees preevat!

END




;---------
SCENE 73 bgUnderWater2, SCENE, "Sea Prison", 1, 0
	ACTOR	1,	"Blarg",		josh76,	1,		20,	90
	POSE	1,4,4

;--
	IF	-T76,	@no76
	; dolphins are back with the king.
	1: Thanks a LOT.
	1: When the King found out about it, he docked me a months pay!
	1: PLUS, he no longer uses my catering service.
	1: So you can just IMAGINE what it's like at home now.
	1: No vacation for US any time soon.
	1: I think I'll work late again tonight.
	END

@no76
	IF	-T75,	@no75
	; need to get dolphins out of prison
	1: Oh...
	1: I see you've been to the king.
	1: I don't suppose I could convince you that he was lying...
	1: Well, let's see if you're a match for me...
	1: And my PRISON ARMY!
	WAIT	3
	MOVE	1, -50, 90, 1

	FIGHT 212,164,154,154,154

	IF ALIVE, @won
	MOVE	1, 20,90

	1: Let that be a lesson to you!
	1: Now get out of here before I lock you up!

	END

@won
	ACTOR	1,	"Flip",		joshAnimals,	32,		-10,	100
	ACTOR	2,	"Flop",		joshAnimals,	2,		-10,	90
	MOVE	1, 20,	100
	MOVE	2,	30,	90

	WAIT	1
	1: Thank you! Eeee-eeeek!
	2: Yes, %1, thank you!
	1: Now you know our complete history!
	2: Yes, honest, no more secrets!
	1: The Sea King is sure to reward you!

	GIVE	T76

	WAIT	3
	; go straight to sea throne
	GOTO	LINK	8,24,1
	END

@no75
	IF	-T74,	@no74
	; king hasn't told us the truth yet
	1: Hey thanks for all your help!
	1: It's great to have those two back in their cells!
	1: They're good workers in my little side-business, as well!
	1: They really have a way with fish!
	END

@no74
	; need to capture dolphins
	IF	-T73,	@no73

	1: My wife is still nagging me about that vacation.
	1: If only I could get back those two escaped prisoners.
	1: You remember, the ones I mentioned?
	1: The ones that escaped to Westin?
	1: Their cells are still waiting for them.
	1: Just the way they left them!
	WAIT	3
	1: Well... I mean I plugged the holes, of course.
	1: Funny how I didn't notice them digging.
	1: Made quite a racket... sort of 'eeee-eeeeek!'
	1: But they sounded like that all the time.
	1: Come to think of it, so does my wife!

	END

@no73
	; need to visit Sea Grotto jailor
	IF	-T72,	@no72

	1: Welcome to the Sea Kingdom Prison!
	1: I am the head jailor!
	1: If you are honest, you have nothing to fear!
	1: But cross the line, and I have your cell waiting for you!
	1: and NO ONE escapes from one of my cells!
	WAIT	3
	1: Well... OK, it  happened once.
	1: Twice, if you count both of them.
	1: But they escaped together, so that's only once.
	1: Right?
	1: Anyway, a right vicious pair of criminals they were!
	1: The guards say they were last seen heading west.
	1: I almost got fired over it.
	1: My wife was FURIOUS.
	1: For one thing, she's never even SEEN Westin.
	1: So, it galled her that these CRIMINALs got to go!
	1: While she was stuck here.
	1: On my salary, we'll NEVER get a vacation.
	1: It's no wonder I have to work side jobs.
	1: At least I have plenty of cheap labor!
	1: Har har!

	GIVE T73

	END

@no72
	; need to kill eels
	IF	-T71,	@no71
	1: Hey, go back to Westin and do this quest right!
	1: You must have talked to the dolphins in an earlier version of Evergreen!
	END

@no71
	; need to feed dolphins
	IF	-T70,	@no70
	1: You will find no fish here, begone cheater!
	END

@no70
	; need to visit dolphins in Westin
	1: How did you even GET here?
	1: Where did you get those water boots from?	

END




;---------
SCENE 74 bgUnderWater1, SCENE, "Sea Throne", 1, 0
	ACTOR	1,	"King Mahi Mahi",		JoshRoyalty,	27,		20,	90
	POSE	27, 28

;--
	IF	-T77,	@no77
	; dolphin's are back with the king, and we got our reward.
	1: Hello, %1!
	1: Thank you once more for freeing my dolphin chefs!
	1: Their food is DELICIOUS
	1: Even if it isn't completely fresh.
	1: I get take-out from their restaurant in Westin.
	1: Blarg runs a sort of Waiters-On-Wheels now
	1: Very entrpreneurial, that Blarg.

	END

@no77
	IF	-T76,	@no76
	; dolphin's have been freed
	1: Ah, %1, I underestimated you!
	1: Thank you for freeing my favorite dolphins!
	1: Please accept this humble reward.
	1: It's a %I185
	1: Whatever that is.
	1: I found it in the back.
	1: But it looks important.
	1: So I hope you get some use out of it.

	GIVE	T77	
	GIVE	I185		; soul horn
	END

@no76
	IF	-T75,	@no75
	1: Oh, hello, idiot.
	1: I mean, %1.
	1: My chef dolphins are still locked up!
	1: You simply MUST negotiate their release with the jailor!
	1: Look what his catering service delivered today for my meal!
	1: SUSHI!
	1: I mean, big fish, little fish, whatever.  This is sick!
	1: Simply repulsive!
	1: PLEASE get them out!
	1: I'm sure I can find a reward for you around here somewhere!
	END

@no75
	; need to get dolphins out of prison
	IF	-T74,	@no74
	1: You caught the escaped prisoners?
	1: That's wonderful news!  I always hated those two.
	1: They were polluting the environment.
	1: All those electric zaps and zings!
	1: Eeels smell funny as well, if you ask me.
	1: Definitely 'little' fish.
	1: What's that?
	1: You captured DOLPHINS?
	1: %1, You idiot!
	1: Those dolphins aren't criminals!
	1: They're gods!
	1: They're my private chefs!  
	1: They disappeared ages ago!
	1: I assumed they went off to start a restaurant somewhere.
	1: Their blackened mackerel was exquisite!
	1: You must rescue them immediately!
	1: I am weary of Blarg's Catering Service!
	1: His prices are good, but he simply has no taste!
	1: It was those EELS who should have been in prison
	1: They stole my favorite pair of %I700
	1: I loved those boots.
	1: A lot like the ones YOU'RE wearing in fact.
	1: Where did you... oh never mind.
	1: You MUST undo the damage your careless meddling has caused!
	1: I COMMAND you, %1, to rescue the dolphins!

	GIVE	T75
	END

@no74
	; need to capture dolphins
	IF	-T73,	@no73
	1: I see you've heard about our little secret
	1: All is not perfect here.
	1: We did have those two nasty criminals who stole my precious.. um..
	1: Well, they were bad, and we put them in jail.
	1: Until they escaped.
	1: I think they went west.
	1: But everywhere looks 'west' to us.
	END

@no73
	; need to visit Sea Grotto jailor
	IF	-T72,	@no72
	1: Welcome, %1, to the Sea Kingdom.
	1: Here in the Sea Grotto we live a life of magical luxury.
	1: And oddly enough, we all enjoy eating fish.
	1: I know that should bother me.
	1: Sounds rather cannabilistic when I put it that way.
	1: But it's really more of the 'big fish eat little fish' sort of thing.
	1: That doesn't sound so bad, does it?
	1: I mean, assuming you're a pretty big fish.
	END

@no72
	; need to kill eels
; should be impossible to get here without water boots, which you get from eels
	IF	-T71,	@no71
	1: Please continue the dolphin quest and come back later..
	END

@no71
	; need to feed dolphins
	IF	-T70,	@no70
	1: Please do what the dolphins said to do..
	END

@no70
	; need to visit dolphins in Westin
	1: Please visit the dolphins in Westin first.. this quest has changed...

END



; comment at the end