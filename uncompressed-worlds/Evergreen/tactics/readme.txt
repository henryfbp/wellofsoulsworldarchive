this is your world's "tactics" folder, which contains assets used by the "WoS Tactics" mini-game.

Please see http://www.synthetic-reality.com/wosTactics.htm for more information.

---

Note that the contents of this folder are NOT included with the Well of Souls download, this is so they can be fetched from the web site later, so it doesn't require a new release of WoS to release new tactics maps, tiles, and pieces.

If you are a world developer, your world's config.ini file can declare the web host for your own WoS Tactics resources (or you can use the Evergreen assets, or you can declare 'none' to suppress WoS Tactics for players of your world, you big meanie!)

The most important file in this folder is:  

	"tactics_ini.txt"

Please see the copy in the Evergreen/Tactics folder for useful information.  But that file declares all your maps, tiles, pieces, sound effects, etc.  And only assets declared in that file will be downloaded from the web folder you specify in config.ini