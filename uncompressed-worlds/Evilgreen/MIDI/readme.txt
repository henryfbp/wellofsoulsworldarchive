this is the MIDI directory of a world
-------------------------------------

this should contain MIDI files which are then played as a result of:

   the MUSIC opcode in a scene script
   the MUSIC setting of a link

Also, the following 'standard' files are expected to be found in this directory

   dead.mid   - played during the initial scrolling 'backstory' when entering a world
   won.mid     - brief tune played once at end of successful fight scene
   lost.mid    - brief tune played once at end of unsuccessful fight scene

If a requested file is missing from the world's personal MIDI folder, the general game MIDI folder is used instead.