;Scenes 1000-1100
;tokens 4000, 4001

SCENE 1006, dungeon05, scene, "Exit", 0, 0, 0
TAKE T4000
TAKE T4001
GOTO LINK 0, 2
END

SCENE 1007, dungeon05, scene, "Boss1", 0, 0, 0
IF T4000 @end
#include aura.txt
FIGHT 1006
#include zerkerstyles.txt
IF WIN @win
END
@win
N: You have defeated the First Boss.
GIVE T4000
END

SCENE 1008, dungeon05, scene, "Boss2", 0, 0, 0
IF T4001 @end
#include aura.txt
FIGHT 1007
#include zerkerstyles.txt
IF WIN @win
END
@win
N: You have defeated the Second Boss.
GIVE T4001
END

SCENE 1009, dungeon05, scene, "Boss3", 0, 0, 0
IF T4000+T4001 @cont
END
@cont
#include aura.txt
FIGHT 1008
#include zerkerstyles.txt
IF WIN @win
END
@win
TAKE T4000
TAKE T4001
SET random, "%r6"
SUB random, "1"
COMPARE #<random>, "5"
IF= @gp
N: You have found %I401#<random>.
GIVE I401#<random>
GOTO @cho
@gp
N: You found 200 GP.
GIVE G200
GOTO @cho
END
@cho
	N: You can either go up a floor.
	N: Or reset this floor.
	MENU "Go up=@up", "Reset=@res"
GOTO @cho
END
@up
' Max floor so far.
END
@res
' Floor reset..
END


SCENE 1000, dungeon05, scene, "Boss1", 0, 0, 0
	IF T4000 @end
#include aura.txt
	FIGHT 1000
#include zerkerstyles.txt
	IF win @win
END
@win
	N: You have defeated the First Boss.
	GIVE T4000
END
@end
END

SCENE 1001, dungeon05, scene, "Boss2", 0, 0, 0
	IF T4001 @end
#include aura.txt
	FIGHT 1001
#include zerkerstyles.txt
	IF win @win
END
@win
	N: You have defeated the Second Boss.
	GIVE T4001
END
@end
END

SCENE 1002, dungeon05, scene, "Boss3", 0, 0, 0
	IF T4000+T4001 @fight
END
@fight
#include aura.txt
	FIGHT 1002
#include zerkerstyles.txt
	IF WIN @win
END
@win
	TAKE T4000
	TAKE T4001
	SET random, "%r6"
	GOTO @r#<random>

@r6
	N: You found 40 gp.
	GIVE G40
GOTO @cho
END
@r1
	N: You found a %i4000
	GIVE I4000
GOTO @cho
END
@r2
	N: You found a %i4001
	GIVE I4001
GOTO @cho
END
@r3
	N: You found a %i4002
	GIVE I4002
GOTO @cho
END
@r4
	N: You found a %i4003
	GIVE I4003
GOTO @cho
END
@r5
	N: You found a %i4004
	GIVE I4004
GOTO @cho
END
@cho
	N: You can either go up a floor.
	N: Or reset this floor.
	MENU "Go up=@up", "Reset=@res"
GOTO @cho
END
@up
	N: Progessing..
	GOTO LINK 101, 0
END
@res
	N: Floor reset.
END

SCENE 1003, dungeon05, scene, "Boss1", 0, 0, 0
	IF T4000 @end
#include aura.txt
	FIGHT 1003
#include zerkerstyles.txt
	IF win @win
END
@win
	N: You have defeated the First Boss.
	GIVE T4000
END
@end
END

SCENE 1004, dungeon05, scene, "Boss2", 0, 0, 0
	IF T4001 @end
#include aura.txt
	FIGHT 1004
#include zerkerstyles.txt
	IF win @win
END
@win
	N: You have defeated the Second Boss.
	GIVE T4001
END
@end
END

;----------------
SCENE 1005, dungeon05, scene, "Boss3", 0, 0, 0
	IF T4000+T4001 @fight
END
@fight
#include aura.txt
	FIGHT 1005
	IF WIN @win
#include zerkerstyles.txt
END
@win
	TAKE T4000
	TAKE T4001
SET random, "%r6"
GOTO @r#<random>
END
@r6
	N: You found 100 gp.
	GIVE G100
GOTO @cho
END
@r1
	N: You found a %i4005
	GIVE I4005
GOTO @cho
END
@r2
	N: You found a %i4006
	GIVE I4006
GOTO @cho
END
@r3
	N: You found a %i4007
	GIVE I4007
GOTO @cho
END
@r4
	N: You found a %i4008
	GIVE I4008
GOTO @cho
END
@r5
	N: You found a %i4009
	GIVE I4009
GOTO @cho
END
@cho
	N: You can either go up a floor.
	N: Or reset this floor.
	MENU "Go up=@up", "Reset=@res"
GOTO @cho
END
@up
	N: Progessing..
	GOTO LINK 102, 0
END
@res
	N: Floor reset.
END