;Scenes 20, 21, 22
;Tokens 1- Talked to warden 2- First part done 3- Secound part done 4- Killed warden
;RESERVED 5

Scene 22, town01, scene, "Training Grounds", 0, 0, 0
IF T5 @forbid
	PARTY "-T5"
	GOTO LINK 2, 0
END
@forbid
	N: The door is locked...
END


Scene 21, town01, scene, "Atlis Exit", 0, 0, 0
Actor 1, "Guard", Monster89, 1, 24, 82
	SEL 1
	IF T5 @exit
	IF T4 @Attack
	IF T2 @exit
	' Sorry %1, I can't allow you to leave yet.
END
@exit
IF T5 @exit2
IF KM11.10 @dourjob
@exit2
	PARTY "T2"
	' Good day %1.
	WAIT 1
	GOTO LINK 0, 1
END
@dourjob
	' You have already killed 10 %m11's.
	' Go do back to the Warden.
END
@attack
	' You killed the Warden!
	' You traitor!
	' Get him!
	Fight 501, 501, 501
	IF won @kildgard
	' Your pathetic get out of my sight.
END
@kildgard
	' Argh fine, you can continue to pass.
	' I won't let you harm the new warden you are cut off from the training grounds.
	GIVE T5
END


Scene 20, dungeon05, scene, "Warden", 0, 0, 0
	IF T4 @empty
	IF T3 @wardatk
Actor 1, "Warden", Monster88, 1, 24, 82
IF DEAD @dead
GIVE H5000
GIVE M5000
	SEL 1
	IF T2 @job2
	IF T1 @kill
	' Hey, welcome to the training grounds.
	' Before you can leave Atlis you have been ordered to train.
	' Kill the following monsters.
	GIVE T1
@kill
SET e1, "4"
SUB e1, #<KM.1>
SET e2, "4"
SUB e2, #<KM.2>
SET e3, "3"
SUB e3, #<KM.3>
SET e4, "3"
SUB e4, #<KM.4>
SET e5, "2"
SUB e5, #<KM.5>
SET e6, "2"
SUB e6, #<KM.6>
SET e7, "1"
SUB e7, #<KM.7>
SET e8, "1"
SUB e8, #<KM.8>
SET e9, "1"
SUB e9, #<KM.9>
SET e10, "1"
SUB e10, #<KM.10>
SET e11, "8"
SUB e11, #<num.hostlevel>
COMPARE #<e1>, 0
IF> @morekill
COMPARE #<e2>, 0
IF> @morekill
COMPARE #<e3>, 0
IF> @morekill
COMPARE #<e4>, 0
IF> @morekill
COMPARE #<e5>, 0
IF> @morekill
COMPARE #<e6>, 0
IF> @morekill
COMPARE #<e7>, 0
IF> @morekill
COMPARE #<e8>, 0
IF> @morekill
COMPARE #<e9>, 0
IF> @morekill
COMPARE #<e10>, 0
IF> @morekill
COMPARE #<e11>, 0
IF> @morekill
@done
	' Good job..
	' Now you will permitted to leave Atlis, I recommend level 10 before leaving.
	' I have another job for you.
GIVE T2
	GOTO @job2
END
@morekill
	' You still have more Punching bags to kill.
COMPARE #<e1>, 0
IF= @1
IF< @1
	' #<e1> more %m1s.
@1
COMPARE #<e2>, 0
IF= @2
IF< @2
	' #<e2> more %m2s.
@2
COMPARE #<e3>, 0
IF= @3
IF< @3
	' #<e3> more %m3s.
@3
COMPARE #<e4>, 0
IF= @4
IF< @4
	' #<e4> more %m4s.
@4
COMPARE #<e5>, 0
IF= @5
IF< @5
	' #<e5> more %m5s.
@5
COMPARE #<e6>, 0
IF= @6
IF< @6
	' #<e6> more %m6s.
@6
COMPARE #<e7>, 0
IF= @7
IF< @7
	' #<e7> more %m7s.
@7
COMPARE #<e8>, 0
IF= @8
IF< @8
	' #<e8> more %m8s.
@8
COMPARE #<e9>, 0
IF= @9
IF< @9
	' #<e9> more %m9s.
@9
COMPARE #<e10>, 0
IF= @10
IF< @10
	' #<e10> more %m10s.
@10
COMPARE #<e11>, 0
IF= @11
IF< @11
	' And you  need to gain #<e11> more level(s).
@11
	' If you ever want freedom you better get out there.
END
@job2
	' I need you to go and kill 10 %m11s.
SET e12, "10"
SUB e12, #<KM.11>
COMPARE #<e12>, "0"
IF= @job2done
IF< @job2done
	' You need to kill #<e12> more %m11s.
END
@job2done
	GIVE T3
	' Your getting good at this..
	' Too good..
	' Are you trying to take my job?!
	' I'll kill you..
	MOVE 1, -40, 82
	WAIT 2
	'  /
	FIGHT 500
	IF won @killdwarden
	MOVE 1, 24, 82
	' You can't take my job..
END
@wardatk
	FIGHT 500
	IF WON @killdwarden
END
@killdwarden
	N: You find 20 gp on his dead body and take it.
	GIVE G20
	GIVE T4
END
@empty
END
@dead
	' Ugh, coming to me dead?
	' Fine, I will revive you.
	GIVE L1
	GIVE H5000
	GIVE M5000
END

;blank line