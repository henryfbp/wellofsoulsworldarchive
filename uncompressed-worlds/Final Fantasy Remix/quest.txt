.+CREDITS
Final Fantasy Remix
by: Gametweeka
gametweeka@yahoo.com
Game engine programming by
Synthetic Reality
Contact us on the web at:
http://www.synthetic-reality.com
-CREDITS

+STORY
:F 
:S 200
:C 255,255,0
|Final Fantasy Remix
:F 
:S 100
:C 255,255,255
|~~~
|by Gametweeka
|A world for the fans by a fan.
|All Final Fantasy materials contained in this world are copyright or trademark properties held exclusively by their respective owners and are used with permission.
|~~~ 
:F 
:S 100
:C 204,153,0
|Let go of yourself...
|
|Live the adventure of a hundred lifetimes.
:F 
:S 150
:C 255,0,0
|Welcome to the fantasy.
-STORY

+TERRAINS
  0,	"Normal",	0
  1,	"Mountain",	0
  2,	"River",	0
  3,	"Forest",	0
  4,	"Ocean",	0
  5,	"Clouds",	0
  6,	"Desert",	0
  7,	"",		0
  8,	"",		0
  9,	"Impassable",	0
-TERRAINS

+EQUIP
0,	""
1,	""
10,	"Chocobo"
11,	"Armor"
12,	""
13,	""
-EQUIP

+THEMES
20,           cavedrip,	campfire1,		10=drip1,10=bird1,10=splash1
21,              Train,            Train,       			,
22,              Alarm,	           Alarm,				,
23,         Helicopter,       Helicopter,				,
24,          GunBattle,            bangs,   3=Machinegun, 5=gunreload,
25,          Heartbeat, 	       HeartLoop,				,
26,         Motorcycle,       Motorcycle,				,
27,	TruckInRain,	Truckinrain,	15=Thunder1.wav,  21=Thunder2.wav,  27=Thunder3.wav
-THEMES

+MAPS
;1	// enables pet vs pet battles (but protects players from pets)
;2	// enables pets to be used against other players or pets (if pkers or no safehaven)
;4	// enables "no protection from PKer"
;8	// enables "no PKing allowed here at all"
;16	// enables "LADDER" reporting. (PK kills on this map are reported to ladder server)
;32	// enables "show all link names." Normally link names are hidden until after first use of link.
;64	// disables "Shadows" under heroes (and monsters) on map and in scenes of that map.
;128	// disables the use of pets (cannot summon them) on this map.
;256	// disables eavesdropping to and from players on this map.  (you can still whisper, shout, etc.)
;512	// designates the map a GUILD HALL map.  (not sure what this means yet...)
;1024	// Prevents Humans from being able to damage monsters or pets
;2048	// NO_REWARD.  After a fight, no GP, XP or items are found to reward the survivors
;	// Pets still get rewarded, coz I am a big softy
;4096	// NO_HEAL.  Heal spells and items provide only 1HP of sustenance.
;8192	// NO_TICKETS.  Tickets won't work here
;16384	// NO_MINI_MAP. upper right minimap is uninformative here
;32768	// NO_STUN.  the non-buff disease spells will not work here (stun, sleep, paralyze, etc.)
;65536	// NO_BUFF.	the buff/debugg disease spells will not work here.
;131072	// NO_PKREZ. PKers cannot use resurrection spells on others
;262144	// MAP_FLAG_HIDE_LINKS. don't show white link boxes on minimap until visited
;
  0, "FinalFantasyRemix.jpg", "FinalFantasyRemix", "FinalFantasyRemix", 0
  1, "Reactor-One-Entering.jpg", "Reactor-One-Entering", "Reactor-One-Entering", 0
  2, "Reactor-One-Inside.jpg", "Reactor-One-Inside", "Reactor-One-Inside", 0
  3, "Reactor-One-Getaway.jpg","Reactor-One-Getaway" ,"Reactor-One-Getaway", 0
  4, "Getaway.jpg","Getaway","Getaway",0
  5, "Sector-7.jpg","Sector-7","Sector-7",0
  6, "Sector-7(2).jpg","Sector-7(2)","Sector-7(2)",0
  7, "Sector-7(3).jpg","Sector-7(3)","Sector-7(3)",0
  8, "Reactor-Five-Entering.jpg", "Reactor-Five-Entering", "Reactor-Five-Entering", 0
  9, "Reactor-Five-Inside.jpg", "Reactor-Five-Inside", "Reactor-Five-Inside", 0
  10,"Reactor-Five-Getaway.jpg","Reactor-Five-Getaway" ,"Reactor-Five-Getaway", 0  
  11,"Church.jpg","Church","Church",0
  12,"Sector-5.jpg","Sector-5","Sector-5",0
  13,"Sector-5(2).jpg","Sector-5(2)","Sector-5(2)",0
  14,"AerisHouse.jpg","AerisHouse","AerisHouse",0
  15,"Sector-6.jpg","Sector-6","Sector-6",0
  16,"WallMarket.jpg","WallMarket","WallMarket",0
  17,"WallMarket(2).jpg","WallMarket(2)","WallMarket(2)",0
  18,"HoneyBeeInn.jpg","HoneyBeeInn","HoneyBeeInn",0
  19,"CorneoMansion.jpg","CorneoMansion","CorneoMansion",0
  20,"TrainGraveyard.jpg","TrainGraveyard","TrainGraveyard",0
  21,"MainPillar.jpg","MainPillar","MainPillar",0,24
  22,"Shinra-HQ-Entrance.jpg","Shinra-HQ-Entrance","Shinra-HQ-Entrance",0
  23,"Shinra-HQ-Stairs.jpg","Shinra-HQ-Stairs","Shinra-HQ-Stairs",0
  24,"Shinra-HQ-Lower.jpg","Shinra-HQ-Lower","Shinra-HQ-Lower",0
  25,"Shinra-HQ-Mid.jpg","Shinra-HQ-Mid","Shinra-HQ-Mid",0
  26,"Shinra-HQ-Upper.jpg","Shinra-HQ-Upper","Shinra-HQ-Upper",0
  27,"ChocoboFarm.jpg","ChocoboFarm","ChocoboFarm",0
  28,"Kalm.jpg","Kalm","Kalm",0
  29,"MythrilMine.jpg","MythrilMine","MythrilMine",0
  30,"FortCondor.jpg","FortCondor","FortCondor",0
  31,"JunonHarbor.jpg","JunonHarbor","JunonHarbor",0
  32,"Junon(1).jpg","Junon(1)","Junon(1)",0
  33,"Junon(2).jpg","Junon(2)","Junon(2)",0
  34,"JunonShops(1).jpg","JunonShops(1)","JunonShops(1)",0
  35,"JunonShops(2).jpg","JunonShops(2)","JunonShops(2)",0
  36,"JunonShops(3).jpg","JunonShops(3)","JunonShops(3)",0
  37,"JunonShops(4).jpg","JunonShops(4)","JunonShops(4)",0
  38,"ShinraBoat.jpg","ShinraBoat","ShinraBoat",0
  39,"MtCorel(1).jpg","MtCorel(1)","MtCorel(1)",0
  40,"MtCorel(2).jpg","MtCorel(2)","MtCorel(2)",0
  41,"CostaDelSol.jpg","CostaDelSol","CostaDelSol",0
  42,"NorthCorel.jpg","NorthCorel","NorthCorel",0
  43,"GoldenSaucer.jpg","GoldenSaucer","GoldenSaucer",0
  44,"BattleSquare.jpg","BattleSquare","BattleSquare",0
  45,"SpeedSquare.jpg","SpeedSquare","SpeedSquare",0
  46,"GhostSquare.jpg","GhostSquare","GhostSquare",0
  47,"WonderSquare.jpg","WonderSquare","WonderSquare",0
  48,"ChocoboRacing.jpg","ChocoboRacing","ChocoboRacing",0
  49,"CorelPrison.jpg","CorelPrison","CorelPrison",0
  50,"CorelPrisonWasteland","CorelPrisonWasteland","CorelPrisonWasteland",0
  51,"GongagaVillage(1).jpg","GongagaVillage(1)","GongagaVillage(1)",0
  52,"GongagaVillage(2).jpg","GongagaVillage(2)","GongagaVillage(2)",0
  53,"CosmoCanyon.jpg","CosmoCanyon","CosmoCanyon",0
  54,"CosmoCanyonShops.jpg","CosmoCanyonShops","CosmoCanyonShops",0
  55,"CosmoObservatory.jpg","CosmoObservatory","CosmoObservatory",0
  56,"GiCave.jpg","GiCave","GiCave",0
  57,"Nibelheim.jpg","Nibelheim","Nibelheim",0
  58,"ShinraMansion.jpg","ShinraMansion","ShinraMansion",0
  59,"ShinraMansionBasement.jpg","ShinraMansionBasement","ShinraMansionBasement",0
  60,"MtNibel.jpg","MtNibel","MtNibel",0
  61,"MtNibelPeak.jpg","MtNibelPeak","MtNibelPeak",0
  62,"MtNibelReactor.jpg","MtNibelReactor","MtNibelReactor",0
  63,"RocketTown.jpg","RocketTown","RocketTown",0
  64,"RocketTownShops.jpg","RocketTownShops","RocketTownShops",0
  65,"Wutai(1).jpg","Wutai(1)","Wutai(1)",0
  66,"Wutai(2).jpg","Wutai(2)","Wutai(2)",0
  67,"DaChao.jpg","DaChao","DaChao",0
  68,"TempleOfTheAncients(1).jpg","TempleOfTheAncients(1)","TempleOfTheAncients(1)",0
  69,"TempleOfTheAncients(2).jpg","TempleOfTheAncients(2)","TempleOfTheAncients(2)",0
  70,"BoneVillage.jpg","BoneVillage","BoneVillage",0
  71,"SleepingForest.jpg","SleepingForest","SleepingForest",0
  72,"CityOfTheAncients(1).jpg","CityOfTheAncients(1)","CityOfTheAncients(1)",0
  73,"CityOfTheAncients(2).jpg","CityOfTheAncients(2)","CityOfTheAncients(2)",0
  74,"CorralValleyCave.jpg","CorralValleyCave","CorralValleyCave",0
  75,"IcicleInn.jpg","IcicleInn","IcicleInn",0
  76,"GaiaCliffs(1).jpg","GaiaCliffs(1)","GaiaCliffs(1)",0
  77,"GaiaCliffs(2).jpg","GaiaCliffs(2)","GaiaCliffs(2)",0
  78,"GaiaCliffs(3).jpg","GaiaCliffs(3)","GaiaCliffs(3)",0
  79,"TheCrater(1).jpg","TheCrater(1)","TheCrater(1)",0
  80,"TheCrater(2).jpg","TheCrater(2)","TheCrater(2)",0
  81,"Mideel.jpg","Mideel","Mideel",0
  82,"DestroyedMideel.jpg","DestroyedMideel","DestroyedMideel",0
  83,"SubBasePassage.jpg","SubBasePassage","SubBasePassage",0
  84,"SubBase.jpg","SubBase","SubBase",0
  85,"SubDock.jpg","SubDock","SubDock",0
  86,"ShinraRocket.jpg","ShinraRocket","ShinraRocket",0
  87,"ReturnToMidgar.jpg","ReturnToMidgar",ReturnToMidgar",0
  88,"InTheCrater.jpg","InTheCrater","InTheCrater",0
  89,"TheRightPath.jpg","TheRightPath","TheRightPath",0
  90,"TheLeftPath.jpg","TheLeftPath","TheLeftPath",0
  91,"PathToTheUnknown.jpg","PathToTheUnknown","PathToTheUnknown",0
  92,"TheNightmare.jpg","TheNightmare","TheNightmare",0
  93,"SideQuestArea.jpg","SideQuestArea","SideQuestArea",0
  94,"CrashedGelinka.jpg","CrashedGelinka","CrashedGelinka",0
  95,"AncientForest.jpg","AncientForest","AncientForest",0
  96,"JunonAirport.jpg","JunonAirport","JunonAirport",0
-MAPS

+TOKENS
#include tokens.txt
-TOKENS

+ELEMENTS
0,	"Holy"		
1,	"Fire"			
2,	"Ice"			
3,	"Lightning"			
4,	"Gravity"		
5,	"Earth"		
6,	"Poison"		
7,	"Wind"			
-ELEMENTS

+HANDS
0,	"Sword",	30,	"sword1.wav"
1,	"Fist", 	20,	"staff.wav"
2,	"Arm-Gun",	100,"fist.wav"
3,	"Shuriken",	50,	"chirp4.wav"
4,	"Fangs",	20,	"fist.wav"
5,	"Handgun",	80,	"dart.wav"
6,	"Bullhorn",	70,	"chant6.wav"
7,	"Spear",	90,	"crow1.wav"
-HANDS

+ITEMS
#include items.txt
-ITEMS

+SPELLS
#include spells.txt
-SPELLS

+MONSTERS
#include monsters.txt
-MONSTERS

+GROUPS
#include groups.txt
-GROUPS

+LEVELS
100,	0,		250,	35,		"Hero",   0,	1
DESCRIPTION	"Ex-member of the elite military group SOLDIER. He now finds himself working as a mercenary for the underground group know as AVALANCHE."
AUTO_MAX 250,5000,35,1000,1	
HAND_RATIO 90
MAGIC_RATIO 60
START_ABILITY 0,0,0,0,0
START_LOCATION  1,0,1
START_ITEMS 1,1,1,97,114,
START_ELEMENT_PP 2500,2500,2500,2500,2500,2500,2500,2500
MAX_ELEMENT_PP 100000,100000,100000,100000,100000,100000,100000,100000
START_HAND_PP 45000,0,0,0,0,0,0,0
MAX_HAND_PP 100000,0,0,0,0,0,0,0	
; now the class level names.. note that since I used the AUTO_MAX command, the maxHP and maxMP values shown in this
; table are actually ignored.  If I weren't so lazy, I would change them all to 0.
101,	0,			1,		0,		"Ex-Soldier"
105,	0,			2,		0,		"Mercenary"
110,	0,			5,		0,		"Wanderer"
120,	0,			8,		10,		"Drifter"
130,	0,			10,		0,		"Nomad"
140,	0,			14,		10,		"Fighter"
150,	0,			22,		0,		"Leader"
160,	0,			28,		20,		"Liberator"
170,	0,			36,		0,		"Champion"
180,	0,			50,		20,		"Legend"
190,	0,			65,		0,		"Hero"
-LEVELS

+SCENES
#include scenes.txt
#include scenesnew.txt
-SCENES
;