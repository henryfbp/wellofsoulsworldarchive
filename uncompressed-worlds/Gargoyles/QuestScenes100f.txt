;----(Ultima's invention for you gardeners!)
;SCENEID#, BKGND, scene/cut, NameofSCENE, FX, Weather, ColorTable
;ACTORID#, "NAME", skinNAME, Pose,<x>,^ Y v,
; Let the farming BEGIN!!!!!!

SCENE 772, Grass, Scene, Farmland, 0, 0, 0
THEME 0
MUSIC 
ACTOR 1, GreenGrower, joshtownsfolk, 1, 25, 85

;-------IF
IF T100 @seedst100


1: Hello, I guess you've never been here before..
h: Nope, what is this place???
1: The Farm of Dreams, You can grow plants here and harvest them for food.
1: Growing certain seeds make certain plants for certain food.
1: The food can be eaten to heal you, i've seen people make food to heal 99999 HP and MP!!!
1: And I've seen people you grow seeds for more seeds! and they sell them for vast amounts of money!
h: So, i could make either more seeds to sell for money? Or grow crops to eat which are better than most healing items?
1: Yes! It's good fun and a great bussiness once your into it!
h: Sounds to simple and plain...
1: Well, you can grow 100's of types of plant and also you can go to the labortory to make new "formulas"
1: These Formulas can help your plants grow differently to make more succesful crops
1: However the Scienctist does want lots of money for simple formulas , he wants 1million GP for most formulas
h: So how long do plants take to grow?
1: Well, Around 30 mins(it differs on the formula) they are fully grown and ebiable
1: You must stay on the game for this period and you will be taken back to this scene to show you your new plant!
h: Wow, sounds Okay. Sounds like you dont gain much money since the Scienctist wants sooo much!
1: Well, it's true, but for crops you can't buy anywhere else, it's worth it!
h: So seeds? They cost around 100k each , is it free to plant?
1: nope i charge a meer 10k for one patch.
1: Very cheap, and is the cheapest part of your cropping career!
h: Ok, if i feel like it , i'll give it a go!
1: Good, Good. I'll be waiting for your return with seeds! and Formula's
GIVE T100
1: See the Scienctist, who is in the dark abyss. He can tell you more about his formula's and how he can help you grow plants
h: Ok i'll give him a visit Bye
1: C ya

@seedst100
1: Go give the scienctist a visit, in the Dark Abyss, He'll tell you more about formulas
1: While your at it get as many seeds as you can for planting


END


;Comment at end - dont want my gardening to die out? eh?---



;----------------------------------------------------------------------------------------------------------------------
;SCENEID#, BKGND, scene/cut, NameofSCENE, FX, Weather, ColorTable
;ACTORID#, "NAME", skinNAME, Pose,<x>,^ Y v,

SCENE 796, scene6, SCENE, Nameisspecial, 0, 0, 0
ACTOR 1, "Grandfather Time", "joshMiscellaneous", 10, 5, 90, 1024
	POSE	10, 11
	SEL		1
	MOVE	1,	15, 80
	1: The following is a test of times and dates...
	; local.timeNow
	SET local.timeNow, #<num.timeNow>
	SET local.timeNowWrite, #<local.timeNow>
	; The "world" began in 1990 our time, and is presently somewhere beyond year 40.
	; I changed this from 2000 so that there's no possibility of a soul being born before the creation of the world.
;	SUB local.timeNowWrite, 631134720
	; Time that is 8x faster than our own.
	MUL local.timeNowWrite, 6
	; local.timeYear
	SET local.timeYear, #<local.timeNowWrite>
	DIV local.timeYear, 31556736
	SET local.timeLeapYear, #<local.timeYear>
	SET local.timeNowLoss, #<local.timeYear>
	MUL local.timeNowLoss, 31556736
	SUB local.timeNowWrite, #<local.timeNowLoss>
	ADD local.timeYear, 1970
	; local.timeMonth
	; local.timeDay
	SET local.timeMonth, 1
	SET local.timeDay, #<local.timeNowWrite>
	DIV local.timeDay, 86400
	SET local.timeNowLoss, #<local.timeDay>
	MUL local.timeNowLoss, 86400
	SUB local.timeNowWrite, #<local.timeNowLoss>
	; January has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; Leap year testing. During leap year, February has 29 days.
	; Also, every 100 years you skip a leap year.
	SET local.timeLeapYearFail, #<local.timeLeapYear>
	MOD local.timeLeapYearFail, 100
	COMPARE local.timeLeapYearFail, 0
	IF= @february28
	SET local.timeLeapYearFail, #<local.timeLeapYear>
	MOD local.timeLeapYearFail, 4
	COMPARE local.timeLeapYearFail, 0
	IF> @february28
	COMPARE #<local.timeDay>, 29
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 29
	ADD local.timeMonth, 1
	GOTO @march	
@february28
	; February has 28 days
	COMPARE #<local.timeDay>, 28
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 28
	ADD local.timeMonth, 1
@march
	; March has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; April has 30 days
	COMPARE #<local.timeDay>, 30
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 30
	ADD local.timeMonth, 1
	; May has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; June has 30 days
	COMPARE #<local.timeDay>, 30
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 30
	ADD local.timeMonth, 1
	; July has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; August has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; September has 30 days
	COMPARE #<local.timeDay>, 30
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 30
	ADD local.timeMonth, 1
	; October has 31 days
	COMPARE #<local.timeDay>, 31
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 31
	ADD local.timeMonth, 1
	; November has 30 days
	COMPARE #<local.timeDay>, 30
	IF< @monthdone
	IF= @monthdone
	SUB local.timeDay, 30
	ADD local.timeMonth, 1
	; There should never be a time when you get this far with more than 31 days, which is teh number of days in December.
@monthdone
	; local.timeHour
	SET local.timeHour, #<local.timeNowWrite>
	DIV local.timeHour, 3600
	SET local.timeNowLoss, #<local.timeHour>
	MUL local.timeNowLoss, 3600
	SUB local.timeNowWrite, #<local.timeNowLoss>
	ADD local.timeHour, 4
	; local.timeMinute
	SET local.timeMinute, #<local.timeNowWrite>
	DIV local.timeMinute, 60
	SET local.timeNowLoss, #<local.timeMinute>
	MUL local.timeNowLoss, 60
	SUB local.timeNowWrite, #<local.timeNowLoss>
	ADD local.timeMinute, 36
	COMPARE #<local.timeMinute>, 60
	IF< @minutedone
	ADD local.timeHour, 1
	SUB local.timeMinute, 60
@minutedone
	COMPARE #<local.timeHour>, 24
	IF< @hourdone
	ADD local.timeDay, 1
	SUB local.timeHour, 24
@hourdone
1: Hello!
1: This area behind me is restricted to be only used on a hourly basis.
1: This area opens at 6:00pm (18:00 hours) only!
1: The time now is #<num.timeHour>:#<num.timeMinute>
	1: By my calculations, it is currently #<local.timeHour>:#<local.timeMinute> on #<local.timeMonth>/#<local.timeDay>/#<local.timeYear>.
	1: It is actually #<num.timeHour>:#<num.timeMinute> on #<num.timeMonth>/#<num.timeDay>/#<num.timeYear>.
	1: This concludes my test.
	1: I'll take you to the HOURLY SPECIAL MAP!
COMPARE #<num.timeHour>, 18
IF= @youarereadytoenterspec

1: Unfortunately its not the right time to enter.
1: Come back at 6pm.

END

@youarereadytoenterspec
1: Seems like it's 12pm!
1: In you go!
1: By my calculations, it is currently #<local.timeHour>:#<local.timeMinute> on #<local.timeMonth>/#<local.timeDay>/#<local.timeYear>.
1: It is actually #<num.timeHour>:#<num.timeMinute> on #<num.timeMonth>/#<num.timeDay>/#<num.timeYear>.
1: This concludes my test.
1: I'll take you to the HOURLY SPECIAL MAP!
GOTO MAP 63
END

