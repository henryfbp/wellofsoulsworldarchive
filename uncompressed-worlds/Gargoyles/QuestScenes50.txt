SCENE 52 black, CUT, "Credits"
	ACTOR 1, "", cred1, 1, 50, 70
	MUSIC ...
        SOUND sky.wav
	WAIT 5.0
	GOTO SCENE 53

;--------
;Cred, Part two
SCENE 53 night,CUT, "Credits"
	ACTOR 1, "", cred1, 2, 50, 70
	WAIT 7
	GOTO SCENE 54
;--------
SCENE 54 night, CUT, "Credits"
	ACTOR 1, "", cred1, 3, 47, 78
	WAIT 7

GOTO SCENE 55
;--------
	SCENE 55 night, CUT, "Credits"
	ACTOR 1, "", cred1, 4, 50, 70
	ACTOR 2, "", cred5, 1, 50,200
        SOUND sky.wav
	ACTOR 3, "", cred5, 2, 50, 200
	WAIT 7
	MOVE 1, 50, 200, 1
	MOVE 2, 50, 70, 1
	WAIT 10.0
	MOVE 2, 50, 200, 1
	MOVE 3, 50, 70, 1
	WAIT 7
	MOVE 3, 50, 200, 1
	GOTO SCENE 56
END
;----------
SCENE 56 end, cut, "Credits"
	ACTOR 1, "", cred6, 1, 50, -15
	ACTOR 2, "", cred6, 2, 30, 80
	ACTOR 3, "", cred6, 3, 50, -25
        ACTOR 4, "The Devil", blackandwhite, 4, 80, 80
	MOVE 1, 50, 30
	WAIT 2.0
	MOVE 3, 90, 40
        WAIT 5
        4: SO you have done all the quests and have nothing left to do?
        4: Do you want a challege? (type yes or no)
        ASK 999
        IF Qyes @challenge
        4: OH well maybe later?
        END
@challenge
        4: IF you have seen this before just type "skip"
        ASK 3
        IF Qskip @skip
	  4:I will now send you to your new destination so that you can do the challenge!
        4:Sorry about not letting you do it before! I thought that you kept saying no!! hehe
        4: This challenge will involve up to 5 ultimate gargoyles!
        4: You can either try this by yourself or get some mates to help!
        4: You won't get anything but at least you can say you've done it or worked as a team!
        4: If you want to do it with your mates just go online and come back later!
        4: If it's just you you'll only have one ultimate gargoyle on at a time!
        4: On your own you'll fight 5 in a row but as a team you will start will one gargoyle on the screen then it'll go up to 5!
        4: I recommend that you have some tri ambers!!
        4: WARNING ONCE IN THE FIGHT YOU CANNOT ESCAPE! well after 20 secs you could if you didnt attack!
        4: you would probably be dead by then though!
@skip
	  4: So which will it be, on you own or with mates? (type "own" or "mates")
        ASK 9999
        IF Qown @own
        IF Qmates @mates
	4: I'm sorry, I couldn't hear you. Come again?
	GOTO @skip
        END
@own
        GOTO SCENE 57
END 
@mates 
        GOTO SCENE 58
END
;----------
        SCENE 57 beach, beach, "Boat house"
        ACTOR 1, "Mysterious guy", characters, 4, 80, 80
        GIVE L1
        GIVE H100000
        GIVE M50000
        1:You are great!! You have killed %k207 ultimate gargoyles!
        1: You have 10 seconds to prepare!
        WAIT 10
        1: HERE WE GO!!!
        FIGHT2 207
        IF DEAD @dead
        FIGHT2 207
        IF DEAD @dead
        FIGHT2 207
        IF DEAD @dead
        FIGHT2 207
        IF DEAD @dead
        FIGHT2 207
        IF DEAD @dead
        IF ALIVE @well done
        END
@well done
        1: You are the best!!! I bet that took you ages!!!!
        1: well done!!
        1: Maybe you could try it again sometime?
        1: I lied about getting no reward! here are is Cyanide!
        GIVE I40
        GIVE I40
        1: Sorry you will only get a couple! I'm doing this so you cannot just kill all the ultimate gargoyes easily!
        1: If I gave you 99 or even 5 of them you could easily do it, then you could easily lvl your characters!
        1: Since this isn't an easy lvling world I will only give you 2 of them!
        1: AT least this can be proof that you have done it!
        1: I Have a feeling that you are wanting more Cyanide? Well you will have to do the challenge again!
        1: Do you want to do the challenge again for more Cyanide? (yes or no)
ASK 999999
IF QYES @ok1
IF QNO @cont
END
@ok1
        1: Feeling confident EH? I will heal you now in a minute because If I heal you now that ultimate gargoyle will attack you!!
        1: You will get healed after you have made your choice
        1: I'm sure you know how this challenge is completed since the devil told you b4
        1: Anyway enough of my talking :)
        1: Will you be fighting with on your own or with your mates? (type own or mates)
ASK 99999
IF Qown @own
IF Qmates @mates
END
@cont
        1: If you don't want to do it again I think you'd better leave
        1: Sorry but I can't wait here all day!!
        WAIT 3
        GOTO EXIT
END
@own
GOTO SCENE 57
END
@mates
GOTO SCENE 58
END

;----------
        SCENE 58 beach, beach, "Boat house"
        ACTOR 1, "Mysterious guy", characters, 4, 80, 80
        GIVE L100000
        GIVE M50000
        1:You are great!! You have killed %k207 ultimate gargoyles!
        1: I hope you are not on your own!! LOL YOU ARE MAD IF YOU ARE!
        1: If you are with mates you'll need at least 5 people and they will have to heal themselves!
        1: If one person messes up you all may die!
        1: You have 10 seconds to prepare!
        WAIT 10
        1: HERE WE GO!!!
        FIGHT2 207
        IF DEAD @dead
        FIGHT2 207,207
        IF DEAD @dead
        FIGHT2 207,207,207
        IF DEAD @dead
        FIGHT2 207,207,207,207
        IF DEAD @dead
        FIGHT2 207,207,207,207
        IF DEAD @dead
        IF ALIVE @woo hoo
        END
@dead
        1: Thats a shame :(
        1: You were doing the challenge so well!!!
        1: Would you like to try again? (type yes or no)
        ASK 999999
        IF QYES @ok
        IF QNO @cont
END
@woo hoo
        1: YOU ARE THE BEST! 
        1: That shows what can be done working together. 
        1: Maybe you could try again sometime?
        1: I lied about getting no reward! you now have Access to Cyanide and the forbidden fights!
        GIVE I9
        GIVE I10
        GIVE T675
	1:If you need the forbidden fight items for your younger characters just email Dazman or ask him if he is online
        1: Sorry you will only get 2! I'm doing this so you cannot just kill all the ultimate gargoyes easily!
        1: If I gave you 99 or even 5 of them you could easily do it, then you could easily lvl your characters!
        1: Since this isn't an easy lvling world I will only give you 2 of them!
        GIVE I40
        GIVE I40
        1: AT least this can be proof that you have done it!
        1: I Have a feeling that you are wanting more Cyanide? Well you will have to do the challenge again!
        1: Do you want to do the challenge again for more Cyanide? (Type yes or no)
ASK 999999
IF QYES @ok1
IF QNO @cont
END

@ok1
        1: Feeling confident EH? I will heal you now in a minute because If I heal you now that ultimate gargoyle will attack you!!
        1: You will get healed after you have made your choice
        1: I'm sure you know how this challenge is completed since the devil told you b4
        1: Anyway enough of my talking :)
        1: Will you be fighting with on your own or with your mates? (type own or mates)
ASK 99999
IF Qown @own
IF Qmates @mates
END
@cont
        1: If you don't want to do it again I think you'd better leave
        1: Sorry but I can't wait here all day!!
        WAIT 3
        GOTO EXIT
END
@own
GOTO SCENE 57
END
@mates
GOTO SCENE 58
END

;------------------------------------------------------------------------------------------------------------------------
        SCENE 59 black, CUT, "Battles"
	  ACTOR	1, "Trainer", 1, 10,	
        IF ALIVE @alive
        TAKE T719 
        IF DEAD  @CONTINUE
END
@CONTINUE
        1:Welcome to the battle arena! It is now possible to put REAL bets onto these monsters!
	1:All you need to do is tell me how much GP you will bet on a monster.
	1:You will then have to choose which side you think will win.
	1:If you are a winner you will recieve 20%% more money that you gave! So the higher the bet the more money you will gain or lose
	1:Go on have a try!
	@money
	1:First tell me how much money you are willing to bet? (enter between 1-100000gp)
	ASK 9999999
	set money, #<lastask>
	1:So you are wanting to bet #<money> GP on a monster??? (Type yes of no)
	ASK 99999999
	IF YES @validate
	IF NO @money
	END
	@validate
	IF -G#<money>, @sorry
	IF G#<money>, @enough_money
	END
	@sorry
	1:sorry but you don't have enough
	1:Please enter if again!
	WAIT 3
	GOTO @money
	END
	@enough_money
	1:You have got enough so you can enter
	1:First I need to take the money off you!!
	1:Don't worry if you win you will get it back with a 20%% bonus!!!
	TAKE G#<money> 
	@Left_Or_Right
	1:Please tell me which side you think will win? (Left or right)
	ASK 99999999
	IF Qleft @correct
	IF Qright @correct
	1:Sorry you have entered it wrong! Please retype it!
	WAIT 3
	GOTO @Left_Or_Right
	END
	@correct
	SET monster, #<lastask>
	1:Ok I'll start the fight in a minute!
        1:If I keep saying that a monster is scared don't worry! One will soon be found
 	WAIT 3
@fight
        GOTO Scene 599
END

@alive
       1: WAIT!! You have you be dead to enter this scene but I have an idea!!
       1: I can kill you without you losing any rating or nothing! That sounds fair doesn't it? 
       ASK 9999
       IF yes @die
       IF Qok @die
       1:Ok I understand but if you want to do this scene you have to dead so you will have to find another way
END
@die
TAKE L99999
1:OK now you can enter the scene I will send you to it now!
WAIT 3
GOTO scene 59
END
;------
        SCENE 599 black, CUT, "Battles"
	ACTOR	1, "Trainer", 1, 10,	9
        1: Let's start the battle!
        1: Here is the challenger on the right side!
        WAIT 3
@select1
IF r1 @-id1
IF r1 @-id2
IF r1 @-id3
IF r1 @-id4
IF r1 @-id10
IF r1 @-id13
IF r1 @-id21
IF r1 @-id22
IF r1 @-id37
IF r1 @-id40
IF r1 @-id45
IF r1 @-id46
IF r1 @-id54
IF r1 @-id57
IF r1 @-id59
IF r1 @-id61
IF r1 @-id73
IF r1 @-id74
IF r1 @-id75
IF r1 @-id80
IF r1 @-id84
IF r1 @-id88
IF r1 @-id89
IF r1 @-id90
IF r1 @-id92
IF r1 @-id95
IF r1 @-id100
IF r1 @-id101
IF r1 @-id102
IF r1 @-id104
IF r1 @-id105
IF r1 @-id106
IF r1 @-id111
IF r1 @-id115
IF r1 @-id123
IF r1 @-id128
IF r1 @-id142
IF r1 @-id143
IF r1 @-id144
IF r1 @-id149
IF r1 @-id177
IF r1 @-id179
IF r1 @-id189
IF r1 @-id190
IF r1 @-id200
IF r1 @-id203
IF r1 @-id204
IF r1 @-id205
IF r1 @-id206
IF r1 @-id207
IF r1 @-id213
1:The monster is scared! Another one shall be found
GOTO @select1
 
@-id1
FIGHT -1
goto @chal1
@-id2
FIGHT -2
goto @chal1
@-id3
FIGHT -3
goto @chal1
@-id4
FIGHT -4
goto @chal1
@-id10
FIGHT -10
goto @chal1
@-id13
FIGHT -13
goto @chal1
@-id21
FIGHT -21
goto @chal1
@-id22
FIGHT -22
goto @chal1
@-id37
FIGHT -37
goto @chal1
@-id40
FIGHT -40
goto @chal1
@-id45
FIGHT -45
goto @chal1
@-id46
FIGHT -46
goto @chal1
@-id54
FIGHT -54
goto @chal1
@-id57
FIGHT -57
goto @chal1
@-id59
FIGHT -59
goto @chal1
@-id61
FIGHT -61
goto @chal1
@-id73
FIGHT -73
goto @chal1
@-id74
FIGHT -74
goto @chal1
@-id75
FIGHT -75
goto @chal1
@-id80
FIGHT -80
goto @chal1
@-id84
FIGHT -84
goto @chal1
@-id88
FIGHT -88
goto @chal1
@-id89
FIGHT -89
goto @chal1
@-id90
FIGHT -90
goto @chal1
@-id92
FIGHT -92
goto @chal1
@-id95
FIGHT -95
goto @chal1
@-id100
FIGHT -100
goto @chal1
@-id101
FIGHT -101
goto @chal1
@-id102
FIGHT -102
goto @chal1
@-id104
FIGHT -104
goto @chal1
@-id105
FIGHT -105
goto @chal1
@-id106
FIGHT -106
goto @chal1
@-id111
FIGHT -111
goto @chal1
@-id115
FIGHT -115
goto @chal1
@-id123
FIGHT -123
goto @chal1
@-id128
FIGHT -128
goto @chal1
@-id142
FIGHT -142
goto @chal1
@-id143
FIGHT -143
goto @chal1
@-id144
FIGHT -144
goto @chal1
@-id149
FIGHT -149
goto @chal1
@-id177
FIGHT -177
goto @chal1
@-id179
FIGHT -179
goto @chal1
@-id189
FIGHT -189
goto @chal1
@-id190
FIGHT -190
goto @chal1
@-id200
FIGHT -200
goto @chal1
@-id203
FIGHT -203
goto @chal1
@-id204
FIGHT -204
goto @chal1
@-id205
FIGHT -205
goto @chal1
@-id206
FIGHT -206
goto @chal1
@-id207
FIGHT -207
goto @chal1
@-id213
FIGHT -213
goto @chal1

@chal1   
	WAIT .5     
        1: Here is the challenger on the left side!        
@select2
IF r1 @id1
IF r1 @id2
IF r1 @id3
IF r1 @id4
IF r1 @id10
IF r1 @id13
IF r1 @id21
IF r1 @id22
IF r1 @id37
IF r1 @id40
IF r1 @id45
IF r1 @id46
IF r1 @id54
IF r1 @id57
IF r1 @id59
IF r1 @id61
IF r1 @id73
IF r1 @id74
IF r1 @id75
IF r1 @id80
IF r1 @id84
IF r1 @id88
IF r1 @id89
IF r1 @id90
IF r1 @id92
IF r1 @id95
IF r1 @id100
IF r1 @id101
IF r1 @id102
IF r1 @id104
IF r1 @id105
IF r1 @id106
IF r1 @id111
IF r1 @id115
IF r1 @id123
IF r1 @id128
IF r1 @id142
IF r1 @id143
IF r1 @id144
IF r1 @id149
IF r1 @id177
IF r1 @id179
IF r1 @id189
IF r1 @id190
IF r1 @id200
IF r1 @id203
IF r1 @id204
IF r1 @id205
IF r1 @id206
IF r1 @id207
IF r1 @id213
1:The monster is scared! Another one shall be found
GOTO @select2 
@id1
FIGHT 1
goto @chal2
@id2
FIGHT 2
goto @chal2
@id3
FIGHT 3
goto @chal2
@id4
FIGHT 4
goto @chal2
@id10
FIGHT 10
goto @chal2
@id13
FIGHT 13
goto @chal2
@id21
FIGHT 21
goto @chal2
@id22
FIGHT 22
goto @chal2
@id37
FIGHT 37
goto @chal2
@id40
FIGHT 40
goto @chal2
@id45
FIGHT 45
goto @chal2
@id46
FIGHT 46
goto @chal2
@id54
FIGHT 54
goto @chal2
@id57
FIGHT 57
goto @chal2
@id59
FIGHT 59
goto @chal2
@id61
FIGHT 61
goto @chal2
@id73
FIGHT 73
goto @chal2
@id74
FIGHT 74
goto @chal2
@id75
FIGHT 75
goto @chal2
@id80
FIGHT 80
goto @chal2
@id84
FIGHT 84
goto @chal2
@id88
FIGHT 88
goto @chal2
@id89
FIGHT 89
goto @chal2
@id90
FIGHT 90
goto @chal2
@id92
FIGHT 92
goto @chal2
@id95
FIGHT 95
goto @chal2
@id100
FIGHT 100
goto @chal2
@id101
FIGHT 101
goto @chal2
@id102
FIGHT 102
goto @chal2
@id104
FIGHT 104
goto @chal2
@id105
FIGHT 105
goto @chal2
@id106
FIGHT 106
goto @chal2
@id111
FIGHT 111
goto @chal2
@id115
FIGHT 115
goto @chal2
@id123
FIGHT 123
goto @chal2
@id128
FIGHT 128
goto @chal2
@id142
FIGHT 142
goto @chal2
@id143
FIGHT 143
goto @chal2
@id144
FIGHT 144
goto @chal2
@id149
FIGHT 149
goto @chal2
@id177
FIGHT 177
goto @chal2
@id179
FIGHT 179
goto @chal2
@id189
FIGHT 189
goto @chal2
@id190
FIGHT 190
goto @chal2
@id200
FIGHT 200
goto @chal2
@id203
FIGHT 203
goto @chal2
@id204
FIGHT 204
goto @chal2
@id205
FIGHT 205
goto @chal2
@id206
FIGHT 206
goto @chal2
@id207
FIGHT 207
goto @chal2
@id213
FIGHT 213
goto @chal2

@chal2
;	  IF Won @rightside	
	  IF Won @loser
	  SET winner, left 
@check
	1: The monster on the #<winner> side has won!
	COMPARE winner,monster
	IF= @winner
	IF<> @loser
	1:For some reason have neither lost or won, weird!!
	END
	@winner
	1:The money you have on this bet is #<money> GP
	1:CONGRATULATIONS!!!! YOU HAVE WON!!!
	Set bonus, #<money>
	mul bonus, 20
	DIV bonus, 100
	ADD money, #<bonus>
	1:You have gained a 20% increase in your money :) Making you #<bonus> GP richer!!
	1:So that makes your win a total of #<money> GP
	1:Here it is!
	GIVE G#<money>
	1:Enjoy your winnings!!	
	WAIT 2
	GOTO @start?
	END
	@loser
	1:I'm sorry but you have lost
	1:Feel free to come again if you want to try to get some more money
	WAIT 3
	GOTO @start?
	END
	@start?
	1:Would you like to place another bet?
	ASK 999999
	IF YES @fight
	IF NO @end
	1:Please type that again I didn't get a word of it!
	WAIT 2
	goto @START?
	END
@fight
        GOTO SCENE 59
END

@rightside
	  SET winner, right 
	  GOTO @check
END
;----------------------
SCENE 5007, arcade, CUT
compare #<skiptoken>, 1
IF=, @actors
SET gamelost, 0
SET game, 0
SET skiptoken, 1
@actors
ACTOR 1 "Dark Shooter" characters2 5,25,75
ACTOR 2 "Monster 1"
ACTOR 3 "Monster 2"
ACTOR 4 "Monster 3"
ACTOR 5 "Monster 4"
ACTOR 6 "Monster 5"
ACTOR 7 "Monster 6"
ACTOR 8 "Monster 7"
ACTOR 9 "Monster 8"
ACTOR 10 "Monster 9"
ACTOR 11 "Ending Monster"
FX 1
SEL 1
' Welcome, I am Dark Shooter.
' In This scene you'll be able to shoot monsters.
' Just for 15K, So how about it?
ASK 9999
IF YES @start
IF Qok @start
IF Qya @start
' Ok, Then I'll be leaving, goodbye.
MOVE 1,-25,75
END
@no
1:Sorry but you have not got enough money to use this feature!
1:Maybe you could just have a couple of games with the cups to gain a little money!
1:Come back soon!
END
@start
IF -G15000 @no
TAKE G15000
MENU "Explain how to play=@explain", "Play=@Countdown"
' Ok, You will have 5 Seconds to Shoot all the monsters.
' If you kill all the monsters with 1 sec remaining at the end you will be able to enter a bonus game!
' As soon as the timer gets to 0 you will start shooting!
' A new timer will then start giving you 5 seconds to complete the challege
' Also there may be other ways to get the bonus game ;)
' I will give you 10 seconds to prepare!!!!
wait 1
@countdown
Compare #<game>, 10
IF=, @Newmenu
IF>, @Newmenu
goto @regular
@NewMenu
' Since you can play the bonus game I will let you choose which you play
WAIT 5
MENU "Play the regular game=@regular", "Play Snert shooter=@bonus"
@regular
ACTOR 1, "",characters,4,50,50
COUNTDOWN 10
WAIT 10
; The Countdown starts here
COUNTDOWN 5
ACTOR 1 "Dark Shooter" LM 1,-50,50
IF XP @theend
GOTO @shoot1
END
; ok here starts shoot1 dont forget first the @shootN and then the evenactorclick
@shoot1
ACTOR 2 "Demon 1" monsternpc 1,50,50
END
@EventActorClick2
ACTOR 2 "Demon 1" monsternpc 2,50,50
SOUND gun.wav
; Give it about 0.02 secs to wait till it diseapears
WAIT 0.2
ACTOR 2 "Demon 1" monsternpc 2,-50,50
;Then let it go to another shooter and let it go on and on as long as you like.
IF XP @theend
GOTO @shoot2
END
@shoot2
ACTOR 3 "Demon 2" monsternpc 3,74,50
ENDm
@EventActorClick3
IF XP @theend
ACTOR 3 "Demon 2" monsternpc 4,74,50
SOUND gun.wav
WAIT 0.3
ACTOR 3 "Demon 2" monsternpc 4,-74,50
IF XP @theend
GOTO @shoot3
END
@shoot3
IF XP @theend
ACTOR 4 "Demon 3" monsternpc 5,19,85
END
@EventActorClick4
ACTOR 4 "Demon 3" monsternpc 6,19,85
SOUND gun.wav
WAIT 0.03
ACTOR 4 "Demon 3" monsternpc 6,-234,85
IF XP @theend
GOTO @shoot4
END
@shoot4
IF XP @theend
ACTOR 5 "Demon 5" monsternpc 7, 50,50
END
@EventActorClick5
ACTOR 5 "Demon 5" monsternpc 8, 50,50
SOUND gun.wav
WAIT 0.03
ACTOR 5 "Demon 5" monsternpc 8, -50,50
IF XP @theend
GOTO @shoot5
END
@shoot5
IF XP @theend
ACTOR 6 "Demon 6" monsternpc 9, 20,30
END
@EventActorClick6
ACTOR 6 "Demon 6" monsternpc 10,20,30
SOUND gun.wav
WAIT 0.03
ACTOR 6 "Demon 6" monsternpc 10,-90,20
IF XP @theend
GOTO @shoot6
END
@shoot6
IF XP @theend
ACTOR 7 "Demon 7" monsternpc 11, 90,50
END
@EventActorClick7
ACTOR 7 "Demon 7" monsternpc 12,90,50
SOUND gun.wav
WAIT 0.03
ACTOR 7 "Demon 7" monsternpc 12,-90,20
IF XP @theend
GOTO @shoot7
END
@shoot7
IF XP @theend
ACTOR 8 "Demon 8" monsternpc 13, 34,80
END
@EventActorClick8
ACTOR 8 "Demon 8" monsternpc 14,34,80
SOUND gun.wav
WAIT 0.03
ACTOR 8 "Demon 8" monsternpc 14,-90,20
IF XP @theend
GOTO @shoot8
END
@shoot8
IF XP @theend
ACTOR 9 "Demon 9" monsternpc 15, 50,48
IF XP @theend
END
@EventActorClick9
ACTOR 9 "Demon 9" monsternpc 16,50,48
SOUND gun.wav
WAIT 0.03
ACTOR 9 "Demon 9" monsternpc 16,-90,20
IF XP @theend
GOTO @shoot9
END
@shoot9
IF XP @theend
ACTOR 10 "Demon 10" monsternpc 17, 20,98
END
@EventActorClick10
ACTOR 10 "Demon 10" monsternpc 18,20,98
SOUND gun.wav
WAIT 0.03
ACTOR 10 "Demon 10" monsternpc 18,-90,20
Compare #<num.countDown>, 1
IF=, @bonus
GOTO @complete
END
@theend
ADD gamelost,1
ACTOR 1 "Dark Shooter" characters2 5,25,75
1:You are to slow sorry!!
@what
1:Would you like another game?
IF Qyes @start
IF Qok  @start
IF Qya  @start
goto @what
END
@complete
ACTOR 1 "Dark Shooter" characters2 5,25,75
1:woohoo!!! You did it!!!
1:I will give you 20k as a prize!
WAIT 1
GIVE G20000
ADD game, 1
1:You have won #<game> games so far!!!
1:But you have also lost #<gamelost> games!!!
Compare #<game>, 10
IF=, @bonus
IF>, @bonus
1:Would you like another game or just shoot until you are bored?? (Type ANOTHER or SHOOT)
ASK 9999999
IF Qanother @start
IF Qyes @start
IF Qshoot @shoot10
1:I hope you will want to visit this attraction again :)
END
@bonus
1:You deserve a bonus game for your performance! You have either killed them quickly or have 10 wins or over!!
WAIT 4
GOTO scene 50077
END
@shoot10
1:OK, I'll dissapear then!
WAIT 2
ACTOR 1 "" characters 4,25,75
@start2
ACTOR 11 "Demon 1" monsternpc 1,50,50
END
@EventActorClick11
ACTOR 11 "Demon 1" monsternpc 2,50,50
SOUND gun.wav
WAIT 0.2
ACTOR 11 "Demon 1" monsternpc 2,-50,50
GOTO @shoot11
END
@shoot11
ACTOR 12 "Demon 2" monsternpc 3,74,50
END
@EventActorClick12
ACTOR 12 "Demon 2" monsternpc 4,74,50
SOUND gun.wav
WAIT 0.3
ACTOR 12 "Demon 2" monsternpc 4,-74,50
GOTO @shoot12
END
@shoot12
ACTOR 13 "Demon 3" monsternpc 5,19,85
END
@EventActorClick13
ACTOR 13 "Demon 3" monsternpc 6,19,85
SOUND gun.wav
WAIT 0.03
ACTOR 13 "Demon 3" monsternpc 6,-234,85
GOTO @shoot13
END
@shoot13
ACTOR 14 "Demon 5" monsternpc 7, 50,50
END
@EventActorClick14
ACTOR 14 "Demon 5" monsternpc 8, 50,50
SOUND gun.wav
WAIT 0.03
ACTOR 14 "Demon 5" monsternpc 8, -50,50
GOTO @shoot14
END
@shoot14
ACTOR 15 "Demon 6" monsternpc 9, 20,30
END
@EventActorClick15
ACTOR 15 "Demon 6" monsternpc 10,20,30
SOUND gun.wav
WAIT 0.03
ACTOR 15 "Demon 6" monsternpc 10,-90,20
GOTO @start2
END

;----------------------
SCENE 50077, arcade, CUT, "Snert Shooter"
ACTOR 1 "Dark Shooter" characters2 5,25,75
SET snertkill, 0
SET phrases, 0
SET phrases2, 0
SET phrases3, 0
SET phrases4, 0
SET phrases5, 0
1:This game is the snert shooter! It will let you kill all the snerts that you want!
1:I will give you 2 minutes in which to kill as many snerts you can!
1:I will give you 200gp for every snert you kill!
1:So if you kill a snert every second you can gain a total of 24k :)
1:At the end I will tell you how many snerts you have killed and tell you the money you have recieved!
1:You have got 10 seconds to prepare
WAIT 1
COUNTDOWN 10
WAIT 10
GOTO @snertkiller
END
@snertkiller
COUNTDOWN 120
@snert1
IF XP @workout
ACTOR 1 "Snert" snert 1, 50,50
Compare #<phrases>, 4
IF=, @phrase5(1)
Compare #<phrases>, 3
IF=, @phrase4(1)
Compare #<phrases>, 2
IF=, @phrase3(1)
Compare #<phrases>, 1
IF=, @phrase2(1)
SET phrases, 1
@phrase1(1)
1:Will you lvl me??
END
@phrase2(1)
1:All of my friends will destroy you!!!
SET phrases, 2
END
@phrase3(1)
1:Teach me how to hack!!
SET phrases, 3
END
@phrase4(1)
1:WHY HAVE I BEEN MUTED???? ALL I SAID WAS bleep!!!!!
SET phrases,4
END
@phrase5(1)
1:AM I STILL MUTED OR ARE PEOPLE IGNORING ME???????
SET phrases,0
END
@EventActorClick1
IF XP @workout
ACTOR 1 "Snert" snert 2, 50,50
SOUND gun.wav
add snertkill, 1
wait 0.03
ACTOR 1 "Snert" snert 2, -140,20
GOTO @snert2
END
@snert2
IF XP @workout
ACTOR 2 "Snert" snert 1, 10,90
Compare #<phrases2>, 4
IF=, @phrase5(2)
Compare #<phrases2>, 3
IF=, @phrase4(2)
Compare #<phrases2>, 2
IF=, @phrase3(2)
Compare #<phrases2>, 1
IF=, @phrase2(2)
@phrase1(2)
2:Gimme pp!!!!
SET phrases2,1
END
@phrase2(2)
2:I've been using an autoclicker and now I'm banned!!!!!!
SET phrases2, 2
END
@phrase3(2)
2:I CAN'T BELIEVE DAN! HE BANNED ME FOR PKING TOO MANY PEOPLE!!!!
SET phrases2, 3
END
@phrase4(2)
2:I HAVE 20 HACKED PETS!!!!
SET phrases2, 4
END
@phrase5(2)
2:WHY CAN'T I TAME SHAPESHIFTERS??? I USED TO OWN LOADS OF THEM!!!!
SET phrases2, 0
END
@EventActorClick2
ACTOR 2 "Snert" snert 2, 10,90
SOUND gun.wav
add snertkill, 1
wait 0.03
ACTOR 2 "Snert" snert 2, -120,90
GOTO @snert3
END
@snert3
IF XP @workout
ACTOR 3 "Snert" snert 1, 90,30
Compare #<phrases3>, 4
IF=, @phrase5(3)
Compare #<phrases3>, 3
IF=, @phrase4(3)
Compare #<phrases3>, 2
IF=, @phrase3(3)
Compare #<phrases3>, 1
IF=, @phrase2(3)
@phrase1(3)
3:Could I have some forbidden items to complete the game with???
SET phrases3, 1
END
@phrase2(3)
3:GIVE ME 10 LEVELS!!!!!
SET phrases3, 2
END
@phrase3(3)
3:My guild will beat yours anyday! All other guilds suck!!!
SET phrases3, 3
END
@phrase4(3)
3:I am the master of WaReZ! What game do you want???
SET phrases3, 4
END
@phrase5(3)
3:Press ALT and f4 to gain 100 levels!
SET phrases3, 0
END
@EventActorClick3
ACTOR 3 "Snert" snert 2, 90,30
SOUND gun.wav
add snertkill, 1
wait 0.03
ACTOR 3 "Snert" snert 2, -120,90
GOTO @snert4
END

@snert4
IF XP @workout
ACTOR 4 "Snert" snert 1, 10,30
Compare #<phrases4>, 4
IF=, @phrase5(4)
Compare #<phrases4>, 3
IF=, @phrase4(4)
Compare #<phrases4>, 2
IF=, @phrase3(4)
Compare #<phrases4>, 1
IF=, @phrase2(4)
@phrase1(4)
4:Let's go and res kill someone!
SET phrases4, 1
END
@phrase2(4)
4:BLEEP YOU!!!!!
SET phrases4, 2
END
@phrase3(4)
4:MOTHERBLEEPER, BLEEP OFF NOW! YoU aRe BlEePiNg Me OfF!
SET phrases4, 3
END
@phrase4(4)
4:CAN I HIT YOU??? I NEED TO TEST MY STRENGH A COUPLE OF TIMES!
SET phrases4, 4
END
@phrase5(4)
4:CaN yOu SHoW mE hOw To ChAnGe My HoUrS?
SET phrases4, 0
END
@EventActorClick4
ACTOR 4 "Snert" snert 2, 10,30
SOUND gun.wav
add snertkill, 1
wait 0.03
ACTOR 4 "Snert" snert 2, -120,90
GOTO @snert5
END
@snert5
IF XP @workout
ACTOR 5 "Snert" snert 1, 90,90
Compare #<phrases5>, 4
IF=, @phrase5(5)
Compare #<phrases5>, 3
IF=, @phrase4(5)
Compare #<phrases5>, 2
IF=, @phrase3(5)
Compare #<phrases5>, 1
IF=, @phrase2(5)
@phrase1(5)
5:You suck!!
SET phrases5, 1
END
@phrase2(5)
5:How can I make my chat come from someone else????
SET phrases5, 2
END
@phrase3(5)
5:IT SAYS I'M A BEEPING MODDER!!! ALL I DID WAS LOOK AT THE TEXT FILES!!!!
SET phrases5, 3
END
@phrase4(5)
5:Bleepin' 'noBleep' rule is bleepin' bleeped!!!
SET phrases5, 4
END
@phrase5(5)
5:Join MY guild: The Kick@ZZ MuthaBleep Shiznits! We guard against Snerts!
SET phrases5, 5
END
@EventActorClick5
ACTOR 5 "Snert" snert 2, 90,90
SOUND gun.wav
add snertkill, 1
wait 0.03
ACTOR 5 "Snert" snert 2, -120,90
GOTO @snert1
END
@workout
ACTOR 6 "Dark Shooter" characters2 5,25,75
6:You have killed a sum total of #<snertkill> snerts!!!
MUL snertkill, 200
6:So you will recieve #<snertkill>gp!
6:Here are your winnings for killing these awful snerts!!!!!
GIVE G#<snertkill>
6:Please don't become a snert because it's not very good for yor reputation and everybody will start hating you!!
6:Spend your winnings wisely! 
6:If you ever need more money you can earn some more by killing these pesky snerts that I just cannot get rid of!!
6:They are very annoying, especially when their used to be no snerts at all!!
6:Cya later %1!
END
Comment to stop things happening