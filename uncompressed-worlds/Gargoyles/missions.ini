; missions.ini
; Well of Souls
; (c) 2006 Dan Samuel
; http://www.synthetic-reality.com
;
; This is the missions description file for the world Gargoyles.  You should have a file like this in YOUR
; world folder, so as to define available missions in your world.  A 'mission' is a simple quest where the
; Hero needs to collect some combination of trophies, in return for a reward of items, money and/or experience
; points.
;
; Missions are simple 'collection' missions in that they require you to accumulate trophies in your trophy bag
; in order to complete the mission.  These trophies could be obtained by killing monsters, purchased, given,
; etc (depending on the trophy table settings) so the mission doesn't HAVE to be about killing monsters
;
; Missions must have a unique 'Job Number' (I say 'job' to help you remember that to test if someone has completed
; a mission, you can use  "IF  J23, @hasFinishedMission23"
;
; Each mission has a 'section' in this file, whose 'name' is its job number.  For example, section "[34]" defines
; job/mission # 34.
;
; Within a mission section, you can define the following (all are optional, but some are obviously important):
;
;[#]			Every mission needs a unique 'Job Number'
;Name= 			Every mission should have a short name
;Qualify= 		this is a conditional string, as used by the script IF command.  Only qualifying characters can do mission
;Desc=  		This is the longish description of the mission.  Not TOO long, I hope
;Trophies= 		This is a list of required trophies, like "3x12,4x17" (Needs 3 trophy12s and 4 trophy17s)
;RewardGold= 		Gold provided as reward (assumed to be 0 if omitted)
;RewardGive= 		Object given,  uses same syntax as GIVE command to give something (S12 = spell12, I33 = item33, ...)
;RewardTake= 		Object taken,  uses same syntax as TAKE command
;RewardLevel= 		Give them enough experience points to take them from 0 to this level, say "3.2"
;RewardPP= 		Participation Points Given
;RewardWP= 		War Points Given
;RewardToken= 		You can give ONE token (use negative value to REMOVE the token)
;AcceptGive=		Object given (GIVE syntax) when you accept the mission (maybe something you'll need)
;AcceptTake=		Object Taken (TAKE syntax) when you accept the mission
;AcceptMsg= 		A message sent to user when they accept the mission
;AbandonGive=		Object given (GIVE syntax) when you abandon the mission (maybe a cursed item or something)
;AbandonTake=		Object taken (TAKE syntax) when you abandon the mission (maybe what they gave you, maybe not)
;AbandonMsg= 		A message sent to user when they abandon the mission
;FootNote= 		A footnote added to completed missions
;
; While you can add comments in your section (start line with a semicolon), do NOT leave completely blank lines in your
; section.
;
; Note: the MISSIONS command is how an NPC offers new missions to players.  And the BOOKs menu "Book of Missions" is
; how you review all your currently open missions.

[1]
Name=Green Guy
Qualify=V1
Desc=Everyone's angry about these damned green guys that flip you off after the battle. Get 5 of their fingers to teach 'em a lesson, and you'll get some XP and PP, as well as some cash.
Trophies=5x1
RewardGold=200
RewardGive=
RewardLevel=4
RewardPP=1000
RewardWP=50
RewardToken=0
AcceptMsg=Get those little buggers for us!
AbandonMsg=We understand, not everyone has the courage to stand up against the easiest monster in the game.
FootNote=Well, it appears our torture tactics of cutting off their fingers didn't work. The buggers are just MORE pissed off now...

[2]
Name=Fire Gargoyles
Qualify=V3
Desc=Fire Gargoyles have been burning crops of some local farmers lately. Bring ten of their heads to us, and you will be rewarded handsomely.
Trophies=10x2
RewardGold=1000
RewardGive=
RewardLevel=8
RewardPP=2000
RewardWP=
RewardToken=
AcceptMsg=Stop those flamers!!
AbandonMsg=We understand. It's okay to be a coward.
FootNote=We used those heads to ward off the flamers from our crops. Thank you, %1!

[3]
Name=Blue Man
Qualify=V5
Desc=I've always hated the blue man group. Bring me ten blue man corpses, and you'll get something special.
Trophies=10x3
RewardGold=10000
RewardGive=
RewardLevel=10
RewardPP=10000
RewardWP=
RewardToken=
AcceptMsg=Thanks. Those bastards drive me crazy!!!
AbandonMsg=I hate you, and never want to speak to you again.
FootNote=There are too many of them still. Maybe I'll never be able to forget the Blue Man Group...

[4]
Name=Happy Halloween!
Desc=I need some interesting halloween decorations. Bring me 15 Weirdos' Skulls, I'll be very grateful, and I'll be able to scare the children this year at halloween. :)
Qualify=V8
Trophies=15x4
RewardGold=15000
RewardLevel=15
RewardPP=3000
AcceptMsg=Thanks a lot! Have fun killing those weirdos!
AbandonMsg=I guess my halloween decorations aren't worth killing for, to you. :(
FootNote=Hah! The children are already scared of the weirdo skulls I have lying around! Thank you.

[5]
Name=Ugly Monster
Qualify=V10
Desc=Ogres are terrible ugly creatures, and they've been scaring some customers at Eddie's Funky Disco. Bring some of their heads to me, so we can scare them off.
Trophies=20x13
RewardGold=10000
RewardGive=
RewardLevel=20
RewardPP=5000
RewardWP=
RewardToken=
AcceptMsg=Thanks. Have fun killing those ugly critters!
AbandonMsg=
FootNote=The Ogres seem to be avoiding the disco a bit, but now the ugly heads are scaring off customers. Oh well...

[6]
Name=Saber Tooth
Qualify=V10
Desc=I wish to study the teeth of wild Saber Teeth monsters to learn more about the evolution of natural weapons. Gather 25 specimins for me, and you'll get a prize.
Trophies=25x14
RewardGold=14000
RewardGive=I20
RewardLevel=20
RewardPP=
RewardWP=
RewardToken=
AcceptMsg=Thank you. Have fun finding teeth for me.
AbandonMsg=I guess I'll have to find a warrior that's slightly less pathetic if I'm going to get my research done...
FootNote=With these teeth, I can finally understand the nature of predatory evolution! As thanks, enjoy the money and a strength seed.

[7]
Name=Eye Candy
Qualify=V75
Desc=In the Maze of Eternity, a clutch of Eye Brawls have taken root.  We need their lenses polished, and you're the microfiber to do it.
Trophies=10x1156
RewardGold=
RewardGive=
RewardLevel=76
RewardPP=2000
RewardWP=20
RewardToken=
AcceptMsg=
AbandonMsg=
FootNote=Hope you used a sharp stick.

[8]
Name=Hot Stiff
Qualify=V80+J7
Desc=You're not done with the Maze of Eternity yet.  We need to have a certain fire danger removed.
Trophies=10x1160,5x1163
RewardGold=8000
RewardGive=
RewardLevel=81
RewardPP=
RewardWP=100
RewardToken=
AcceptMsg=
AbandonMsg=
FootNote=Looks like we won't have to flood the place after all.

[9]
Name=Her Name was Rodan
Qualify=V26
Desc=The Wyrm Cavern is overrun with a certain flying pest.  Please clear out the air space in there.
Trophies=25x1016
RewardGold=2000
RewardGive=
RewardLevel=27
RewardPP=5000
RewardWP=
RewardToken=
AcceptMsg=
AbandonMsg=
FootNote=Thanks to you the floor has a lot less squishy stuff as well!

[10]
Name=Watery Grave
Qualify=V72
Desc=In Sea Grotto, we've lost a lot of fishing boats recently to Water Manglers.  Perhaps you're the one with the right bait to take them out.
Trophies=20x1151
RewardGold=20000
RewardGive=
RewardLevel=73
RewardPP=
RewardWP=
RewardToken=
AcceptMsg=
AbandonMsg=
FootNote=Perhaps now our fishermen will stop calling in sick.

[11]
Name=Fire Gargoyle
Qualify=V95+T666
Desc=Kill 100 Fire Gargoyles for a summon spell.
Trophies=100x2
RewardGold=
RewardGive=S270
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1311
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[12]
Name=Earth Gargoyle
Qualify=V95+T666
Desc=Kill 100 Aerial Gargoyles for a summon spell.
Trophies=100x8
RewardGold=
RewardGive=S271
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1312
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[13]
Name=Tidal Gargoyle
Qualify=V95+T666
Desc=Kill 11 Tidal Gargoyles for a summon spell.
Trophies= 100x6
RewardGold=
RewardGive=S272
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1313
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[14]
Name=Aqua Gargoyle
Qualify=V95+T666
Desc=Kill 100 Aqua Gargoyles for a summon spell.
Trophies=100x11
RewardGold=
RewardGive=S273
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1314
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[15]
Name=Aerial Gargoyle
Qualify=V95+T666
Desc=Kill 100 Aerial Gargoyles for a summon spell.
Trophies=100x5
RewardGold=
RewardGive=S274
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1315
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[16]
Name=Legendary Gargoyle
Qualify=V95+T666
Desc=Kill 100 Legendary Gargoyles for a summon spell.
Trophies=100x9
RewardGold=
RewardGive=S275
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1316
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[17]
Name=Golden Gargoyle
Qualify=V95+T666
Desc=Kill 11 Golden Gargoyles for a summon spell.
Trophies=100x12
RewardGold=
RewardGive=S276
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1317
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[18]
Name=Livid Gargoyle
Qualify=V95+T666
Desc=Kill 100 Livid Gargoyles for a summon spell.
Trophies=100x10
RewardGold=
RewardGive=S277
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1318
AcceptMsg=Good luck.
AbandonMsg=
FootNote=Congratulations.

[19]
Name=Ultimate Gargoyle
Qualify=V95+T666+J11+J12+J13+J14+J15+J16+J17+J18
Desc=Kill 100 Ultimate Gargoyles for a summon spell. This is a useful summon spell, but it may be difficult to obtain.
Trophies=100x7
RewardGold=
RewardGive=S278
RewardLevel=
RewardPP=
RewardWP=
RewardToken=1319
AcceptMsg=Good luck. You'll need it.
AbandonMsg=
FootNote=Congratulations! You've now earned all of the summon spells for a Master Gargoyle!

[20]
Name=Wolfboy of Pan
Qualify=V65
Desc=That wolf boy has the pangolins in the Isle of Light all stirred up again.  Grab his cape and see if that helps.
Trophies=1x1128,10x1127
RewardGold=15000
RewardGive=
RewardLevel=66
RewardPP=
RewardWP=25
RewardToken=
AcceptMsg=
AbandonMsg=
FootNote=
