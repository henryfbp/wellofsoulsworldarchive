
;--------------
SCENE 28	plaza, SCENE, "Castrire Castle", 0,0
	ACTOR 1, "Lord Tiler", joshRoyalty, 17, 30, 70
	POSE 17,18,29
	1: What are you doing here?
	1: Gaurds! get rid of this %3!
	GOTO EXIT
END

;--------------
SCENE 29	cave1, SCENE, "Weapons Shop", 0, 0
	ACTOR 1, "Smith", "joshTownsfolk", 13, 20, 80
	POSE 13,14,15
	OFFER2 20,40,12, 20,40,13, 20,40,14, 20,40,15, 20,40,16, 20,40,17, 20,40,18, 20,40,19
	1: How about a new blade?
	1: Or some other weapon?
	1: I sell them all!
END

;--------------
SCENE 30		cave1, SCENE, "Armor Shop", 0, 0
	ACTOR 1, "Squire", "joshTownsfolk", 10, 20, 80
	POSE 10,9,9
	OFFER2 20,40,10, 20,40,11, 20,40,20, 20,40,21
	1: I have pleanty of Armor for sale.
	1: There are also some boots and such.
END

;--------------
SCENE 31	cave1, SCENE, "Jewlery Shop", 0, 0
	ACTOR 1, "Jewler", "joshTownsfolk", 29, 20, 80
	OFFER2 20,40,22  20,40,23
	1: How about a magical tailsman to use on your quest?
END

;--------------
SCENE 32	cave1, SCENE, "Alchemist", 0, 0
	ACTOR 1, "Alchemist", "joshTownsfolk", 21, 20, 80
	POSE 21,22,21
	OFFER 1,2,3,4,5,6,7,8,10
	1: Can I interest you in some food or drink?
	1: I also have a good supply of potions.
END

;--------------
SCENE 33	cave2, SCENE, "Forge", 0, 0
	ACTOR 1, "Black Smith", "joshTownsfolk", 21, 30, 85
	1: Have you heard of the Soul Blade?
	ASK 20
	IF YES @skip
	1: It was before your time.
	1: The Soul Blade is a sword of immense power.
@skip
	1: Many years ago a forged that blade in this very furnace.
	1: When the island was seized by the demon he stole the Soul Blade!
	1: However when his evil minions tried to use the blade it broke apart.
	1: Only the hilt remains.  If you could recover the hilt I could reforge the Soul Blade!
	1: The hilt is located in the Fortress of Darkness.
	1: You will also need some rare ore found in the caves of wealth.
	GIVE t29
	1: Oh one other thing, you will have to wait until Joe Finishes the quest!
END

;--------------

SCENE 34	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END

;--------------

SCENE 35	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END

;--------------

SCENE 36	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END

;--------------

SCENE 37	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END
;--------------

SCENE 38	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END

;--------------

SCENE 39	aztec, SCENE, "Temple", 0, 0
	N: Wow! an unfinished scene!
END


; If you are looking for more comments you have too much spare time.
