
;--------------
SCENE 11	cave1, SCENE, "Entrance to the Cave", 0,0
	ACTOR 1, "Jore Golin", "joshTownsfolk", 21, 20, 80
	POSE 21,22,21

	1: Good day Traveler!
	if T6 @rune
	END
@rune
	if T7+T8 @have
	1: I see you require the Portal Rune.
	1: That could be a problem.
	1: I felt it was too dangerous for me to hold the rune.
	1: So I broke it into four pieces.
	1: You will have to recover all four and unite them.
	1: Two are gaurded by powerful demons.
	1: The first is in the Corner Marsh.
	1: And the second is in Inferno's Gate.
	1: Here is one to start you on your way.
	1: Come back when you have those two.
	GIVE T5
	END
@have
	if T3 @done
	1: You have all but one piece of the Portal Rune.
	1: The last piece is held by Quars.
	1: He can unite the four pieces.
	1: But unlike the demons you fought to obtain these.
	1: He will not engage you in combat.
	1: He is much too smart for that.
	1: His only weakness is Gold.
@done
	END


;---------
SCENE 12	pond, SCENE, "Corner Marsh", 2, 1
	if T6-T7 @go
	END
@go
	ACTOR 1, "Water Demon", josh10, 2, 50, 80
	1: I defend the rune from all!
	1: I call upon the creatures of water to destroy you!
	FIGHT 1000,1000,50,50,52,52
	if WIN @iwin
	1: HA HA HA! No one gets the rune!
	END
@iwin
	1: Your powers in combat are exceptional.
	1: The rune is yours.
	GIVE t7
END

;---------
SCENE 13	bgFireLake2, SCENE, "Inferno's Gate", 0, 0
	if T6-T8 @go
	END
@go
	ACTOR 1, "Fire Demon", josh70, 2, 50, 80
	1: I allow no one access to the rune!
	1: The creatures of fire shall stop you!
	FIGHT 61,61,12,12,12,12
	if WIN @iwin
	1: I shall always keep the rune safe!
	END
@iwin
	1: You have defeated my minoins.
	1: The rune is yours.
	GIVE t8
END

SCENE 14	giza5, SCENE, "Quars House", 0, 0
	if T6+T7+T8-T3 @go
	if t11 @go
	END
@go
	ACTOR 1, "Quars", joshTownsfolk, 1, 50, 80
if t14 @no
if t15 @no
if t13 @no
if t11 @go2
	1: I am Quars!
	1: You seek the Portal Rune?
	1: I have the last piece.
	1: And I can restore the entire rune.
	1: But I want something for my efforts.
	1: Say 1000 gold pieces?
	1: If not you can always try me in combat.
	1: Answer COMBAT or PAY.
	ASK 30
	IF QCOMBAT @fight
	IF QPAY @pay
	1: Don't have an answer for that do you?
	1: Come back later
	END
@fight
	1: You are going to fight me?
	1: HA HA HA HA HA HA HA HA HA!
	FIGHT 43, 43, 43, 43
	IF LOSE @loss
	1: Your skills impress me.
	GOTO @ok
@pay
	if G1000 @buy
	1: Gome back when you HAVE 1000 pieces of gold!
@loss
	END
@buy
	TAKE G1000
@ok
	GIVE T3
	1: There is your Rune.
END
@go2
	1: Hello Again %1.
	if i100 @curse
	1: You seek the Orb of Caelum?
	1: I know it is in Mrys but Where I cannot tell.
	1: It is a sphere of great power.
	1: It must not fall into the wrong hands.
	1: THE RUNE GAURD???? Those are definatly the wrong hands.
	1: When you find it bring it to me and I will curse the orb.
@no
END
@curse
	if t13 @did
	1: I see you have the Orb.
	1: Cursing the Orb will trap the Gaurd in Evergreen.
	1: He will quickly complete all the Quests and be trapped in eternal boredom.
	1: Do you want me to curse the Orb now? answer YES or NO
	ASK 20
	IF QYES @docurse
	1: You are making a mistake!
END
@docurse
	GIVE t13
	1: It is done.
END
@did
	1: You must deliver the Orb to the Rune Gaurd.
	1: He will be trapped in Evergreen and suffer from eternal boredom.
END

; No bug will get me!