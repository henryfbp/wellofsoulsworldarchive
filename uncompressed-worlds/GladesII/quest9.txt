
;--------------
SCENE 24	karnak, SCENE, "Slime Command Post", 0,0
	IF t25 @gone
	ACTOR 1, "Slime", sliem, 1, 50, 90
	POSE 1,4,2
	IF t19 @fight
	1: Get out of my village!
	GOTO EXIT
@fight
	1: So you want to try and stop us?
	1: Ha! you will not be the first to perish!
	1: Your friends in Mrys will fall before us like leaves!
	1: Prepare to die you %3!
	MOVE 1, 30, 50
	FIGHT 1,1,1,1,1,1,1,1
	IF LOSE @loss
	1: Your skills are worthy of praise.
	1: But you will still die against the elite core!
	MOVE 1, 60, 60
	FIGHT 52,52
	IF LOSE @loss
	1: You are a powerful enemy, we surrender our camp.
	N: Mrys is no longer in danger.
	1: I must warn you though we have many more forces gathered.
	1: They in the Sea Caves, and there you shall die.
	GIVE t20
	GIVE t25
@gone
END

; FIGHT evilbug, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11 . . .
