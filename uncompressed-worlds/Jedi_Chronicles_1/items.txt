; ITEMS
;
;
;	E#,       name,				cl     Img	$		lvl     Spl	El	D	A	HP	MP	AP	FP	Description					Sound Effect			Attack Path
;	0	1				2	3	4		5	6	7	8	9	10	11	12	13	14							15				16

	1,	"Medpac",		        0,	0,	10,		0,	0,	0,	0,	0,	20,	0,	0,	100,	"Recharges HP a little bit.", "petGulp.wav"
	2,	"Magpec",			0,	3,	10,		0,	0,	0,	0,	0,	0,	15,	0,	100,	"Recharges MP a little bit.", "petGulp.wav"
	3,	"Poison Antidote",		0,	1,	25,		0,	0,	0,	0,	0,	0,	0,	-2,	90,	"Cures poisoning."
	4,	"Sap Antidote",			0,	1,	25,		0,	0,	0,	0,	0,	0,	0,	-3,     90,	"Cures magic sapping."
	8,	"Force Shard",			105,	4,	900000,		20,	0,	0,	0,	0,	0,	0,	10,	1,	"Enhances ANY your abilities.",magic7.wav,		,				
	12,	"Improved Medpac",		0,	0,	50,		0,	0,	0,	0,	0,	100,     0,	0,	80,	"A man-sized health potion.", "squirt1.wav"
	13,	"Improved Magpac",		0,	3,	50,		0,	0,	0,	0,	0,	0,	75,	0,	80,	"Magic-restoring nectar, stolen from Mount Aegis.", "squirt1.wav"
	16,	"Jedi Medpac",			0,	0,	100,		0,	0,	0,	0,	0,	300,	0,	0,	60,	"A king-sized health potion.", "summon8.wav"
	17,	"Jedi Magpac",			0,	3,	500,		0,	0,	0,	0,	0,	0,	1750,	0,	60,	"For the wizard in a hurry to recharge MP.", "summon8.wav"
	18,	"Advance Magpac",		0,	3,	100,		0,	0,	0,	0,	0,	0,	225,	0,	50,	"Concentrated MP from the previous age."
	19,	"Advance Medpac",		0,	0,	500,		0,	0,	0,	0,	0,	2000,	0,	0,	50,	"Most likely a full health recharge.", "summon8.wav"
	20,	"Life Support Pack",		0,	2,	1000,		0,	0,	0,	0,	0,	2000,2000,	0,	20,	"Like getting hit by a recharge thunderstroke.", "summon8.wav"
	
; >>>HELMETS<<<   (#200    cl 10)
;	E#, name,			cl     Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1			2	3	4	5	6	7	8	9	10	11	12	13	14
	200,"Jedi Initiate Hood",	10,     1,	200,	17,	0,	0,	5,	0,	0,	0,	0,	1,	""
	201,"Old Jedi Hood",	        10,     3,	200,	19,     0,	0,	10,	0,	0,	0,	0,	1,	""
	202,"Bounty Hunter Hood",     	10,     5,	200,	21,     0,	0,	15,	0,	0,	0,	0,	1,	""
	203,"Criminal Helmet",	       	10,     7,	200,	23, 	0,	0,	20,	0,	0,	0,	0,	1,	""
	204,"Jedi Learner Hood",	10,     1,	600,	25,	0,	0,	25,	0,	0,	0,	0,	1,	""
	205,"Republic Initiate Hood",   10,     3,	600,	27,	0,	0,	30,     0,	0,	0,	0,	1,	""
	206,"Republic Guard Helmet",    10,     5,	600,	29,	0,	0,	35,	0,	0,	0,	0,	1,	""
	207,"Theif Helmet",        	10,     7,	600,	31,	0,	0,	40,	0,	0,	0,	0,	1,	""
	208,"Jedi Padawan Hood",	10,     1,	1000,	33,	0,	0,	45,	0,	0,	0,	0,	1,	""
	209,"Bendak's Helmet",          10,     3,	1000,	35,	0,	0,	50,     0,	0,	0,	0,	1,	""
	210,"Republic Mechanic Helmet",	10,     5,	1000,	37,	0,	0,	55,	0,	0,	0,	0,	1,	""
	211,"Bandit Helmet",		10,     7,	1000,	39,	0,	0,	60,	0,	0,	0,	0,	1,	""
	212,"Jedi Disciple Hood",	10,     1,	1400,	41,	0,	0,	65,	0,	0,	0,	0,	1,	""
	213,"Sand People Hood",         10,     3,	1400,	43,	0,	0,	70,     0,	0,	0,	0,	1,	""
	214,"Republic Soldier Helmet",	10,     5,	1400,	45,	0,	0,	75,	0,	0,	0,	0,	1,	""
	215,"Rogue Helmet",		10,     7,	1400,	47,	0,	0,	80,	0,	0,	0,	0,	1,	""
	216,"Jedi Charge Hood",		10,     2,	1800,	49,	0,	0,	85,	0,	0,	0,	0,	1,	""
	217,"Rakatan Warrior Hood",     10,     3,	1800,	51,	0,	0,	90,     0,	0,	0,	0,	1,	""
	218,"Republic Officer Helmet",	10,     5,	1800,	53,	0,	0,	95,	0,	0,	0,	0,	1,	""
	219,"Drifter Helmet",	        10,     7,	1800,	55,	0,	0,	100,	0,	0,	0,	0,	1,	""
	220,"Jedi Reserve Hood",	10,     2,	2150,	57,	0,	0,	105,    0,	0,	0,	0,	1,	""
	221,"Jedi Knight Helmet",       10,     4,	2150,	59,	0,	0,	110,    0,	0,	0,	0,	1,	""
        222,"Republic Mod Helmet",      10,     6,	2150,	61,	0,	0,	115,    0,	0,	0,	0,	1,	""
	223,"Tracker Helmet",		10,     8,	2150,	63,	0,	0,	120,    0,	0,	0,	0,	1,	""
	224,"Jedi Dicipline Hood",	10,     2,	2350,	65,	0,	0,	125,    0,	0,	0,	0,	1,	""
	225,"Wookiee Shadow Hood",      10,     4,	2350,	67,	0,	0,	130,    0,	0,	0,	0,	1,	""
	226,"Wraid Skull Helmet",    	10,     6,	2350,	69,	0,	0,	135,    0,	0,	0,	0,	1,	""
	227,"Assassin Helmet",		10,     8,	2350,	71,	0,	0,	140,    0,	0,	0,	0,	1,	""
	228,"Jedi Cast Hood",	        10,     2,	2550,	73,	0,	0,	145,    0,	0,	0,	0,	1,	""
	229,"Rodian Helmet",            10,     4,	2550,	75,	0,	0,	150,    0,	0,	0,	0,	1,	""
	230,"Davik's Helmet",           10,     6,	2550,	77,	0,	0,	155,    0,	0,	0,	0,	1,	""
	231,"Mandalorian Hood",	        10,     8,	2550,	79,	0,	0,	160,    0,	0,	0,	0,	1,	""
	232,"Jedi Knight Hood",	        10,     2,	2750,	81,	0,	0,	165,    0,	0,	0,	0,	1,	""
	233,"Mandalorian Helmut",       10,     4,	2750,	83,	0,	0,	170,    0,	0,	0,	0,	1,	""
	234,"Republic Fleet Helmet",	10,     6,	2750,	85,	0,	0,	175,    0,	0,	0,	0,	1,	""
	235,"Calo Nord's Helmet",	10,     8,	2750,	87,	0,	0,	180,    0,	0,	0,	0,	1,	""
	236,"Jedi Mentor Hood",	        10,     2,	2950,	89,	0,	0,      185,    0,	0,	0,	0,	1,	""
	237,"Bacca's Grand Hood",       10,     4,	2950,	91,	0,	0,      190,    0,	0,	0,	0,	1,	""
	238,"Republic Chancelor Helm",  10,     6,	2950,	92,	0,	0,	195,    0,	0,	0,	0,	1,	""
	239,"Elite Hunter Helmet",	10,     8,	2950,	93,	0,	0,	200,    0,	0,	0,	0,	1,	""
	240,"Jedi Council Hood",	10,	2,	500000,	94,  	0,	0,	205,    0,    	0,	0,	0,	0,	,						,				,				
	241,"Rakatan Ancient Helm",	10,	4,	500000,	95,  	0,	0,	210,    0,    	0,	0,	0,	0,	,						,				,				
	242,"Republic Admiral Helm",	10,	6,	500000,	97,  	0,	0,	215,    0,    	0,	0,	0,	0,	,						,				,				
	243,"Jedi Master Helm",		10,	8,	500000,	99,  	0,	0,	220,    0,    	0,	0,	0,	0,	,						,				,				
;
;   >>> CLOTHES <<<
;	E#, name,			cl  	Img	 $	lvl            Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1			2    	3	 4	5	        6	7	8	9	10	11	12	13	14

	250,"Jedi Initiate Robe",	11,  	1,	200,	13,		0,	0,	 5,	0,	0,	0,	0,	1,	""
	251,"Old Jedi Cloak",	        11,  	3,	200,	15,	        0,	0,	10,	0,	0,	0,	0,	1,	""
	252,"Republic Armour",     	11,  	5,	200,	17,	        0,	0,	15,	0,	0,	0,	0,	1,	""
	253,"Swoop Gang Suit",	       	11,  	7,	200,	19,	        0,	0,	20,	0,	0,	0,	0,	1,	""
	254,"Jedi Learner Robe",	11,  	1,	600,	21,		0,	0,	25,	0,	0,	0,	0,	1,	""
	255,"Sith Initiate Cloak",      11,  	3,	600,	23,		0,	0,	30,     0,	0,	0,	0,	1,	""
	256,"Sand People Clothes",	11,  	5,	600,	25,		0,	0,	35,	0,	0,	0,	0,	1,	""
	257,"Energy Resistance Suit",   11,  	7,	600,	27,		0,	0,	40,	0,	0,	0,	0,	1,	""
	258,"Jedi Padawan Robe",	11,  	1,	1000,	29,		0,	0,	45,	0,	0,	0,	0,	1,	""
	259,"Rodian Learner Cloak",     11,  	3,	1000,	31,		0,	0,	50,     0,	0,	0,	0,	1,	""
	260,"Black Vulkar Armour",	11,  	5,	1000,	33,		0,	0,	55,	0,	0,	0,	0,	1,	""
	261,"Benjak's Armout",		11,  	7,	1000,	35,		0,	0,	60,	0,	0,	0,	0,	1,	""
	262,"Jedi Disciple Robe",	11,  	1,	1400,	37,		0,	0,	65,	0,	0,	0,	0,	1,	""
	263,"Echani Fibre Armour",   	11,  	3,	1400,	39,		0,	0,	70,     0,	0,	0,	0,	1,	""
	264,"Light Battle Armour",	11,  	5,	1400,	41,		0,	0,	75,	0,	0,	0,	0,	1,	""
	265,"Rogue Suit",		11,  	7,	1400,	43,		0,	0,	80,	0,	0,	0,	0,	1,	""
	266,"Jedi Charge Robe",		11,  	2,	1800,	45,		0,	0,	85,	0,	0,	0,	0,	1,	""
	267,"Heavy Battle Armour",      11,  	3,	1800,	47,		0,	0,	90,     0,	0,	0,	0,	1,	""
	268,"Republic Officer Armour",	11,  	5,	1800,	49,		0,	0,	95,	0,	0,	0,	0,	1,	""
	269,"Selkath Suit",	        11,  	7,	1800,	51,		0,	0,	100,	0,	0,	0,	0,	1,	""
	270,"Jedi Reserve Robe",	11,  	2,	2150,	53,		0,	0,	105,    0,	0,	0,	0,	1,	""
	271,"Tarisian Noble Cloak",     11,  	4,	2150,	55,		0,	0,	110,    0,	0,	0,	0,	1,	""
	272,"Czerka Corp. Clothes",	11,  	6,	2150,	57,		0,	0,	115,    0,	0,	0,	0,	1,	""
	273,"Tracker Suit",		11,  	8,	2150,	59,		0,	0,	120,    0,	0,	0,	0,	1,	""
	274,"Jedi Dicipline Robe",	11,  	2,	2350,	61,		0,	0,	125,    0,	0,	0,	0,	1,	""
	275,"Wookiee Shadow Cloak",     11,  	4,	2350,	63,		0,	0,	130,    0,	0,	0,	0,	1,	""
	276,"Republic Mod Armour",	11,  	6,	2350,	65,		0,	0,	135,    0,	0,	0,	0,	1,	""
	277,"Assassin Suit",		11,  	8,	2350,	67,		0,	0,	140,    0,	0,	0,	0,	1,	""
	278,"Jedi Cast Robe",	        11,  	2,	2550,	69,		0,	0,	145,    0,	0,	0,	0,	1,	""
	279,"Calo Nord's Battle Armour",11,  	4,	2550,	71,		0,	0,	150,    0,	0,	0,	0,	1,	""
	280,"Republic Senator Clothes", 11,  	6,	2550,	73,		0,	0,	155,   	0,	0,	0,	0,	1,	""
	281,"Mercenary Suit",		11,  	8,	2550,	75,		0,	0,	160,    0,	0,	0,	0,	1,	""
	282,"Jedi Knight Robe",	        11,  	2,	2750,	77,		0,	0,	165,   	0,	0,	0,	0,	1,	""
	283,"Mandalorian Fibre Armour", 11,  	4,	2750,	79,		0,	0,	170,   	0,	0,	0,	0,	1,	""
	284,"Invisible Mandalore Suit",	11,  	6,	2750,	81,		0,	0,	175,   	0,	0,	0,	0,	1,	""
	285,"Mandalorian Suit",		11,  	8,	2750,	83,		0,	0,	180,   	0,	0,	0,	0,	1,	""
	286,"Jedi Mentor Robe",	        11,  	2,	2950,	85,		0,	0,	185,   	0,	0,	0,	0,	1,	""
	287,"Wookiee Grand Cloak",      11,  	4,	2950,	87,		0,	0,	190,   	0,	0,	0,	0,	1,	""
	288,"Mandalorian Leader Armour",11,  	6,	2950,	89,		0,	0,	195,   	0,	0,	0,	0,	1,	""
	289,"Elite Hunter Suit",	11,  	8,	2950,	91,		0,	0,	200,   	0,	0,	0,	0,	1,	""
	290,"Echani Fire Robe",		11,	2,	500000,	93,		0,	0,	205,	0,	0,	0,	0,	0,	0,						,				,				
	291,"Star Forge Robe",		11,	4,	500000,	95,		0,	0,	210,	0,	0,	0,	0,	0,	0,						,				,				
	292,"Republic Admiral Armour",	11,	6,	500000,	97,		0,	0,	215,	0,	0,	0,	0,	0,	0,						,				,				
	293,"Jedi Master Robe",	        11,	8,	500000,	99,		0,	0,	220,	0,	0,	0,	0,	0,	0,						,				,				

;    >>> JEDI SABERS <<<
;	E#,   name,				cl     Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1				2	3	4	5	6	7	8	9	10	11	12	13	14
	300,"Youngling Lightsaber",		12,	1,	50,	0,	0,	0,	0,	10,	0,	0,	0,	1,	"","saber1.wav",4
	301,"New Youngling Lightsaber",		12,	1,	100,	5,	0,	0,	0,	20,	0,	0,	0,	1,	"","saber1.wav",4
	302,"Improved Youngling Lightsaber",	12,	1,	150,	10,	0,	0,	0,	30,	0,	0,	0,	1,	"","saber1.wav",4
	303,"Advanced Youngling Lightsaber",	12,	1,	200,	15,	0,	0,	0,	40,	0,	0,	0,	1,	"","saber1.wav",4
	304,"Old Initiate Lightsaber",		12,	1,	250,	20,	0,	0,	0,	50,	0,	0,	0,	1,	"","saber1.wav",4
	305,"Initiate Lightsaber",		12,	1,	300,	25,	0,	0,	0,	60,	0,	0,	0,	1,	"","saber1.wav",4
	306,"Advanced Initiate Lightsaber",	12,	1,	350,	30,	0,	0,	0,	70,	0,	0,	0,	1,	"","saber1.wav",4
	307,"Discipline Lightsaber",		12,	1,	400,	35,	0,	0,	0,	80,	0,	0,	0,	1,	"","saber1.wav",4
	308,"Learner Lightsaber",		12,	1,	450,	40,	0,	0,	0,	90,	0,	0,	0,	1,	"","saber1.wav",4
	309,"Old Padawan Lightsaber",		12,	1,	500,	45,	0,	0,	0,	100,	0,	0,	0,	1,	"","saber1.wav",4
	310,"Padawan Lightsaber",		12,	2,	550,	50,	0,	0,	0,	110,	0,	0,	0,	1,	"","saber2.wav",4
	311,"New Padawan Lightsaber",		12,	2,	600,	55,	0,	0,	0,	120,	0,	0,	0,	1,	"","saber2.wav",4
	312,"Improved Padawan Lightsaber",	12,	2,	650,	60,	0,	0,	0,	130,	0,	0,	0,	1,	"","saber2.wav",4
	313,"Discipline Lightsaber",		12,	2,	700,	65,	0,	0,	0,	140,	0,	0,	0,	1,	"","saber2.wav",4
	314,"Advanced Padawan Lightsaber",	12,	2,	750,	70,	0,	0,	0,	150,	0,	0,	0,	1,	"","saber2.wav",4
	315,"Old Knight Lightsaber",		12,	2,	800,	75,	0,	0,	0,	160,	0,	0,	0,	1,	"","saber2.wav",4
	316,"Knight Lightsaber",		12,	2,	850,	80,	0,	0,	0,	170,	0,	0,	0,	1,	"","saber2.wav",4
	317,"Improved Knight Lightsaber",	12,	2,	900,	85,	0,	0,	0,	180,    0,  	0,	0,	1,	"","saber2.wav",4
	318,"Mentor Lightsaber",		12,	2,	1000,	90,	0,	0,	0,	190,    0,	0,	0,	1,	"","saber2.wav",4
        319,"Advanced Mentor Lightsaber",	12,	2,	1050,	95,	0,	0,	0,	200,    0,	0,	0,	1,	"","saber2.wav",4
	320,"Jedi Master Lightsaber",		12,	2,	500000,	99,	0,	0,	0,	225,    0,	0,	0,	0,	"","saber2.wav",4				

;   >>> SITH SABERS <<<
;	E#, name,				cl    Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1				2	3	4	5	6	7	8	9	10	11	12	13	14
	400,"Old Seduced Lightsaber",		13,	1,	50,	0,	0,	0,	0,	10,	0,	0,	0,	1,	"", "saber1.wav",4
	401,"Seduced Lightsaber",		13,	1,	100,	5,	0,	0,	0,	20,	0,	0,	0,	1,	"", "saber1.wav",4
	402,"Old Initiate Lightsaber",		13,	1,	150,	10,	0,	0,	0,	30,	0,	0,	0,	1,	"", "saber1.wav",4
	403,"Sith Initiate Lightsaber",		13,	1,	200,	15,	0,	0,	0,	40,	0,	0,	0,	1,	"", "saber1.wav",4
	404,"Old Learner Lightsaber",		13,	1,	250,	20,	0,	0,	0,	50,	0,	0,	0,	1,	"", "saber1.wav",4
	405,"Sith Learner Lightsaber",	        13,	1,	300,	25,	0,	0,	0,	60,	0,	0,	0,	1,	"", "saber1.wav",4
	406,"Old Apprentice Lightsaber",	13,	1,	350,	30,	0,	0,	0,	70,	0,	0,	0,	1,	"", "saber1.wav",4
	407,"Sith Apprentice Lightsaber",	13,	1,	400,	35,	0,      0,	0,	80,	0,	0,	0,	1,	"", "saber1.wav",4
	408,"Old Warrior Lightsaber",		13,	1,	450,	40,	0,      0,	0,	90,	0,	0,	0,	1,	"", "saber1.wav",4
	409,"Sith Warrior Lightsaber",	        13,	1,	500,	45,	0,	0,	0,	100,	0,	0,	0,	1,	"", "saber1.wav",4
	410,"Old Knight Lightsaber",		13,	2,	550,	50,	0,	0,	0,	110,	0,	0,	0,	1,	"", "saber2.wav",4
	411,"Sith Knight Lightsaber",		13,	2,	600,	55,	0,      0,	0,	120,	0,	0,	0,	1,	"", "saber2.wav",4
	412,"Old Shadow Lightsaber",		13,	2,	650,	60,	0,      0,	0,	130,	0,	0,	0,	1,	"", "saber2.wav",4
	413,"Sith Shadow Lightsaber",	        13,	2,	700,	65,	0,	0,	0,	140,	0,	0,	0,	1,	"", "saber2.wav",4
	414,"Old Circle Lightsaber",	        13,	2,	750,	70,	0,      0,	0,	150,	0,	0,	0,	1,	"", "saber2.wav",4
	415,"Sith Circle Lightsaber",		13,	2,	800,	75,	0,      0,	0,	160,	0,	0,	0,	1,	"", "saber2.wav",4
	416,"Old Master Lightsaber",		13,	2,	850,	80,	0,  	0,	0, 	170,    0,	0,	0,	1,	"", "saber2.wav",4
	417,"Sith Master Lightsaber",		13,	2,	900,	85,	0,  	0,	0, 	180,    0,	0,	0,	1,	"", "saber2.wav",4
	418,"Old Grand Master Lightsaber",	13,	2,	950,	90,	0,	0,	0,	190,    0,	0,	0,	1,	"", "saber2.wav",4
	419,"Sith Grand Master Lightsaber",	13,	2,	1000,	95,	0,	0,	0,	200,	0,	0,	0,	1,	"", "saber2.wav",4
	420,"Dark Lord Lightsaber",	        13,	2,	500000,	99, 	0,	0,	0,	210,	0,	0,	0,	0,	"", "saber2.wav",4

;   >>> FEDERATION BLASTERS <<<
;	E#, name,			cl     Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1			2	3	4	5	6	7	8	9	10	11	12	13	14	
        500,"ECP Disruptor Rifle",	14,	1,	50,	0,	0,	0,	0,	10,	0,	0,	0,	10,	"", "blaster1.wav", 		30.190.0.0.0
	501,".45 calibre Blaster",	14,	1,	100,	5,	0,	0,	10,	10,	0,	0,	0,	5,	"", "blaster1.wav", 		30.190.0.0.0
	502,"Aeris Laser Carbine",	14,	1,	150,	10,	0,	0,	20,	10,	0,	0,	0,	2,	"", "blaster1.wav", 		30.190.0.0.0
	503,"Republic Blaster Rifle",	14,	1,	200,	15,	0,	0,	30,	10,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	504,"Tunsken Raider Carbine",	14,	1,	250,	20,	0,	0,	30,	20,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	505,"DMI Twi'lek Rifle",	14,	1,	300,	25,	0,	0,	40,	20,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	506,"Duros Twin Carbine",	14,	1,	350,	30,	0,	0,	50,	20,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	507,"Carth's Blaster",		14,	1,	400,	35,	0,	0,	60,	20,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	508,"HK-47 Assasin Blaster",	14,	1,	450,	40,	0,	0,	60,	30,	0,	0,	0,	1,	"", "blaster1.wav", 		30.190.0.0.0
	509,"Bendak's Blaster",	        14,	1,	500,	45,	0,	0,	70,	30,	0,	0,	0,	1,	"", "blaster1.wav", 	 	30.190.0.0.0
	510,"AMD Sniper Rifle",		14,	1,	550,	50,	0,	0,	80,	30,	0,	0,	0,	1,	"", "blaster2.wav",             30.191.0.0.0
	511,"Echani Blaster Carbine",	14,	2,	600,	55,	0,	0,	90,	30,	0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	512,"Sonic Blaster Rifle",	14,	2,	650,	60,	0,	0,	90,	40,	0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	513,"Wookiee Crossbow",		14,	2,	700,	65,	0,	0,	100,	40,	0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	514,"Ion Blaster Carbine",	14,	2,	750,	70,	0,	0,	110,	40,	0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	515,"Ithorian Blaster Rifle",	14,	2,	800,	75,	0,	0,	120,	40,	0,	0,	0,	1,	"", "blaster2.wav", 	        30.191.0.0.0
	516,"MX Assassin Carbine",	14,	2,	850,	80,	0,	0,	120,	50,	0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	517,"Mandalorian Blaster",	14,	2,	900,	85,	0,	0,      130,	50,     0,	0,	0,	1,	"", "blaster2.wav",  	        30.191.0.0.0
	518,"Neutron Beam Rifle",	14,	2,	950,	90,	0,	0,	140,	50,     0,	0,	0,	1,	"", "blaster2.wav",      	30.191.0.0.0
	519,"Ordo's Repeating Rifle",	14,	2,	800,	95,	0,	0,	150,	50,     0,	0,	0,	1,	"", "blaster2.wav", 		30.191.0.0.0
	520,"Calo Nord's Twin Blaster",	14,	2,	500000,	99,	0,	0,	175,	50,     0,	0,	0,	0,	"", "blaster2.wav", 		30.191.0.0.0

;
; >>> BOUNTY HUNTER WEAPONS <<<
;	E#,     name,				cl    Img	$	lvl   Spl	El	D	A	HP	MP	AP	FP	Description
;	0	  1				2	3	4	5	6	7	8	9	10	11	12	13	14
	600,"Stun Blaster Mk1",			15,	1,	50, 	0,	0,	0,	5,	5,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	601,"Stun Blaster Mk2",		        15,	1,	100,	5,	0,	0,	10,	10,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	602,"Stun Blaster Mk3",		        15,	1,	150,	10,	0,	0,	15,	15,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	603,"Stun Blaster Mk4",			15,	1,	200,	15,	0,	0,	20,	20,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	604,"Stun Blaster Mk5",			15,	1,	250,	20,	0,	0,	25,	25,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	605,"Stun Blaster Mk6",			15,	1,	300,	25,	0,	0,	30,	30,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	606,"Stun Blaster Mk7",			15,	2,	350,	30,	0,	0,	35,	35,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	607,"Stun Blaster Mk8",			15,	2,	400,	35,	0,	0,	40,	40,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	608,"Stun Blaster Mk9",			15,	2,	450,	40,	0,	0,	45,	45,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	609,"Stun Blaster Mk10",		15,	2,	500,	45,	0,	0,	50,	50,	0,	0,	0,	1,	"", "blaster1.wav", 	30.191.0.0.0
	610,"Stun Blaster Mk11",		15,	2,	550,	50,	0,	0,	55,	55,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	611,"Stun Blaster Mk12",		15,	2,	600,	55,	0,	0,	60,	60,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	612,"Stun Blaster Mk13",		15,	2,	650,	60,	0,	0,	65,	65,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	613,"Stun Blaster Mk14",		15,	3,	700,	65,	0,	0,	70,	70,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	614,"Stun Blaster Mk15",		15,	3,	750,	70,	0,	0,	75,	75,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	615,"Stun Blaster Mk16",		15,	3,	800,	75,	0,	0,	80,	80,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	616,"Stun Blaster Mk17",		15,	3,	850,	80,	0,	0,	85,	85,	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	617,"Stun Blaster Mk18",		15,	3,	900,	85,	0,	0,  	90,	90,  	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	618,"Stun Blaster Mk19",		15,	3,	950,	90,	0,	0,  	95,	95,  	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	619,"Stun Blaster Mk20",		15,	3,	1000,	95,	0,	0, 	100,	100,  	0,	0,	0,	1,	"", "blaster2.wav", 	30.191.0.0.0
	620,"Stun Blaster Mk21",		15,	3,  12500000,	99, 	0,	0, 	100,	125,  	0,	0,	0,	0,	"", "blaster2.wav", 	30.191.0.0.0

;    >>> BOOTS <<<
;	E#,     name,		cl     Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	  1		2	3	4	5	6	7	8	9	10	11	12	13	14

	700,"Initiate Shoes",	20,	1,	100,	5,	0.12,	0,	10,	10,	0,	0,	0,	1,	""
	701,"Hidden Bek Boots",	20,	3,	100,	10,	0.12,	0,	15,	10,	0,	0,	0,	1,	""
	702,"Rodian Shoes",	20,	5,	100,	15,	0.12,	0,	15,	15,	0,	0,	0,	1,	""
	703,"Criminal Boots",	20,	7,	100,	20,	0.12,	0,	-10,	30,	0,	0,	0,	1,	""
	704,"Padawan Shoes",	20,	1,	200,	25,	0.12,	0,	20,	15,	0,	0,	0,	1,	""
	705,"Learner Boots",	20,	3,	200,	30,	0.12,	0,	20,	20,	0,	0,	0,	1,	""
	706,"Mechanic Shoes",	20,	5,	200,	35,	0.12,	0,	25,	20,	0,	0,	0,	1,	""
	707,"Bandit Boots",	20,	7,	200,	40,	0.12,	0,	25,	25,	0,	0,	0,	1,	""
	708,"Wookiee Shoes",	20,	2,	300,	45,	0.12,	0,	-30,	50,	0,	0,	0,	1,	""
	709,"Warrior Boots",	20,	4,	300,	50,	0.12,	0,	30,	25,	0,	0,	0,	1,	""
	710,"Sand People Shoes",20,	6,	300,	55,	0.12,	0,	30,	30,	0,	0,	0,	1,	""
	711,"Assassin Boots",	20,	8,	300,	60,	0.12,	0,	35,	30,	0,	0,	0,	1,	""
	712,"Czerka Corp Shoes",20,	2,	400,	65,	0.12,	0,	35,	35,	0,	0,	0,	1,	""
	713,"Knight Boots",	20,	4,	400,	70,	0.12,	0,	-40,	60,	0,	0,	0,	1,	""
	714,"Rakatan Sandals",	20,	6,	400,	75,	0.12,	0,	40,	35,	0,	0,	0,	1,	""
	715,"Mercenary Boots",	20,	8,	400,	80,	0.12,	0,	40,	40,	0,	0,	0,	1,	""
	716,"Saul Karath Shoes",20,	2,	500000,	85,  	0.12,   0,	45,	40,	0,	0,	0,	0,	,						,				,				
	717,"Mandalorian Shoes",20,	4,	500000,	90,  	0.12,   0,	45,	45,	0,	0,	0,	0,	,						,				,				
	718,"Republic Boots",	20,	6,	500000,	95,  	0.12,   0,	-50,	70,	0,	0,	0,	0,	,						,				,				
	719,"Master Boots",	20,	7,	500000,	99,  	0.12,   0,	55,	50,	0,	0,	0,	0,	,						,				,				

; >>GUARDS<<<
;	E#,     name,			cl  Img	 $	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	  1			2    3   4	5	6	7	8	9	10	11	12	13	14
	800,"Jedi Initiate Bracer",	21,  	1,	200,	13,	0,	5,	0,	0,	0,	0,	0,	1,	""
	801,"Old Jedi Guard",	        21,  	3,	200,	15,	0,	10,	0,	0,	0,	0,	0,	1,	""
	802,"Mandalorian Melee Shield", 21,  	5,	200,	17,	0,	15,	0,	0,	0,	0,	0,	1,	""
	803,"Energy Deflector",      	21,  	7,	200,	19,	0,	20,	0,	0,	0,	0,	0,	1,	""
	804,"Jedi Learner Bracer",	21,  	1,	600,	21,	0,	25,	0,	0,	0,	0,	0,	1,	""
	805,"Initiate Melee Shield",    21,  	3,	600,	23,	0,	30,	0,     	0,	0,	0,	0,	1,	""
	806,"Republic Guard Wrap",  	21,  	5,	600,	25,	0,	35,	0,	0,	0,	0,	0,	1,	""
	807,"Echani Deflector",        	21,  	7,	600,	27,	0,	40,	0,	0,	0,	0,	0,	1,	""
	808,"Jedi Padawan Bracer",	21,  	1,	1000,	29,	0,	45,	0,	0,	0,	0,	0,	1,	""
	809,"Sith Learner Guard",       21,  	3,	1000,	31,	0,	50,	0,      0,	0,	0,	0,	1,	""
	810,"Rodian Mechanic Wrap",	21,  	5,	1000,	33,	0,	55,	0,	0,	0,	0,	0,	1,	""
	811,"Tunksten Raiders Shield",	21,  	7,	1000,	35,	0,	60,	0,	0,	0,	0,	0,	1,	""
	812,"Jedi Disciple Bracer",	21,  	1,	1400,	37,	0,	65,	0,	0,	0,	0,	0,	1,	""
	813,"Hidden Bek Guard",   	21,  	3,	1400,	39,	0,	70,	0,      0,	0,	0,	0,	1,	""
	814,"Republic Soldier Wrap",	21,  	5,	1400,	41,	0,	75,	0,	0,	0,	0,	0,	1,	""
	815,"Rogue Deflector",		21,  	7,	1400,	43,	0,	80,	0,	0,	0,	0,	0,	1,	""
	816,"Jedi Charge Bracer",	21,  	2,	1800,	45,	0,	85,	0,	0,	0,	0,	0,	1,	""
	817,"Sith Warrior Guard",       21,  	3,	1800,	47,	0,	90,	0,      0,	0,	0,	0,	1,	""
	818,"Republic Officer Wrap",	21,  	5,	1800,	49,	0,	95,	0,	0,	0,	0,	0,	1,	""
	819,"Drifter Deflector",	21,  	7,	1800,	51,	0,	100,	0,	0,	0,	0,	0,	1,	""
	820,"Jedi Reserve Bracer",	21,  	2,	2150,	53,	0,	150,	0,    	0,	0,	0,	0,	1,	""
	821,"Tomb Knight Guard",        21,  	4,	2150,	55,	0,	110,	0,    	0,	0,	0,	0,	1,	""
	822,"Verpine Energy Shield",	21,  	6,	2150,	57,	0,	115,	0,    	0,	0,	0,	0,	1,	""
	823,"Tracker Deflector",	21,  	8,	2150,	59,	0,	120,	0,    	0,	0,	0,	0,	1,	""
	824,"Jedi Dicipline Bracer",	21,  	2,	2350,	61,	0,	125,	0,    	0,	0,	0,	0,	1,	""
	825,"Wookiee Shadow Guard",     21,  	4,	2350,	63,	0,	130,	0,    	0,	0,	0,	0,	1,	""
	826,"Bacca's Energy Shield",   	21,  	6,	2350,	65,	0,	135,	0,    	0,	0,	0,	0,	1,	""
	827,"Assassin Deflector",	21,  	8,	2350,	67,	0,	140,	0,    	0,	0,	0,	0,	1,	""
	828,"Jedi Cast Bracer",	        21,  	2,	2550,	69,	0,	145,	0,    	0,	0,	0,	0,	1,	""
	829,"Rodian Leader Guard",      21,  	4,	2550,	71,	0,	150,	0,    	0,	0,	0,	0,	1,	""
	830,"Benjak's Arm Wrap",        21,  	6,	2550,	73,	0,	155,	0,    	0,	0,	0,	0,	1,	""
	831,"Mercenary Deflector",	21,  	8,	2550,	75,	0,	160,	0,    	0,	0,	0,	0,	1,	""
	832,"Jedi Knight Bracer",       21,  	2,	2750,	77,	0,	165,	0,    	0,	0,	0,	0,	1,	""
	833,"Force Immunity Shield",    21,  	4,	2750,	79,	0,	170,	0,    	0,	0,	0,	0,	1,	""
	834,"Republic Mod Shield",	21,  	6,	2750,	81,	0,	175,	0,    	0,	0,	0,	0,	1,	""
	835,"Bounty Hunter Deflector",	21,  	8,	2750,	83,	0,	180,	0,    	0,	0,	0,	0,	1,	""
	836,"Jedi Mentor Bracer",       21,  	2,	2950,	85,	0,	185,	0,    	0,	0,	0,	0,	1,	""
	837,"Wookiee Grand Guard",      21,  	4,	2950,	87,	0,	190,	0,    	0,	0,	0,	0,	1,	""
	838,"Ancient Rakatan Wrap",     21,  	6,	2950,	89,	0,	195,	0,    	0,	0,	0,	0,	1,	""
	839,"Elite Hunter Deflector",	21, 	8,	2950,	91,	0,	200,	0,    	0,	0,	0,	0,	1,	""
	840,"Jedi Master Bracer",	21,	2,	500000,	93,	0,	205,	0,	0,	0,	0,	0,	0,	,						,				,				
	841,"Echani Fibre Guard",	21,	4,	500000,	95,	0,	210,	0,	0,	0,	0,	0,	0,	,						,				,				
	842,"Republic Admiral Wrap",	21,	6,	500000,	97,	0,	215,	0,	0,	0,	0,	0,	0,	,						,				,				
	843,"Jedi Master Shield",       21,	8,	500000,	99,	0,	220,	0,	0,	0,	0,	0,	0,	,						,				,				
;
; >>>RING<<<
;	E#, name,			cl  	Img	$	lvl           Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1			2    	3	4	5	        6	7	8	9	10	11	12	13	14
	900,"Jedi Initiate Ring",	22,  	1,	200,	11,	        0,	0,	5,	0,	0,	0,	0,	1,	""
	901,"Old Jedi Ring",	        22,  	3,	200,	15,	        0,	0,	5,	5,	0,	0,	0,	1,	""
	902,"Mandalorian Ring",     	22,  	5,	200,	17,	        0,	0,	10,	5,	0,	0,	0,	1,	""
	903,"Hidden Bek Ring",	       	22,  	7,	200,	19,	        0,	0,	10,	10,	0,	0,	0,	1,	""
	904,"Jedi Learner Ring",	22,  	1,	600,	21,		0,	0,	-15,	30,	0,	0,	0,	1,	""
	905,"Echani Power Ring",        22,  	3,	600,	23,		0,	0,	15,     10,	0,	0,	0,	1,	""
	906,"Republic Guard Ring",	22,  	5,	600,	25,		0,	0,	15,	15,	0,	0,	0,	1,	""
	907,"Rodian Ring",        	22,  	7,	600,	27,		0,	0,	20,	15,	0,	0,	0,	1,	""
	908,"Jedi Padawan Ring",	22,  	1,	1000,	29,		0,	0,	20,	20,	0,	0,	0,	1,	""
	909,"Black Vulkar Ring",        22,  	3,	1000,	31,		0,	0,	-25,    40,	0,	0,	0,	1,	""
	910,"Twilek Mechanic Ring",	22,  	5,	1000,	33,		0,	0,	25,	20,	0,	0,	0,	1,	""
	911,"Bandit Ring",		22,  	7,	1000,	35,		0,	0,	25,	25,	0,	0,	0,	1,	""
	912,"Jedi Disciple Ring",	22,  	1,	1400,	37,		0,	0,	30,	25,	0,	0,	0,	1,	""
	913,"Sith Apprentice Ring",   	22,  	3,	1400,	39,		0,	0,	-30,    50,	0,	0,	0,	1,	""
	914,"Republic Soldier Ring",	22,  	5,	1400,	41,		0,	0,	30,	30,	0,	0,	0,	1,	""
	915,"Tunksten Raider Ring",     22,  	7,	1400,	43,		0,	0,	35,	30,	0,	0,	0,	1,	""
	916,"Jedi Charge Ring",		22,  	2,	1800,	45,		0,	0,	35,	35,	0,	0,	0,	1,	""
	917,"Mandalorian Warrior Ring", 22,  	3,	1800,	47,		0,	0,	40,     35,	0,	0,	0,	1,	""
	918,"Fed. Officer Ring",	22,  	5,	1800,	49,		0,	0,	-40,	60,	0,	0,	0,	1,	""
	919,"Drifter Ring",	        22,  	7,	1800,	51,		0,	0,	40,	40,	0,	0,	0,	1,	""
	920,"Jedi Reserve Ring",	22,  	2,	2150,	53,		0,	0,	45,     40,	0,	0,	0,	1,	""
	921,"Old Knight Ring",          22,  	4,	2150,	55,		0,	0,	45,     45,	0,	0,	0,	1,	""
	922,"Republic Coucilman Ring",	22,  	6,	2150,	57,		0,	0,	50,     45,	0,	0,	0,	1,	""
	923,"Tracker Ring",		22,  	8,	2150,	59,		0,	0,	-50,    70,	0,	0,	0,	1,	""
	924,"Jedi Dicipline Ring",	22,  	2,	2350,	61,		0,	0,	50,     50,	0,	0,	0,	1,	""
	925,"Wookiee Shadow Ring",      22,  	4,	2350,	63,		0,	0,	55,     50,	0,	0,	0,	1,	""
	926,"Republic Mod Ring",    	22,  	6,	2350,	65,		0,	0,	55,     55,	0,	0,	0,	1,	""
	927,"Assassin Ring",		22,  	8,	2350,	67,		0,	0,	60,     55,	0,	0,	0,	1,	""
	928,"Jedi Cast Ring",	        22,  	2,	2550,	69,		0,	0,	-60,    70,	0,	0,	0,	1,	""
	929,"Rakatan Prime Ring",       22,  	4,	2550,	71,		0,	0,	60,     60,	0,	0,	0,	1,	""
	930,"Republic Senate Ring",     22,  	6,	2550,	73,		0,	0,	65,     60,	0,	0,	0,	1,	""
	931,"Mercenary Ring",		22,  	8,	2550,	75,		0,	0,	65,     65,	0,	0,	0,	1,	""
	932,"Jedi Knight Ring",	        22,  	2,	2750,	77,		0,	0,	70,     65,	0,	0,	0,	1,	""
	933,"Energy Resistance Ring",   22,  	4,	2750,	79,		0,	0,	-70,    80,	0,	0,	0,	1,	""
	934,"Republic Minister Ring",	22,  	6,	2750,	81,		0,	0,	70,     70,	0,	0,	0,	1,	""
	935,"Bounty Hunter Ring",	22,  	8,	2750,	83,		0,	0,	75,     70,	0,	0,	0,	1,	""
	936,"Jedi Mentor Ring",	        22,  	2,	2950,	85,		0,	0,      75,     75,	0,	0,	0,	1,	""
	937,"Selkath Grand Ring",       22,  	4,	2950,	87,		0,	0,	80,     75,	0,	0,	0,	1,	""
	938,"Republic Chancelor Ring",  22,  	6,	2950,	89,		0,	0,	-80,    90,	0,	0,	0,	1,	""
	939,"Elite Hunter Ring",	22,  	8,	2950,	91,		0,	0,	80,     80,	0,	0,	0,	1,	""
	940,"Bacca's Ring",		22,	2,	500000,	93,		0,	0,	85,	80,	0,	0,	0,	0,	,						,				,				
	941,"Saul Karath's Ring",	22,	4,	500000,	95,		0,	0,	85,	85,	0,	0,	0,	0,	,						,				,				
	942,"Republic Viceroy Ring",	22,	6,	500000,	97,		0,	0,	90,	85,	0,	0,	0,	0,	,						,				,				
	943,"Jedi Master Ring",	        22,	8,	500000,	99,		0,	0,	90,	90,	0,	0,	0,	0,	,						,				,				
;	
;  >>>AMULETS<<<
;       E#,    name,			cl  Img	$	lvl            Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1			2    3	4	5	        6	7	8	9	10	11	12	13	14
	950,"Jedi Initiate Necklace",	23,  1,	200,	13,	        0,	0,	10,	 5,	0,	0,	0,	1,      ""
	951,"Old Jedi Amulet",	        23,  3,	200,	15,	        0,	0,	10,	10,	0,	0,	0,	1,	""
	952,"Promise Land Pendant",     23,  5,	200,	17,	        0,	0,	10,	15,	0,	0,	0,	1,	""
	953,"Criminal Collar",	       	23,  7,	200,	19,	        0,	0,	10,	20,	0,	0,	0,	1,	""
	954,"Jedi Learner Necklace",	23,  1,	600,	21,		0,	0,	30,	25,	0,	0,	0,	1,	""
	955,"Mandalorian Amulet",       23,  3,	600,	23,		0,	0,	30,     30,	0,	0,	0,	1,	""
	956,"Republic Guard Pendant",	23,  5,	600,	25,		0,	0,	30,	35,	0,	0,	0,	1,	""
	957,"Theif Collar",        	23,  7,	600,	27,		0,	0,	30,	40,	0,	0,	0,	1,	""
	958,"Jedi Padawan Necklace",	23,  1,	1000,	29,		0,	0,	50,	45,	0,	0,	0,	1,	""
	959,"Energy Resistant Amulet",  23,  3,	1000,	31,		0,	0,	50,     50,	0,	0,	0,	1,	""
	960,"Republic Mechanic Pendant",23,  5,	1000,	33,		0,	0,	50,	55,	0,	0,	0,	1,	""
	961,"Bandit Collar",		23,  7,	1000,	35,		0,	0,	50,	60,	0,	0,	0,	1,	""
	962,"Jedi Disciple Necklace",	23,  1,	1400,	37,		0,	0,	75,	65,	0,	0,	0,	1,	""
	963,"Force Immunity Amulet",  	23,  3,	1400,	39,		0,	0,	75,     70,	0,	0,	0,	1,	""
	964,"Fed. Soldier Pendant",	23,  5,	1400,	41,		0,	0,	75,	75,	0,	0,	0,	1,	""
	965,"Rogue Collar",		23,  7,	1400,	43,		0,	0,	75,	80,	0,	0,	0,	1,	""
	966,"Jedi Charge Necklace",	23,  2,	1800,	45,		0,	0,	100,	85,	0,	0,	0,	1,	""
	967,"Sith Warrior Amulet",      23,  3,	1800,	47,		0,	0,	100,    90,	0,	0,	0,	1,	""
        968,"Military Officer Pendant",	23,  5,	1800,	49,		0,	0,	100,	95,	0,	0,	0,	1,	""
	969,"Drifter Collar",	        23,  7,	1800,	51,		0,	0,	100,	100,	0,	0,	0,	1,	""
	970,"Jedi Reserve Necklace",	23,  2,	2150,	53,		0,	0,	120,    105,	0,	0,	0,	1,	""
	971,"Echani Ceremonial Amulet", 23,  4,	2150,	55,		0,	0,	120,    110,	0,	0,	0,	1,	""
	972,"Republic Diplomat Pendant",23,  6,	2150,	57,		0,	0,	120,    115,	0,	0,	0,	1,	""
	973,"Tracker Collar",		23,  8,	2150,	59,		0,	0,	120,    120,	0,	0,	0,	1,	""
	974,"Jedi Dicipline Necklace",	23,  2,	2350,	61,		0,	0,	140,    125,	0,	0,	0,	1,	""
	975,"Wookiee Shadow Amulet",    23,  4,	2350,	63,		0,	0,	140,    130,	0,	0,	0,	1,	""
	976,"Echani Holy Pendant",	23,  6,	2350,	65,		0,	0,	140,    135,	0,	0,	0,	1,	""
	977,"Assassin Collar",		23,  8,	2350,	67,		0,	0,	140,    140,	0,	0,	0,	1,	""
	978,"Jedi Cast Necklace",       23,  2,	2550,	69,		0,	0,	160,    145,	0,	0,	0,	1,	""
	979,"Saul Karath Amulet",       23,  4,	2550,	71,		0,	0,	160,    150,	0,	0,	0,	1,	""
	980,"Republic Senator Pendant", 23,  6,	2550,	73,		0,	0,	160,    155,	0,	0,	0,	1,	""
	981,"Mercenary Collar",		23,  8,	2550,	75,		0,	0,	160,    160,	0,	0,	0,	1,	""
	982,"Jedi Knight Necklace",     23,  2,	2750,	77,		0,	0,	180,    165,	0,	0,	0,	1,	""
        983,"Sith Master Amulet",       23,  4,	2750,	79,		0,	0,	180,    170,	0,	0,	0,	1,	""
	984,"Minister Pendant",		23,  6,	2750,	81,		0,	0,	180,    175,	0,	0,	0,	1,	""
	985,"Bounty Hunter Collar",	23,  8,	2750,	83,		0,	0,	180,    180,	0,	0,	0,	1,	""
	986,"Jedi Mentor Necklace",     23,  2,	2950,	85,		0,	0,	200,    185,	0,	0,	0,	1,	""
	987,"Bacca Grand Amulet",       23,  4,	2950,	87,		0,	0,	200,    190,	0,	0,	0,	1,	""
	988,"Czerka Corp. Pendant",     23,  6,	2950,	89,		0,	0,	200,    195,	0,	0,	0,	1,	""
	989,"Elite Hunter Collar",	23,  8,	2950,	91,		0,	0,	200,    200,	0,	0,	0,	0,	,						,				,				
	991,"Bendak's Necklace",        23,  4,	500000,	95,		0,	0,	225,	205,	0,	0,	0,	0,	,						,				,				
	992,"Chancellor Pendant",	23,  6,	500000,	97,		0,	0,	225,	210,	0,	0,	0,	0,	,						,				,				
	993,"Jedi Master Amulet",	23,  8,	500000,	99,		0,	0,	225,	215,	0,	0,	0,	0,	,						,				,				
;
; pets (note: these are handled very specially and can only be offered via the OFFER command
; and not the OFFER2 command).  arg3 is not an image#, it is the MONSTER ID of the monster which is
; to be 'sellable' in a shop.  The monsters statistics will be the pet statistics, so do not set
; the pet's stats in the ITEM table.  For example, here the 'level' entry just means the minimum
; player level before the player can purchase the pet.  the element entry does NOT specify the
; pet's element, which comes from the MONSTER table.  Make the MONSTER entry first.  I hope this
; isn't way too confusing.
;
; A "Pet Shop" is just a regular shop using the OFFER command with the pets enumerated, as in:
;
;    "OFFER 1000,1002"  (offers two pets only)
;
; You can NOT use the OFFER2 command with pets. (have I said that enough times yet?)
; And you can never sell a pet back to a storekeeper.
;
;	E#, name,				cl	Img	$	lvl    Spl	El	D	A	HP	MP	AP	FP	Description
;	0	1				2	3	4	5	6	7	8	9	10	11	12	13	14
	1000,"Slave #1",			200,	1000,	30000,	30,	0,	0,	0,	0,	0,	0,	0,	0,	"Elom"
	1001,"Slave #2",			200,	1001,	10000,	10,	0,	0,	0,	0,	0,	0,	0,	0,	"Jawa"
	1002,"Slave #3",			200,	1002,   50000,	50,	0,	0,	0,	0,	0,	0,	0,	0,	"Ithorian"
	1003,"Slave #4",			200,	1003,   70000,	70,	0,	0,	0,	0,	0,	0,	0,	0,	"Wookiee"
	1004,"Slave #5",			200,	1004,	30000,	30,	0,	0,	0,	0,	0,	0,	0,	0,	"Dianoga"
	1005,"Slave #6",			200,	1005,	10000,	10,	0,	0,	0,	0,	0,	0,	0,	0,	"Nuna"
	1006,"Slave #7",			200,	1006,   50000,	50,	0,	0,	0,	0,	0,	0,	0,	0,	"Opee"
	1007,"Slave #8",			200,	1007,	70000,	70,	0,	0,	0,	0,	0,	0,	0,	0,	"Nexu"
	
;-------------------------- ITEMS 1024-5119 are SINGLETON items.. user can have only one of EACH -----------------

	1024,	"Guild Hall",		201, 4, 0, 		0,	0,	0,	0,	0,	0,0,	0,	0,	"Guild Hall.", 	"http://www.synthetic-reality.com/cgi-bin/myCGI/sr-guilds.cgi/WOS/hall"
	5119,	"Forum",				201, 4, 0, 		0,	0,	0,	0,	0,	0,0,	0,	0,	"Forum.", 		"http://www.synthetic-reality.com/cgi-bin/ultimatebb.cgi"




;	
;	OFFER2 shop styles
;
; The script commands OFFER and OFFER2 let you populate the shop window with the items of your
; choice.  Here are some example commands for various specialty shops
;
;	// basic shop, levels 0-10, all items
;	OFFER2	0,10
;	OFFER2	5,15
;	OFFER2	10,20
;	OFFER2	15,25
;	OFFER2	20,30
;	OFFER2	25,35
;	
;	// specialty shops
;	OFFER2	0,30,10			;helmets
;	OFFER2	0,30,11			;armor
;	OFFER2	0,30,12			;swords
;	OFFER2	0,30,13			;staffs
;	OFFER2	0,30,14			;bows
;	OFFER2	0,30,15			;music
;	OFFER2	0,30,16			;rh5
;	OFFER2	0,30,17			;rh6
;	OFFER2	0,30,18			;rh7
;	OFFER2	0,30,19			;rh8
;	OFFER2	0,30,20			;boots
;	OFFER2	0,30,21			;shields
;	OFFER2	0,30,22			;rings
;	OFFER2	0,30,23			;amulets

;
; Be sure to leave a blank line at the end of this file

