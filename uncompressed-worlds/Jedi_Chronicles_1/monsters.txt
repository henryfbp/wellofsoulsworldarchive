;							skin		scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav		attack path
;	0	1................	2			3		4	5		6	7	8	9	10	11	12	13	14	15	16		17						18			19
;
	0,	"Algorithm Scale %",0,			0,		0,	100,	100,100,100,100,100, 0,	100,100,100,100,100
;
; --------------
;
; OK, now here are the REAL monsters:
;
;					skin		scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav		attack path
;	0	1................	2		3		4	5		6	7	8	9	10	11	12	13	14	15	16		17						18			19
; Easy Monsters 1 - 49
;
	1,	"Kinrath",	        kinrath,	0.34,	       	2,	0,		0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",	-1
	3,	"Kath Hound",	        kath1,		0.34,		2,	0,		0,	0,	0,	0,	0,	3,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",	-1
	5,	"Shyrack",		shyrack,	0.34,		2,	0,		0,	0,	0,	0,	0,	4,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",	-1
	7,	"Horned Kath Hound",	kath2,		0.34,		2,	0,		0,	0,	0,	0,	0,	6,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",	-1
	9,	"Forest Kinrath",	kinrath1,	0.34,		2,	0,		0,	0,	0,	0,	0,	8,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",	-1
	
; And here I cram in some new monsters for A58, thus proving that any attempt to ORGANIZE your monster list is doomed to failure unless
; you are either willing to renumber existing monsters (thus messing up people's pets) or simply work it all out in advance and never change it.
; Had I to do it over again, I might organize by element instead.
;
;					skin		scale	el	hp	mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav		attack path
;	0	1................	2		3	4	5	6	7	8	9	10	11	12	13	14	15	16		17						18			19

	11,	"A-Wing Fighter",	awing,		0.35,	4,	0,	0,	0,	0,	0,	0,	9,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	13,	"B-Wing Fighter",	bwing,		0.35,	4,	0,	0,	0,	0,	0,	0,	19,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	15,	"E-Wing Fighter",	ewing,		0.35,	4,	0,	0,	0,	0,	0,	0,	23,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	17,	"K-Wing Fighter",	kwing,		0.35,	4,	0,	0,	0,	0,	0,	0,	27,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	19,	"X-Wing Fighter",	xwing,		0.35,	4,	0,	0,	0,	0,	0,	0,	31,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	21,	"Y-Wing Fighter",	ywing,		0.35,	4,	0,	0,	0,	0,	0,	0,	35,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	23,	"TIE Fighter",		tie,		0.35,	4,	0,	0,	0,	0,	0,	0,  	39,	0,	0,	0,	0,	0,		machgun.WAV,			explosions.WAV,		30.104,				
	25,	"Leviathan",		leviathan,	0.34,	4,	0,	0,	0,	0,	0,	0,	57,	0,	0,	0,	0,	0,		brains.wav,			scream.wav,		-1,				
;
; Now some high end scary monsters

	27,	"Dark Jedi Knight",	darkjedi01,		0.35,	     4,		0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	29,	"Dark Jedi Knight",	darkjedi02,		0.35,        4,		0,	0,	0,	0,	0,	0,	85,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	31,	"Dark Jedi Knight",	darkjedi03,		0.35,	     4,		0,	0,	0,	0,	0,	0,	93,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	33,	"Dark Jedi Knight",	darkjedi04,		0.35,        4,		0,	0,	0,	0,	0,	0,	98,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	35,	"Dark Jedi Knight",	darkjedi05,		0.35,	     4,		0,	0,	0,	0,	0,	0,	100,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	37,	"Dark Jedi Knight",	darkjedi06,		0.35,	     4,		0,	0,	0,	0,	0,	0,	102,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	39,	"Dark Jedi Knight",	darkjedi07,		0.35,	     4,		0,	0,	0,	0,	0,	0,	105,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	41,	"Dark Jedi Knight",	darkjedi08,		0.35,        4,		0,	0,	0,	0,	0,	0,	109,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	43,	"Dark Jedi Knight",	darkjedi09,		0.35,        4,		0,	0,	0,	0,	0,	0,	112,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	45,	"Dark Jedi Knight",	darkjedi10,		0.35,        4,		0,	0,	0,	0,	0,	0,	116,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	47,	"Dark Jedi Knight",	darkjedi11,		0.35,        4,		0,	0,	0,	0,	0,	0,	120,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				
	49,	"Darth Nippon",		darkjedi12,		0.35,	     4,		0,	0,	0,	0,	0,	0,	125,	0,	0,	0,	0,	-1,		attack.wav,			weak.wav,		-1,				

;					skin		scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav
;	0	1................	2			3		4	5		6	7	8	9	10	11	12	13	14	15	16		17						18
;
; 50 - 99
;
	51,     "Dark Jedi",		darkjedi14,		0.35,		4,	0,		0,	0,	0,	0,	0,	9,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",		-1
	53,	"Selin Mek",		selin,			0.35,		2,	0,		0,	0,	0,	0,	0,	11,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		30.104
	55,	"Clone Trooper",	clone,		       -4.35,		4,	0,		0,	0,	0,	0,	0,	13,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		-1
	57,	"Sith Trooper",		sith,		       -4.35,		4,	0,		0,	0,	0,	0,	0,	15,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		-1
	59,	"Gun Turret",		turret,			0.34,		2,	0,		0,	0,	0,	0,	0,	18,	0,	0,	0,	0,	0,		"machgun.WAV",			"explosions.WAV",	30.104
	61,	"Destroyer",		destroyer,		4.35,		4,	0,		0,	0,	0,	0,	0,	22,	0,	0,	0,	0,	0,		"machgun.WAV",			"explosions.WAV",	30.104
	63,	"AT-AT",		atat,			0.34,		4,	0,		0,	0,	0,	0,	0,	26,	0,	0,	0,	0,	0,		"machgun.WAV",			"explosions.WAV",	30.104
	65,	"AT-TE",		atte,			0.34,		4,	0,		0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	0,		"machgun.WAV",			"explosions.WAV",	30.104
	67,	"Dessicus",		dessicus,		0.35,		2,	0,		0,	0,	0,	0,	0,	34,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		30.104
	69,	"Jedi Initiate",	padawan,		0.35,		0,	0,		0,	0,	0,	0,	0,	38,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",		-1
	71,	"Mandalorian",		mandalorian,		0.35,		2,	0,		0,	0,	0,	0,	0,	59,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		30.104
	79,	"Dianoga",		dianoga,		0.34,		2,	0,		0,	0,	0,	0,	0,	12,	0,	0,	0,	0,	0,		"brains.wav",			"scream.wav",		-1
	81,	"Republic Soldier",	republic,		0.35,		2,	0,		0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	0,		"yes.wav",			"no.wav",		30.104
	83,	"Nigel",		murderer,		0.35,		2,	0,		0,	0,	0,	0,	0,	44,	0,	0,	0,	0,	0,		"attack.wav",			"weak.wav",		-1
	
;					skin		      scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav
;	0	1................	2			3		4	5		6	7	8	9	10	11
;
; 150 - 199
;
	151,"Dark Jedi",	        darkjedi16,		0.35,		4,	0,		0,	0,	0,	0,	0, 	73,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	153,"Dark Jedi",		darkjedi17,		0.35,		4,	0,		0,	0,	0,	0,	0, 	74,  	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	155,"Dark Jedi",		darkjedi18,		0.35,		4,	0,		0,	0,	0,	0,	0, 	76,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	157,"Dark Jedi",		darkjedi19,		0.35,		4,	0,		0,	0,	0,	0,	0, 	79,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	159,"Dark Jedi",		darkjedi20,		0.35,		4,	0,		0,	0,	0,	0,	0, 	81,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	161,"Dark Jedi",	        darkjedi21,		0.35,		4,	0,		0,	0,	0,	0,	0, 	83,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	163,"Dark Jedi",		darkjedi22,		0.35,		4,	0,		0,	0,	0,	0,	0, 	85,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	165,"Dark Jedi",		darkjedi23,		0.35,		4,	0,		0,	0,	0,	0,	0, 	87,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	167,"Dark Jedi",		darkjedi24,		0.35,		4,	0,		0,	0,	0,	0,	0, 	89,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	169,"Padawan",			lightjedi,		0.35,		0,	0,		0,	0,	0,	0,	0, 	47,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	171,"Padawan",		        lightjedi01,		0.35,		0,	0,		0,	0,	0,	0,	0, 	56,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	173,"Jedi Knight",		lightjedi02,		0.35,		0,	0,		0,	0,	0,	0,	0, 	75,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	175,"Jedi Knight",		lightjedi03,		0.35,		0,	0,		0,	0,	0,	0,	0, 	84,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	177,"Jedi Knight",	        lightjedi04,		0.35,		0,	0,		0,	0,	0,	0,	0, 	93,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
        179,"Jedi Master",	        lightjedi05,		0.35,		0,	0,		0,	0,	0,	0,	0, 	102,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	
;							skin		scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav
;	0	1................	2			3		4	5		6	7	8	9	10	11	12	13	14	15	16		17						18
;
; Big Bosses 200 - 255
;
	201,"Dark Jedi Master",		darkjedi25,		0.35,	4,	0,		0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	203,"Dark Jedi Master",		darkjedi26,		0.35,	4,	0,		0,	0,	0,	0,	0,	60,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	205,"Dark Jedi Master",		darkjedi27,		0.35,	4,	0,		0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	207,"Dark Jedi Master",		darkjedi28,		0.35,	4,	0,		0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	209,"Dark Jedi Master",		darkjedi29,		0.35,	4,	0,		0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	211,"Dark Jedi Master",		darkjedi30,		0.35,	4,	0,		0,	0,	0,	0,	0,	100	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	213,"Dark Jedi Master",		darkjedi31,		0.35,	4,	0,		0,	0,	0,	0,	0,	110,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	241,"Dark Jedi Master",	        jedimaster,		0.35,	4,	0,		0,	0,	0,	0,	0,	120,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	243,"Master Adar-Sheni",	jediknight,		0.35,	0,	0,		0,	0,	0,	0,	0, 	130,	0,	0,	0,	0,	-1,		"yes.wav",			"no.wav",	-1
	253,"Ajunta Pall",		darkjedi32,		0.35,	4,	0,		0,	0,	0,	0,	0,	200,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	255,"Ajunta Pall",		darkjedi32,		0.35,	4,	0,		0,	0,	0,	0,	0,	250,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1
	257,"Ajunta Pall",		darkjedi32,		0.35,	4,	0,		0,	0,	0,	0,	0,	300,	0,	0,	0,	0,	-1,		"attack.wav",			"weak.wav",	-1

  
; Slaves 1000-1003
;				      \\skin	scale	el	hp	mp	def	off	exp	gld	lev	str	sta	agi	dex	wis	growl.wav	pain.wav
;	0	1................2			3	4	5	6	7	8	9	10	11	12	13	14	15	16		17		18

	1000,"Elom",		elom,		0.4,	8,	0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1001,"Jawa",		jawa,		0.4,	8,	0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1002,"Ithorian",	ithorian,	0.4,   	8,    	0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1003,"Wookiee",		wookiee,	0.4,	8,	0,	0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1,				
        1004,"Mott",		mott,		0.4,	8,	0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1005,"Nuna",            nuna,		0.4,	8,	0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1006,"Opee",            opee,		0.4,	8,	0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1
	1007,"Nexu",            nexu,		0.4,	8,	0,	0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,	"brains.wav",	"scream.wav",	-1

; Be sure to leave a blank line at the end of this file

