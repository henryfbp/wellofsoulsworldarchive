; slots.ini
; Well of Souls
; (c) 2004 Dan Samuel
;
; This INI file controls the Slobber Slots mini game.
;
; There is some math involved here, so pay attention...  If your world does not have this
; file in its root folder, then slots in your world will use the hard-compiled defaults
; (which may actually be different than the Evergreen defaults, since I may tune the evergreen
; settings over time and not bother to update the code.)
;
; The slot machine has 3 'wheels' and each wheel has 100 symbols on it.
; There are actually only 8 unique symbols, and you draw them in your world's  art/slots.bmp file
; Each symbol has a number which represents it, with # 0 being the top symbol in the art/slots.bml file
;
; the "art/slots.bmp" file is 48 pixels wide, and 384 pixels tall, with each symbol being 48x48 pixels.
;
; +---+
; | 0 |
; +---+
; | 1 |
; +---+
; | 2 |
; +---+
; | 3 |
; +---+
; | 4 |
; +---+
; | 5 |
; +---+
; | 6 |
; +---+
; | 7 |
; +---+
;
; A wheel can be thought of as having 100 slots in it, where each slot contains one of the eight symbols.
;
; You get to define what the statistical probability of the 8 symbols are within a wheel, then the
; game will dynamic build 3 wheels with different symbol ordering, but which match the statistics you
; provide.  Since there are 100 slots on the wheel, you assign a 'percent' value for each symbol.  Your
; eight percent values (one for each symbol) should add up to 100.  If you set the percent of a symbol
; to, say, five, then you know there will be exactly 5 copies of that symbol on each wheel.  You don't 
; know exactly WHERE on the wheel those symbols will be (it's random!)  Just that there will be five.
;
; When the three wheels stop turning, there will be 3 symbols in a row which line up (one from each wheel)
; You get to specify what the payout is for 3 of a kind for any symbol, and for 2 of a kind for any symbol.
;
; 2 of a kind is a bit odd, because it doesn't match ANY two in the line.  It compares the FIRST symbol
; (far left) with the other two.  So XX0 and X0X count as two symbol matches, but 0XX does not.  That's
; just the way the world works, so don't bother arguing about it!
;
; Now, what values make sense?  This is where the math comes in.  Each time you spin the wheels, they end
; up showing only one of their hundred slots each.  Hence there are a total of 100 * 100 * 100 total possible
; combinations.  (that's a million.  that's easy to remember)
;
; Say you have a symbol whose percent is set to '5'  That means there are five of them on each wheel.  How
; many ways are there to get three in a row?  Well, there are 5 * 5 * 5 ways to do that, or 125 total.
;
; So you have 125 chances out of a million to get 3 of those symbols in a row.  That's a 0.0125% chance. 
; (or one chance in 8000... so you would have to  pull the level 8000 times, on average, to get 3 of those
; symbols to line up.)  At 10 GP a pull, that's 80000 GP, on average, that you would spend before winning
; this.  So... the reward should be bigger than 80000 GP, to make it seem worthwhile.  On the other hand,
; a one in 8000 chance is pretty boring.  I mean, better than Vegas or the Lottery, but still pretty boring
; in game land.
;
; If you set the percent to '1' (a single copy of that symbol on each wheel), then you have only one
; chance in a million of winning.  That should pay the highest reward.  Or maybe you don't even want to use such
; a lame value.
;
; Let's do one more to be sure you get the idea.  What if the percent was set to 25?  So 25% of each wheel
; was that symbol?  25 per wheel.  Total ways of getting three of a kind are then 25 * 25 * 25, or 15,625
; and that starts to sound like a lot.  That's 15,625 chances in a million, or 1.5% (one chance in 64), which
; sounds like it might happen once in a while.  You're going to pay 640GP on average before you win it though, so
; it should probably pay more than that.  But maybe not a lot more.
;
; Now, when I say 'on average' there, I am playing a bit fast and loose with the math (and should probably say 320 instead
; of 640 anyway), so please don't let this be your only source of information about statistics.  Get yourself a real
; book on the subject if you're interested.
;
; But let's talk about 'expectation.'  If you have a 10 percent chance of winning a hundred dollars, you have an
; EXPECTATION of winning 10 dollars  (ten percent, 0.1, times the full prize).  If you set up 7 payout conditions
; for your slots (say you declare 4 three-of-a-kinds and 3 two-of-a-kinds), then you compute the expectation of 
; each of those 7 conditions separately, then add them all together to get the total expectation.  This is the
; amount of money the player will win on average.  A real casino then adjusts that expectation so that it ends
; up being slightly less than the amount the player must pay to play.  Thus, over the statistical long run, the
; casino makes a profit.  But there can be local anomalies where an individual player can be a winner (from their
; perspective).  And, of course, in the modern casino they don't have to risk using truly random numbers which 
; might on occasion cost them lots of money... they can pick just the right numbers to make it all work out
; perfectly... not really cheating, since it is still random as to which individual players win, but it is not
; at all random how much profit the casino will make in the long run.
;
; OK, now the actual values.  Note that for each symbol (0 - 7) we have three numbers: "percent", "double" and "triple"
;
; percent is how many of that symbol will appear on each wheel (the 8 percent values MUST add up to 100)
; triple is how many GP you win for getting 3 of that symbol in a row  (same symbol on all 3 wheels)
; double is the GP you win for getting the first and second, or first and third wheels to have that symbol.
;
; Not every payout needs to happen, so feel free to use lots of zeroes for the payout values
;
; In evergreen, I make symbol 0 appear the fewest times per wheel.
[0]
percent=2
triple=1000000
double=50000
[1]
percent=5
triple=100000
double=5000
[2]
percent=10
triple=3000
double=50
[3]
percent=10
triple=3000
double=50
[4]
percent=10
triple=3000
double=50
[5]
percent=10
triple=3000
double=50
[6]
percent=10
triple=3000
double=50
[7]
percent=43
triple=100
double=5