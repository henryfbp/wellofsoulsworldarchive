SCENE 1111, "Dock"
	ACTOR 1, "Dock Protector", josh96, 1, 21, 60
	1: Pssst, here take this bag. I have a quest for you.
    
 COMPARE #<num.TrophyBagWidth>, 3
    IF> @hasBag
	1: Ok Sir, here is your bag.
	1: It's not very big, but it will do for this quest. You may upgrade it later.
	WAIT 1
	SET num.TrophyBagWidth, 4
	SET num.TrophyBagHeight, 3
	SET num.TrophyBagOpen, 1
	1: You can open your bag at any time, using the ITEMS screen.
	1: There is a bag button there.
	1: You will see what i mean.
@hasBag
	COMPARE #<num.Trophy1>, 99
	IF> @has100Gold Pieces
        COMPARE #<num.Trophy1>, 1
	IF> @hasSomeGold Pieces
; Remind him to collect som
	1: Theifs drop gold peices in Alderley Edge, come on, i need 100.
	1: Also come on by any time after this quest if you have 100 peices, i will buy them from you,
	END

@has100Gold Pieces
	1: Fantastic!  You have #<num.Trophy1> %Z1s!
	1: If you don't mind, I will just relieve you of 100 of them!
	SET num.TrophyBagOpen, 1
	WAIT 1
	TAKE Z1.100
	WAIT 1
	1: And here is a small token of my appreciation
	WAIT 1
	GIVE G1000
	1: You can bring me more of these at any time!
	END

@hasSomeGold Pieces
	1: That's great!  You have #<num.Trophy1> %Z1s!
	SET num.TrophyBagOpen, 1
	1: Unfortunately, I can only process them in batches of 100.
	1: Come on back when you have enough!
	END

END


;---------------------------------------------------------------------------------------------------
