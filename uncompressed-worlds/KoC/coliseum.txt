SCENE 45 shop, HEAL, "Tournament", 0, 0
	BKGND "Castle6"
	ACTOR 1, "Arena Master", giant, 1, 25, 90
IF G300000, @welcome
1: I'm sorry, you don't seem to have enough money to pay the entry fee.
1: It is 300 000 gold. Come back when you have that much.
END
@welcome
1: Welcome to the Arena!
1: This Coliseum holds Battles for nice prizes!
1: But only if you beat their former owners in a fight!
1: And of course you have to pay a small entry fee.
1: Theres only 3 items to fight for but more will come.
1: You can fight for one item multiple times.
1: Entry fee will be collected when the fight starts.
1: 10 Multi Orbs.
1: Want to fight for it?
ASK 30
IF Qyes, @ice
1: Okie dokie, what about..
1: Bling Chain?
ASK 30
IF Qyes, @snickh
1: Fine, last item:
1: Gold Boots.
1: Want to fight for it?
ASK 30
IF Qyes, @dragon
1: I have no more items to fight for.
END
@ice
1: Very well. Prepare for battle!
MOVE 1, -50, 100, 1
WAIT 4
HOST_TAKE G300000
THEME 53
FIGHT 1006
IF WON, @ice2
THEME 70
END
@snickh
1: Very well. Prepare for battle!
MOVE 1, -50, 100, 1
WAIT 4
HOST_TAKE G300000
THEME 53
FIGHT 1004
IF WON, @snickh2
THEME 70
END
@dragon
1: Very well. Prepare for battle!
MOVE 1, -50, 100, 1
WAIT 4
HOST_TAKE G300000
THEME 53
FIGHT 1005
IF WON, @dragon2
THEME 70
END
@dragon2
THEME 71
MOVE 1, 25, 90, 1
WAIT 4
1: Well done.
1: Here is your reward.
HOST_GIVE I1278
N: Recieved Golden Boots!
END
@ice2
THEME 71
MOVE 1, 25, 90, 1
WAIT 4
1: Well done.
1: Here is your reward.
HOST_GIVE I8.10
N: Recieved 10 Multi Orbs!
END
@snickh2
THEME 71
MOVE 1, 25, 90, 1
WAIT 4
1: Well done.
1: Here is your reward.
HOST_GIVE I1281
N: Recieved Bling Chain!
END
