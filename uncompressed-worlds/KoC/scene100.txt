SCENE 100 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Angel", "M_K's angel", 17, 25, 90
	SEL		1
	MOVE	1,	15, 80
	OFFER2	31,60,10, 31,60,11, 31,60,21, 31,60,20
	' Finest armor in the kingdom!
END
@eventActorGive1
	IF  C51, @traderskill
	IF  -C51, @endfi
@traderskill
SET itemsell, #<item.id>
SET itemcost, #<item.price>
MUL itemcost, "6"
DIV itemcost, "4"
DIV itemcost, "2"
SET itemcostn,#<item.price>
DIV itemcostn, "2"
COMPARE #<item.price>,#<itemcost>
IF< 	@banfi	
	1: Ah a #<item.name>, normally I would give you #<itemcostn> GP for it, but since you are a %C10 I will give you #<itemcost> GP.
	HOST_GIVE G#<itemcost>
	END
@endfi
	1: You dont look like a Merchant to me.
	END

SCENE 21
	IF T700
	WAIT 1
	ACTOR	1,	"Jasmine",			"AerisGainsborough",    305,40,90

	1: Hi! i'm %0,....can i take your order?
	ASK 30
	IF -yes, @later
	PARTY EXIT
	OFFER 20,21,22,23,25
	' enjoy
end
@exit
	EXIT
end
@later
	
	ACTOR	1,	"Jasmine",			"AerisGainsborough",    305,40,90
	' ok sweetie, i'll come back later. dont get sunburn.
	MOVE 1,-5,90
	WAIT 3
	GOTO @piss
	
end
@piss
	MOVE 1,40,90
	WAIT 1
	' ready to take your order now?
	ASK 30
	IF -yes, @piss2
	PARTY EXIT
	OFFER 20,21,22,23,25
	' enjoy
end
@piss2
	ACTOR	1,	"Jasmine",			"AerisGainsborough",    305,40,90
	' ok %1, you starting to piss me of you %3 
	' Call Me!
	MOVE 1,-5,90
	WAIT 3
	GOTO @rpiss
end
@rpiss
	MOVE 1,40,90
	WAIT 1 	' ready now you %3?
	ASK 30
	IF -yes, @later2
	PARTY EXIT
	OFFER 20,21,22,23,25
	' enjoy
end
@later2
	ACTOR	1,	"Jasmine",			"AerisGainsborough",    305,40,90	
	' don't call me, i'll call you , you %3,
	WAIT 2
	' hope you get sunburn, you little %3
	MOVE 1,-5,90
	WAIT 3
	GOTO @piss
	
end
;----------------------------------------------------
SCENE 22
	THEME
	IF	T400, @Offer
	IF DEAD, @dead
	ACTOR	1,	"Merlin",   "- Atsumori 2003 Amakusa -",	5,	25,	90
	POSE	1,4,2
    IF -T399, @boo
	H: Hi, are you Merlin?.
	H: I'm %1, on a journey to join Arthurs Knights.  	1: Hello, %1,..i'm %0,
	' Yes, I know of your journey, and i know it aint gonna be easy.
	' you have to beat me on my test...Prepare yourself..!
	MOVE 1,-30,80
	WAIT	2
	FIGHT	72
	IF WIN,	@heWon
@dead
	WAIT 2
	ACTOR	1,	"Merlin",		 "- Atsumori 2003 Amakusa -",	302,	-5,90
	MOVE 1,20,80
	1: Ha ha ha!  and they say you're our only hope.!
	' guess even the Brass make mistakes...hahaha....
END
@heWon
	MOVE	1, 30,80
	GIVE	T400
	OFFER2	0,10
	OFFER2	5,15
	OFFER2	10,20
	OFFER2	15,25
	OFFER2	20,30
	OFFER2	25,35

	1: Well done %1!  You win!  you are worthy..
	1: I will be keeping an eye on your progress..
    	1: Take this sheild, surely you will need it in later battles.
        GIVE T400
        GIVE I191
        N: Dont be fooled by the ease in which you won!
	
	END

@Offer
	IF DEAD, @dead
	ACTOR	1,	"Merlin",   "- Atsumori 2003 Amakusa -",	5,	25,	90
	POSE	1,4,2
	' Be careful, i am not a doctor %3, 	' ..Hahahaaa..... 	OFFER2	0,10
	OFFER2	5,15
	OFFER2	10,20
	OFFER2	15,25
	OFFER2	20,30
	OFFER2	25,35

	END
@boo    1:Go away, i am meditating!
END
;---------------------------------------------------
SCENE	23	wetMesa2, SCENE, "Mesa Island"
	THEME	1
	MUSIC	x.mid
	
	ACTOR	1,	"Lammy", "Arthurs Knight", 32, 32, 32
	POSE	32,32,32
	SELECT	1	
	IF DEAD,@dead
        

        GIVE T50
        1: If you are viewing this scene, you have been a naughty hero.
        N: A snert, cheat, or an annoyance.
        1: Well, hopefully, you have learnt your lesson.
        1: If you want this soul back in the world of Camelot, and you dont commit any other rule breakers,
        1: Then, im one of the admins or myself from the soul list.
        1: The Downside to being sent to Hell is your only escape is a token that disables some features in Camelot.
        1: This includes Bank ban
        N: Can be fixed if you are wrongly here or you redeem yourself.
        1: And other features that you will find for yourself.        
        1: Whilst in this map, you can not be heard in the world so dont try talking to admin through chat box.
        1: Well. Please follow the rules in future coz id hate to have to ban you completely.
        N: Your name will be added to a name shame on Knights of Camelot Homepage. 
        TAKE I1277
END

@dead
         	GIVE L1
       	GIVE H5000          
	GIVE M5000
End
;---------------------------------------------------
SCENE 24 shop, HEAL, "Hand Training", 0, 0
	THEME	0
	MUSIC	#silence
	ACTOR	1,	"Karate Master", josh90, 1, 5, 85
	POSE	1,4,2
	MOVE	1,	15, 85
	IF T18, @done
	1: Im here to find out if you know what you are doing
	1: So if you are sure on what you are doing say yes if not say no
	ASK 100
	IF yes, @yes
	1: Well since you don't know what your doing you did a good job of coming here first
	1: Well lets get started
	1: If you look along the bottom right side of the Well of Souls window
	1: You see 7 Buttons
	1: I will Explain them in order from Left to right
	WAIT 1.0
	1: The first one showing a Well that looks on fire is to get to that starting well where you can pick characters and incarnate with them/ make new chars
	1: We don't need that one just yet
	1: The next one over looking like a pair of feet is to exit a scene/camp don't use that one till the end
	1: The next one is for a map, you can see the people in the world with you where they are on the map
	1: The next Button looking like a bunch of cards is telling you stats about your Character
	1: The attack button looking like a sheild and a sword is the one you will use first off
	1: Then we got the Magic button
	1: And then the Item button
	1: Ok click on the Attack button
	1: Once there look for any green Plus signs if you get those that means and Item can be equipted to your character
	1: So Equip  all of those
	WAIT 2.0
	1: Now im going to call out a monster stay on the attack Screen and when it comes out Hit it
	GOTO @yes

@yes
	WAIT 1.0
	FIGHT 1
	WAIT 1.0
	IF WIN, @win1
	IF LOSE, @lose
	END

@win1
	1: You did well 
	GIVE G100
	GIVE H200
	GIVE M200
	1: I will give you gold for every monster that you Defeat
	1: You can Exit scene at any time for you have the expreience to succed or stay here and fight more monsters, but once you leave you will not be able to fight monsters again
	GIVE T18
	FIGHT 2
	IF WIN, @win2
	IF LOSE, @lose
	END

@win2
	1: Good job heres 200 Gold
	GIVE G200
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 3
	IF WIN, @win3
	IF LOSE, @lose
	END

@win3
	1: Good job heres 300 Gold
	GIVE G300
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 4
	IF WIN, @win4
	IF LOSE, @lose
	END

@win4
	1: Good job heres 400 Gold
	GIVE G400
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 6
	IF WIN, @win6
	IF LOSE, @lose
	END

@win6
	1: Good job heres 600 Gold
	GIVE G600
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 7
	IF WIN, @win7
	IF LOSE, @lose
	END

@win7
	1: Good job heres 700 Gold
	GIVE G700
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 8
	IF WIN, @win8
	IF LOSE, @lose
	END

@win8
	1: Good job heres 800 Gold
	GIVE G800
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 9
	IF WIN, @win9
	IF LOSE, @lose
	END

@win9
	1: Good job heres 900 Gold
	GIVE G900
	GIVE H9999
	GIVE M9999
	1: Heres another
	FIGHT 10
	IF WIN, @win10
	IF LOSE, @lose
	END

@win10
	1: Good job heres 1000 goldd
	GIVE G1000
	GIVE H9999
	GIVE M9999
	1: You have passed
	GOTO @done
	END

@done
	1: You have passed Congratulations
	1: Go Seek out King Arthurs stable if for your next quest!
	1: Now I need to think please leave
	
	END

@lose
	1: That sucks I guess your done
	END
;-------------------------------------------
SCENE 25, SCENE, "Sir Pademore"
	THEME 6
	MUSIC "17-Enemy_Attack.midi"
	ACTOR 1, "Sir Pademore", mountedknight, 17, 10, 80
	IF T56, @done
	IF T55, @55
	SET 1
	
	' Hello my name is Sir Pademore
	' I was wondering if you could do me a favor
	' Could you destroy a pest that won't stop tormenting me in my sleep?!
	N: (yes or no)
	ASK 30
	IF YES, @yes
	' Then leave me alone and let me weep in peace!!
	END

@yes
	' Im not Sure where he is located, But there are rumors that he lives somewhere to the east of Camelot
	' In a place called Sacred Lake.
	GIVE I709
	' Come back once you have Defeated the beast and recieve a price.
	GIVE T55
	END

@55
	' What are you doing
	' You have not slayed the beast
	' Please go slay the beats
	END

@done
	' You have destroyed the Beast and freeed us all from his terror
	' I reward you with the Avril's Blade, a fine sword of your stature
	GIVE I320
	END
;-------------------------------------------------
SCENE 26, Stones8, SCENE, "Beast of Torment"
	THEME 4
       	MUSIC "Decisive_Battle.midi"
	ACTOR 2, "Beast of Torment, KumataShiningPhoenix, 9, 32, 32
	IF T57, @defeated
	IF T55, @continue
	IF T56, @defeated1
	2: Leave
	GOTO EXIT
	END

@continue
	2: Have you come to try and finish what many others have tried and failed.
	N: (yes or no)
	ASK 30
	IF yes, @yes
	2: Then leave me be.
	END
	
@yes
	2: Than fight me in my true form!!!!!!!!
	MOVE 2, -50, 50
	WAIT 2.0
	FIGHT 209
	IF WIN, @won
	2: You are just as pathetic as everyone else
	2: I will remeber this for ever
	TAKE T55
	GIVE T57
	END

@defeated
	2: Its you again **laugh**
	2: Seriously don't make me laugh
	2: Its just such a funny sight I beat you once and you come back for more
	2: If you really wanna try again
	GOTO @fight2

@fight2
	MOVE 2, -50, 50
	WAIT 2.0
	FIGHT 209
	IF WIN, @won
	GIVE T57
	END

@won
	2: You have beaten me and shown me true humility, I will stop tormenting you and others forever
	2: Take this Ring, I wont need it any more
	GIVE I192
	TAKE T55
	TAKE T57
	GIVET56
	END

@defeated1
	2: You have one now please leave me
	GOTO EXIT
	END

SCENE 996 black, black
	IF T3500, @HasT3500
	N: Get out or you will be poodled.
END
@HasT3500
	N: Welcome /a, this is your class changer link.
	N: This is because, i am not sure of your actual Soul Id, LOL.
    N: Enjoy the class.
    WAIT 3.0
	N: Hold onto your hat and exit scene once you have changed.
	WAIT 4.0
	SET num.hostClass, 6 ;Admin id Number
	1: There you are, Now leave scene to avoid freezage.
        EXIT