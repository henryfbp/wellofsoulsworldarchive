;-------------------
; character levels
;
; You can define various character 'classes' which control the
; basic limits of a character (what weapon they can hold in their
; right hand, whether they get magic points, the rate at which
; their levels increase, and some other things)
;
; But a character also has a LEVEL which starts at '1' and goes up along
; with experience points.
;
; This section lets you specify the experience points needed to attain each
; level, and lets you associate a name with each level, so your character
; can appear to evolve through various stages (mage, wizard, warlock, sorceror, etc.)
; even though REALLY only the actual level number is important.
;
; The first argument is a number which indicates the class and the level.  
;
;    arg0:		Class+Level (103 = warrior level 3, 203 = wizard level 3)
;    arg1:		[NO LONGER USED] Additional Experience points to attain level from previous level
;	 arg2:		Additional max HP earned when level is attained
;	 arg3:		Additional max MP earned when level is attained
;    arg4:      Level Name (use the same name for several levels in a row)
;
; These arguments are only valid for level 0 in each character class.  They control
; common aspects of the class
;
;	 arg5		(level 0 only)  magic-ratio
;	 arg6		(level 0 only)  right-hand weapon 
;
; Levels run from 1 to 98 maximum.  
;
; You only need to have entries for levels where you want the hp, mp, new spell, or
; level name to change.  Minimum experience levels are now computed automatically
; for you.  If you give an 'empty' level name (except for level 1 where it is
; mandatory) the previous name will be used for that level
;
; Note that LEVEL ZERO (100,200, etc) is a SPECIAL ENTRY which names the actual
; character class, and is not a level in of itself.  I know this is horribly
; confusing, but it is just so dang CONVENIENT to do it this way!
; So, for LEVEL 0 ONLY:  maxHP and maxMP are the starting levels of a new character
; in that class, level_name is the 'character class name' which appears in the
; new soul dialog box.  
;
; LEVEL ZERO is where you define a characters magic-ration and right-hand weapon.
; Their 'magic ratio' determines what percentage of their attack points are used for magic versus sword attack;
;
; Hence a MAGIC-USER would have a high magic ratio (80-90) while a SWORD-USER
; would have lower one (10-20), and some other characters might be more 50-50
; A character needs to be more than 50% in the respective sense in order to 
; equip swords or staffs.  For example a magic ratio > 50 would allow staff,
; < 50 would allow sword, and == 50 would allow both.
;
; The right-hand weapon controls which class of EQUIP item they can hold in their
; right hand (each class is hard-wired to a single right-hand item, but the
; world designer can define what those actual items are).  Between the combination
; of magic-ratio and right-hand you can create some interesting classes.  For example
; an ARCHER and a MAGIC-ARCHER might both have right-hand set to 'BOW' but different
; magic-ratios.  The magic-archer (with the higher magic-ratio) would be better at
; casting spells, but his actual bow-use would be weaker.
;
; The default right-hand weapons are:
;
;	1	Sword
;	2	Staff
;	3	Bow
;	4	Musical Instrument
;
; OPTIONAL CHARACTER LIMITATIONS
;
; If you (as the world designer) like, you can apply certain special commands to each
; character class, generally to limit its access to element and hand training,  You
; place these commands (one per line) just after the introductory line of the class
; For example:
;
; The class name
;	100,	0,			20,		0,		"Sword-User",   10,				1
;    DESCRIPTION	"Warriors are often the first to battle, and last to leave...."
;    MAGIC_RATIO	10
;    HAND_RATIO		90
;    MAX_ABILITY 1,2,3,4,0
;    START_ELEMENT_PP 556,1251,2143,3334,5001,7501,11667,20001		<-- no particular sense to these example values, just levels in order
;    MAX_ELEMENT_PP 1251,2143,3334,5001,7501,11667,20001,45001
;    START_HAND_PP 557,1251,2143,3334,5001,7501,11667,20001
;    MAX_HAND_PP 1251,2143,3334,5001,7501,11667,20001,45001
; now the class levels
;	101,	0,			1,		0,		"Barbarian"
;   etc.
;
; Each command is optional and applies only to the class within which is is embedded.  You
; must provide the information in a fixed syntax
;
;   DESCRIPTION - short text which appears on character creation dialog.  Put text in double quotes
;	MAGIC_RATIO - 0 to 100, specifies effectiveness with magical attacks
;   HAND_RATIO - 1-100, specifies effectiveness of non-magical attacks.  If you set to '0' it will use "100 - magic ratio"
;   MAX_ABILITY str, wis, sta, agi, dex  - must have 5 values in that order.  Max is still 255 each
;   START_ELEMENT_PP  ...   must have 8 values, in order of elements.  sets initial training level
;   MAX_ELEMENT_PP ... must have 8 values, in order of elements.  sets max possible training
;   START_HAND_PP ... mmust have 8 values, in order of hands. sets initial right-hand training
;   MAX_HAND_PP ...  must have 8 values, in order of hands. sets max right-hand training.
;   AUTO_MAX minHP, maxHP, minMP, maxMP, startMPLevel  .. must have five values.  sets rate of HP/MP maximums
;				initial level 0 HP will be minHP, final level 100 HP will be maxHP
;				magic works the same, only the fifth arg (startMPLevel) lets you postpone first MP.
;				NOTE: Actual maximums will be a little lower than you specify because of curve-shaping
;
; Note that the 'preferred right hand training' of each class will always be level 5 at start, no 
; matter what START_HAND_PP says.  This is for compatibility with existing worlds.  Sorry.
;
; PP Levels
;
; The PP required for each level grows according to a nasty equation, but here are the PP
; limits as of the time of this writing.
;
; level 0		PP 0 - 555
; level 1		PP 556 - 1250
; level 2		PP 1251 - 2142
; level 3		PP 2143 - 3333
; level 4		PP 3334 - 5000
; level 5		PP 5001 - 7500
; level 6		PP 7501 - 11666
; level 7		PP 11667 - 20000
; level 8		PP 20001 - 45000
; level 9		PP 45000 - 1000000 (one million)(you approach level 9.9999999 asymptotically)
;
; Note that is the total PP which affects your skill and the level numbers are somewhat arbitrary
; so a training level 3 with 3333 PP will hit harder than a training level 3 with 2143 PP
;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; First the Warrior levels (100 + level #)
;  CCLL	    ???????.	MaxHP	maxMP	Level_name		Magic Ratio		Right-Hand
;	0		1			2		3		4				5				6
;
; The class name
	100,	0,			20,		0,		"Swordsman",   10,				1
DESCRIPTION	"One educated in the swordsman class dedicates his life in the use of swords and only swords. Higher level swordsmen learn magic, allowing life-giving spells."
; note, the lines following are commented out and here just as an example - they do NOT necessarily make sense for a warrior
;MAGIC_RATIO	10
;HAND_RATIO	90
;MAX_ABILITY 1,2,3,4,0
;START_ELEMENT_PP 556,1251,2143,3334,5001,7501,11667,20001
;MAX_ELEMENT_PP 1251,2143,3334,5001,7501,11667,20001,45001
;START_HAND_PP 557,1251,2143,3334,5001,7501,11667,20001
;MAX_HAND_PP 1251,2143,3334,5001,7501,11667,20001,45001

AUTO_MAX	20, 2500,  10, 1200,  20		; <-- note: this is actually used, not just a comment, so entries in table are ignored
; now the class levels
	101,	0,			1,		0,		"Trainee"
	105,	0,			2,		0,		"Recruit"
	110,	0,			5,		0,		"Guard"
	115,	0,			8,		0,		"High Guard"
	120,	0,			10,		2,		"Warrior"
	125,	0,			13,		2,		"Master Warrior"
	130,	0,			17,		4,		"Knight"
	135,	0,			22,		4,		"Knight Caster"
	140,	0,			25,		8,		"Specialist"
	145,	0,			29,		8,		"Royal Guard"
	150,	0,			32,		12,		"Royal Warrior"
	155,	0,			35,		12,		"Elite Warrior"
	160,	0,			39,		16,		"Elite Knight"
	165,	0,			42,		16,		"Paladin"
	170,	0,			45,		20,		"Paladin Spellcaster"
	175,	0,			49,		20,		"Paladin Magi"
	180,	0,			52,		24,		"Death Knight"
	185,	0,			55,		24,		"Death Paladin"
	190,	0,			69,		28,		"Minor Battlemage"
	195,	0,			72,		28,		"Master Battlemage"
;
;---
; Now the Wizard levels (200 + level #)
;  CCLL	    ???			MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	200,	0,			15,		5,		"",		90,				2
DESCRIPTION	"No one trusts a magician.  Their arcane activities are opaque to the outsider. Yet their hearts beat to the same rhythm."
AUTO_MAX	15, 1800,  10, 2500,  0	
; the class levels
	201,	0,			1,		2,		"Shaman"
	205,	0,			2,		5,		"Enchanter",		
	210,	0,			4,		10,		"Magician",			
	220,	0,			7,		15,		"Conjuror",			
	230,	0,			8,		15,		"Mage",				
	240,	0,			11,		20,		"Wizard",			
	250,	0,			18,		25,		"Sorcerer",			
	260,	0,			22,		30,		"Thaumaturge",		
	270,	0,			28,		35,		"Demonologist",		
	280,	0,			40,		40,		"Necromancer"
	290,	0,			55,		55,		"Deity",
;			
;---
; Now the Archer levels (300 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	300,	0,			18,		2,		"Bow-User",			40,				3
DESCRIPTION	"Archers tend to be aloof, as their elegant weapons do not require such closed quarters. But only a narrow shaft stands between them and their doom."
AUTO_MAX	20, 2000,  10, 1500,  5	
; the class levels
	301,	0,			2,		2,		"Stringer"			
	305,	0,			4,		3,		"Shafter",		
	310,	0,			6,		6,		"Bowman",			
	320,	0,			9,		9,		"Arrow Head",			
	330,	0,			12,		11,		"Archer",				
	340,	0,			15,		16,		"Long Arrow",			
	350,	0,			20,		20,		"Boltman",			
	360,	0,			25,		22,		"Master Bowman",		
	370,	0,			32,		27,		"Arbelest",		
	380,	0,			48,		32,		"Master Archer"
	390,	0,			65,		40,		"Quiver Lord",
;
;	
;---
; Now the MUSE levels (400 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	400,	0,			18,		2,		"Music-User",		60,				4
DESCRIPTION	"Everyone loves a good song, but people take notice when a muse in the vicinity. Trained in the dark side of melody, their instruments can kill."
AUTO_MAX	20, 2000,  10, 1500,  5	
; the class levels
	401,	0,			1,		2,		"Castrati"			
	405,	0,			2,		5,		"Novice",		
	410,	0,			5,		10,		"Hummer",			
	420,	0,			8,		15,		"Yodeler",			
	430,	0,			10,		15,		"Crooner",				
	440,	0,			12,		20,		"Chanter",			
	450,	0,			15,		25,		"Singer",			
	460,	0,			20,		30,		"Musician",		
	470,	0,			30,		35,		"Troubador",		
	480,	0,			50,		40,		"Virtuoso"
	490,	0,			65,		55,		"Muse",

;
;	
;---
; Now the FIST levels (500 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	500,	0,			20,		0,		"Fist-User",		5,				5
DESCRIPTION	"These feisty warriors take a hands-on approach, with a minimum of metal between their fists and their enemy's tender parts. They also disdain the use of magic."
AUTO_MAX	20, 2400,  10, 1300,  18	
; the class levels
	501,	0,			2,		1,		"Baby"			
	505,	0,			3,		2,		"Punk",		
	510,	0,			7,		4,		"Bully",			
	520,	0,			10,		7,		"Scrapper",			
	530,	0,			12,		9,		"Tussler",				
	540,	0,			14,		11,		"Rumbler",			
	550,	0,			22,		15,		"Brawler",			
	560,	0,			28,		18,		"Contender",		
	570,	0,			36,		21,		"Boxer",		
	580,	0,			50,		24,		"Pugilist"
	590,	0,			65,		27,		"Heavyweight",


;
;	
;---
; Now the DART levels (600 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	600,	0,			18,		0,		"Dart-User",		30,				6
DESCRIPTION	"These shy fighters prefer to lurk in the shadows, from which they can hurl sharp objects at their enemies with minimum chance of personal injury."
AUTO_MAX	20, 2000,  10, 1500,  10	
; the class levels
	601,	0,			1,		1,		"Camper"			
	605,	0,			2,		2,		"Pitcher",		
	610,	0,			5,		4,		"Hurler",			
	620,	0,			8,		6,		"Pelter",			
	630,	0,			10,		8,		"Lobber",				
	640,	0,			12,		11,		"Sniper",			
	650,	0,			15,		15,		"Shadow",			
	660,	0,			20,		20,		"Assassin",		
	670,	0,			30,		25,		"Ballista",		
	680,	0,			50,		30,		"Catapulter"
	690,	0,			65,		35,		"Ninja",


;
;	
;---
; Now the BOOK levels (700 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	700,	0,			10,		8,		"Book-User",		70,				7
DESCRIPTION	"Education is key with these fighters. Their vast knowledge of arcane lore gives them almost magical powers."
AUTO_MAX	20, 1900,  10, 2200,  0	
; the class levels
	701,	0,			1,		2,		"Flunky"			
	705,	0,			2,		5,		"Diletante",		
	710,	0,			5,		8,		"Pupil",			
	720,	0,			8,		12,		"Student",			
	730,	0,			10,		14,		"Tutor",				
	740,	0,			12,		16,		"Instructor",			
	750,	0,			15,		19,		"Teacher",			
	760,	0,			20,		23,		"Professor",		
	770,	0,			30,		30,		"Scholar",		
	780,	0,			50,		35,		"Researcher"
	790,	0,			65,		55,		"Pundit",


;
;	
;---
; Now the SPIRIT levels (800 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	800,	0,			8,		20,		"Spirit-User",		80,				8
DESCRIPTION	"These deeply religious souls act as mediums, channeling the forces of spirits from another domain. They are also skillful magicians."
AUTO_MAX	20, 2000,  10, 2200,  0	
; the class levels
	801,	0,			1,		5,		"Charlatan"			
	805,	0,			2,		8,		"Gentile",		
	810,	0,			4,		15,		"Medium",			
	820,	0,			6,		20,		"Preacher",			
	830,	0,			8,		25,		"Channeler",				
	840,	0,			10,		30,		"Priest",			
	850,	0,			13,		35,		"Monastic",			
	860,	0,			16,		40,		"Bishop",		
	870,	0,			19,		45,		"Spirit Guide",		
	880,	0,			24,		50,		"Ecclesiast"
	890,	0,			65,		55,		"Oracle",


; Be sure to leave this comment or a blank line at the very end
