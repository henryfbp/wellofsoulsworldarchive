saved HEROEs folder
----------------------

Each world has its own "savedheroes" folder which keeps records for all the souls you have created in that world.

Note that you cannot move savedhero files from one world to another.

Your hero is saved automatically when you exit the game, or return to the Well of Souls within the game.