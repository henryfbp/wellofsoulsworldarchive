;
; this file is used by the monster race mini-game
; it controls which 'monster IDs' are raceable
; you can change it to be monsters from your own world
; put one monster id per line, up to 32 monsters total
; use a semi-colon to add a comment
1		;	green jelly
3		;	archer fish
4		;	flesh crab
5		;	slobber
6		;	poison newt
7		;	flame weed
8		;	carnivorous hat
9		;	gray widow
10		;	giant flea
52		;	ice jelly
53		;	ghoul
56		;	devil shell
57		;	stingtail
59		;	giant wasp
60		;	gray wolf
61		;	viper
64		;	banana slobber
65		;	tooth worm
67		;	terabul
68		;	 mad box
100		;	brain jelly
101		;	devil ghoul
106		;	devil saur
108		;	snow Wolf
116		;	flytrap
117		;	fire cobra
120		;	parasite
124		;	shriek
127		;	pangolin
153		;	flying rage
155		;	iron jelly
156		;	eyebrawl
160		;	lava jelly

; be sure to leave a blank line at the end, just in case
