this is a WORLD\ART folder
--------------------------

This folder contains miscellaneous art assets unique to a world.

The following files must be 'RLE encoded BMP' files which have been coerced to use the 256 color palette in "souls.pal" (which can be found in the ROOT\ART folder)

Each is a 'filmstrip' of images arranged in a horizontal list.  The first image in the list is image '0' the next is image '1' etc.  In the quest.txt file each ITEM has an image number which represents its index in the appropriate BMP file.

For example a HELMET item with index 3 would be the fourth image in the world's HELMET.BMP file.

Image sizes vary with file in question.

   AMULETS.BMP		Amulet items (48x48)
   ARMOR.BMP		Armor items (48x64) (48 wide by 64 high)
   BOOTS.BMP            Boot Items (48x48)
   DARTS.BMP            Throwable items (48x48)
   HELMETS.BMP          Helmet Items (48x48)
   ITEMS.BMP            General items (48x48)
   RINGS.BMP            Ring Items (48x48)
   SHIELDS.BMP          Shields (48x64)
   STAFFS.BMP           Wizard Staffs (48x64)
   SWORDS.BMP           Warrior Swords (48x64)

These file just control the visual appearance of items on the ITEM and EQUIP screens.  The actual nature of items is controlled by the QUEST.TXT script file.

If any file is missing from the world's ART folder, then it is looked for in the general ART folder instead.  This allows you to reuse image files shipped with the game itself.