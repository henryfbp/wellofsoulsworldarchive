;---------------------
; ELEMENTS Section
; 
; Every world has exactly 8 'elements' which you must define (or terrible things will happen..
; woe is you!)  The 8 elements form a circle which describes their effectiveness against
; each other.  The opposing element is most effective against an element.
;
; Visualize the IDs like this (many thanks to Janne Jaula for this):
;
;         0
;      7  |  1
;       \ | /
;    6 ---+--- 2
;       / | \
;      5  |  3
;         4
;
; With ID 0 at the NORTH position.  This must be your 'healing' spell. and will apply healing
; equally to all characters, independent of their element affinities.
;
; The rest of the elements wield pain according to their position on the chart with element 1, 
; for example, delivering the most damage to element 5, somewhat less to 4 and 6, and a little to 7 and 3
; while hardly any to 0 1 and 2.
;
; It is likely that element 4 (the opposite of 0) will also be singled out for some special
; effect in the future... the element table is new :-)
;
;	arg0	the element ID  0-7
;   arg1	the name of the Element (keep it short, it shows up on the element affinity chart)
;									(also if the first letter could be unique, it would help)
;
;	arg0	arg1				
;
	0,		"Human"		
	1,		"Coastal"			
	2,		"Deep sea"			
	3,		"Deep ocean"			
	4,		"Land-based"		
	5,		"Ship"		
	6,		"Spirit"		
	7,		"Air"		
