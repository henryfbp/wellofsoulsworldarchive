this is a WORLD\MAPS folder
---------------------------

Each WORLD folder has its own MAP folder containing maps unique to that world.

Maps are numbered, so that the names of map files are "MAP0, MAP1", 
etc.

MAP0 is always the large map of the world (512x512 recommended).  Other maps are used for cities, towns, caves, etc. unique to the world.

Each map is then defined by two files:

    MAPn.JPG    a JPEG file of map itself

    MAPn.OBL    A file created by the LINK editor which defines where
                links are placed on the base file, and special aspects
                about each link (whether it links to a scene, another
                map, etc.)

optionally a map can also have a

    MAPn.ter    a file created by the terrain editor which indicates areas which
		require special boots (or objects) to be crossed.

And each world also requires these two files.

 OBJECTS.JPG    a JPEG file containing images which are used as LINKs
                and pasted on top of the base map.  (trees, towns,
                cave entrances, etc.)

 OBJECTS.OBR    A file created by the OBJECT editor which gives a
                name to selected rectangles of the MOB file

Objects from the object files can be placed on all maps.  That is to say, each map does NOT  have a unique ojbjects file, but each world *does*

The quest file has a MAPS section which links the file names together for a single map.
