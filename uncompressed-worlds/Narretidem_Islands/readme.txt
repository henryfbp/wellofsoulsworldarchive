this is a WORLD folder
----------------------

Each world has its own folder which contains:

   quest.txt  - the script file which controls the quests available
                in this world.  See the quest.txt file comments for
                additional information.
   world.ini  - an INI file which contains descriptive information
		about the world (this is used during publication of 
		the world)

   art	      - a folder containing world-specific artwork
   maps       - a folder containing world-specific maps
   midi       - a folder containing world-specific music files
   scenes     - a folder containing background images for scenes
   savedheroes - a folder containing .her files for 'saved games' in
                 this world.  Heroes from one world cannot be played in
                 other worlds.