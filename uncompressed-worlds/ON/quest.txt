
+CREDITS
ON

Story by Mortis Ghost
Most skins & art recycled from OFF
Most music by Alias Conrad Coldwood
	
Well of Souls by Synthetic Reality
World by Geb
http://www.synthetic-reality.com
-CREDITS




+STORY
:S 177
:C 246,176,11
|ON
:S 75
:C 204,153,0
|This world is a tribute to OFF.  OFF is the critically acclaimed French surreal adventure RPG made by Mortis Ghost in 2008.
|The game was translated into English in 2011 and is considered a gem among indie adventure RPGs.
|OFF is widely praised for its in-depth and complex story, its rich and immersing environment, and its memorable characters.
:F Verdana
:S 60
:C 246,176,11
|Enjoy yourself...
-STORY


+TERRAINS
;	arg0	arg1				arg2	arg3
;
	0,		"No Hindrance",		0,	0
	1,		"Lava",			10,	0
	2,		"Smoke",		10,	0
	3,		"",			0,	0
	4,		"Deep Water",		0,	0
	5,		"Clouds",		0,	0
	6,		"",			0,	0
	7,		"",			0,	0
	8,		"Healing",		-10,	0
	9,		"Impassable",		0,	0
-TERRAINS





+EQUIP
;	arg0	arg1
;	ID		Name

  	0,		"Helmet"
 	1,		"Armor"
 	10,		"Boots"
 	11,		"Shield"
 	12,		"Ring"
 	13,		"Amulet
-EQUIP





+THEMES
;	arg0	arg1		arg2			arg3 - N
;	ID		name		loopSound		seconds=sound
;
;	note: reserve first 20 for the hard-coded themes (also note that 0 means 'silence')
;
	20,		cavedrip,	campfire1,		10=drip1,10=bird1,10=splash1
	21,		ds,			DS
	22,		EmptyWarehouseOut, z0p1
;
-THEMES

+MAPS
#include maps.txt
-MAPS


+TOKENS

-TOKENS

+ELEMENTS
;	arg0	arg1				
;
	0,		"Life"		
	1,		"Plastic"			
	2,		"Nature"			
	3,		"Earth"			
	4,		"Death"		
	5,		"Metal"		
	6,		"Smoke"		
	7,		"Meat"	

; Note that the elements.bmp file has placeholder images for each element in the order shown in this table
; so if you make up or reorder elements, be sure to provide an updated art/elements.bmp for your world.
;
-ELEMENTS



+HANDS
;  
; arg0		arg1		arg2		arg3
;  ID		Name		Range		SoundFX
;
	0,		"Sword",	30,			"sword1.wav"
	1,		"Staff",	20,			"staff.wav"
	2,		"Bow",		100,		"sword8.wav"
	3,		"Bat",	30,			"staff.wav"
	4,		"Fist",		20,			"fist.wav"
	5,		"Dart",		80,			"dart.wav"
	6,		"Book",		70,			"chant6.wav"
	7,		"Spirit",	90,			"crow1.wav"
-HANDS


+ITEMS
#include items.txt
-ITEMS


+TROPHIES
#include trophies.txt
-TROPHIES


+SPELLS
#include spells.txt
-SPELLS


+MONSTERS
#include monsters.txt
-MONSTERS


+GROUPS
#include groups.txt
-GROUPS


+LEVELS
#include levels.txt
-LEVELS





+SCENES

;--------------
; SCENE 0 must be primary well of souls
;
; It is only used for switching souls (saved games), not for general resurrection and recharge
;
; First we need a 'SCENE' command to assign:
;  a scene number (zero), 
;  a BACKGROUND JPEG (temple.jpg),
;  the scene 'style' (WELL), 
;  the scene name "Well of Souls", 
;  the current EFFECT (2 =lake ripple),
;  and the current WEATHER (7 = snow)
;
SCENE 0		blk, WELL, "???", 2, 3

; Then we start a sound 'theme' (1=rippling stream)
	THEME	21

; Then we define ACTOR #1 (The Blind Sage) using frame 32 of the MONSTERS skin "joshMiscelleneous.bmp"
; The actor begins at location (0, 60) which is in the  bottom left of the screen
	ACTOR	1,	"The Judge", testpablo, 1, 0, 60

; Now we select actor 1 as the current actor
	SEL		1

; Now we set the POSE of the current actor (actor #1) using the 3-frame form.  This is 
; pretty boring since we use the same frame for all 3 choices (frame 32).  Normally you would
; use 3 different frames.  The game will automatically cycle between the first two frames
; with roughly equal probability.  The third frame will be used only briefly now and then (so
; it should be the "blinking eye" or "lashing tongue" or something else you only want to see now and
; then, while the first two frames would be the shifting between feet, or breathing, effect.
	POSE	32,32,32

; Now we start actor one walking towards (50,60) which is directly to the right of where he started.
; He will walk at a 'standard speed'
	MOVE	1,	50, 60

; Now we wait one second to let his motion be apparent to the player
	WAIT	2.0

; Now we have him talk.  Note the use of the ' command to make the chat bubble appear over the "Current" actor.
; I hardly ever do that anymore, since it is so easy to use "1:blah" instead.
	' Oh, hello there.  I didn't expect to see such a thing here, of all places..

; Now we have him move back to the left a bit.  The chat bubble is still over his head.  Since we waiting only
; one second after the first move command, we don't really know how far he made it before we changed direction.
; anyway, now he is moving "up" a little, relative to his previous destination.
	MOVE	1,	45, 60

; Now we have him deliver his second line of dialog, which replaces the previous chat bubble.  Usually chat bubbles
; pop automatically to make room for the next one, with each bubble remaining long enough to be read (so short
; sentences pop faster than long ones.

; And this actor is a bit fidgety, so he moves again.  Let's watch him move and chat.. move and chat for a bit.
	MOVE	1,	30, 70
	' Miaou.
; OK, he finally shut up.  We don't have anything else to accomplish in this scene, so we need to end it using the
; END command.  It ends the scene.
END

; Tada, a scene!  Please note that some player actions are blocked until the scene finishes.  For example, you can't
; cast a spell, use a weapon, or toss an item until the scene finishes.

;--------------
; SCENE 1 must be primary CAMP scene
;
SCENE 1
	THEME 
;	MUSIC   (no music in camp scene, just theme)

; Check is this is a PK Attack (uses CAMP scene, not FIGHT scene, oddly enough)

	COMPARE #<num.isPKAttack>, 1
	IF= @pk
	COMPARE #<num.isPKAttack>, 2
	IF= @pk	

; Not a PK Attack, check if it is in Macguyver castle

	COMPARE #<num.mapNum>, 2
	IF=	@inCastle

; Not Macguyver castle, so we're done.  It's just a regular camp with no special stuff

	END

;
; It *is* a PK Attack, so we set up some special rules to make it 'honorable'
;
@pk
	; Set map flags: 32768 (no nasty spells) + 8192 (no tickets) + 4096 (no Heal)
	FLAGS	45056

	; Lock the room
	LOCK 1

	; Announce to the players that this is a special place
	N: PK Attack! 
	N: Locked Camp!
	N: No Healing, Stunning, or Tickets!

	; we have to END or they can't fight
	END

@eventPkAttackDone

	; maybe someday this event will work, and we can unlock the camp as soon as 
	; only one side is left standing

	LOCK 0
	FLAGS 0
	END


; We *are* in macguyver castle, so we have the little lock icon in the upper right,
; and can manually lock/unlock the camp.. to discusss our secret plans!
;
@inCastle
	; we get a little lock icon while in macguyver castle camps.
	SET sceneLock, 0
	ACTOR	9, "Lock", danNoFloat, 6, 95, 10
END

; The scene host has clicked on the little lock icon

@eventActorClick9
	COMPARE #<sceneLock>, 0
	IF= @lockScene
	LOCK 0
	SET sceneLock, 0
	SEL 9
	POSE	6
    WAIT 3.0
	END
@lockScene
	LOCK 1
	SET sceneLock, 1
	SEL 9
	POSE	5 
    WAIT 3.0
	END


SCENE 2
	THEME
;	MUSIC  mortal.mid

	FIGHT

END

;--------------
; SCENE 3 (this is the evergreen starting scene on the main map, where you can return to be healed)
;
SCENE 3		blk
	THEME	0
;	MUSIC	waltz2.mid
	ACTOR	1,	"The Judge", testpablo, 5, 25, 25
	POSE	1, 1, 1
	SEL		1
	MOVE	1,	15, 80

	IF T1, @continue
	' Oh, hello again.. what was your name?  %1?  Ahh, yes.. 
	' I am The Judge.  And you, should not be here.
	' The land in which you have befallen is perilous.
	' I have a limited amount of supplies you can purchase for your quest.
	' You can find them at the bottom-right of the screen in which your eyeballs scan endlessly. 

	GIVE T1
	GOTO @continue
	MOVE	1, -30, 80
	END

@continue

	IF ALIVE, @skip1
	' How nice of you to return, let me lick your wounds.
	WAIT    3.0
	SOUND	"thunder1.wav"
	WAIT	1.0
@skip1
	MOVE	1,	10, 75
	GIVE	L1
	GIVE	H5000
	GIVE	M5000 
	IF I99,@skipTicket
	GIVE	I99				; give him a ticket home
@skipTicket
	; offer EVERYTHING under level 10
	OFFER2	0,10


    COMPARE #<num.TrophyBagWidth>, 3
    IF> @hasBag
	WAIT 1
	' Before you go, take this bag.  You humans like to collect the remains of dead foes, too, yes?
	SET num.TrophyBagWidth, 4
	SET num.TrophyBagHeight, 3
	SET num.TrophyBagOpen, 1
@hasBag
	' You ever wonder why we're here?

END





;--------------
; SCENE 4 Generic Casino
;
SCENE 4		aztec
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Barker", joshRoyalty, 2, 25, 90
	SEL		1
	MOVE	1,	15, 80
	GAME	1
	' Try your luck!
	' Our slots pay the most!
END




;---------
; general armor shop

SCENE 5 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Daniel", joshTownsFolk, 17, 25, 90
	SEL		1
	MOVE	1,	15, 80
	OFFER2	0,99,10,0,99,11,0,99,20,0,99,21
	' Finest armor in Evergreen!
END

;---------
; general weapons shop

SCENE 6 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Josh", joshTownsfolk, 9, 25, 90
	POSE	9,10
	SEL		1
	MOVE	1,	15, 80
	OFFER2	0,99,12,0,99,13,0,99,14,0,99,15
	' Finest weapons in Evergreen!
END

;---------
; general baubles shop

SCENE 7 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Michelle", joshTownsFolk, 24, 25, 90
	POSE	24,25
	SEL		1
	MOVE	1,	15, 80
	OFFER2	0,99,22,0,99,23
	' Finest rings and amulets in Evergreen!
END


;---------
; item/potion shop
;
SCENE 8 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Charley", joshTownsFolk, 9, 25, 90
	POSE	9,10
	SEL		1
	MOVE	1,	15, 80
	OFFER2	0,99,0

	' Note our new items for sale.
END

;---------
; seed shop
;
SCENE 9 bwHangar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Abe", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	OFFER	8,20,21,22,23,25
	' Please peruse my wares for the experienced adventurer.
END



;---------


SCENE 1000 jd1, SCENE, "Speaking with The Judge", 0, 0

	ACTOR	1,	"The Judge",		testpablo, 1, 0, 60
	SEL		1
	MOVE 1, 50, 70
	IF T10, @hasT10
	GIVE	T10
	1: There cannot be any other living beings in Zone 0, so I must assume that you are only a mere figment of imagination.
	WAIT	3
	1: Nevertheless, I will introduce myself.  I am the Judge, and I am aching to know your name..
	WAIT 1
	1: .. dear elusory interlocutor.
	WAIT 3
	1: %1, you say?  And you have been assigned to the sacred mission of purifying the world?
	WAIT 2
	1: It is a pleasure.  Indeed, there is no objective more laudable than yours.  I shall guide you through this area..
	WAIT 1
	1: ..if it is of any help to you.  I have a limited number of supplies you may find useful--please help yourself.
@hasT10
	1: Miaou.
MOVE 1, 50, 70
	OFFER2	0,10
	
END


SCENE 1001 jd1, SCENE, "Speaking with The Judge", 0, 0

	ACTOR	1, "The Judge", 		testpablo, 1, 0, 90
	SEL 1

	IF T11, @hasT11
	GIVE	T11
	MOVE 1, 10, 85
	1: Allow me to confess.. I find you quite tangible for a phantasmagorial being.
	WAIT 1
	1: Might you be a creature of flesh and blood?
	1: If so, I have been mistaken from the beginning.  You did not even interrupt me in my deluded phantasmal daydreams.
	WAIT 1
	1: This is.. relatively bizzare, I must say, for you are the first living being I have encountered in this realm.
	1: In fact, I had concluded that Zone 0 was an empty land.  
	WAIT 1
	1: Obviously, I was mislead.
	1: However, other zones exist.  And in those territories, the risk of hostile.. individuals.. attacking you is..
	1: .. to be expected, I'm afraid.
	WAIT 1
	1: Your sacred mission will likely lead you into these lands.  I sincerely hope you are skilled in the art of violent confrontation.
	WAIT 1
	1: Be it as it may, your training has not reached its end yet.  Let me ask you to follow me, if you still want me as your guide.
	1: I shall await for you, in the up-downstairs.
	MOVE 1, -30, 50

@hasT11

	MOVE 1, -30, 50

END


SCENE 1002 jd1, FIGHT, "Treasure Chest", 0, 0
;to be fixed

IF T12, @hasT12
FIGHT 1

@hasT12

END



SCENE 1003 jd1, SCENE, "Speaking with The Judge", 0, 0

	ACTOR	1, "The Judge", 		testpablo, 1, 0, 90
	SEL 1

	IF T13, @hasT13
	GIVE	T13
	MOVE 1, 10, 85
	1: Ahh, yes.  To pass through here, you need to use your cerebral organ.  You know, the one bathing flabbily in your tired cranium.
	WAIT 1
	1: I believe those floating blocks correspond to the symbols you can see on the wall in some way or another.

	END
@hasT13

	MOVE 1, 10, 85
	1: Look at the numbers on the walls.. you can figure this out--I am sure of it.
	END

END



SCENE 1004 z0sc1, FIGHT, "Box Fight #1", 0, 0

END


SCENE 1005 z0sc1, FIGHT, "Box Fight #2", 0, 0

END

SCENE 1006 z0sc1, FIGHT, "Box Fight #3", 0, 0

END

SCENE 1007 z0sc1, FIGHT, "Box Fight #4", 0, 0

END

; Here is the last line of the SCENES section of the quest file.
-SCENES




;


