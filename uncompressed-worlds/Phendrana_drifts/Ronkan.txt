;---------
; Ronkan Armor Shop

SCENE 40 bazaar 	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Gene Hawk", joshTownsFolk, 9, 25, 90
	SET minequip, #<num.hostlevel>
	SUB minequip, "12"
	SET maxequip, #<num.hostlevel>
	ADD maxequip, "12"
	OFFER2	 #<minequip>, #<maxequip>,10, #<minequip>, #<maxequip>,11, #<minequip>, #<maxequip>,20, #<minequip>, #<maxequip>,21
	' All your armor needs.
END

;---------
; Ronkan Weapon Shop

SCENE 41 bazaar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Davie Boy", joshTownsfolk, 1, 25, 90
	SET minequip, #<num.hostlevel>
	SUB minequip, "12"
	SET maxequip, #<num.hostlevel>
	ADD maxequip, "12"
	OFFER2	 #<minequip>, #<maxequip>,12, #<minequip>, #<maxequip>,13, #<minequip>, #<maxequip>,14, #<minequip>, #<maxequip>,15, #<minequip>, #<maxequip>,16, #<minequip>, #<maxequip>,17, #<minequip>, #<maxequip>,18, #<minequip>, #<maxequip>,19
	' Slice and dice your enemies.
END

;---------
; Ronkan Ring Shop

SCENE 42 bazaar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Apocalypse", joshTownsFolk, 15, 25, 90
	SET minequip, #<num.hostlevel>
	SUB minequip, "12"
	SET maxequip, #<num.hostlevel>
	ADD maxequip, "12"
	OFFER2	 #<minequip>, #<maxequip>,22, #<minequip>, #<maxequip>,23
	' Check out how shiny this is.
END


;---------
; Ronkan Item Shop
;
SCENE 43 bazaar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Lunatix", joshTownsFolk, 19, 25, 90
		OFFER2	0,99,0

	' How may I help you?
END

;---------
; Ronkan Seed Shop
;
SCENE 44 bazaar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Death Angel", joshTownsfolk, 25, 25, 90
		OFFER	8,20,21,22,23,25 	
	' Stat boosting items are always in high demand.
END

;---------
; Ronkan Training
;
SCENE 45 fort
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Falco", joshRoyalty, 2, 25, 90

IF T40, @haveT40
	1: Hey there kid, I've already heard the King sent you.
	1: Depending on your class I can teach you a few things you couldn't learn on your own.
	1: I see you are a %C, so let's get to work.
	WAIT 2.0

 command1
     @label1
 command2
 IF  C4, @label2
 IF  C1, @label3
 IF  C2, @label4
 IF  C3, @label5
 IF  C5, @label6
 IF  C6, @label7
 IF  C7, @label8
 IF  C8, @label9
 IF  C9, @label10
 IF  C10, @label11
 IF  C11, @label12
 GOTO label1
     @label2
 HOST_Give S602
 HOST_Give S603
 HOST_Give S604
 HOST_Give S605
 HOST_Give S606
	N: Learned Debuff All spells.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label3
 HOST_Give S613
	N: Learned Resurrect All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label4
 HOST_Give S612
	N: Learned Antidote All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label5
 HOST_Give S610
	N: Learned Raise Agility All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label6
 HOST_Give S621
	N: Learned Charm All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label7
 HOST_Give S609
	N: Learned Raise Stamina All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label8
 HOST_Give S608
	N: Learned Raise Dexterity All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label9
 HOST_Give S700
	N: Learned Shock!
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label10
 HOST_Give S611
	N: Learned Raise Wisdom All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label11
 HOST_Give S611
	N: Learned Raise Wisdom All.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END
	 @label12
 HOST_Give S622
 HOST_Give S623
 HOST_Give S624
 HOST_Give S625
 HOST_Give S626
 HOST_Give S627
 HOST_Give S628
	N: Learned Summon Spells.
	Wait 2.0
	1: There, we are done.
Give T40
 command3
END

@haveT40
	1: I can teach you no more.
END

;---------
; Ronkan Castle

SCENE 46 throne
	THEME	0
	ACTOR	1,	"King Dino", joshRoyalty, 1, 25, 90

IF T41, @haveT41
	1: King Carnage told you to come see me?
	H: Yeah, that's right.
	H: He said I can get more formal training here in Ronkan.
	1: Yes, that is correct.
	1: But before you are to be trained I must test your worth.
	1: A lighthouse to the west holds a powerful artifact known as a Memory Editor.
	1: This tool may be our only chance to stand again the might of Animae.
	H: So I just have to head west and get this Memory Editor for you?
	1: Yes, afterwards return here and I will let you access our training grounds.
GIVE T41
END

@haveT41
IF T42, @haveT42
	1: Head west to the lighthouse.

END

@haveT42
IF T43, @haveT43
	1: Good show! You did it.
	1: Maybe not all hope is lost.
	1: Now, give me the Memory Editor.
Take I100
	1: Thanks, %1.
	: I'll grant you permission to use the training grounds now.
Give T43
END

@haveT43
	1:Thanks again, %1.
END

;---------
; Ronkan Lighthouse

SCENE 47 throne
	THEME	0

	IF T42, @haveT42
	N: A strange object sits atop a pedestal.
	WAIT 2.0
	N: You assume it's the memory editor the King spoke of.
	WAIT 2.0
	N: Will you pick it up?
	WAIT 2.0
	ASK 20
	IF Yes, @saidYes
	N: You let the object rest.
	WAIT 2.0
END
	@saidYes
	Give I100
	Give T42
	N: You quickly snatch up the Memory Editor.
	WAIT 2.0
End
	@haveT42
	N: There is nothing left.
END

;---------
; Ronkan Ferries

SCENE 48 pond
	THEME	0
	ACTOR	1,	"Kon", joshTownsfolk, 10, 25, 90

	IF T51, @haveT51
	1: Flash floods have made the river uncrossable at the moment.
	1: Come back in a while and you can cross the river though.
END

	@haveT51
	IF M0, @youCHEATED
	1: Welcome to the river crossing.
	1: 500 Credits to cross the river.
	ASK 20
	IF Yes, @saidYes
	1: Ok, later man.
END
	
	@saidYes
	Take G500
	WAIT 3.0
	GOTO LINK 0,6
	1: Ride's over, man.
END

@youCHEATED
	GOTO LINK 30,0
	N: Welcome to the Cheater Arena.
	WAIT 2.0
	N: There is no escape but feel free to battle any other cheaters.
	END

;---------
; Ronkan Ferries

SCENE 49 pond
	THEME	0
	ACTOR	1,	"Kon", joshTownsfolk, 10, 25, 90

	IF M0, @youCHEATED
	1: Welcome to the river crossing.
	1: 500 Credits to cross the river.
	ASK 20
	IF Yes, @saidYes
	1: Ok, later man.
END
	
	@saidYes
	Take G500
	WAIT 3.0
	GOTO LINK 3,9
	1: Ride's over, man.
END

@youCHEATED
	GOTO LINK 30,0
	N: Welcome to the Cheater Arena.
	WAIT 2.0
	N: There is no escape but feel free to battle any other cheaters.
	END


;---------
; Ronkan Burn Town

SCENE 94 bwOilFire

	H: A destroyed village...
	H: No doubtedly this was done by Animae.
	H They must pay for their atrocities.
END


;---------
; Ronkan Tickets Shop

SCENE 50 bazaar 	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Ticket Master", joshTownsFolk, 9, 25, 90
		OFFER	99,98
	' I am the Ticket Master!
END
	






