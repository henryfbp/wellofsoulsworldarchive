; GROUPS
;
; NOTE: You probably don't even want to bother with monster groups any more, though an understanding
; of them might be of some use.  Most world designers prefer to use the Link Editor to graphically
; place monsters individually on the map(s).
;
;-----------------------------------------------------------------------------
; Monster Groups
;
; These define the particular groups of monsters encountered in automatic fight
; scenes.
;
; When an automatic fight scene starts, the nearest link points 'difficulty level'
; is used to find a monster group.  Then monsters from that group are added to
; the fight scene, but randomly.  The likelyhood of a monster being added increases
; with distance from the link point (this is so that monsters get tougher the
; father you go from town.
;
; You can duplicate monster ids in the list if you want there to be a chance of
; several of that kind of monster in the same fight.  For example, the list:
;
;     1,1,1,2,2,3
;
; Could have as many as three monster 1, two monster 2, and one monster 3, though it
; is more likely to have fewer.  Close to the link point, there is likely to be only
; a single monster, and it will be most likely to be a type 1 monster.  Farther from
; the link point you are more likely to get the entire list of monsters.
;
; The idea, by the way, is to group monsters which you think are more likely to 
; hang around together. Like lots of water monsters in a group tied to a watery link.
;
; If the link point specifies a POSITIVE difficulty level, then the monster list will
; get EASIER the closer you are to the link (this is suitable for 'town' links).  If
; the difficulty level is NEGATIVE, then the monster list will get HARDER as you get
; closer to the link.  (suitable for some 'guarded' area).  So, difficulty levels
; -3 and 3 both refer to monster group 3.
;
; By convention, we will let difficulty level '0' mean 'no particular monsters near
; this link.  Suitable for some sort of healing pool area.
;
;  arg 0 :  ID# (1-255)  This is the 'monster difficulty level' argument in the link
;  arg 1-9; up to 9 monster IDs
;
;	id	potential_monster_list
;
; NEW FEATURE:  Group 0 entry sets flags for special effects
;
;   0,		1     <-- this means monster groups are NOT RANDOM and all members will appear
;
;  0 - special entry
	0,		1
;
; 1 - 99 land based
; Jellies
	1,		1,1,1,4,10,10,10
	2,		4,4,4,10
	3,		5,5,5
	4,		6,6,6,9
	5,		8,8,8,9
;
	6,		53,53,53
	7,		54,54,54
	8,		55,55,55
	9,		57,57,57
	10,		58,58,58
	11,		59,59,59
	12,		60,60,60
	13,		61,61,61,59,59
	14,		62,62,62,63,63,64
	15,		63,63,62,62
	16,		64,64,64,5,5,5,5
	17,		65,65,65,68
;
	18,		66,66,66,10,10,1
	19,		67,67,67,53,53
	20,		68,68,68
	21,		69,69,69
	22,		70,70,70
	23,		100,100,100
	24,		101,101,101
	25,		106,106,106
	26,		107,107,107
	27,		108,108,108
	28,		110,110,110
	29,		111,111,111
	30,		112,112,112
;
	31,		113,113,113
	32,		114,114,114
	33,		115,115,115
	34,		116,116,116
	35,		117,117,117
	36,		118,118,118
	37,		119,119,119
	38,		120,120,120
	39,		121,121,121
	40,		122,122,122
	41,		123,123,123
	42,		124,124,124
	43,		125,125,125
	44,		126,126,126
	45,		127,127,127
	46,		128,128,128
	47,		129,129,129
	48,		130,130,130
	49,		153,153,153
	50,		154,154,154
	51,		155,155,155
;
	52,		201,160,160,160
	53,		202,154,154,154

;
; 100-149  pools of water

	100,	2,2,2,4,4
	101,	50,50,50
	102,	52,52,52,4,4
	103,	56,56,56
	104,	104,104,104
	105,	150,150,120,120
	106,	204

;
; 150-199  completely underwater

	150,	3,3,3
	151,	4,4,4
	152,	51,51,51
	153,	105,105,105
	154,	109,109,109
	155,	151
	156,	205


;
; 200-249  Special elements or big bosses

	200,	7,7,7
	201,	203
	202,	206
	203,	207
	204,	208
	205,	209
	206,	210
	207,	211

	255,	255

; 10xx Hot Sandy Graveyard Monsters

	1000,	7,7,7,7
	1001,	9,9,9,9
	1002,	10,10,10,10
	1003,	53,53,53,70
	1004,	57,57,57,57
	1005,	61,61,61,61
	1006,	65,65,65,65
	1007,	70,70,70,101
	1008,	101,101,101,121
	1009,	117,117,117,117
	1010,	121,121,121,101
	1011,	154,154,154,154,154,154
	1012,	162,162,162,162
	1013,	164,164,162,162
	1014,	167,167,167,164
	1015,	168,168,168,168
	1016,	168,167,167,164
	1017,	169,167,167,167
	1018,	169,169,167,70,70
	1019,	7,7,57,57,117,117
	1020,	168,121,121,53,53
	1021,	61,61,65,65,10,10,70
	1022,	53, 101, 167, 169
;
; 11xx	underwater groups

	1100,	3,3,3,3
	1101,	51,51,51,51
	1102,	56,56,56,56
	1103,	69,69,69,69
	1104,	105,105,105,105
	1105,	109,109,109,109
	1106,	115,115,115,115
	1107,	150,150,150,150
	1108,	151,151,151,151
	1109,	3,3,109,109
	1110,	51,3,3,3,3
	1111,	115,109,109
	1112,	151,115,115,115
	1113,	105,69,69,69
	1114,	56,56,115
	1115,	151,151,105,105
	1116,	105,105,69,56
	1117,	3,3,69,4,4
	1118,	3,3,4,4,4,4
	1119,	109,109,109,3,3,3
	1120,	51,51,105,105
	1121,	56,56,150,109
	1122,	69,69,3
	1123,	151,109,109
	1124,	115,109,109
	1125,	56
	1126,	150

;
; 12xx Sandy Beachy Monsters and Pirates
;
	1200,	2,2,2,2
	1201,	4,4,4,4
	1202,	6,6,6,6
	1203,	50,50,50,50
	1204,	69,69,69,69
	1205,	104,104,104,104
	1206,	111,111,111,112
	1207,	112,112,112,113
	1208,	113,113,113,114
	1209,	4,4,69,4,4
	1210,	104,6,6,6
	1211,	114,114,111
	1212,	112,113,111,113
	1213,	2,2,4,4,104,104
	1214,	111,111,104,69
	1215,	113,69,69
;
; 13xx	Animal Kingdom Monsters
;
	1300,	59,59,59,59
	1301,	60,60,60,60
	1302,	64,64,64,64
	1303,	65,65,65,65
	1304,	67,67,67,67
	1305,	106,106,106,106
	1306,	116,116,116,116
	1307,	118,118,118,118
	1308,	120,120,120,120
	1309,	122,122,122,122
	1310,	123,123,123,123
	1311,	127,127,127,127
	1312,	128,128,128,128
	1313,	157,157,157,157
	1314,	159,159,159,159
	1315,	165,165,165
	1316,	166,166,166
	1317,	170,170,170
	1318,	171,171,171,171
	1319,	128,60,60,60
	1320,	122,122,123,123
	1321,	165,166,170,170
	1322,	118,116,116,116
	1323,	67,67,106,106,106
	1324,	159,159,65,65,65
	1325,	59,59,127,127
	1326,	122,122,122,128
;
; 14xx goblins and ogres and trolls, oh my!
	
	1400,	58,58,58,58
	1401,	61,61,61,61
	1402,	100,100,100,100
	1403,	107,107,107,107
	1404,	119,119,119,119
	1405,	124,124,124,124
	1406,	126,126,126,126
	1407,	153,153,153,153
	1408,	158,158,158,158
	1409,	161,161,161,161
	1410,	169,169,169
	1411,	173,173,173
	1412,	174,174,174,174
	1413,	176,161,161,161,161
	1414,	178,178,178
	1415,	178,169,169,169
	1416,	176,174,174,107,107
	1417,	174,161,161,107,58
	1418,	119,119,126,126
	1419,	169,100,100,100
	1420,	153,124,124,124
	1421,	173,124,124
	1422,	100,61,61,61
	1423,	178,178,173,173,173
	1424,	126,126,178
	1425,	178,58,58,58,58
	1426,	169,124,124
	1427,	178,119,119,126,126
	1428,	174,174,153,153
;
; 15xx stonehengy monsters
;
	1500,	9,9,9,9
	1501,	53,53,53,53
	1502,	70,70,70,70
	1503,	101,101,101,101
	1504,	116,116,116,116
	1505,	118,118,118,118
	1506,	120,120,120,120
	1507,	121,121,121,121
	1508,	155,155,155,155
	1509,	156,156,156,156
	1510,	162,162,162,162
	1511,	167,167,167,167
	1512,	168,168,168,168
	1513,	119,119,119,119
	1514,	126,126,126,126
	1515,	178,178,178,178
	1516,	118,116,116,116
	1517,	121,70,70,11,11
	1518,	156,155,155,155
	1519,	167,162,162,162
	1520,	155,116,116,9,9,9
	1521,	178,167,167,167
	1522,	178,126,126,119
	1523,	168,167,167,167
	1524,	119,168,168,168
	1525,	178,178,168,168
	1526,	9,9,120,120
	1527,	162,121,121,70,11,11,9
;
; 16xx Infernal Monsters
;
	1600,	7,7,7,7
	1601,	53,53,53,53
	1602,	66,66,66,66
	1603,	70,70,70,70
	1604,	101,101,101,101
	1605,	103,103,103,103
	1606,	117,117,117,117
	1607,	121,121,121,121
	1608,	125,125,125,125
	1609,	154,154,154,154
	1610,	160,160,160,160
	1611,	163,163,163,163
	1612,	164,164,164,164
	1613,	167,167,167,167
	1614,	169,169,169,169
	1615,	172,172,172,172
	1616,	177,177,177,177
	1617,	11,7,7,7
	1618,	66,101,101,101
	1619,	172,160,160,160,160
	1620,	169,167,167,167
	1621,	164,162,162,162
	1622,	125,103,103,103
	1623,	163,154,154,154
	1624,	160,7,7,7,7
	1625,	172,160,7,7
	1626,	163,117,117,117
;
; 17xx Dwarves
;
	1700,	68,68,68,68
	1701,	110,110,110,110
	1702,	120,120,120,120
	1703,	127,127,127,127
	1704,	129,129,129,129
	1705,	130,130,130,130
	1706,	175,175,175,175
	1707,	110,68,68,68
	1708,	127,120,120,120
	1709,	129,127,127
	1710,	129,175,175,175
	1711,	130,129,129
	1712,	130,175,175
	1713,	130,130,110
	1714,	175,175,68,68
	1715,	129,129,127,127
	1716,	175,175,175,127
	1717,	120,110,110
	1718,	129,130,175,175
	1719,	130,130,130,175
;
; 18xx Icy Monsters

	1800,	52,52,52,52
	1801,	62,62,62,62
	1802,	63,63,63,63
	1803,	66,66,66,66
	1804,	108,108,108,108
	1805,	108,108,66,66,66
	1806,	63, 52, 52, 52
	1807,	66, 62, 62	
