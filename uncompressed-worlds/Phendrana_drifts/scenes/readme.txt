this is a WORLD\SCENES folder
-----------------------------

Each WORLD folder has a SCENES folder which contains the background images for scenes 

These are JPG files of size:

   360 horizontal
   256 vertical

The LINK editor binds particular link points to scene numbers.  The quest.txt file binds scene numbers to scene scripts.  Scene scripts call out a background .JPG file, which is expected to be in this folder.