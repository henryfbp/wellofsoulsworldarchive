


+CREDITS
RWInvasion

Story by Rhamadaeus
Most Skins by Josh Worts
Some Skin Contributions by Havok Hanzo Rham
Maps by Riona and Rham
Scene Scripting by Rhamadaeus and Hanzo
Some music and art by Dan Samuel, with the help of JAMMER(r)
	
Programming by Synthetic Reality
Contact us on the web at:
http://www.synthetic-reality.com
-CREDITS

+STORY
:S 200
:C 255,255,0
|RWInvasion
:S 75
:C 255,255,255
|~~~
|by Rhamadaeus
|Ever wonder what it would be like to face an invasion?
|Ever wonder what it would be like to be the invader?
|Both sides have reason to fight on, which will you choose?
|A battle will ensue that will test your love and loyalty of your homeworld whichever one it may be.
|~~~
:S 100
:C 204,153,0
|This world is still in it's beginning phase, as of yet there are no quests.
|Classes are presently in a state of test and go. Have patience as our world develops.
|Golden Souls now have access to the first of planned perks. Don't abuse the gift please. The wrath of RW may sully your soul if you do.
|After the two starter classes are complete, the quests that evolve them into an advanced class will be scripted. (This includes equip overhaul)
|Please feel free to offer input at any time at http://rhamswerld.webs.com/ this is your werld too.
:F Arial Italic
:C 255,0,0
|~~~
|Prepare Yourself!
|~
-STORY

+TERRAINS
;	arg0	arg1				arg2
;
	0,		"No Hindrance",		0
	1,		"Lava",				10
	2,		"Electricity",		10
	3,		"",					0
	4,		"Deep Water",		0
	5,		"Clouds",			0
	6,		"",					0
	7,		"",					0
	8,		"Healing",			-10
	9,		"Impassable",		0
-TERRAINS


+EQUIP
;	arg0	arg1
;	ID		Name

  	0,		"Skull Cap"
 	1,		"Armor"
 	10,		"Footwear"
 	11,		"Shield"
 	12,		"Ring"
 	13,		"Amulet"
-EQUIP


;--------------------
; THEMES section
;
; This section allows you to extend the sound themes available within your scenes.	A sound theme
; consists of a single looped sound (played over and over for as long as the theme is in progress)
; and one or more one-shot sounds (played at random intervals).
;
; arg0 ID (1-255)	(note that these override the built-in themes, so avoid using the lowe numbered
;					themes if you want to keep the built-in themes available
;
; arg1 name			this is just your friendly name for the theme
;
; arg2 loopSound	The name of the .wav file to be looped.  Do not include the .wav extension here, as
;					it is assumed.  The sound effect file is expected to be in your world's SFX folder, and
;					if not found there, the WoS/SFX folder is checked next.  This sound is optional, but
;					if you leave it out, don't forget to leave the comma where it would have been.
;
; argN oneshots		One or more single-shot sounds may be defined.  These are defined with the format
;					"seconds=sound" as in: "10=splash" meaning: play the splash sound about every 10 seconds
;					on average, but randomly.  You should adjust the seconds field until you get the sound
;					you like.  You will need to adjust the timing, volume, and number of single-shots until
;					you get a sound you like.

+THEMES
;	arg0	arg1		arg2			arg3 - N
;	ID		name		loopSound		seconds=sound
;
;	note: reserve first 20 for the hard-coded themes (also note that 0 means 'silence')
;
	20,		cavedrip,	campfire1,		10=drip1,10=bird1,10=splash1
;
-THEMES

+MAPS


	0,		Rham'sWerld.jpg,		Rham'sWerld,		"iSherwood",		3,	16
	1,		RWAncientRuins.jpg,		RWAncientRuins,		"Ancient Ruins",	131072
	2,		RCastle1J.jpg,			RCastle1J,		"Music Hall",		8
	3,		RWEternalMaze.jpg,		RWEternalMaze,		"Eternal Maze",		131072
	4,		strangepyramid.jpg,		strangepyramid,		"Strange Pyramid",	131072
	5,		RWUFOCrashSite.jpg,		RWUFOCrashSite,		"UFO Crash Site",	131072
	6,		RWJellyArena.jpg,		RWJellyArena,		"Jelly Arena",			8
	7,		town5.jpg,			shrimpee,		"Flea Market",		8
	8,		RWDampSpot.jpg,			RWDampSpot,		"Damp Spot",		131072
	9,		RWPetArena.jpg,			RWPetArena,		"Pet Arena",		9	
	10,		RWSteamRoom.jpg,		RWSteamRoom,		"Steam Room",		81927
	11,		dungDesert.jpg,			roundtree,		"Cave by the Beach",	131072;	12,
	12,		isrwdfrst.jpg,			isrwdfrst,		"iSherwood Forest",	131072
	13,		RWCubbyCave.jpg,		RWCubbyCave,		"Cubby Cave",		131072
	14,		pkarena.jpg,			pkarena,		"Ring of Honor",	22
	15,		graveyardmap.jpg,		graveyardmap,		"Grave Yard",		3,	16
	16,		icemazemap.jpg,			icemazemap,		"Frozen Maze of Time",	81927
	17,		RWDeepCubbyCave.jpg,		RWDeepCubbyCave,	"Deep Cubby Cave",	3,	16
	18,		RWLinkMeUp.jpg,			RWLinkMeUp		"Admin Link Map",	3,	16



#include maps.txt

-MAPS

+TOKENS
	1,"You have agreed to obey the golden rule."
	2,
    3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	70,"I'm an Alien"
;	Dev ID
	99,"Admin"


-TOKENS


+ELEMENTS

	0,		"Regeneration"		
	1,		"Liquify"			
	2,		"Mind Control"			
	3,		"Matter"			
	4,		"Degeneration"		
	5,		"Inceneration"		
	6,		"Self Projection"		
	7,		"Anti Matter"		
	8,		"Atomic"

-ELEMENTS

+HANDS


	0,		"Sword",				30,			"sword1.wav"
	1,		"Staff",				20,			"staff.wav"
	2,		"Bow",				100,		"sword8.wav"
	3,		"Developer",			50,			"petDead.wav"
	4,		"Hand Implant",		20,			"SABER2.wav"
	5,		"Dart",				80,			"dart.wav"
	6,		"Book",				70,			"chant6.wav"
	7,		"Spirit",			90,			"crow1.wav"


-HANDS


+ITEMS


#include items.txt

-ITEMS

+SPELLS

#include spells.txt

-SPELLS

+MONSTERS

#include monsters.txt

-MONSTERS


+GROUPS

#include groups.txt

-GROUPS


+LEVELS

#include levels.txt

-LEVELS


+SCENES


SCENE 0		RWWoSscene02a, WELL, "Well of Souls", 2, 7

	THEME	1
	ACTOR	1,	"Scary Floating Head", rhamsactors, 5, 25, 90
	SEL	1
	POSE	1,1,1
	MOVE	1,	50, 60
	WAIT	1.0
	' Welcome to the Well of Souls, traveler.
	MOVE	1,	45, 60
	' I can help you create a new soul, or restore a treasured one.
	MOVE	1,	30, 70
	' When you are ready, select your soul and then incarnate into the physical world.
	MOVE	1,	40, 75
	' Notice the buttons along the bottom of your screen.  Point to a button to see its name.
	' Press the NEW button to create a new soul, and select its skin and abilities.
	MOVE	1,	25, 70
	' Press the RESTORE button to select an old soul, and resume its struggle.
	MOVE	1,	35, 65
	' Press the INCARNATE button to enter the physical world again.
	MOVE	1,	30, 70
	' Press the MAP button to see what other souls roam the world.
	MOVE	1,	40, 77
	' Press the HAUNT button to spy invisibly over another incarnated soul.
	MOVE	1,	20, 70
	' You may rest here safely, for as long as you like.
	WAIT	60
	GOTO	@repeat
End

@repeat
	' You're still here?
	MOVE	1,	50, 60
	WAIT	2
	' Son of a %3.
	WAIT	2
	' Incarnate already.
	Wait	60
	GOTO @repeat

	END

SCENE 1
	THEME
	COMPARE #<num.isPKAttack>, 1
	IF= @pk
	COMPARE #<num.isPKAttack>, 2
	IF= @pk	
	COMPARE #<num.mapNum>, 2
	IF=	@inCastle
	COMPARE #<num.mapNum>, 0
	IF=	@inCastle
	END

@pk
	FLAGS	45056
	N: PK Attack! 
	N: No Healing, Stunning, or Tickets! This means you %1.
	END

@eventPkAttackDone
	FLAGS 0
	END

@inCastle
	; we get a little lock icon while in macguyver castle camps.
	SET sceneLock, 0
	ACTOR	9, "Lock", danNoFloat, 6, 95, 10
	END

@eventActorClick9
	COMPARE #<sceneLock>, 0
	IF= @lockScene
	LOCK 0
	SET sceneLock, 0
	SEL 9
	POSE	6
	END

@lockScene
	LOCK 1
	SET sceneLock, 1
	SEL 9
	POSE	5 
	END

SCENE 2
	THEME

	FIGHT

END

SCENE 3		aztec
	THEME	0
	ACTOR	1,	"Soul Brother", joshRoyalty, 5, 25, 90
	POSE	5, 5, 13
	SEL		1
	MOVE	1,	15, 80

	IF T1, @continue
	' Greetings, new soul. You are about to embark upon an amazing voyage of discovery.
	' When sharing your journey with other souls, it is important that you obey the golden rule.
	' Do you agree to obey the golden rule? (type YES or NO)
	ASK 30
	IF -YES, @saidNo
	' Thank you, my child.  You shall never regret your decision.
	' Push the SHOP button when it appears.  You might find better equipment once you start training.
	GIVE T1
	GOTO @continue
@saidNo
	' It is a sorry state that the world has fallen into.  You sadden me.
	' I cannot bring myself to give sustenance to such a renegade as you.
	' Begone!
	MOVE	1, -30, 80
	END

@continue

	IF ALIVE, @skip1
	' You seem to be dead, let me resurrect you.
	WAIT    4.0
	SOUND	"thunder1.wav"
	WAIT	1.0
@skip1
	MOVE	1,	10, 75
	GIVE	L1
	GIVE	H5000
	GIVE	M5000 
	IF I99,@skipTicket
	GIVE	I99
@skipTicket
	OFFER2	0,10
	' Blessings upon you, Soul %1.
	' You are always welcome here, to rest and recover...
	END

SCENE 4		aztec
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Barker", joshRoyalty, 2, 25, 90
	SEL		1
	MOVE	1,	15, 80
	GAME	1
	' Try your luck!
	' Our slots pay the most!
END

;SCENE 5 bwEuroStreet
;	THEME	0
;	MUSIC	waltz2.mid
;	ACTOR	1,	"Daniel", joshTownsFolk, 17, 25, 90
;	SEL		1
;	MOVE	1,	15, 80
;	OFFER2	0,99,10,0,99,11,0,99,20,0,99,21
;	' Finest armor in Evergreen!
;END

;SCENE 6 bwEuroStreet
;	THEME	0
;	MUSIC	waltz2.mid
;	ACTOR	1,	"Josh", joshTownsfolk, 9, 25, 90
;	POSE	9,10
;	SEL		1
;	MOVE	1,	15, 80
;	OFFER2	0,99,12,0,99,13,0,99,14,0,99,15
;	' Finest weapons in Evergreen!
;END


;SCENE 7 bwEuroStreet
;	THEME	0
;	MUSIC	waltz2.mid
;	ACTOR	1,	"Michelle", joshTownsFolk, 24, 25, 90
;	POSE	24,25
;	SEL		1
;	MOVE	1,	15, 80
;	OFFER2	0,99,22,0,99,23
;	' Finest rings and amulets in Evergreen!
;END

SCENE 8 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	9,10
	SEL		1
	MOVE	1,	15, 80
	GAME 1

	' Note our new items for sale.
END

SCENE 9 bwHangar
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	OFFER	8,20,21,22,23,25
	' It's your skill that matters.
END

SCENE 103 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	IF -I96, @giveticket
	IF I96, @invest
	@giveticket
	GIVE I96
	N: You Recieved a Home James Ticket
End

	@invest
	GAME 1
	' Fancy a Game of Slots??

	STRCMP #<str.soul>, "SOUL 1038"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
END

@Admin
	' Oh it's you %1.
	' Sorry I didn't recognise you.
	' Let's make sure your pockets are full eh.
	N: You recieve 1 Million GP
	GIVE	G1000000
	' I am also Scripted to give you this Ticket.
	GIVE	I1025
	N: You recieved a Grave Yard Ticket.
	GIVE I1030
	' and these Reusable Seeds
	N: You recieved a Killamee Seed.
	GIVE I1028
	N: You recieved a Rezamee Seed.
End

SCENE 104 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GAME 2
	' Care to place a bet?
END

SCENE 105 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GAME 3
	' How about some Pi?
END

SCENE 106 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GAME 4
	' I'm the best Card Dealer in this Werld.
	' Yea I know...
	' I'm the only Card Dealer here, pfft, whatever...
	' I'm still the best Card Dealer in this Werld.
	' Soooo...
	' Fancy a game of Black Jack?
END

SCENE 107 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GIVE	I1027
	N: You recieved a Pet Arena Ticket.
	' Fancy a Game of Space Rocks?
	GAME 6
END

SCENE 108 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	IF -I1, @giveticket
	IF I1, @invest
END
@giveticket
	GIVE I1
	N: You Recieved a PK Arena Ticket
End
	@invest
	GAME 7
	' Never a bad idea to invest that Gold.
END

SCENE 109 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GIVE	I183
	N: You recieved a broken Soul Staff.
	' Never hurts to lend a helping hand %1. You can Ressurect dead people now.
	GAME 8
	' Fancy a Game of Quadris?
END

SCENE 110 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav
;	0	1................	2			3	4	5		6	7	8	9	10	11	12	13	14	15	16		17						18

	OFFER 1000,"Stinger",			pet04,		0,	1,	99,	100,	100,	100,	100,	100,	10,	0,	0,	0,	0,	-1,		"growl5.wav",			"pain9.wav",	-1; 1001,"Grubber",			pet02,		0,	6,	0,		0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,		"growl5.wav",			"pain9.wav",	-1

	' These pets may not be available in the future.
END

SCENE 111 bwEuroStreet
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GAME 5
	' Take Care of that pet %1.
	' Press The Game Button please.
END

SCENE 112 bgUnderWater1
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	GIVE	H30000
	GIVE	M30000
END

SCENE 113 bgUnderWater1
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Fred", joshRoyalty, 11, 25, 90
	POSE	11,4
	SEL		1
	MOVE	1,	15, 80
	IF I1, @hasI1
	IF -I1 @gothere
	@hasI1
	Take I1
	N: You lose one PK Arena Ticket
	' Hello %1
	' In order to make you carry the correct amount of the PK Arena Ticket (which should only be one)
	' I'll need you to enter this link untill you have only one PK Arena Ticket
	' If you only had one PK Arena Ticket when you entered this link, then please visit the Dow Jones to recieve another one
	' The Dow Jones is located to the left of the Pet Arena, 2nd link from bottom
	' Thanx for helping out %1
END
	@gothere
	' Hello %1
	' Please Visit the Dow Jones to recieve one reusable PK Arena Ticket
	' The Dow Jones is located to the left of the Pet Arena, 2nd link from bottom
END

SCENE 114 bgUnderwater1
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15, 80
	IF I96, @hasI96
	IF -I96, @gothere
	@hasI96
	Take I96
	N: You lose one Home James
	' Hello %1
	' In order to make you carry the correct amount of the reusable Ticket Called "Home James" (which should only be one)
	' I'll need you to enter this link untill you have ony one Home James Ticket
	' If you only had one Home James Ticket when you entered this link, then please visit the Slots Link to recieve another one
	' The Slots Link is the seventh link up at the bottom left of the Pet Arena
	' Thanx for helping out %1
END
	@gothere
	' Hello %1
	' Please Visit the Slots Link to recieve one reusable Home James Ticket
	' The Slots Link is the seventh link up at the bottom left of the Pet Arena
END

SCENE 115 graveyard
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15, 80
	' >.>
	STRCMP #<str.soul>, "SOUL 1038"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
	END

@Admin
	EJECT	"T99"
	' Hi %1 you.. you.. you
	' %3 %3 %3
	' Here take these Items.
	GIVE I1025
	GIVE I1029
	GIVE I1031
	GIVE I5116
	N: You recieved a Rham's Werld Link.
	GIVE I5117
	N: You recieved a Skin Fetcher Link.
	GIVE I5118
	N: You recieved a Dev Pages Link.
	GIVE I5119
	N: You recieved a Forum Link
	N: You recieved a Grave Yard Ticket.
	give I101
	N: You recieved a Riona's Safe Spot Ticket.
	GIVE I1026
	N: You recieved a Hanzo's Lake Ticket.
	N: You recieve one of every Godly Item.
	GIVE	I800
	GIVE	I900
	GIVE	I550
	GIVE	I250
	GIVE	I650
	GIVE	I300
	GIVE	I706
	GIVE	I350
	GIVE	I200
	GIVE	I450
	GIVE	I400
	GIVE	I500
	GIVE	I600
	GIVE	I950
	GIVE	I7
END

SCENE 116 graveyard, WELL, "Well of Death"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15, 80
	' >.>
	STRCMP #<str.soul>, "SOUL 1038"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
	Take	H30000
	H: Why did you kill me?
	' Because you are unauthorised for this link.
	H: You are a %3.
	' Good Bye %1
	H: See ya.
	WAIT 5
	GOTO	EXIT
	END

	@Admin
	' Hi %1
	GOTO @whattodo
	END

	@whattodo
	' What would you like to do?
	' Would you like to play Piano?
	ASK 20
	IF YES,@playpiano
	' Die?
	ASK 20
	IF YES,@timetodie
	' Live?
	ASK	20
	IF	YES,@timetolive
	' Would you like to go to the main Dressing Room?
	ASK 20
	IF	YES,@getdressed
	END

	@playpiano
	WAIT	3
	SET	num.hostClass,	"88"
	WAIT	3
	N: You recieve 50 Lizard Seeds
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	WAIT	4
	' Ally Oop
	WAIT	3
	GOTO LINK	10,3
	END

	@timetodie
	TAKE H30000
	' Would you like to go to the main Dressing Room?
	ASK 20
	IF	YES,@getdressed
	END

	@timetolive
	Give L1
	Give H30000
	Give M30000
	Give G1000000
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	Give I8
	' Would you like to go to the main Dressing Room?
	ASK 20
	IF	YES,@getdressed
	END

	@getdressed
	GOTO LINK	10,3
	END

SCENE 117 rhamsworldscene1,	well,	"Hanzo's Lake"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15,80
	EJECT "T99"
	N: Checking Soul ID
	STRCMP #<str.soul>, "SOUL 1038"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
	' Your Soul Number is not authorised for this location %1.
	H: Why am I not allowed in?
	' I'm sorry to say this testing area is restricted to only a couple people, perhaps they can lead you in.
	H: your a %3.
	' Good Bye %1
	H: See ya.
	WAIT 5
	GOTO	EXIT
	END

	@Admin
	' Hi %1, you.. you.. you
	' %3 %3 %3
	H: Who are you talking to?
	' I am addressing you %1 :P
	H: I bet %4 made you say that.
	' <.<
	' >.>
	WAIT 3
	' Did I mention you were a %3?
	H: Your Pathetic.
	' I was j/k hehe
	' Since your Soul Number identifies you as an Admin (or family/guest), I will let you through.
	' Have fun %1 you %3
	GOTO LINK	10,3
	END

SCENE 118 rhamsworldscene1,	well,	"Chews Wisely"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15,80
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
	' Your Soul Number is not authorised for this location %1.
	H: Why am I not allowed in?
	' I'm sorry to say this testing area is restricted to only a couple people.
	H: your a %3.
	' Good Bye %1
	H: See ya.
	WAIT 5
	GOTO	EXIT
	END

	@Admin
	' I'm Sending you to the Admin's Link Map.
	' See ya %1
	WAIT	3
	GOTO LINK	18,0
	END

;---------
SCENE 119 pkmainscenehighres,	well,	"Riona's Safe Spot"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15,80

	N: Checking Soul ID
	STRCMP #<str.soul>, "SOUL 1038"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
	' Your Soul Number is not authorised for this location %1.
	H: Why am I not allowed in?
	' I'm sorry to say this testing area is restricted to only a couple of Testers, maybe they can lead you in.
	H: your a %3.
	' Good Bye %1
	H: See ya.
	WAIT 5
	GOTO	EXIT
	END

	@Admin
	' Hi %1
	' Your Still a %3.
	H: And your still Pathetic
	' Would you like to go to the Dressing Room?
	ASK 20
	IF	YES,@gotodressingroom
END

@gotodressingroom	
	' Ok, hold on this is gonna take a couple seconds.
	WAIT 10
	' Ok,your Dressing Room is ready for you.
	' Have fun %1 you %3.
	WAIT 3
	GOTO LINK 10, 3
	END

SCENE 101	rhamsworldwellscene,		well	"Healing Well"
	THEME	0
;	MUSIC	waltz2.mid
	ACTOR	1,	"AJ", joshanimals, 5, 25, 90
	POSE	2, 2, 32
	SEL		1
	MOVE	1,	15, 80

	IF T1, @continue
	' Greetings, new soul. You are about to embark upon an amazing voyage of discovery.
	' When sharing your journey with other souls, it is important that you obey the golden rule.
	' Do you agree to obey the golden rule? (type YES or NO)
	ASK 30
	IF -YES, @saidNo
	' Thank you, my child.  You shall never regret your decision.
	' Push the SHOP button when it appears.  You might find better equipment is available for you as your level increases.
	GIVE T1
	GOTO @continue
@saidNo
	' It is a sorry state that the world has fallen into.  You sadden me.
	' I cannot bring myself to give sustenance to such a renegade as you.
	' Begone!
	MOVE	1, -30, 80
	END

@continue

	IF ALIVE, @skip1
	' You seem to be dead, let me resurrect you.
	WAIT    4.0
	SOUND	"thunder1.wav"
	WAIT	1.0
@skip1
	MOVE	1,	10, 75
	GIVE	L1
	GIVE	H30000
	GIVE	M30000 
	IF I99,@skipTicket
	GIVE	I99				; give him a ticket home
@skipTicket
	; offer EVERYTHING under level 10
	OFFER2	0,10
	MOVE	100,	50
	' Blessings upon you, Soul %1.
	MOVE	1,	50,70
	' You are always welcome here, to rest and recover...
	WAIT 1
	MOVE	50,	100, -10
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
END
@Admin
	' Sorry I didn't recognise you.
	' Let's make sure your pockets are full eh.
	N: You and your party recieve 1 Million GP
	GIVE	G1000000
	EJECT
	WAIT	5
	IF -T99, @givetoken
	GIVE	T99
End
@givetoken
	GIVE	T99
	N:	You have been given a Token that identifies you as an Admin.
END


;-------------------------
SCENE 102	pkwellhighres,	well,	"Dolphin Fountain"
	THEME	0
	MUSIC	waltz2.mid
	ACTOR	1,	"AJ", joshanimals, 5, 25, 90
	POSE	2, 2, 32
	SEL		1
	MOVE	1,	15, 80

	IF T1, @continue
	' Greetings, new soul. You are about to embark upon an amazing voyage of discovery.
	' When sharing your journey with other souls, it is important that you obey the golden rule.
	' Do you agree to obey the golden rule? (type YES or NO)
	ASK 30
	IF -YES, @saidNo
	' Thank you, my child.  You shall never regret your decision.
	' Push the SHOP button when it appears.  You might find better equipment is available for you as your level increases.
	GIVE T1
	GOTO @continue
@saidNo
	' It is a sorry state that the world has fallen into.  You sadden me.
	' I cannot bring myself to give sustenance to such a renegade as you.
	' Begone!
	MOVE	1, -30, 80
	END

@continue

	IF ALIVE, @skip1
	' You seem to be dead, let me resurrect you.
	WAIT    4.0
	SOUND	"thunder1.wav"
	WAIT	1.0
@skip1
	MOVE	1,	10, 75
	GIVE	L1
	GIVE	H30000
	GIVE	M30000 
	IF I99,@skipTicket
	GIVE	I99				; give him a ticket home
	' Would you like to go to the Steam Room?
	ASK 20
	IF	YES,@gotosteamroom
@skipTicket
	; offer EVERYTHING under level 10
	OFFER2	0,10
	MOVE	1,	50,50
	' Blessings upon you, Soul %1.
	MOVE	1,	50,70
	' You are always welcome here, to rest and recover...
	WAIT 1
	MOVE	1,	40,80
	' Would you like to go to the Steam Room?
	ASK 20
	IF	YES,@gotosteamroom
	STRCMP #<str.soul>, "SOUL 1113"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1263"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 1297"
	IF= @Admin
	STRCMP #<str.soul>, "SOUL 3467"
	IF= @Admin
END
@Admin
	' Oh it's you %1.
	' Sorry I didn't recognise you.
	' Let's make sure your pockets are full eh.
	N: You and your party recieve 1 Million GP
	GIVE	G1000000
	' Would you like to go to the Steam Room?
	ASK 20
	IF	YES,@gotosteamroom
End

@gotosteamroom
	' Gotta love those Cats, isn't that right %1 ?
	WAIT 2
	' Ok, off ya go. Have fun %1 .
	WAIT 2
	GOTO LINK 10, 0
END

SCENE 150 RWWoSscene, WELL, "GS Perk"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15, 80

	' >.>
	STRCMP #<str.soul>, #<str.soul>
	IF= 	@hellogoldensoul
	' You'll need a Golden Soul to go beyond this point.
END

@hellogoldensoul
	EJECT #<str.soul>, #<str.soul>
	' Hello Golden Soul.
	WAIT 2
	' Would you like to become the Golden Class?
	ASK 20
	IF	YES,@givehiddenclass	
	WAIT 4
	GOTO LINK	10,3
END


@givehiddenclass
	EJECT #<str.soul>, #<str.soul>
	WAIT	3
	SET	num.hostClass,	"87"
	WAIT	3
	' Ally Oop.
	GOTO LINK	10,3
END


SCENE 160 RWUFOCrashSiteCS,	well,	"Alien Mentor"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15,80

	IF T70,	@continue
	GIVE	T70
	GIVE	I1032
	' I just gave you a ticket that you can use to return to this area %1. It's in your item inventory and is called Crash Site.
	' Would you like to go to Isherwood?
	ASK	20
	IF	YES, @sendtomainmap
END


@continue

	' I'm filling in for your trainer until Rham can get off his keister and render a skin for your Mentor.
	' Your Mentor can't project his thoughts without a skin to inhabit. It'd be distasteful.
	' Be careful not to expose who you are when you get to the main map %1 , The humans may try to kill you if they know your intentions.
	' Would you like to go to Isherwood?
	ASK	20
	IF	YES, @sendtomainmap

END

@sendtomainmap
	GOTO LINK	0,0
END


SCENE 161 RWUFOCrashSiteCS,	well,	"Alien Mentor"
	THEME	0
	MUSIC	Jeremy_East.mid
	ACTOR	1,	"Scary Floating Head", rhamsactors, 11, 25, 90
	POSE	1,1
	SEL		1
	MOVE	1,	15,80

	' Hi %1.
	' Would you like to return to the crash site?
	ASK 20
	IF	YES, @sendtoufo
END


@sendtoufo
	EJECT	T70
	' Please make sure your not being followed %1, We would hate to blow our cover before we can obtain the objective.
	WAIT 2
	GOTO LINK	5,0
END

-SCENES

; Now we are at the extreme end of the file
;
; Note: You should add a comment to the end of the file, since otherwise an old bug might rise
; up and smite you!


