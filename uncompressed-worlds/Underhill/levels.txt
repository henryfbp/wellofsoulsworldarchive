;-------------------
; character levels

;----------------- Elf ------------------------------------------

100, 0, 10, 10, "Elf", 10, 0
	DESCRIPTION	"Young Elf that has yet to choose his path. (intro class)"
	MAX_HAND_PP 	5000,0,0,0,0,0,0,0
	START_HAND_PP   0,0,0,0,0,0,0,0
	START_ABILITY   10, 0, 0, 0, 0
	MAGIC_RATIO 	10
	HAND_RATIO 	90
	AUTO_MAX 	10,2000,10,200
	START_LOCATION 1,1
	101,	0,	1,	1,	"Young Elf"

;----------------- Mystic Elf ------------------------------------------
200, 0, 10, 10, "Mystic Elf", 10, 0
	DESCRIPTION	"Young Elfen mage that has yet to choose his allegiance. (intro class)"
	MAX_HAND_PP 	5000,0,0,0,0,0,0,0
	START_HAND_PP   0,0,0,0,0,0,0,0
	START_ABILITY   0, 10, 0, 0, 0
	MAGIC_RATIO 	100
	HAND_RATIO 	0
	AUTO_MAX 	10,2000,10,200
	START_LOCATION 1,1
	201,	0,	1,	1,	"Young Mystic Elf"

;----------------- Mage ------------------------------------------
300, 0, 10, 10, "Mage", 10, 0
	DESCRIPTION	"Young Human mage that needs to choose upon which side to draw his power. (intro class)"
	MAX_HAND_PP 	5000,0,0,0,0,0,0,0
	START_HAND_PP	0,0,0,0,0,0,0,0
	START_ABILITY	0, 10, 0, 0, 0
	MAGIC_RATIO 	90
	HAND_RATIO 	10
	AUTO_MAX 	10,2000,10,200
	START_LOCATION 1,1
	301,	0,	1,	1,	"Aprentice Wizard"

;----------------- Fist ------------------------------------------
400, 0, 10, 10, "Fist", 10, 0
	DESCRIPTION  "Young Human fighter that is undecided on which side to fight. (intro class)"
	MAX_HAND_PP 	5000,0,0,0,0,0,0,0
	START_HAND_PP	0,0,0,0,0,0,0,0
	START_ABILITY	5, 0, 0, 0, 5
	MAGIC_RATIO 	5
	HAND_RATIO 	95
	AUTO_MAX 	10,2000,10,200
	START_LOCATION 1,1
	401,	0,	1,	1,	"Kid with a stick"

;----------------- Elfin Healer ------------------------------------------
500, 0, 10, 8, "Elfin Healer", 50, 2
	DESCRIPTION	"Elves with the gift of healing"
	AUTO_MAX	20, 4000,  10, 2500,  0	 
	START_ABILITY   0,  10,   0,   0,   0
	MAGIC_RATIO 	50
	HAND_RATIO 	50
	START_HAND_PP	3000,5000,0,0,0,0,0,0
	MAX_ABILITY 	240, 245, 250, 250, 220
	MAX_HAND_PP 	5100,70000,0,0,0,0,0,0
	START_ELEMENT_PP 2200,0,0,0,0,0,0,0
	MAX_ELEMENT_PP 	1000000,1300,1300,1300,1300,1300,1300,1300
	START_TOKENS 5,11
	START_ITEMS  950
	START_LOCATION 3,4

	501,	0,	0,	0,	"Bandager"					
	510,	0,	0,	0,	"Medic",			
	550,	0,	0,	0,	"Healer",			
	599,	0,	0,	0,	"Mystic Healer",

;----------------------------- Elfin Lord -----------------------------------------------		
;beta testers only
600, 0, 30, 15, "Elfin Lord", 20, 1
	DESCRIPTION	"A helping hand in this troubled world"
	MAX_ABILITY 	255,250,250,250,250
	START_HAND_PP 	12000,0,0,0,0,0,0,0
	MAX_ELEMENT_PP  12000,4000,4000,4000,4000,4000,4000,4000
	MAX_HAND_PP 	999999,5000,5000,5000,5000,5000,5000,5000
	MAGIC_RATIO 	40
	HIDDEN_CLASS
	HAND_RATIO 	95
	AUTO_MAX 	30, 4000, 15, 1600
	START_ITEMS  950
	START_TOKENS 6,5,400,401,402,403,404

	601,	0,	0,	0,	"Helping Hand"
	699,	0,	0,	0,	"King of Beta's ||Queen of Beta's",

;----------------------------- Enchanter -----------------------------------------------		
700, 0, 18, 2, "Enchanter", 50, 4
	DESCRIPTION	"Not the best fighters, but they have the power to enchant beasts.."
	AUTO_MAX	20, 3100,  10, 1435,  5	
	START_ABILITY   0,  5,   0,   10,   5
	START_HAND_PP	0,0,0,5000,0,0,0,0
	MAX_HAND_PP 	0,5000,5000,999000,0,0,0,0
	MAGIC_RATIO	50
	HAND_RATIO 	40
	MAX_ABILITY 	200, 200, 255, 255, 255
	MAX_ELEMENT_PP	7600,2500,2500,2500,2500,2500,50000,2500
	START_TOKENS 5
	START_ITEMS 950
	START_LOCATION 3,4

	701,	0,	0,	0,	"Soothing Voice"			
	750,	0,	0,	0,	"Calmer of the wild",			
	790,	0,	0,	0,	"Enchanter||Enchantress",

;

;----------------------------- Mystic Guardian -----------------------------------------------		
800, 0, 8, 20, "Mystic Guardian", 100, 1
	DESCRIPTION	"Maker and protector of this world"
	MAX_ABILITY 	255,255,255,255,255
	START_ELEMENT_PP 999000,999000,999000,999000,999000,999000,999000,999000
   	MAX_ELEMENT_PP	999999,999999,999999,999999,999999,999999,999999,999999
	START_HAND_PP	1000000,990000,990000,990000,990000,990000,990000,990000
	MAX_HAND_PP	1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000
	MAGIC_RATIO	100
	HIDDEN_CLASS
	HAND_RATIO	100
	AUTO_MAX	20, 20000,  10, 5000,  0	
	START_ITEMS  950
	START_TOKENS 6, 5,11,400,401,402,403,404

	801,	0,	0,	0,	"Mystic Guardian"			
	890,	0,	0,	0,	"Mystic Guardian",

;------------------------------------------- HACKER ----------------------------------------------------
900, 0, 1, 1,  "Lowlife CHEATER", 1, 0
	MAGIC_RATIO	1
	HAND_RATIO	1
	HIDDEN_CLASS	1
	AUTO_MAX	1, 1, 1, 1
	MAX_ELEMENT_PP 0,0,0,0,0,0,0,0
	MAX_WALLET	100
	901,	0,	0,	0,	"Worthless Scum"

;------------------------------------------- Elfin Blade ----------------------------------------------------
1000, 0, 20, 0, "Elfin Blade", 10, 1

	DESCRIPTION	 "Elfin fighter that has pledged his blade to the Light"
	AUTO_MAX	20, 3600,  10, 500,  5		
	MAGIC_RATIO	10
	HAND_RATIO	90
	START_HAND_PP	5000,0,0,0,0,0,0,0
	MAX_ABILITY	250, 200, 250, 245, 255
	MAX_ELEMENT_PP	5100,0,0,0,0,0,0,0
	MAX_HAND_PP	990000,5100,0,0,0,0,0,0
	HIDDEN_CLASS
	START_TOKENS 8,401

	1001,	0,	0,	0,	"Bungling Blade"
	1010,	0,	0,	0,	"Dull Blade"
	1050,	0,	0,	0,	"Elfin Blade"
	1099,	0,	0,	0,	"Blade of Righteousness"

;------------------------------------------- Dark Elf ----------------------------------------------------
1100, 0, 20, 0, "Dark Elf", 10, 1
	DESCRIPTION	"Renegade Elfin fighter that was suduced by the Darkness"
	AUTO_MAX	20, 3200,  10, 1500,  5		
	MAGIC_RATIO	10
	HAND_RATIO	90
	MAX_ABILITY	255, 200, 255, 240, 240
	START_HAND_PP	5000,0,0,0,0,0,0,0
	MAX_HAND_PP	990000,5100,0,0,0,0,0,0
	MAX_ELEMENT_PP	5000,0,0,0,600,0,0,0
	HIDDEN_CLASS
	START_TOKENS 7,400

	1101,	0,	0,	0,	"Torturer of Bugs"
	1110,	0,	0,	0,	"Tormentor of Kittens"
	1150,	0,	0,	0,	"Decapitater"
	1190,	0,	0,	0,	"Blade of Darkness"

;------------------------------------------- Mystic Light Elf ----------------------------------------------------
1200, 0, 15, 5, "Mystic Light Elf", 100, 8
	DESCRIPTION	"Elfin master of magics that draw from the power of Light"
	AUTO_MAX	15, 3600,  10, 3000,  0	
	MAX_ABILITY	220, 255, 235, 240, 220
	START_HAND_PP	0,0,0,0,0,0,0,5000
	MAX_HAND_PP	0,5100,0,0,0,0,0,990000
	MAX_ELEMENT_PP	5000,990000,1300,1300,1300,1300,1300,990000
	MAGIC_RATIO	100
	HAND_RATIO	0
	HIDDEN_CLASS
	DEFAULT_SKIN	Warlock2, Warlock2, Enchantress, Enchantress
	START_TOKENS 8,11,402

	1201,	0,	0,	0,	"Bungler of spells"
	1210,	0,	0,	0,	"Scribe",			
	1250,	0,	0,	0,	"Sorcerer||Sorceress",			
	1299,	0,	0,	0,	"Mystic Light Elf",

;------------------------------------------- Mystic Dark Elf ----------------------------------------------------
1300, 0, 15, 5, "Mystic Dark Elf", 100, 8
	DESCRIPTION	"Elfin msster of the occult that dabbles in the darker arts"
	AUTO_MAX	15, 3600,  10, 3000,  0	
	MAX_ABILITY	220, 255, 230, 245, 220
	START_HAND_PP	0,0,0,0,0,0,0,5000
	MAX_HAND_PP	0,5100,0,0,0,0,0,990000
	MAX_ELEMENT_PP	5000,1300,1300,990000,1300,990000,1300,1300
	MAGIC_RATIO	100
	HAND_RATIO	0
	HIDDEN_CLASS
	DEFAULT_SKIN	Warlock2, Warlock2, Enchantress, Enchantress
	START_TOKENS 7,11,404

	1301,	0,	0,	0,	"Deviant"
	1310,	0,	0,	0,	"Student",			
	1350,	0,	0,	0,	"Studier of Chaos",			
	1390,	0,	0,	0,	"Mystic Dark Elf",

;------------------------------------------- Wizard ----------------------------------------------------
1400, 0, 15, 5, "Wizard", 90, 7
	DESCRIPTION	"Human mage raised in the Light"
	AUTO_MAX	15, 3300,  10, 2500,  0	
	MAX_ABILITY	220, 250, 240, 240, 220
	START_HAND_PP 	0,0,0,0,0,0,5000,0
	MAX_HAND_PP 	0,5100,0,0,0,0,990000,0
	MAX_ELEMENT_PP	5000,2500,2500,2500,2500,990000,5000,990000
	MAGIC_RATIO	90
	HAND_RATIO	10
	HIDDEN_CLASS
	DEFAULT_SKIN	Warlock2, Warlock2, Enchantress, Enchantress
	START_TOKENS 8,11,460,404

	1401,	0,	0,	0,	"Shaman"
	1410,	0,	0,	0,	"Magician",			
	1450,	0,	0,	0,	"Conjuror",			
	1499,	0,	0,	0,	"Wizard||Wizardess",

;------------------------------------------- Necromancer ----------------------------------------------------
1500, 0, 15, 5, "Necromancer", 90, 7
	DESCRIPTION	"Most Vile of all mages, not even the dead are safe from his powers"
	AUTO_MAX	15, 3300,  10, 2500,  0	
	MAX_ABILITY 	220, 250, 240, 240, 220
	START_HAND_PP 	0,0,0,0,0,0,5000,0
	MAX_HAND_PP 	0,5100,0,0,0,0,990000,0
	MAX_ELEMENT_PP	5000,990000,8000,990000,2500,2500,2500,2500
	MAGIC_RATIO	90
	HAND_RATIO	10
	HIDDEN_CLASS
	DEFAULT_SKIN	Warlock2, Warlock2, Enchantress, Enchantress
	START_TOKENS 7,11,461,402

	1501,	0,	0,	0,	"Dabbler of Death"
	1510,	0,	0,	0,	"Demonologist",			
	1550,	0,	0,	0,	"Necromancer",			
	1599,	0,	0,	0,	"Master of Death",

;------------------------------------------- Noble Warrior ----------------------------------------------------
1600, 0, 20, 0, "Noble Warrior", 5, 5
	DESCRIPTION	"This Human has pledged his fight arm to the power of Light"
	AUTO_MAX  	20, 3549,  10, 1285,  12	
	MAX_ABILITY	255, 180, 250, 235, 250
	MAX_ELEMENT_PP 	5000,0,0,0,1300,0,0,0
	START_HAND_PP   0,0,0,0,5000,0,0,0
	MAX_HAND_PP 	0,5100,14000,0,990000,0,0,0
	MAGIC_RATIO	5
	HAND_RATIO	95
	HIDDEN_CLASS
	DEFAULT_SKIN	Brawler2, Brawler2, Feisty2, Feisty2
	START_TOKENS 8,400

	1601,	0,	0,	0,	"Kid"			
	1610,	0,	0,	0,	"Scrapper",			
	1650,	0,	0,	0,	"Contender",			
	1690,	0,	0,	0,	"Noble Warrior",

;------------------------------------------- Assassin ----------------------------------------------------
1700, 0, 20, 0, "Assassin", 5, 5
	DESCRIPTION	"Human that has sold his fist and Soul to his new Dark masters for a few silvers and the secrets of the assassin's dagger"
	AUTO_MAX	20, 3549,  10, 1285,  12	
	MAX_ABILITY  	255, 180, 255, 230, 250
	START_HAND_PP	0,0,0,0,5000,0,0,0
	MAX_ELEMENT_PP	4000,0,0,0,2500,0,0,0
	MAX_HAND_PP	0,5100,0,0,990000,14000,0,0
	MAGIC_RATIO	5
	HAND_RATIO	95
	HIDDEN_CLASS
	DEFAULT_SKIN	Brawler2, Brawler2, Feisty2, Feisty2
	START_TOKENS 7,401

	1701,	0,	0,	0,	"Punk"			
	1710,	0,	0,	0,	"Bully",			
	1750,	0,	0,	0,	"Bringer of Death",			
	1790,	0,	0,	0,	"Assassin",


;	
; Be sure to leave this comment or a blank line at the very end