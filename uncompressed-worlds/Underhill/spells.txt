;
; SPELLS

;					Damage			// Specialty
;
;						 0			damage computed automatically from level (preferred value if not HEAL spell)
;
;						-1			RESURRECTION		(brings dead to life, with 1 HP)
;
;						-2			DISEASE_POISON		(Makes HP restores ineffective)		
;						-3			DISEASE_SAP			(Makes Spells cost more MP to cast)		
;						-4			DISEASE_STUN		(cannot move, or attack)
;						-5			DISEASE_GAG			(cannot case spells)(* wears off)		
;						-6			DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-7			DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;						-8			DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-9			DISEASE_CHARM		(once charmed, monster fights for you)	
;						-10			DISEASE_TAME		(taming a charmed monster makes it a pet)(NOT IMPLEMENTED YET)	
;						-11			DISEASE_FREEZE		(cannot move)
;						-20			DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-21			DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-22			DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-23			DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-24			DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	
;						-200		SUMMON_SAME			Summon a monster of same type as caster
;						-201		SUMMON_SAME_ELEMENT	Summon a monster of the same element as caster
;						-202		SUMMON_RANDOM		Summon a random monster at or below level of caster (within 10 levels)
;						-203		SUMMON_SPECIFIC		Summon a specific monster (monster id in optional dotted arg)
;						-300		SUMMON_ITEM			Summon an item to be used on the target (item id in dotteg arg)
;											0 = no flags (in case you want to use minPLayerLevel option)
;											1 = monsters/pets cannot use this spell
;											2 = humans are not offered this spell on LEARN SPELL dialog
;											4 = this spell can only be used on yourself
;
;	//	arg7	path;				// path style (9 digit decimal number AAABBBCDD)
;
;					DD	'cloud' shape of magic accumulation
;					00 - tight random cloud
;					01 - looser random cloud
;					02 - loose random cloud
;					03 - single circle
;					04 - two circles
;					05 - single circle - vertical
;					06 - two circles - vertical
;					07 - Big round circle
;					08 - Halo
;					09 - Vertical Halo
;					10 - Horizontal Line
;					11 - Vertical Line
;					12 - Slow Large Dome
;					13 - Fast LArge Dome
;					14 - Slow Small Dome
;					15 - Fast Small Dome
;					16 - increasing to large radius
;					17 - increasing to medium radius
;					18 - decreasing from large radius
;					19 - decreasing from meidum radius
;					20 - Spiraling Outward Circle
;					21 - Spiraling inward Circle
;					22 - horizontal Spiraling Outward 
;					23 - horizontal Spiraling inward 
;					24 - vertical Spiraling Outward 
;					25 - vertical Spiraling inward 
;
;					C	Accumulation of magic around caster, before it 'launches', and when it hits
;					0	- accumulate over caster, land in body of target
;					1	- accumulate within caster, land in body of target
;					2	- accumulate under caster, land in body of target
;					3	- body to body, but starting 128pixels away from target
;					4	- body to body, but starting 128pixels towards target
;					5	- body to body, but starting halfway between caster and target
;					6	- Over the head of both caster and receiver
;					7	- at the feet of both caster and receiver
;		
;					BBB Travel Path
;					000 - straight line
;					001 - looser line
;					002 - looser line
;					003 - loose line
;					004 - slow wavy
;					005 - medium wavy
;					006 - faster wavy
;					007 - fast wavy
;					008 - spiral
;					009 - spiral
;					010 - spiral
;					011 - spiral
;					012 - column at foot of target
;					013 - column at head of target
;					014 - column at top of screen (use gravity)
;					015 - column at bottom of screen (use gravity)
;					016 - column at random between foot and head
;					017 - column at random between foot and top of screen
;					018 - 'ballistic'
;					019 - Shallow 'ballistic'
;
;					AAA Attack Cloud Shape (same options as accumulate cloud shape when > 4)
;					000 - random cloud
;					001	- looser random cloud
;					002 - loose random cloud
;					003 - single circle
;					004 - two circles
;
; For example:   
;
;		004(2circ) + 011(spiral) + 2(under) + 04(2circ) 
;
; would be written:  004011204, or just 4011204
;
;	//	arg8	row; (0-199)		// row of effects table to use
;									// Magical effects are animated from a film-strip of up to 16
;									// images which are played from left to right.  Up to 200 such
;									// film-strips may be defined by the world designer.  Rather than
;									// cram all 200 filmstrips into a single file, they are broken
;									// into groups of ten and placed in files with the name:
;									// "EffectsNN.bmp" where NN runs from 00 to 19 and represents the
;									// first two digits of the effect 'row number' (000-199)
;									// For example, row 45 would be at offset 5 in the file "Effects04.bmp"
;									// Effects files 00 through 09 are stored in the evergreen ART folder
;									// and are reserved for the use of synthetic-reality (feel free to submit
;									// cool effects to be added).  World designers should put their custom
;									// effects into the files "effects10.bmp" through "effects19.bmp" and
;									// place them in the world's private ART folder (this means world designers
;									// control rows 100-199).
; 
;									// effects are 16x16 graphical glyphs stored in a separate
;									// row within the file. Columns represent individual images
;									// of the animation and are generally shown left to right.
;									// It would probably be most useful to open art\effects00.bmp and
;									// look at it for illumination.
;
;	//	arg9	maxCols; (1-31)		// # of columns to use (of the selected row in effects.bmp)
;	//	arg10	maxFx;	(0 - 100)	// # of trails to remember (increases the 'amount' of sparkles)
;	//	arg11	tixPerCol;			// step time between columns (in msec)
;									// controls the speed of the animation through the current row.
;	//	arg12	gravity;			<gravity>.<effects>.<weather>
;									Three 'dotted arguments' specifyng the gravity, special effects, and
;									weather present while the spell is being cast.  The format has changed,
;									but the values are the same as the old style:
;
;									// OLD STYLE ARG12 (pre-A57) was a 9 digit number AAABBBCCC
;									// gravity style controls 'dripping' behaviour(0 is none)
;
;					CCC
;					000	- no gravity
;					001	- magic falls slowly
;					002	- falls faster
;					003	- falls very fast
;					004   - falls way too fast
;					005	- magic RISEs slowly
;					006	- rises faster
;					007	- rises very fast
;					008	- rises way too fast
;
;					BBB
;					xxx	- FX modifier
;		
;					AAA
;					xxx - weather modifier
;			
;	//	arg13	loop;				// loop style (0 is none)
;
; these optional parameters control the SOUND of the spell
;
;	//  arg 14	sfxSummon[80];		// sound of spell being summoned	"summon.wav"
;	//  arg 15	sfxTravel[80];		// sound of spell traveling			"travel.wav"
;	//  arg 16	sfxStrike[80];		// sound of spell striking target	"strike,wav"

;
; SPELL ZERO
;
; There is no real spell zero, this line just sets some percentage tweaks which can be used to adjust the 
; algorithmically derived values.
;
; You can adjust: PP Cost, Damage (painful spells only), and MP Cost
;
	0,	"Algorithm Scale %",100,0,	250,100,0

;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15				16
;	ID	Name				PP	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
; Healing Spells
	1,	Bandaid,			0,	0,	50,	0,	0.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	2,	"Big Bandaid",		0,	0,	100,0,	101.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	3,	Tonic,				0,	0,	200,0,	102.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	4,	Aspirin,			0,	0,	300,0,	3.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	5,	Cure,				0,	0,	500,0,	4.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	6,	"Big Cure",			0,	0,	750,0,	105.0.0.5.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	7,	Heal,				0,	0,	850,0,	6.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	8,	"Big Heal",			0,	0,	1000,0,	107.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	9,	"Healing Touch",	0,	0,	2000,0,	8.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	10,	"Healing Aura",		0,	0,	2000,0,	108.0.0.5.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	11,	Transfusion,		0,	0,	4000,0,	9.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	12,	"Total Transfusion",0,	0,	4000,0,	109.0.0.452.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	13,	Antidote,			0,	0,	-102,0,	0.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	14,	"Full Antidote",	0,	0,	-102,0,	104.0.0.453.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	15,	"Drain Plug",		0,	0,	-103,0,	1.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	16,	"Pry Bar",			0,	0,	-111,0,	2.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	17,	Rage,				0,	0,	-120,0,	6.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	18,	"Roid Rage",		0,	0,	-120,0,	107.0.0.454.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	19,	Thought,			0,	0,	-121,0,	7.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	20,	"Deep Thought",		0,	0,	-121,0,	108.0.0.455.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	21,	Armor,				0,	0,	-122,0,	8.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	22,	"Armor all",		0,	0,	-122,0,	109.0.0.456.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	23,	Dodge,				0,	0,	-123,0,	6.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	24,	"Dodge All",		0,	0,	-123,0,	108.0.0.457.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	25,	"Deadly Aim",		0,	0,	-124,0,	8.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	26,	"Full Aim",			0,	0,	-124,0,	109.0.0.458.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	27,	Freehand,			0,	0,	-106,0,	8.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	28,	FreeHands,			0,	0,	-106,0,	109.0.0.459.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	29,	"Throat Spray",		0,	0,	-105,0,	8.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	30,	"Alarm Clock",		0,	0,	-107,0,	2.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	31,	Sanity,				0,	0,	-108,0,	7.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			
	32,	Rez,				0,	0,	-1,	0,	9.0.0.0.0.0,0,		41,	5,	2,	100,	0,		0,		missionAbandoned.wav,sparklymagic.wav,,			

; Water Spells
	50,	Drip,				0,	1,	0,	0,	0,		,		40,	17,	20,	100,	,		,		,			,			,			
	51,	Sprinkle,			0,	1,	0,	0,	1,		,		40,	17,	20,	100,	,		,		,			,			,			
	52,	"Rain Storm",		0,	1,	0,	0,	102,	,		18,	17,	20,	100,	,		,		,			,			,			
	53,	Sleet,				0,	1,	0,	0,	3,		,		48,	17,	20,	100,	,		,		,			,			,			
	54,	Hail,				0,	1,	0,	0,	4,		,		121,17,	20,	100,	,		,		,			,			,			
	55,	"Snow Storm",		0,	1,	0,	0,	5,		,		16,	17,	20,	100,	,		,		,			,			,			
	56,	Blizard,			0,	1,	0,	0,	106,	,		104,17,	20,	100,	,		,		,			,			,			
	57,	Hurricane,			0,	1,	0,	0,	7,		,		14,	17,	20,	100,	,		,		,			,			,			
	58,	Typhoon,			0,	1,	0,	0,	8,		,		18,	20,	80,	1000,	,		,		,			,			,			
	59,	Tsunamia,			0,	1,	0,	0,	109,	,		40,	17,	20,	100,	,		,		,			,			,			
;	60,
;	61,
;	62,
;	63,
;	64,
;	65,
;	66,
;	67,
;	68,
;	69,
;	70,

; Nature Spells
	100,"Poison Sting",		0,	2,	-2,	99,	4.0.0.0,,		10,	8,	1,	100,	,		,		,			,			,			
	101,Twigs,				0,	2,	0,	0,	0,		,		46,	8,	15,	100,	,		,		,			,			,			
	102,"Twig all",			0,	2,	0,	0,	101,	,		46,	8,	15,	100,	,		,		,			,			,			
	103,Leaf,				0,	2,	0,	0,	2,		,		46,	8,	15,	100,	,		,		,			,			,			
	104,Leaves,				0,	2,	0,	0,	103,	,		46,	8,	15,	100,	,		,		,			,			,			
	105,Vine,				0,	2,	0,	0,	4,		,		46,	8,	15,	100,	,		,		,			,			,			
	106,"Vine Whip",		0,	2,	0,	0,	105,	,		46,	8,	15,	100,	,		,		,			,			,			
	107,Branch,				0,	2,	0,	0,	6,		,		46,	8,	15,	100,	,		,		,			,			,			
	108,Branches,			0,	2,	0,	0,	107,	,		46,	8,	15,	100,	,		,		,			,			,			
	109,"Tree Attack",		0,	2,	0,	0,	8,		,		46,	8,	15,	100,	,		,		,			,			,			
	110,"Forest Attack",	0,	2,	0,	0,	109,	,		46,	17,	25,	150,	,		,		,			,			,			
;	111,
;	112,
;	113,
;	114,
;	115,
;	116,
;	117,
;	118,
;	119,
;	120,


; Earth Spells
	150,Dust,				0,	3,	0,	0,	0,		,		23,	17,	25,	100,	,		,		,			,			,			
	151,Dusty,				0,	3,	0,	0,	101,	,		23,	17,	25,	100,	,		,		,			,			,			
	152,Dirt,				0,	3,	0,	0,	2,		,		23,	17,	25,	100,	,		,		,			,			,			
	153,"Dirt Clod",		0,	3,	0,	0,	103,	,		23,	17,	25,	100,	,		,		,			,			,			
	154,Mud,				0,	3,	0,	0,	4,		,		23,	17,	25,	100,	,		,		,			,			,			
	155,"Mud Slide",		0,	3,	0,	0,	105,	,		23,	17,	25,	100,	,		,		,			,			,			
	156,Rock,				0,	3,	0,	0,	6,		,		23,	17,	25,	100,	,		,		,			,			,			
	157,Boulders,			0,	3,	0,	0,	107,	,		23,	17,	25,	100,	,		,		,			,			,			
	158,Tremor,				0,	3,	0,	0,	8,		,		23,	17,	25,	100,	,		,		,			,			,			
	159,"Earth Quake",		0,	3,	0,	0,	109,	,		23,	17,	25,	100,	,		,		,			,			,			
;	160,
;	161,
;	162,
;	163,
;	164,
;	165,
;	166,
;	167,
;	168,
;	169,
;	170,
; Death Spells	
	200,Boo,				0,	4,	0,	0,	0,		20,		69,	11,	80,	100,	0,		0,		,			,			,			
	201,Spook,				0,	4,	0,	0,	1,		20,		69,	11,	80,	100,	0,		0,		,			,			,			
	202,Specter,			0,	4,	0,	0,	2,		20,		69,	11,	80,	100,	0,		0,		,		,		,
	203,"Deadly Voice",		0,	4,	0,	0,	103,	20,		69,	11,	80,	100,	0,		0,		,			,			,			
	204,"Deadly Touch",		0,	4,	0,	0,	4,		20,		69,	11,	80,	100,	0,		0,		,		,		,
	205,Death,				0,	4,	0,	0,	5,		20,		69,	11,	80,	100,	0,		0,		,		,		,
	206,"Deadly Thought",	0,	4,	0,	0,	106,	20,		69,	11,	80,	100,	0,		0,		,			,			,			
	207,Rot,				0,	4,	0,	0,	7,		20,		69,	11,	80,	100,	0,		0,		,		,		,
	208,"Putrid Chant",		0,	4,	0,	0,	8,		20,		69,	11,	80,	100,	0,		0,		,		,		,
	209,"Deadly Chant",		0,	4,	0,	0,	109.1,	20,		69,	11,	80,	100,	0,		0,		,			,			,			
	210,Unmaker,			2,	4,	999,2,	9.1.0.999,20,		24,	11,	80,	100,	0,		0,		,			,			,			
	211,"Unmake All",		2,	4,	999,4,	109.1.0.999,20,		24,	11,	80,	100,	0,		0,		,			,			,			
	212,Handcuff,			0,	4,	-6,	0,	0.0.0.400,20,		47,	11,	80,	100,	0,		0,		,			,			,			
	213,"Drain Magic",		0,	4,	-3,	0,	0.0.0.404,20,		47,	11,	80,	100,	0,		0,		,			,			,			
	214,"Tongue Tie",		0,	4,	-5,	0,	0.0.0.402,20,		47,	11,	80,	100,	0,		0,		,			,			,			
	215,"Blurried Vision",	0,	4,	-24,0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	216,"Bright Target",	0,	4,	-23,0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	217,"Thin Skin",		0,	4,	-22,0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	218,Nitwit,				0,	4,	-21,0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	219,"Muscle Blaster",	0,	4,	-20,0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	220,Anchor,				5000,4,	-11,0,	0,		20,		47,	11,	80,	100,	0,		0,		,			,			,			
	221,Frighten,			0,	4,	-4,	0,	9.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	222,Handcuffs,			0,	4,	-6,	0,	109,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	223,"Tongue Tied",		0,	4,	-5,	0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	224,"Blurried Vision",	0,	4,	-24,0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	225,"Bright Target",	0,	4,	-23,0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	226,"Thin Skin",		0,	4,	-22,0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	227,Nitwit,				0,	4,	-21,0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
	228,"Muscle Blaster",	0,	4,	-20,0,	109.2,	20,		47,	11,	80,	100,	0,		0,		,			,			,			
; Fire Spells
	250,Match,				0,	5,	0,	0,	,		,		1,	20,	1,	100,	,		,		,			,			,			
	251,Candles,			0,	5,	0,	0,	101,	,		1,	20,	2,	100,	,		,		,			,			,			
	252,Torch,				0,	5,	0,	0,	2,		,		1,	20,	3,	100,	,		,		,			,			,			
	253,Torches,			0,	5,	0,	0,	103,	,		1,	20,	4,	100,	,		,		,			,			,			
	254,Campfire,			0,	5,	0,	0,	4,		,		1,	20,	5,	100,	,		,		,			,			,			
	255,Bonfire,			0,	5,	0,	0,	105,	,		1,	20,	6,	100,	,		,		,			,			,			
	256,Thermite,			0,	5,	0,	0,	6,		,		1,	20,	7,	100,	,		,		,			,			,			
	257,Flamethrower,		0,	5,	0,	0,	107,	,		1,	20,	8,	100,	,		,		,			,			,			
	258,Lava,				0,	5,	0,	0,	8,		,		1,	20,	9,	100,	,		,		,			,			,			
	259,Volcano,			0,	5,	0,	0,	109,	,		31,	20,	10,	100,	,		,		,			,			,			
;	260,
;	261,
;	262,
;	263,
;	264,
;	265,
;	266,
;	267,
;	268,
;	269,
;	270,

; Spirit Spells
	300,Insult,				0,	6,	0,	0,	0,		,		48,	10,	20,	100,	,		,		,			,			,			
	301,Torment,			0,	6,	0,	0,	101,	,		48,	10,	20,	100,	,		,		,			,			,			
	302,Slander,			0,	6,	0,	0,	2,		,		48,	10,	20,	100,	,		,		,			,			,			
	303,Indignify,			0,	6,	0,	0,	103,	,		48,	10,	20,	100,	,		,		,			,			,			
	304,Dispare,			0,	6,	0,	0,	4,		,		48,	10,	20,	100,	,		,		,			,			,			
	305,Decry,				0,	6,	0,	0,	105,	,		48,	10,	20,	100,	,		,		,			,			,			
	306,Anguish,			0,	6,	0,	0,	6,		,		48,	10,	20,	100,	,		,		,			,			,			
	307,Badger,				0,	6,	0,	0,	107,	,		48,	10,	20,	100,	,		,		,			,			,			
	308,Harrass,			0,	6,	0,	0,	8,		,		48,	10,	20,	100,	,		,		,			,			,			
	309,Hector,				0,	6,	0,	0,	109,	,		48,	10,	20,	100,	,		,		,			,			,			
;	310,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	311,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	312,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	313,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	314,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	315,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	316,
;	317,
;	318,
;	319,
	320,Lullaby,			0,	6,	-7,	0,	2,		,		48,	10,	20,	100,	,		,		,			,			,			
	321,"Weaken Shield",	0,	6,	-22,0,	4.0.0.403,,		48,	10,	20,	100,	,		,		,			,			,			
	322,Befriend,			-1,	6,	-10,0,	6.0.0.403,,		59,	4,	15,	100,	,		,		,			,			,			
	323,"Blurred Thought",	0,	6,	-8,	0,	6,		,		48,	10,	20,	100,	,		,		,			,			,			
	324,"Enchanting Melody",-1,	6,	-9,	0,	9,		,		59,	10,	5,	100,	,		,		,			,			,			
;	325,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
;	326,,					0,	6,	0,	0,	,		,		48,	10,	20,	100,	,	
; Wind Spells
	350,Breeze,				0,	7,	0,	0,	0,		,		49,	31,	20,	100,	,		,		,			,			,			
	351,"Full Breeze",		0,	7,	0,	0,	101,	,		49,	31,	20,	100,	,		,		,			,			,			
	352,Gust,				0,	7,	0,	0,	2,		,		49,	31,	20,	100,	,		,		,			,			,			
	353,"Strong Gust",		0,	7,	0,	0,	103,	,		49,	31,	20,	100,	,		,		,			,			,			
	354,Storm,				0,	7,	0,	0,	4,		,		49,	31,	20,	100,	,		,		,			,			,			
	355,"Full Storm",		0,	7,	0,	0,	105,	,		49,	31,	20,	100,	,		,		,			,			,			
	356,Gale,				0,	7,	0,	0,	6,		,		49,	31,	20,	100,	,		,		,			,			,			
	357,"Shear Winds",		0,	7,	0,	0,	107,	,		49,	31,	20,	100,	,		,		,			,			,			
	358,Tornado,			0,	7,	0,	0,	8,		,		49,	31,	20,	100,	,		,		,			,			,			
	359,"Mega Cyclone",		0,	7,	0,	0,	109,	,		49,	31,	20,	100,	,		,		,			,			,			
;	360,
;	361,
;	362,
;	363,
;	364,
;	365,
;	366,
;	367,
;	368,
;	369,
;	370,
	400,"Summon Angel",		-1,	0,	-203.400,0,	0.2,	152,	57,	12,	80,	100,	0,		0,		,			,			,			
	401,"Summon Undead",	-1,	4,	-203.401,0,	0.2,	152,	57,	12,	80,	100,	0,		0,		,			,			,			


; Book Spells
; These are generally spellbound to book-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	 summon.wav	travel.wav	strike.wav
;
	700	"Book Ichi",		-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	701	"Book Ni",			-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	702	"Book San",			-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""
	703	"Book Shi",			-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"chant4.wav",	"",	""
	704	"Book Roku",		-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"chant5.wav",	"",	""
	705	"Book Hachi",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"chant6.wav",	"",	""
	706	"Book Mitsu",		-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"chant7.wav",	"",	""
	707	"Book Chi",			-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	708	"Book Ba",			-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	709	"Book Cho",			-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""

; Summoning Spells
; These are generally spellbound to spirit-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	750	"Summon",			-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	751	"Summon Ash",		-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	752	"Summon Ember",		-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	753	"Summon Coral",		-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	754	"Summon Spirit",	-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	755	"Summon Atmos",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	756	"Summon Chronos",	-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	757	"Summon Pragma",	-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	758	"Summon Arcana",	-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	759	"Summon Ethos",		-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""

;
; Be sure to leave a blank line at the end of this file
