;
; MONSTERS
;				* monsterFlags: (optional)
;				AVAILABLE MONSTER FLAGS: (add together to combine: 3 = 1+2)
;					1 - monster cannot be tamed (cannot capture as a pet)
;					2 - monster can only cast spells in his or her own element (no matter how wise)
;					4 - monster CAN use 'summon' spells, if within element and level restrictions
;                   8 - monster can use magic only (has no physical attack to fall back on)
;				   16 - when picking spells, can ONLY use those from the Arg20 AI command
;                  32 - monster will never flee from battle
;							skin			scale	el	hp	mp	def	off	exp	gld	lev	str	sta	agi	dex	wis	growl.wav	pain.wav	attack path
;	0	1................	2			3		4	5	6	7	8	9	10	11	12	13	14	15	16	17		18		19
;
	0,	"Algorithm Scale %",0,			0,		0,		110,100,110,100,110,80,	0,	100,110,100,100,90,		,						,				,				,				

	1,	"Green Jelly",		josh2,		0.34,	1,		0,	0,	0,	0,	60000,10000,1,	0,	0,	0,	0,	0,		CRACK2.WAV,				pain4.wav,		-1,				,				
	2,	"Archer Fish",		josh11,		0.2,	1,		0,	0,	0,	0,	0,	0,	2,	0,	0,	0,	0,	0,		growl17.wav,			pain7.wav,		-1,				,				
	3,	"Archer Fish",		josh54,		0.2,	1,		0,	0,	0,	0,	0,	0,	3,	0,	0,	0,	0,	0,		growl17.wav,			pain7.wav,		-1,				,				
	4,	"Flesh Crab",		josh15,		0.2,		1,	0,	0,	0,	0,	0,	0,	4,	0,	0,	0,	0,	0,	"growl6.wav",	"pain7.wav",	-1
	5,	"Slobber",			josh20,		0.2,		3,	0,	0,	0,	0,	0,	0,	4,	0,	0,	0,	0,	-1,	"growl3.wav",	"pain10.wav",	-1
	6,	"Pack rat",			rat,		0.2,	3,		0,	0,	0,	1,	2,	0,	1,	1,	0,	0,	1,	0,		rat1.wav,				rat2.wav,		-1,				,				
	7,	"Dust Bunny",		Bunny2,		0.0.9,	2,		0,	0,	0,	1,	2,	0,	1,	1,	0,	0,	1,	0,		rat1.wav,				rat2.wav,		-1,				,				
	8,	Jellyfish,			josh45,		0.2,		1,	0,	0,	0,	0,	0,	0,	7,	0,	0,	0,	0,	-1,		growl8.wav,				pain8.wav,		-1,				,				
	9,	"Gray Widow",		josh74,		0.2,		3,	0,	0,	0,	0,	0,	0,	8,	0,	0,	0,	0,	-1,	"growl6.wav",	"pain2.wav",	-1
	10,	"Giant Flea",		josh87,		0.2,	7,	0,	0,	0,	0,	0,	0,	3,	0,	0,	0,	0,	-1,	"growl17.wav",	"pain8.wav",	-1
	11,	"Devil Crab",		josh15,		0.2.9,	2,		0,	0,	0,	0,	0,	0,	9,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain17.WAV,		-1,				,				
	12,	"Lava Pixie",		Pic008c,	0.2.11,	5,		0,	0,	0,	0,	0,	0,	17,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	13,	"Water Pixie",		Pic008b,	0.2.7,	1,		0,	0,	0,	0,	0,	0,	19,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	14,	"Nature Pixie",		Pic008b,	,		2,		0,	0,	0,	0,	0,	0,	21,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		,				,				
	15,	"Earth Pixie",		Pic008b,	0.2.6,	3,		0,	0,	0,	0,	0,	0,	23,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	16,	"Death Pixie",		Pic008c,	0.3.24.1,4,		0,	0,	0,	0,	0,	0,	25,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	17,	"Spirit Pixie",		Pic008b,	0.2.25,	6,		0,	0,	0,	0,	0,	0,	27,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	18,	"Air Pixie",		Pic008f,	0.0.17,	7,		0,	0,	0,	0,	0,	0,	29,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	19,	"Bed Bug",			162,		0.1,	4,		0,	0,	0,	0,	0,	0,	20,	0,	200,255,0,	0,		growl8.wav,				pain18.WAV,		,				,				
;	20,
	21,	Intruder,			ImpCap,		0.33.0,	4,		0,	0,	0,	0,	0,	0,	40,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain6.wav,		-1,				,				
	22,	Intruder,			paladinswizard,0.33.0,	7,		0,	0,	0,	0,	0,	0,	40,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain6.wav,		-1,				,				
	23,	Kiwi,				josh45,		0.2.5.0,6,		0,	0,	0,	0,	0,	0,	39,	0,	0,	0,	0,	-1,		growl12.wav,			pain5.wav,		-1,				,				
	24,	"Medic Goblin",		Pic059d,	0.3.0,	,		0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	25,	"Water Goblin",		Pic059a,	0.2.4,	1,		0,	0,	0,	0,	0,	0,	32,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	26,	"Goblin of Nature",	Pic059c,	0.2,	2,		0,	0,	0,	0,	0,	0,	34,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	27,	"Dirt Goblin",		Pic059d,	0.2.28,	3,		0,	0,	0,	0,	0,	0,	36,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	28,	"Goblin Undertaker",josh6,		0.3.26.0,4,		0,	0,	0,	0,	0,	0,	38,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	29,	"Flame Goblin",		Pic059e,	0.2.9,	5,		0,	0,	0,	0,	0,	0,	40,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	30,	"Ghost Goblin",		Pic059c,	0.2.25.1,6,		0,	0,	0,	0,	0,	0,	42,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
	31,	"Ventilation Goblin",Pic059a,	0.2.9,	7,		0,	0,	0,	0,	0,	0,	44,	0,	0,	0,	0,	-1,		GROWL24.WAV,			pain2.wav,		,				,				
;	32,
;	33,
;	34,
;	35,
;	36,
	37,	"Earth Spirit",		josh70,		0.2.8,	6,	0,	0,	0,	0,	0,	0,	102,	0,	0,	0,	0,	-1,	growl12.wav,	pain17.WAV,	-1,				
;	38,
;	39,
;	40,
;	41,
	42,	"Blood Wyrm",		josh66,		0.2.2,	5,	0,	0,	0,	0,	0,	0,	111,	0,	0,	0,	0,	-1,	growl12.wav,	pain5.wav,	-1,				
	43,"Blood Wolf",		josh36,		0.2.31,	7,	0,	0,	0,	0,	0,	0,	41,	0,	0,	0,	0,	-1,	growl2.WAV,	pain14.wav,	-1,				
;	44,
;	45,
	46,	"Blister Crab",		josh81,		0.2.9,	5,	0,	0,	0,	0,	0,	0,	93,	0,	0,	0,	0,	-1,	growl12.wav,	pain13.WAV,	-1,				
	47,	"Blaze Wyrm",		josh41,		0.3.31.0,4,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		growl11.wav,			pain15.WAV,		-1,				,				
	48,	Cerberasaurus,		josh43,		0.2.9,	5,	0,	0,	0,	0,	0,	0,	122,	0,	0,	0,	0,	-1,	growl17.wav,	pain8.wav,	-1,				
	49,	Revenge,			pet05,		8.2,	2,		0,	0,	0,	0,	0,	0,	125,0,	0,	0,	0,	-1,		growl11.wav,			pain4.wav,		-1,				,				
	50,	"Bog Shark",		josh12,		0.2,	1,	0,	0,	0,	0,	0,	0,	9,	0,	0,	0,	0,	0,	"growl3.wav",	"pain3.wav",	-1
	51, "Bog Shark",		josh48,		0.2,	1,	0,	0,	0,	0,	0,	0,	9,	0,	0,	0,	0,	0,	"growl3.wav",	"pain3.wav",	-1
;	52,
;	53,
;	54,
;	55,
	56,	"Devil Shell",		josh21,		0.2,	6,	0,	0,	0,	0,	0,	0,	14,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain10.wav",	-1
	57,	"Stingtail",		josh28,		0.2,	3,	0,	0,	0,	0,	0,	0,	15,	0,	0,	0,	0,	0,	"growl20.wav",	"pain8.wav",	-1
	58,	"Goblin",		josh26,		0.2,	6,	0,	0,	0,	0,	0,	0,	16,	0,	0,	0,	0,	-1,	"growl6.wav",	"pain11.wav",	-1
	59,	"Giant Wasp",		josh34,		0.2,	7,	0,	0,	0,	0,	0,	0,	18,	0,	0,	0,	0,	0,	"growl8.wav",	"pain8.wav",	-1
	60,	"Grey Wolf",		josh35,		0.2,	3,	0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	0,	"growl1.wav",	"pain14.wav",	-1
	61,	"Viper",		josh38,		0.2,	5,	0,	0,	0,	0,	0,	0,	22,	0,	0,	0,	0,	0,	"growl12.wav",	"pain12.wav",	-1
;	62,
;	63,
;	64,
;	65,
	66,	"Trickster",		josh77,		0.2,	7,	0,	0,	0,	0,	0,	0,	32,	0,	0,	0,	0,	-1,	"growl15.wav",	"pain11.wav",	-1
;	67,
;	68,
	69,	"Kelp Crab",		josh81,		0.2,		1,		0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	0,		growl12.wav,			pain3.wav,		-1,				,				
; fish
	70,	Guppy,				josh11,		-1,		1,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	71,	Trout,				josh54,		0.2.7,	1,		0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	72,	Piranha,			piranha,	0.2.10,	1,		0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	73,	"Tiger Shark",		josh12,		0.2.32,	1,		0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	74,	"Kermit's Revenge",	65,			0.2.6,	2,		0,	0,	0,	0,	0,	0,	40,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	75,	Flipper,			josh48a,	0.0.7,	1,		0,	0,	0,	0,	0,	0,	60,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	76,	"Sword Fish",		62,			0.0.0,	1,		0,	0,	0,	0,	0,	0,	60,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	77,	Tenticalis,			MSH_Shuma-Gorath,,		1,		0,	0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	78,	"King Crab",		josh81,		1.0.3,	3,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	79,	Leachacuda,			josh61,		1.2.16,	6,		0,	0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	80,	"Neptunes Daughter",nyx,		0.3,	7,		0,	0,	0,	0,	0,	0,	100,0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
	81,	Jaws,				josh48,		2.2.16,	1,		0,	0,	0,	0,	0,	0,	110,0,	0,	0,	0,	-1,		growl8.wav,				pain14.wav,		,				,				
;	82,
;	83,
;	84,
;	85,
;	86,
;	87,
;	88,
;	89,
;	90,
;	91,
;	92,
;	93,
;	94,
;	95,
;	96,
;	97,
;	98,
;	99,
;	100,
;	101,
	102,"Dark Fairy",		josh52,		-5.2.0,	7,		0,	0,	0,	0,	0,	0,	27,	0,	0,	0,	0,	-1,		growl17.wav,			pain8.wav,		-1,				,				
	103,"Devil Imp",		josh6,		0.2,	5,	0,	0,	0,	0,	0,	0,	44,	0,	0,	0,	0,	-1,	"growl4.wav",	"pain11.wav",	-1
	104,"Crocosaur",		josh18,		0.2,	1,	0,	0,	0,	0,	0,	0,	45,	0,	0,	0,	0,	0,	"growl11.wav",	"pain3.wav",	-1
	105,Crocosaur,			josh46,		0.2,		1,		0,	0,	0,	0,	0,	0,	25,	0,	0,	0,	0,	0,		growl11.wav,			pain3.wav,		-1,				,				
	107,"Goblin Warrior",		josh32,		0.2,	6,	0,	0,	0,	0,	0,	0,	48,	0,	0,	0,	0,	-1,	"growl19.wav",	"pain11.wav",	-1
	108,"Snow Wolf",		josh36,		0.2,	6,		0,	0,	0,	0,	0,	0,	56,	0,	0,	0,	0,	0,		growl1.wav,				pain14.wav,		-1,				,				
	109,"Man o' War",		josh45,		0.2,	1,	0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,	"growl21.wav",	"pain2.wav",	-1
	110,"The Man",			josh55,		0.2,	2,	0,	0,	0,	0,	0,	0,	51,	0,	0,	0,	0,	-1,	"growl12.wav",	"pain18.wav",	-1
	111,"Highwayman",		josh56,		0.2,	2,	0,	0,	0,	0,	0,	0,	52,	0,	0,	0,	0,	-1,	"growl22.wav",	"pain16.wav",	-1
	112,"Scurvy Pirate",		josh58,		0.2,	2,	0,	0,	0,	0,	0,	0,	53,	0,	0,	0,	0,	-1,	"growl22.wav",	"pain10.wav",	-1
	113,"Cutthroat",		josh57,		0.2,	2,	0,	0,	0,	0,	0,	0,	54,	0,	0,	0,	0,	-1,	"growl12.wav",	"pain17.wav",	-1
	114,Piker,				josh64,		0.2,	6,		0,	0,	0,	0,	0,	0,	55,	0,	0,	0,	0,	-1,		growl7.wav,				pain15.wav,		-1,				,				
	117,"Fire Cobra",		josh78,		0.2,	5,	0,	0,	0,	0,	0,	0,	58,	0,	0,	0,	0,	-1,	"growl9.wav",	"pain12.wav",	-1
	120,Parasite,			josh83,		0.2,	6,		0,	0,	0,	0,	0,	0,	61,	0,	0,	0,	0,	0,		growl5.wav,				pain13.wav,		-1,				,				
	122,"Elven Amazon",		josh93,		0.2,	2,		0,	0,	0,	0,	0,	0,	72,	0,	0,	0,	0,	-1,		growl22.wav,			pain10.wav,		-1,				,				
	123,"Elven Hunter",		josh92,		0.2,	3,		0,	0,	0,	0,	0,	0,	78,	0,	0,	0,	0,	-1,		growl22.wav,			pain19.wav,		-1,				,				
	124,"Shriek",			josh91,		0.2,	7,	0,	0,	0,	0,	0,	0,	65,	0,	0,	0,	0,	-1,	"growl16.wav",	"pain8.wav",	-1
	127,Pangolin,			josh89,		0.3,	4,		0,	0,	0,	0,	0,	0,	68,	0,	0,	0,	0,	-1,		growl1.wav,				pain5.wav,		-1,				,				
	128,"Wolf Boy",			josh94,		0.2,	2,		0,	0,	0,	0,	0,	0,	89,	0,	0,	0,	0,	-1,		growl21.wav,			pain6.wav,		-1,				,				
	129,"Battle Dwarf",		josh95,		0.2,	6,		0,	0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,		growl12.wav,			pain1.wav,		-1,				,				
	130,"Dwarf Opa",		josh96,		0.2,	2,	0,	0,	0,	0,	0,	0,	71,	0,	0,	0,	0,	-1,	"growl12.wav",	"pain6.wav",	-1
	131,"ShapeShifter",		mirror,		0.3,	6,	0,	0,	0,	0,	0,	0,	71,	0,	0,	0,	0,	-1,	"growl12.wav",	"pain6.wav",	-1
	150,"Water Mangler",		josh13,		0.2,	1,	0,	0,	0,	0,	0,	0,	 72,	0,	0,	0,	0,	-1,	"growl21.wav",	"pain3.wav",	-1
	151,"Water Mangler",		josh47,		0.2,	1,	0,	0,	0,	0,	0,	0,	 73,	0,	0,	0,	0,	-1,	"growl21.wav",	"pain3.wav",	-1
	153,"Flying Rage",		josh8,		0.2,	7,	0,	0,	0,	0,	0,	0,	 74,    0,	0,	0,	0,	-1,	"growl8.wav",	"pain8.wav",	-1
	158,"Goblin Centurion",		josh33,		0.2,	3,	0,	0,	0,	0,	0,	0,  	 80,	0,	0,	0,	0,	-1,	"growl4.wav",	"pain11.wav",	-1
	159,"Carrion Maw",		josh29,		0.2,	7,		0,	0,	0,	0,	0,	0,	100,0,	0,	0,	0,	-1,		growl9.wav,				pain8.wav,		-1,				,				
	161,"Goblin Knight",	josh37,		0.2,	3,		0,	0,	0,	0,	0,	0,	88,	0,	0,	0,	0,	-1,		growl21.wav,			pain11.wav,		-1,				,				
	162,"Bat Clops",		josh40,		0.2,	9,		0,	0,	0,	0,	0,	0,	95,	0,	0,	0,	0,	-1,		growl9.wav,				pain8.wav,		-1,				,				
	163,"Little Devil",		josh51,		0.2,	5,	0,	0,	0,	0,	0,	0,	 85,	0,	0,	0,	0,	-1,	"growl22.wav",	"pain6.wav",	-1
	164,"Dark Angel",		josh52,		0.2,	7,	0,	0,	0,	0,	0,	0,	 86,	0,	0,	0,	0,	-1,	"growl11.wav",	"pain10.wav",	-1
	167,"Gnomix",			josh63,		0.2,	6,	0,	0,	0,	0,	0,	0,	 89,	0,	0,	0,	0,	-1,	"growl15.wav",	"pain11.wav",	-1
	169,"Thrasher",			josh62,		0.2,	3,	0,	0,	0,	0,	0,	0,	 91,	0,	0,	0,	0,	-1,	"growl2.wav",	"pain3.wav",	-1
	170,"Thorn Wyrm",		josh66,		0.2,	5,		0,	0,	0,	0,	0,	0,	99,	0,	0,	0,	0,	-1,		growl23.wav,			pain3.wav,		-1,				,				
	172,"Etherion",			josh70,		0.2,	5,	0,	0,	0,	0,	0,	0,	 94,	0,	0,	0,	0,	-1,	"growl11.wav",	"pain3.wav",	-1
	174,"Goblin Cavalry",	josh53,		0.3,	,		0,	0,	0,	0,	0,	0,	96,	0,	0,	0,	0,	-1,		growl4.wav,				pain11.wav,		-1,				,				
	176,"Forest Dwarf",		josh97,		0.2,	2,	0,	0,	0,	0,	0,	0,	 98,	0,	0,	0,	0,	-1,	"growl21.wav",	"pain6.wav",	-1
	177,"Goblin Catapault",	josh99,		0.0,	10,		0,	0,	0,	0,	60000,0,	115,0,	0,	0,	0,	0,		growl22.wav,			boom2.wav,		32.89.3.0.5,	,				
	202,"Lord Web",			josh60,		0.3,	4,		0,	0,	0,	0,	0,	0,	55,	0,	0,	0,	0,	0,		growl11.wav,			pain9.wav,		-1,				,				
	203,"Harleking",		josh10,		0.3,	6,	0,	0,	0,	0,	0,	0,	60,	0,	0,	0,	0,	-1,	"growl7.wav",	"pain3.wav",	-1
	204,"Moraysaur",		josh14,		0.3,	1,	0,	0,	0,	0,	0,	0,	65,	0,	0,	0,	0,	-1,	"growl3.wav",	"pain9.wav",	-1
	205,"Moraysaur",		josh49,		0.3,	1,	0,	0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,	"growl3.wav",	"pain9.wav",	-1
	206,"Manglord",			josh17,		0.3,	2,	0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,	"growl21.wav",	"pain1.wav",	-1
	207,"Lord Fist",		josh19,		0.3,	3,	0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	-1,	"growl4.wav",	"pain1.wav",	-1
	209,"Ice Wyrm",			josh41,		0.3,	1,	0,	0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,	"growl7.wav",	"pain9.wav",	-1
	210,"Fire Wyrm",		josh42,		0.3,	5,	0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,	"growl11.wav",	"pain9.wav",	-1
	211,"Tri Wyrm",			josh43,		0.3,	2,	0,	0,	0,	0,	0,	0,	100	0,	0,	0,	0,	-1,	"growl2.wav",	"pain3.wav",	-1
	212,Chameloraptor,		josh76,		0.3,	6,		0,	0,	0,	0,	0,	0,	105,0,	0,	0,	0,	-1,		growl2.wav,				pain3.wav,		-1,				,				
	213,"Sand Wyrm",		josh80,		0.3,	3,		0,	0,	0,	0,	0,	0,	110,0,	0,	0,	0,	-1,		growl11.wav,			pain3.wav,		-1,				,				
	241,"Giant Dwarf",		josh95,		4.3,	2,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		growl19.wav,			pain1.wav,		-1,				,				
	242,"Giant Goblin",		josh37,		6.3,	3,	0,	0,	0,	0,	0,	0, 	125,	0,	0,	0,	0,	-1,	"growl19.wav",	"pain1.wav",	-1
	243,"Giant Troll",		josh98,		6.3,	6,	0,	0,	0,	0,	0,	0, 	130,	0,	0,	0,	0,	-1,	"growl19.wav",	"pain1.wav",	-1
	255,"Black Wrath",		josh50,		0.3,	4,	0,	0,	0,	0,	0,	0,	200,	0,	0,	0,	0,	-1,	"growl2.wav",	"pain9.wav",	-1
;  specials
	256,"Cheat Stopper",		Cheater,	4.3,	14,	6666,	9999,	6666,	6666,	-2,	-2,	666,	255,	255,	255,	255,	0,	petDead.WAV,	"pain9.wav",	-1	"use #0",		
	257,"Yo Momma",			Chaos&FuryMod,	4.3,	14,	6666,	9999,	6666,	6666,	-2,	-2,	666,	255,	255,	255,	255,	0,	fleeFailed.wav,	fleeFailed.wav,	-1,	"use #0",		
	302,"Light Pixie",		"light pixie",0.35,	0.light,0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	303,"Dark Pixie",		GothNymph,	0.35,	0.dark,	0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	304,"Light Pixie",		"light pixie",0.35,	0.light,0,	0,	0,	0,	0,	0,	100,0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	305,"Dark Pixie",		GothNymph,	0.35,	0.dark,	0,	0,	0,	0,	0,	0,	100,0,	0,	0,	0,	-1,		growl16.wav,			pain10.wav,		-1,				,				
	350,"Rogue Healer",		human99,	0.1.8,	,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	-1,		,						,				,				,				
	351,"Rogue Enchanter",	paladinswizard,0.1.13,	6,		0,	0,	0,	0,	0,	0,	82,	0,	0,	0,	0,	-1,		,						,				,				,				
	352,"Rogue Blade",		Pic053b,	0.1.0,	10,		0,	0,	0,	0,	0,	0,	84,	0,	0,	0,	0,	0,		,						,				-1,				,				
	353,"Rogue Dark Elf",	Pic053d,	0.1.0,	10,		0,	0,	0,	0,	0,	0,	86,	0,	0,	0,	0,	0,		,						,				,				,				
	354,"Rogue Mystic Light Elf",majo3,		0.1.7,	1,		0,	0,	0,	0,	0,	0,	88,	0,	0,	0,	0,	-1,		,						,				,				,				
	355,"Rogue Mystic Dark Elf",majo3,		0.1.9,	5,		0,	0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,		,						,				,				,				
	356,"Rogue Wizard",		a93,		0.5.16,	7,		0,	0,	0,	0,	0,	0,	92,	0,	0,	0,	150,-1,		,						,				,				"259,0,309,0,359,0,400,0,358",
	357,"Rogue Necromancer",Atsumori,	0.5.17,	4,		0,	0,	0,	0,	0,	0,	94,	0,	0,	0,	150,-1,		,						,				,				"159,0,59,0,110,0,401,0,158",
	358,"Rogue Warrior",	a40,		0.1.0,	3,		0,	0,	0,	0,	0,	0,	96,	0,	0,	0,	0,	0,		,						,				,				,				
	359,"Rogue Assassin",	a28,		0.1.0,	2,		0,	0,	0,	0,	0,	0,	98,	0,	0,	0,	0,	0,		,						,				,				,				
	360,"Ghostly Healer",	human99,	0.2.10.1,,		0,	0,	0,	0,	0,	0,	100,0,	0,	0,	0,	-1,		,						,				,				,				
	361,"Ghost Enchanter",	paladinswizard,0.0.12.1,6,		0,	0,	0,	0,	0,	0,	102,0,	0,	0,	0,	-1,		,						,				,				,				
	362,"Ghostly Blade",	Pic053b,	0.0.0.1,10,		0,	0,	0,	0,	0,	0,	104,0,	0,	0,	0,	0,		,						,				-1,				,				
	363,"Dark Elf's Spirit",Pic053d,	0.0.0.1,10,		0,	0,	0,	0,	0,	0,	106,0,	0,	0,	0,	0,		,						,				,				,				
	364,"Haunting Light Elf",majo3,		0.0.7.1,1,		0,	0,	0,	0,	0,	0,	108,0,	0,	0,	0,	-1,		,						,				,				,				
	365,"Ghost  Dark Elf",	majo3,		0.0.9.0,5,		0,	0,	0,	0,	0,	0,	110,0,	0,	0,	0,	-1,		,						,				,				,				
	366,"Wizard Spirit",	a93,		0.4.16.1,7,		0,	0,	0,	0,	0,	0,	112,0,	0,	0,	200,-1,		,						,				,				"259,309,359,400,0,358",
	367,"Failed Necromancer",Atsumori,	0.4.17,	4,		0,	0,	0,	0,	0,	0,	114,0,	0,	0,	200,-1,		,						,				,				"159,59,110,401,0,158",
	368,"Warrior's Ghost",	a40,		0.0.13.1,3,		0,	0,	0,	0,	0,	0,	116,0,	0,	0,	0,	0,		,						,				,				,				
	369,"Ghost Assassin",	a28,		0.0.23.1,2,		0,	0,	0,	0,	0,	0,	118,0,	0,	0,	0,	0,		,						,				,				,				

	400,Angel,				f,			0.3.23,	0.Light,1500,0,	0,	200,0,	0,	80,	0,	0,	0,	0,	0,		growl8.wav,				pain10.wav,		,				"Bite s",		
	401,Undead,				josh84,		0.3.0,	4.dark,	1500,0,	0,	200,0,	0,	80,	0,	0,	0,	0,	0,		growl11.wav,			pain15.WAV,		,				"Bite s",		
	500,"Undead Guardian",	DarkMage,	0.0.6,	,		0,	0,	0,	0,	0,	0,	165,0,	0,	0,	0,	-1,		,						,				,				,				
	501,Undead,				josh85,		0.0.0,	4,		0,	0,	0,	0,	0,	0,	130,0,	0,	0,	0,	-1,		,						,				,				,				
	600,"Elfin Healer",		human99,	0.2.10,	,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		,						,				,				"17,19,21,23,25,12",
	601,Enchanter,			paladinswizard,0.0.12,	,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		,						,				,				,				
	602,"Elfin Blade",		Pic053b,	0.0.0,	10,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	0,		,						,				-1,				,				
	603,"Dark Elf",			Pic053d,	0.0.0,	10,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	0,		,						,				,				,				
	604,"Mystic Light Elf",	majo3,		0.0.7,	1,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		,						,				,				,				
	605,"Mystic Dark Elf",	majo3,		0.0.9,	5,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	-1,		,						,				,				,				
	606,Wizard,				a93,		0.4.16,	7,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	250,-1,		,						,				,				"400,259,309,359,400,0,358",
	607,Necromancer,		Atsumori,	0.4.17,	4,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	250,-1,		,						,				,				"401,159,59,110,401,0,158",
	608,Warrior,			a40,		0.0.0,	11,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	0,		,						,				,				,				
	609,Assassin,			a28,		0.0.0,	11,		0,	0,	0,	0,	0,	0,	120,0,	0,	0,	0,	0,		,						,				,				,				
	610,Guard,				josh55,		0.0.28,	11,		0,	0,	0,	0,	0,	0,	110,0,	0,	0,	0,	0,		,						,				,				,				
	611,"Town's folk"
	612,Armorer,			josh37,		,		10,		0,	0,	1500,400,0,	0,	110,0,	0,	0,	0,	0,		,						,				,				,				
	613,Smithy,				josh96,		,		11,		0,	0,	0,	700,0,	0,	110,0,	0,	0,	0,	0,		,						,				,				,				
	614,"Enchanter's pet Sparky",josh70,		0.0.13,	5,		0,	0,	0,	0,	0,	0,	99,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				"bite #0",		

	1000,"Stinger",			pet04,		0.2,	1,	0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
	1001,"Grubber",			pet02,		0.2,	6,	0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
	1002,"Light Pixie",		"light pixie",-3.2,	0.light,0,	0,	0,	0,	0,	0,	1,	10,	25,	30,	25,	40,		growl16.wav,			pain10.wav,		-1,				,				
	1003,"Dark Pixie",		GothNymph,	-3.2,	0.dark,	0,	0,	0,	0,	0,	0,	1,	10,	25,	30,	25,	40,		growl16.wav,			pain10.wav,		-1,				,				
;	1004,"Leghorn",			petKDH,		0.2,	7,	0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
;	1005,"Bantam",			petKDH2,	0.2,	7,	0,	0,	0,	0,	0,	0,	25,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
;	1006,"Bad Andy",		petKDrH,	0.2,	7,	0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
;	1007,"Mean Comb",		petKDrH2,	0.2,	7,	0,	0,	0,	0,	0,	0,	35,	0,	0,	0,	0,	-1,	"growl5.wav",	"pain9.wav",	-1
	1008,"Death ",			mirror,		0.18.0,	4,		32000,32000,32000,0,	0,	0,	80,	0,	255,0,	0,	255,	growl5.wav,				pain9.wav,		-1,				"use #213,215,216,0,217,218,219,0",
	1009,Life,				dove,		0.2.18,	0,		32000,32000,32000,0,	0,	0,	80,	0,	255,0,	0,	255,	growl5.wav,				pain9.wav,		-1,				,				

; these are quest-prize pets, unavailable elsewhere
	2000,Rocky,				rocky,		0.0.13.0,3,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl9.wav,				pain4.wav,		-1,				,				
	2001,Falcon,				226,		0.0.0,	7,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				,				
	2002,Sparky,				josh70,		0.0.13,	5,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				,				
	2003,Nessy,				josh14,		0.0.28,	1,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				,				
	2004,Venus,				josh71,		0,		2,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				,				
	2005,Draco,				josh41,		0.0.0.0,6,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	0,		growl5.wav,				pain9.wav,		-1,				,				
	2006,"Death ",			mirror,		0.18.0,	4,		0,	20000,10000,0,	0,	0,	1,	0,	0,	0,	0,	255,	growl5.wav,				pain9.wav,		-1,				"use #213,215,216,0,217,218,219,0",
	2007,Life,				dove,		0.2.18,	0,		0,	20000,10000,0,	0,	0,	1,	0,	0,	0,	0,	255,	growl5.wav,				pain9.wav,		-1,				,				
	2008,"Death ",			mirror,	0.18.0,	4,		0,	32000,0,	0,	0,	0,	100,0,	0,	0,	0,	255,	growl5.wav,				pain9.wav,		-1,				"use #31, 40, 41, 0, 42, 43, 45, 46",
	2009,Life,				dove,		0.2.18,	0,		0,	32000,0,	0,	0,	0,	100,0,	0,	0,	0,	255,	growl5.wav,				pain9.wav,		-1,				,				

; Be sure to leave a blank line at the end of this file

