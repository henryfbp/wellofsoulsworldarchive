SCENE 9, DarkLightTower, SCENE, "DarkLightTower"
	IF T12, @HasT12
	H: The door is locked.
	H: It looks like a skeleton shaped keyhole.
END
@HasT12
	IF T15, @HasT15
	H: Hey, the skeleton key fits!
	H: I can finally enter!
	GIVE T15
	GOTO LINK 4, 0, 0
END
@HasT15
	GOTO LINK 4, 0, 0
END
;------------
SCENE 24, DarkLightTower, SCENE, "Floor2"
	ACTOR 1, "Phoenix", DLTprotector1, 1, 25, 90
	IF T20, @HasT20
	H: Can I continue through?
	1: Yes, But only in one condition.
	1: You need to defeat me in battle.
	WAIT 1.0
	MOVE 1, -50, 50, 1
	WAIT 1.0
	MUSIC Miniboss4.mid
	FIGHT 56
	IF Won, @Won
	ACTOR 1, "Phoenix", DLTprotector1, 1, 25, 90
	1: Im sorry, but I cannot let you through.
	1: You are not worthy enough.
END
@Won
	ACTOR 1, "Phoenix", DLTprotector1, 1, 25, 90
	1: Ah, Finally, Defeat..
	1: You are worthy enough to enter through.
	1: Here, take this key.
	GIVE T20
	1: Now you can pass whenever you like...
	WAIT 1.0
	GOTO LINK 5, 0, 0
END
@HasT20 
	GOTO LINK 5, 0, 0
END
;-------------
SCENE 25, DarkLightTower, SCENE, "Floor3"
	ACTOR 1, "DarkWaterBeast", DLTprotector2, 1, 25, 90
	IF T21, @HasT21
	H: Can I continue through?
	1: No you cannot.
	1: Only way I will let you through is if you have a pure heart.
	H: How am I going to prove that?
	1: If you defeat someone evil, like me!
	WAIT 1.0
	MOVE 1, -50, 50, 1
	MUIC Miniboss4.mid
	WAIT 1.0
	FIGHT 58
	IF Won, @Won
	ACTOR 1, "DarkWaterBeast", DLTprotector2, 1, 25, 90
	1: Your heart isnt pure enough.
	1: Return when it is.
END
@Won
	ACTOR 1, "DarkWaterBeast", DLTprotector2, 1, 25, 90
	GIVE T21
	1: I see..
	1: Your heart is pure, You may pass.
	WAIT 1.0
	GOTO LINK 6, 0, 0
END
@HasT21
	GOTO LINK 6, 0, 0
END
;------------
SCENE 26, DarkLightTower, SCENE, "Floor4"
	ACTOR 1, "Demonic Bird", DLTprotector3, 1, 25, 90
	IF T22, @HasT22
	H: Can I pass through.
	1: Only if you're as swift as a cheetah.
	1: You need to defeat me and live.
	WAIT 1.0
	MOVE 1, -50, 50, 1, 
	WAIT 1.0
	MUSIC miniboss5.mid
	FIGHT 59
	IF Won, @Won
	ACTOR 1, "Demonic Bird", DLTprotector3, 1, 25, 90
	1: You are not fast enough.
	1: Return to me later.
END
@Won
	ACTOR 1, "Demonic Bird", DLTprotector3, 1, 25, 90
	GIVE T22
	1: I see, You are a quick one.
	1: Here, I will let you pass through.
	WAIT 1.0
	GOTO LINK  7, 0, 0

END
@HasT22
	GOTO LINK 7, 0, 0
END
;-------------
SCENE 27, DarkLightTower, SCENE, "Treasure Room"
	ACTOR 1, "Scarlet", DLTProtector4, 1, 25, 90
	IF T23, @HasT23
	1: Welcome, Traveler.
	1: I was never expecting anyone to get this far.
	1: It seems like you are a strong one indeed.
	H: Thank you.
	1: Beyond me is the Treasure room.
	1: If you defeat me, you may enter.
	1: But, I am going to be your worst nightmare!	
	1: Prepare yourself!
	WAIT 2.0
	MOVE 1, -50, 50, 1
	MUSIC Miniboss5.mid
	WAIT 2.0
	FIGHT 60
	IF Won, @Won	
	ACTOR 1, "Scarlet", DLTProtector4, 1, 25, 90
	1: Heh, Nice try, but next time.. try harder.
END
@Won
	ACTOR 1, "Scarlet", DLTProtector4, 1, 25, 90
	1: You.. You are a strong one..
	1: Beyond me lays the Unknown Teleporter.
	1: Here take this, Its the key to DarkSpine Rocks.
	1: Where the dragon lays.
	GIVE T23
	1: You may pass.
	WAIT 1.0
	GOTO LINK 8, 0, 0
END
@HasT23
	GOTO LINK 8, 0, 0
END
;--------------
SCENE 28, Black, SCENE, "???"
	IF T24, @HasT24
	H: I found 10000 gold!
	GIVE T24
	GIVE G10000
END
@HasT24
	H: Nothing is here anymore..
END
;-------------
SCENE 29, Black, SCENE, "???"
	IF T25, @HasT25
	N: You found a pair of soul boots!
	GIVE T25
	GIVE I190
END
@HasT25
	H: Nothing is here anymore.
END
;-------------
SCENE 30, Black, SCENE, "???"
	IF T26, @HasT26
	N: You found a Soul Ring!
	GIVE T26
	GIVE I192
END
@HasT26
	H: Nothing is here anymore.
END
;------------
SCENE 31, Black, SCENE, "Unknown Teleporter"
	IF T27, @HasT27
	H: Hey the unknown teleporter.
	H: To bad I dont have a Dragon Scale.
	H: Maybe Jason from EredaneVillage knows where I can get one.
	GIVE T27
END
@HasT27
	IF T40, @HasT40
	H: Jason should know where I can get one.
END
@HasT40
	IF T44, @HasT44
	H: Ok, all that I needed was a Dragon Scale..
	H: Time to figure what to do now..	
	H: Hm.... What if I were to put the scale on the teleporter..	
	WAIT 3.0
	N: You put the scale on the teleporter.
	WAIT 3.0
	N: The teleporter starts to glow dimly.
	WAIT 1.0
	N: The teleporter starts glowing brightly.
	WAIT 2.0
	N: The Dragon Scale starts to hover in the air.
	WAIT 1.0
	H: Huh, Seems like the Dragon Scale is the powersource of the teleporter.
	H: Well..... Time to pursue the rest of my adventures!
	WAIT 2.0
	GIVE T44
	GOTO LINK 11,0,0
END
@HasT44
	GOTO LINK 11,0,0
END
	
	
	
	