; Ambrose's fleas.


SCENE 33 bgstage
IF T34, GOTO SCENE 300

	ACTOR	1,	"Ambrose",	ambrose,  1
	POSE	1
 
IF T34, @hasT34
IF T33, @hasT33
IF T32, @hasT10
IF T10, @hasT10
IF T31, @hasT31
If T4, @hasT4
IF T30, @hasT4
IF T29, @hasT29

	1: I'm having a little problem. 
	1: I've got a slight infestation!
	1: Nasty little things and very annoying.
	1: Will you get rid of them for me please?
ASK 30
IF -YES, @saidNo
GOTO SCENE 34
END

@hasT29
	1: Thank you.
	1: Maybe this will help someone as brave as you!
GIVE T30
GIVE I8
END


@hasT4
	1: I'm having a little problem. 
	1: I've got a slight infestation!
	1: Nasty little things and very annoying.
	1: Will you get rid of them for me please?
ASK 30
IF -YES, @saidNo
GOTO SCENE 35
END


@hasT47
GOTO SCENE 300
END

	
@saidNo 
	1: I'll need to get the exterminator in then!
	1: That'll cost..
	1: I'll need to forget my new crystal tea service that I was going to buy!
END

@hasT10
	1: I'm having a little problem. 
	1: I've got a slight infestation!
	1: Nasty little things and very annoying.
	1: Will you get rid of them for me please?
ASK 30
IF -YES, @saidNo
GOTO SCENE 36
END

@hasT31
	1: Thank you.
	1: Maybe this will help someone as brave as you!
GIVE T32
GIVE I8
END

@hasT33
	1: Thank you.
	1: I think I built my house in the wrong place.
	1: I'll move away..somewhere nice and sunny.
	1: Maybe this will help someone as brave as you, come and visit me in my new home.
GIVE T34
GIVE I8
END


SCENE 34
	THEME
;	MUSIC  mortal.mid
	FIGHT 10,10,10,10,10,10,10,10,10,10,10,10

IF	WIN	@heWon1

END

@heWon1
GIVE T29
GOTO SCENE 33
End

SCENE 35
	THEME
;	MUSIC  mortal.mid
	FIGHT 9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9

IF	WIN	@heWon2

END

@heWon2
GIVE T31
GOTO SCENE 33
End

SCENE 36
	THEME
;	MUSIC  mortal.mid
	FIGHT 279,279,279,279,279,279,279,279,279,279,279
IF	WIN	@heWon2

END

@heWon2
GIVE T33
GOTO SCENE 33
End

SCENE 300 bgstage
	ACTOR	1,	"Ambrose",	ambrose,  1
	POSE	1

IF T48, @hasT48
IF T47, @hasT47
	1: I'm having a little problem. 
	1: I've got a slight infestation!
	1: Nasty little things and very annoying.
	1: Will you get rid of them for me please?
ASK 30
IF -YES, @saidNo
GOTO SCENE 301
END

@saidNo 
	1: Well I give up.
	1: I think I built my house in the wrong place.
	1: I'll move away..somewhere nice and sunny.
GIVE T48
END




@hasT47
	1: Thank you.
	1: Maybe this will help someone as brave as you!
	1: It's my life savings.
	1: I think I built my house in the wrong place.
	1: I'll  sell up and move away..somewhere nice and sunny.
	1: Thanks anyway.	
GIVE T48
GIVE G50000
END

SCENE 301
	THEME
;	MUSIC  mortal.mid
	FIGHT 13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13

IF	WIN	@heWon

END

@heWon
GIVE T47
GOTO SCENE 300
End

SCENE 302 bgstage
IF T34, GOTO SCENE 300
@hasT34
IF T47 @hasT47
	1: I'm having a little problem. 
	1: I've got a slight infestation!
	1: Nasty little things and very annoying.
	1: Will you get rid of them for me please?
ASK 30
IF -YES, @saidNo
GOTO SCENE 301
END


SCENE 303 bgstage
	ACTOR	1,	"Ambrose",	ambrose,  1
	POSE	1
IF T303, @hasT303
	1: I've moved here, the weather's better.
	1: But my problems continue.
	1: I just DON'T like all these creepy crawlies.
	1: Will you offer me aid again?
WAIT 5
IF -YES, @saidNo
GOTO SCENE 304
END

@saidNo
 	1: Can't say I blame you.
END

@hasT303
	1: Hi %1.
	1: With your help I can zap any nasties myself now.
	1: So I don't need to keep asking strangers to help me.
END

SCENE 304
ACTOR	1,	"Ambrose",	ambrose,  1
	POSE	1
	THEME
;	MUSIC  mortal.mid
	FIGHT 202,202,202,202,202,202
IF	WIN	@heWon
	FIGHT 202,202,202,202,202,202,202
	FIGHT 202,202,202,202,202,202,202,202.202
	FIGHT 202,202,202,202,202,202,202,202.202,202,202
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202,202,202
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202,202,202,202,202
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202,202,202,202,202,202,202,154
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202,202,202,202,154,154,154
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,202,202,154,154,154,154,154,154,154
	FIGHT 202,202,202,202,202,202,202,202.202,202,202,202,202,154,154,154,154,154,154,154,154,154,154
	FIGHT 202,202,202,202,202,202,202,202.202,202,154,154,154,154,154,154,154,154,154,154,154,154,154,154
	FIGHT 202,202,202,202,202,202,202,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154
	FIGHT 154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154,154

END

@heWon
IF	C7 @isC7
END

@isC7
	1: Thank you.
	1: I have one last request.
	1: Someone sold me this VERY expesive insect repellant
	1: but the instructions are very complicated and I can't read.
	1: You look like the learned sort..
	1: so if you'd just read them to me All my problems would be solved.
	1: Thank you very much.
	1: here are some items to show my gratitude.
GIVE I8
GIVE I27
GIVE T303
END


