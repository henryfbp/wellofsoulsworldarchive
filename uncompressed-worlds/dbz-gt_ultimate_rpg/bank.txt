SCENE 405 bank, SCENE, "Bank"
ACTOR 1, "Bank Clerk", mis, 9, 37, 74
POSE 10, 9, 10
1: Welcome to the bank of the DBZ-GT Ultimate RPG World, %1.
1: This is our Account and Saveings Section.
COMPARE #<a>, "1"
IF= @normal
1: Here, you can deposit any amount of money.
SET money, "0"
SET a, "0"
1: When you enter the bank after this, you will have a few choices.
1: The first is to deposit.  You mearly click "Deposit" To give money to the bank.
1: The second option is to "Withdraw".  This allows you to take money *out* of the bank.
SET a, "1"
@normal
1: Your current balance is #<money> Zenny.
1: Right click me to bring this menu back up at any time.
@eventActorClick1
IF -GS, @nogss
MENU "Deposit=@deposit","Withdraw=@withdraw","Exit=@exit"
END
@deposit
1: How much money would you like to deposit? (Enter the amount)
SET deposit, "0"
ASK 9999999
SET deposit, "#<lastAsk>"
IF G#<deposit>, @continued
1: You do not have that much money.
GOTO @deposit
END
@continued
1: You would like to deposit #<deposit>? (YES/NO)
IF NO, @no
IF -G#<deposit>, @nogo
1: Okay, please wait a moment..
WAIT 2.5
SOUND Kaching.wav
TAKE G#<deposit>
ADD money, "#<deposit>"
1: Thank you for your deposit.
GOTO @normal
END
@withdraw
1: How much money would you like to withdraw? (Enter the amount)
SET withdraw, "0"
ASK 99999
SET withdraw, "#<lastAsk>"
COMPARE #<money>, #<withdraw>
IF> @continuew
IF= @continuew
1: You do not have that much money deposited.  Your current balance is #<money>.
GOTO @withdraw
END
@continuew
1: You would like to withdraw #<withdraw> Zenny? (YES/NO)
IF No, @no
1: Okay, one moment please.
GIVE G#<withdraw>
SUB money, #<withdraw>
1: Thank you for using our bank!
GOTO @normal
END 
@no
1: No? Okay.
GOTO @normal
END
@nogo
1: You no longer have enough money.
GOTO @normal
END
@exit
1: Thank you for using the bank.
END

;
;-----
;

SCENE 406 bank, SCENE, "Bank"
ACTOR 1, "Bank Clerk", mis, 9, 37, 74
POSE 10, 9, 10
IF -I106,@NOKEY
COMPARE #<num.peopleInScene>, "1" ; Won't allow more than one person.
IF>,@MOB
COMPARE #<PY>,#<num.timeYear> ; If year has changed since last payment, player must pay rent (PY=year of last payment).
IF<,@RENT
COMPARE #<PM>,#<num.timeMonth> ; If month has changed since last payment, player must pay rent (PM=month of last payment).
IF<,@RENT
GOTO @ROOM ; I106 is the BOX KEY. @ROOM is the players box.
@NOKEY 
1: Hello Welcome to the DBZ-GT Ultimate RPG World Bank.
1: This is the Storge ficility, here you can stor you items!
IF -V50,@SORRY ; Player must be level 50 to purchase a key.
1: You can percuse a Box for a small opening fee.
1: And a small monthly fee 
1: I dont supose you'd be interested in it?
ASK 9999
IF YES,@BUYKEY
1: No? are you sure?
1: well ok,
1: Well come back if you ever want to open an account.
END
@BUYKEY
1: It costs five thousand Zenny up front.
1: That doesn't include monthly rent.
1: As soon as you pay, I'll hand the key over to you.
1: So, can you pay now?
ASK 9999
IF YES,@CHECK
@CHECK
IF G5000,@KEY
1: I'm sorry, but you don't have enough money.
1: I said five thousand Zenny.
1: Come back when you can afford it.
@KEY
TAKE G5000
GIVE I106
IF -V50,@END
SET PY,#<num.timeYear>
SET PM,#<num.timeMonth>
1: Ok, let me put all your info into the computer.
WAIT 2.5
SOUND Kaching.wav
1: ok there all done, Now let me show you your new deposit Box
WAIT 2
SET EXP,"1"
GOTO @ROOM
@MOB
ACTOR 1, "Bank Clerk", mis, 9, 37, 74
POSE 10, 9, 10
1: I'm sorry, but you all have to come back later.
1: Every one is on a break and i cant help you all by myself
1: Now if there were only one of you, there wouldn't be a problem...
END
@SORRY
1: Im sorry
1: I'm afraid we have no Boxes for Rent at the moment.
END
@END
TAKE G500
TAKE H10000
END
@RENT
ACTOR 1, "Bank Clerk", mis, 9, 37, 74
POSE 10, 9, 10
SET YG,#<num.timeYear>; This stuff calculates the rent due. (rent = <months since last payment>*<monthly payment> )
SUB YG,#<PY>
MUL YG,"12"
1: Hello, %1!
SET MG,#<num.timeMonth>
SUB MG,#<PM>
ADD MG,#<YG>
SET MP,#<MG>
MUL MP,"500"
1: You owe #<MG> months rent!
1: That's #<MP> Zenny.
1: Can you pay now?
ASK 9000
IF YES,@OH
1: Well, too bad.
1: You have to pay as soon as posible or you will lose your Box.
END
@OH
IF G#<MP>,@PAY ; Checks if player has enough money.
1: Well im sorry %1 if you dont have the money
1: But that doesnt mean you can lie to me.
1: Come back when you can pay your rent!
END
@PAY
TAKE G#<MP>
SET PY,#<num.timeYear>
SET PM,#<num.timeMonth>
1: Thankyou, %1
WAIT 3
; The player's apartment.

@ROOM
ACTOR 2,"Box", mis, 8,45,97
COMPARE #<EXP>,"1"
IF<,@endit
SET EXP,"0"
WAIT 1
1: As you can see, Here is your Box
1; And your key Be sure not to lose that.
1: You just right-click on it to use.
1: Try it now.
END
@eventActorClick2
MENU "Get Item=@remove","Store Item=@store","Finished=@endit"
END 

@remove
SET CA,%I#<CACH>
SET CB,%I#<CBCH>
SET CC,%I#<CCCH>
SET CD,%I#<CDCH>
SET CE,%I#<CECH>
SET CF,%I#<CFCH>
SET CG,%I#<CGCH>
SET CH,%I#<CHCH>
SET CI,%I#<CICH>
SET CIT,"0"
ADD CIT,#<CAN>
ADD CIT,#<CBN>
ADD CIT,#<CCN>
ADD CIT,#<CDN>
ADD CIT,#<CEN>
ADD CIT,#<CFN>
ADD CIT,#<CGN>
ADD CIT,#<CHN>
ADD CIT,#<CIN>
COMPARE #<CIT>,"0"
IF=,@WHATITEMS ; If there are no items in the chest, the player cannot remove anything.
MENU "#<CA> (#<CAN> )=@ta","#<CB> (#<CBN> )=@tb","#<CC> (#<CCN> )=@tc","#<CD> (#<CDN> )=@td","#<CE> (#<CEN> )=@te","#<CF> (#<CFN> )=@tf","#<CG> (#<CGN> )=@tg","#<CH> (#<CHN> )=@th","#<CI> (#<CIN> )=@ti","Done=@endit"
END
@WHATITEMS
N: There are no items in the Box!
END
@ta
COMPARE #<CAN>,"1"
IF<,@endit
MENU "Take One=@tao","Take All=@taa","Cancel=@endit"
END
@tb
COMPARE #<CBN>,"1"
IF<,@endit
MENU "Take One=@tbo","Take All=@tba","Cancel=@endit"
END
@tc
COMPARE #<CCN>,"1"
IF<,@endit
MENU "Take One=@tco","Take All=@tca","Cancel=@endit"
END
@td
COMPARE #<CDN>,"1"
IF<,@endit
MENU "Take One=@tdo","Take All=@tda","Cancel=@endit"
END
@te
COMPARE #<CEN>,"1"
IF<,@endit
MENU "Take One=@teo","Take All=@tea","Cancel=@endit"
END
@tf
COMPARE #<CFN>,"1"
IF<,@endit
MENU "Take One=@tfo","Take All=@tfa","Cancel=@endit"
END
@tg
COMPARE #<CGN>,"1"
IF<,@endit
MENU "Take One=@tgo","Take All=@tga","Cancel=@endit"
END
@th
COMPARE #<CHN>,"1"
IF<,@endit
MENU "Take One=@tho","Take All=@tha","Cancel=@endit"
END
@ti
COMPARE #<CIN>,"1"
IF<,@endit
MENU "Take One=@tio","Take All=@tia","Cancel=@endit"
END
@tao
SUB CAN,"1"
GIVE I#<CACH>
GOTO @chknuma
@taa
SUB CAN,"1"
GIVE I#<CACH>
COMPARE #<CAN>,"0"
IF>,@taa
GOTO @chknuma
@tbo
SUB CBN,"1"
GIVE I#<CBCH>
GOTO @chknumb
@tba
SUB CBN,"1"
GIVE I#<CBCH>
COMPARE #<CBN>,"0"
IF>,@tba
GOTO @chknumb
@tco
SUB CCN,"1"
GIVE I#<CCCH>
GOTO @chknumc
@tca
SUB CCN,"1"
GIVE I#<CCCH>
COMPARE #<CAN>,"0"
IF>,@tca
GOTO @chknumc
@tdo
SUB CDN,"1"
GIVE I#<CDCH>
GOTO @chknumd
@tda
SUB CDN,"1"
GIVE I#<CDCH>
COMPARE #<CDN>,"0"
IF>,@tda
GOTO @chknumd
@teo
SUB CEN,"1"
GIVE I#<CECH>
GOTO @chknume
@tea
SUB CEN,"1"
GIVE I#<CECH>
COMPARE #<CEN>,"0"
IF>,@tea
GOTO @chknume
@tfo
SUB CFN,"1"
GIVE I#<CF>
GOTO @chknumf
@tfa
SUB CFN,"1"
GIVE I#<CFCH>
COMPARE #<CFN>,"0"
IF>,@tfa
GOTO @chknumf
@tgo
SUB CGN,"1"
GIVE I#<CGCH>
GOTO @chknumg
@tga
SUB CGN,"1"
GIVE I#<CGCH>
COMPARE #<CGN>,"0"
IF>,@tga
GOTO @chknumg
@tho
SUB CHN,"1"
GIVE I#<CHCH>
GOTO @chknumh
@tha
SUB CHN,"1"
GIVE I#<CHCH>
COMPARE #<CHN>,"0"
IF>,@tha
GOTO @chknumh
@tio
SUB CIN,"1"
GIVE I#<CICH>
GOTO @chknumi
@tia
SUB CIN,"1"
GIVE I#<CICH>
COMPARE #<CIN>,"0"
IF>,@tia
GOTO @chknumi
;---------
; 'Check' section
; (Checks to see if there are any of the items the player has removed left in the chest, if not clears item name and number)
@chknuma
COMPARE #<CAN>,"0"
IF>,@remove
SET CA,""
SET CACH,""
GOTO @remove
@chknumb
COMPARE #<CBN>,"0"
IF>,@remove
SET CB,""
SET CBCH,""
GOTO @remove
@chknumc
COMPARE #<CCN>,"0"
IF>,@remove
SET CC,""
SET CCCH,""
GOTO @remove
@chknumd
COMPARE #<CDN>,"0"
IF>,@remove
SET CD,""
SET CDCH,""
GOTO @remove
@chknume
COMPARE #<CEN>,"0"
IF>,@remove
SET CE,""
SET CECH,""
GOTO @remove
@chknumf
COMPARE #<CFN>,"0"
IF>,@remove
SET CF,""
SET CFCH,""
GOTO @remove
@chknumg
COMPARE #<CGN>,"0"
IF>,@remove
SET CG,""
SET CGCH,""
GOTO @remove
@chknumh
COMPARE #<CHN>,"0"
IF>,@remove
SET CH,""
SET CHCH,""
GOTO @remove
@chknumi
COMPARE #<CIN>,"0"
IF>,@remove
SET CI,""
SET CICH,""
GOTO @remove
;--------------------------
;
; This label is used whenever an IF or MENU command needs to END the scene.
;
@endit
END

; Item Storage

@store
SET A,"0"
GOTO @settozero
@loop
ADD A,"1"
IF I#<A>,@seta
COMPARE #<A>,"250" ;Checks Host's "pockets" for items 1-250
IF>,@DEPMEN
GOTO @loop
; Each time I<n> is true, the name and number of the item are stored in
; cookies. After nine of these cookie 'pairs' have been filled, the
; DEMPENMORE label is executed (giving the scene host the option of
; depositing any of the nine items, or displaying another nine.
; If the scene host does not have nine items, the DEPMEN label is
; executed.
@seta
COMPARE #<A>,"106"
IF=,@loop ; Prevents players from depositing their box keys, and hence being unable to re-enter the flat!
COMPARE #<SACH>,"0"
IF>,@setb
SET SA,%I#<A>
SET SACH,#<A>
GOTO @LOOP
@setb
COMPARE #<SBCH>,"0"
IF>,@setc
SET SB,%I#<A>
SET SBCH,#<A>
GOTO @LOOP
@setc
COMPARE #<SCCH>,"0"
IF>,@setd
SET SC,%I#<A>
SET SCCH,#<A>
GOTO @LOOP
@setd
COMPARE #<SDCH>,"0"
IF>,@sete
SET SD,%I#<A>
SET SDCH,#<A>
GOTO @LOOP
@sete
COMPARE #<SECH>,"0"
IF>,@setf
SET SE,%I#<A>
SET SECH,#<A>
GOTO @LOOP
@setf
COMPARE #<SFCH>,"0"
IF>,@setg
SET SF,%I#<A>
SET SFCH,#<A>
GOTO @LOOP
@setg
COMPARE #<SGCH>,"0"
IF>,@seth
SET SG,%I#<A>
SET SGCH,#<A>
GOTO @LOOP
@seth
COMPARE #<SHCH>,"0"
IF>,@seti
SET SH,%I#<A>
SET SHCH,#<A>
GOTO @LOOP
@seti
COMPARE #<SICH>,"0"
IF>,@DEPMENMORE
SET SI,%I#<A>
SET SICH,#<A>
GOTO @LOOP
@DEPMENMORE
SUB A,"1"
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","#<SF>=@six","#<SG>=@seven","#<SH>=@eight","#<SI>=@nine", "[MORE]=@settozero"
GOTO @dep
END
@settozero
SET SACH,""
SET SBCH,""
SET SCCH,""
SET SDCH,""
SET SECH,""
SET SFCH,""
SET SGCH,""
SET SHCH,""
SET SICH,""
GOTO @loop
@DEPMEN
; The labels below are executed according to the
; number of items left to display, so if two items are left,
; only two items are displayed, etc.
COMPARE #<SBCH>,"0"
IF>,@twoleft
MENU "#<SA>=@one","DONE=@endit" ; Only arrives here if onel item is left.
END
@twoleft
COMPARE #<SCCH>,"0"
IF>,@threeleft
MENU "#<SA>=@one","#<SB>=@two","DONE=@endit"
END
@threeleft
COMPARE #<SDCH>,"0"
IF>,@fourleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","DONE=@endit"
END
@fourleft
COMPARE #<SECH>,"0"
IF>,@fiveleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","DONE=@endit"
END
@fiveleft
COMPARE #<SFCH>,"0"
IF>,@sixleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","DONE=@endit"
END
@sixleft
COMPARE #<SGCH>,"0"
IF>,@sevenleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","#<SF>=@six","DONE=@endit"
END
@sevenleft
COMPARE #<SHCH>,"0"
IF>,@eightleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","#<SF>=@six","#<SG>=@seven","DONE=@endit"
END
@eightleft
COMPARE #<SICH>,"0"
IF>,@nineleft
MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","#<SF>=@six","#<SG>=@seven","#<SH>=@eight","DONE=@endit"
END
@nineleft

MENU "#<SA>=@one","#<SB>=@two","#<SC>=@three","#<SD>=@four","#<SE>=@five","#<SF>=@six","#<SG>=@seven","#<SH>=@eight","#<SI>=@nine", "DONE=@endit" 
END

;----------------
; These lines set SS as the number of the item which will be deposited, then takes all of the items from the player.
; It could be arranged so that the player can deposit a given number of items, but I can't see why anyone
; would want to, since you can carry as many of an item around with you as you want, and they only fill one
; space in your items list.
;

@one 
SET SS, #<SACH>
GOTO @dep
@two 
SET SS, #<SBCH>
GOTO @dep
@three 
SET SS, #<SCCH>
GOTO @dep
@four 
SET SS, #<SDCH>
GOTO @dep
@five 
SET SS, #<SECH>
GOTO @dep
@six 
SET SS, #<SFCH>
GOTO @dep
@seven 
SET SS, #<SGCH>
GOTO @dep
@eight 
SET SS, #<SHCH>
GOTO @dep
@nine 
SET SS, #<SICH>
@dep
COMPARE #<CACH>,#<SS>
IF=,@depa
COMPARE #<CBCH>,#<SS>
IF=,@depb
COMPARE #<CCCH>,#<SS>
IF=,@depc
COMPARE #<CDCH>,#<SS>
IF=,@depd
COMPARE #<CECH>,#<SS>
IF=,@depe
COMPARE #<CFCH>,#<SS>
IF=,@depf
COMPARE #<CGCH>,#<SS>
IF=,@depg
COMPARE #<CHCH>,#<SS>
IF=,@deph
COMPARE #<CICH>,#<SS>
IF=,@depi
COMPARE #<CAN>,"0"
IF=,@depa
COMPARE #<CBN>,"0"
IF=,@depb
COMPARE #<CCN>,"0"
IF=,@depc
COMPARE #<CDN>,"0"
IF=,@depd
COMPARE #<CEN>,"0"
IF=,@depe
COMPARE #<CFN>,"0"
IF=,@depf
COMPARE #<CGN>,"0"
IF=,@depg
COMPARE #<CHN>,"0"
IF=,@deph
COMPARE #<CIN>,"0"
IF=,@depi
N: The chest is full!
WAIT 1
END
@END
TAKE G500
TAKE H10000
END
@depa
IF -I#<SS>,@endit
TAKE I#<SS>
SET CACH, #<SS>
ADD CAN,"1"
GOTO @depa
@depb
IF -I#<SS>,@endit
TAKE I#<SS>
SET CBCH, #<SS>
ADD CBN,"1"
GOTO @depb
@depc
IF -I#<SS>,@endit
TAKE I#<SS>
SET CCCH, #<SS>
ADD CCN,"1"
GOTO @depc
@depd
IF -I#<SS>,@endit
TAKE I#<SS>
SET CDCH, #<SS>
ADD CDN,"1"
GOTO @depd
@depe
IF -I#<SS>,@endit
TAKE I#<SS>
SET CECH, #<SS>
ADD CEN,"1"
GOTO @depe
@depf
IF -I#<SS>,@endit
TAKE I#<SS>
SET CFCH, #<SS>
ADD CFN,"1"
GOTO @depf
@depg
IF -I#<SS>,@endit
TAKE I#<SS>
SET CGCH, #<SS>
ADD CGN,"1"
GOTO @depg
@deph
IF -I#<SS>,@endit
TAKE I#<SS>
SET CHCH, #<SS>
ADD CHN,"1"
GOTO @deph
@depi
IF -I#<SS>,@endit
TAKE I#<SS>
ADD CIN,"1"
SET CICH, #<SS>
GOTO @depi
@clear
SET SA,"0"
SET SB,"0"
END
;--------------
;comment at the end, we must always have this i dunno why