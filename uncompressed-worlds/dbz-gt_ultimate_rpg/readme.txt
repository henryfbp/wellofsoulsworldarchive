this is a WORLD folder
----------------------

Each world has its own folder which contains:

   quest.txt  - the script file which controls the quests available
                in this world.  See the quest.txt file comments for
                additional information.  The #include directive may
		be used in this file to break tables out into separate
		files, like "spells.txt", "monsters.txt" etc.

   world.ini  - an INI file which contains descriptive information
		about the world (this is used during publication of 
		the world)  Keep the "version" field of this file set
		to the same value as in the 'info' file on your web site.
		See publishing details on web site.

   music.ini  - an INI file which describes which MIDI files should be
		played for each map of your world.  If missing, will
		use old-style assignments.

   stocks.ini - an INI file which describes which companies are available to
		be traded in your world (if missing, will use the default
		WoS\stocks.ini file)

   art	      - a folder containing world-specific artwork
   maps       - a folder containing world-specific maps
   midi       - a folder containing world-specific music files
   scenes     - a folder containing world-specific background images for scenes
   monsters   - a folder containing world-specific image bitmaps for monsters
   sfx	      - a folder containing world-specific WAV file sound effects
   savedheroes - a folder containing .her files for 'saved games' in
                 this world.  Heroes from one world cannot be played in
                 other worlds.