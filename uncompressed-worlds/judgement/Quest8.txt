;Quest 8: First Steps into the Land of Mist + the Moon Temple
;80, The village of Uluein is being constantly ransacked by a monster that steals the villagers' food.
;81, The Cave Gorger was merely feeding her young... Was killing her truly the right course of action?
;82, Who cares? I got Luna's Insanity as a reward; now I can go to the Moon Temple!

;==========================Uluein Village scenes===============================

SCENE 800 Bedroom, SCENE, "Uluein Inn"
	ACTOR 1, "Alemea", JoshTownsfolk, 5, 17, 71
	SET Innprice, "90"
	MUL Innprice, #<num.peopleinscene>
	1: Welcome to the Uluein Inn!
	1: Or would that be the Ulueinn?
	1: Hehe, well, a night costs #<innprice> Septi.
	1: How 'bout it?
	@innmenu
	MENU "Yeah=@yes", "Nope=@nope", "Set as Save point=@save"
	GOTO @innmenu
	@yes
	IF -G#<innprice>, @nomoney
	1: Thanks, have a good night!
	HOST_TAKE G#<innprice>
	GIVE L1
	GIVE H32000
	GIVE M32000
	@no
	1: Please come again!
	END
	@nomoney
	1: I'm sorry, but you don't have the money...
	GOTO @no
	@save
	SET SPlocation, "Uluein"
	SET SPid, 122
	SET SPset, 1
	N: Save point set as Uluein.
	GOTO @innmenu

;==============

SCENE 801 ArmorShop, SCENE, "Uluein Armaments"
	ACTOR 1, "Selkio", joshTownsfolk, 10, 17, 71
	1: Welcome to Uluein Armaments!
	1: We have a large selection of powerful equipment!
	1: Go ahead, take a look!
	OFFER 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279
	END

;==============

SCENE 802 ItemShop, SCENE, "Uluein Essentials"
	ACTOR 1, "Muletim", joshTownsfolk, 20, 17, 71
	1: Welcome to my establishment.
	1: You'll have one shell of a good time shopping here!
	OFFER 1, 2, 3, 4, 7, 8, 12, 13, 15, 16, 99, 253
	END

;===============

SCENE 803 Kitchen, SCENE, "Prime Minister's Office"
	ACTOR 1, "Ulubalrin", JoshRoyalty, 5, 17, 71
	IF T80, @hasT80
	1: /a sighs...
	1: Woe is me...
	1: Our fair city is under attack by a powerful monster that lives in the Tunnels of Mist...
	1: It steals our food, and we're starving to death...
	1: Woe is me...
	GIVE T80
	END
	@hasT80
	IF T81, @hasT81
	1: If there's any kind soul brave enough to venture into the Tunnels of Mist and recover our stolen food, I can reward them with this magical stone...
	END
	@hasT81
	IF T82, @hasT82
	1: You... You've saved us!
	1: Thanks to your help, we no longer have to fear for our lives.
	1: Living in marsh like we do, food is a very precious commodity, you know.
	1: It's worth its weight in Septi!
	1: But here, for all of your help, I'd like you to have this.
	1: This stone is called Luna's Insanity.
	1: It's not actually insane, mind you.
	1: I hope.
	GIVE I280
	GIVE T82
	1: Anyway, thanks for everything. Your deeds will be remembered by Ulueinians for a long time to come.
	END
	@hasT82
	1: Thanks for saving the city!
	1: By the way, now that we have our food back, a grocer has opened up in town.
	END

SCENE 804 Cave2, SCENE, "Cave Gorger's Lair"
	IF T80, @hasT80
	N: The cave is dark and you can see some crates in the distance...
	WAIT 3
	N: But otherwise, you see nothing out of the ordinary.
	END
	@hasT80
	IF T81, @hasT81
	N: You....
	WAIT 3
	N: You dare to challenge me...?
	WAIT 3
	N: Is it a crime to feed one's children...?
	WAIT 3
	N: You remain... For the sake of my children, I am prepared to defend myself.
	MUSIC Boss1.mid
	FIGHT2 162
	IF WIN, @won
	N: Leave me and my children be...
	END
@won
	N: ...Children, I have failed you...
	WAIT 3
	H: Well, that's that! I'll just gather up some of the food here and take it back to the Prime Minister.
	GIVE T81
	END
@hasT81
	N: There's nothing here anymore...
	END

;===================Renkin Scenes==================
;===============
SCENE	805	ItemShop, SCENE, "Renkin Battlesmith"
	ACTOR	1, "Keliroa", joshTownsfolk, 16, 17, 71
	1: Welcome to the Renkin Battlesmith!
	1: I may not be able to find my glasses, but I'm sure we can find the perfect piece of gear for you!
;	281-298
	OFFER	281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298
	END
;================
SCENE	806	ArmorShop, SCENE, "The Alchemical Apothecary"
; Note to self:	Holmenas-neun tamgueseo jeongsange geureul ttollyeobonaegi wihayeo iyong doel.
	
	ACTOR	1, "Holmenas", joshRoyalty, 21, 17, 71
	1: Welcome to the Alchemical Apothecary.
	1: Don't mind how I look.
	1: There's really not much I can do about it; it happened in an Alchemy accident...
	1: Anyway, here's some medicine, but I wish it would cure what happened to me...
	OFFER 1, 2, 3, 4, 7, 8, 12, 13, 15, 16, 99, 253
	END
;================
SCENE	807	Bedroom, SCENE, "Renkin Rest Spot"
	ACTOR	1, "Soalef", joshMiscellaneous, 10, 17, 71
	@begin
		SET	Innprice, 120
		MUL Innprice, #<num.PeopleinScene>
		1: Hello, and welcome to the Renkin Rest Spot, the foremost retreat for tired adventurers!
		1: It'll cost you #<Innprice> Septi to stay the night. You interested, youngin'?
	@menu
		MENU "Yes=@yes", "No=@no", "Set Renkin as Save Point=@save"
		GOTO @menu
	@no
		1: Please come again.
		END
	@save
		SET SPlocation, "Renkin"
		SET SPid, 133
		SET SPset, 1
		N: Save point set as Renkin.
		END
	@yes
		IF G#<Innprice>, @hascash
		1: Back in my day, if a young fart didn't have the cash, #<g.he>'d be thrown out on #<g.his> smooth ass!	
		1: So that's just what I'm gonna do!
		WAIT 2
		GOTO EXIT
		END
	@hascash
		HOST_TAKE G#<Innprice>
		GIVE	L1
		GIVE	H32000
		GIVE	M32000
		END
	@eventactorclick1
		GOTO @begin
;===================Moon Temple Scenes======================
;	82, "For rescuing the town, the prime minister gave me Luna's Insanity."
;	83, "I found a strange machine inside a temple that reacted to Luna's Insanity."
;	84, "I arrived in the Moon Temple and defeated a powerful monster."
;	85, "I lit the pillar of the Moon."
;	86, "I overcame the puzzles of the Temple.
;	87, "I returned the light to the pillar!"
;	88, "I am in an area with no air."
;	801, Terminal code 1
;	802, Terminal code 2
;	803, Terminal code 3
;	804, Terminal code 4
;	805, Terminal code 5

SCENE	810	MoonTemple1, SCENE, "Moon Temple Gate"
		FX	3
		IF	T82, @hasT82
		WAIT 3
		N: ...
		WAIT 3
		N: You see what appears to be a machine, but it's getting no electricity.
		WAIT 3
		H: It looks like I need to come here later.
		END
	@hasT82
		IF	T83, @hasT83
		WAIT	3
		N: ...
		WAIT	3
		N: The area is dark...
		WAIT	3
		N: In your hands, Luna's Insanity shines brightly.
		WAIT	3
		N: As you hold up your hands, the machines around you react and turn on!
		FX	0
		WAIT	3
		N: The doors slide open, and you enter cautiously...
		GIVE	T83
		GOTO LINK 135, 0
		END
	@hasT83
		FX	0
		GOTO LINK 135, 0
		END
;======================
	SCENE 811	MoonTemple1, SCENE, "Airlock Gate"
		IF T86, @skip
		IF T88, @lifecheck
		ACTOR 1, "Airlock Gate", invisible, 1, 50, 50
		1: The following area is an airlocked location. You will be provided with 15 minutes of oxygen.
		1: Should you fail to overcome the security protocol in the alotted time, your life will be placed at risk.
		1: Feel free to return here to recharge your oxygen levels, but as a precaution, the security protocol will reset.
		1: Are you ready to enter?
	@menu
		MENU	"Yes=@yes", "No=@no"
		GOTO @menu
	@no
		1: Return here when you have decided to enter.
		END
	@yes
		1: Very well. You will have approximately 15 minutes before your oxygen runs out and you suffocate.
		1: I know it sounds pleasant. Please attempt to enjoy your trek through the temple.
		COUNTDOWN	900
		GIVE	T88	
		GOTO LINK	136, 0
		END
	@lifecheck
		IF XP, @dead
		TAKE T88
		1: You're not out of air yet! Get back in there!
		GIVE T88
		GOTO LINK 136, 0
	@dead
		TAKE	T88
		1: I see that you've run out of air. That's unfortunate.
		1: I will recover your HP and return you inside.
		1: However, as a precaution, the security protocol has been reset. 
		TAKE	T801
		TAKE	T802
		TAKE	T803
		TAKE	T804
		TAKE	T805
		1: You will now be able to enter the area.
		GIVE L1
		GIVE H7000
		GIVE M2000
		COUNTDOWN	900
		GIVE	T88
		GOTO LINK	136, 0
		END
;===========================================
	SCENE	812	MoonTemple1, SCENE, "Security Terminal 1"
		WAIT	3
		IF	T88, @hasT88
		N: The computer doesn't respond...
		END
	@hasT88
		IF T801, @hasT801
		IF XP, @dead
		N: The computer is marked "Security Terminal 1".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		SET pc1, %R9
		N: The passcode is #<pc1>.
		GIVE T801
		END
	@dead
		TAKE H32000
		TAKE M32000
		N: You've run out of air!
		END
	@hasT801
		IF XP, @dead
		N: The computer is marked "Security Terminal 1".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		N: The passcode is #<pc1>.
		END
;================================
	SCENE	813	MoonTemple1, SCENE, "Security Terminal 2"
		WAIT	3
		IF	T88, @hasT88
		N: The computer doesn't respond...
		END
	@hasT88
		IF T802, @hasT802
		IF XP, @dead
		N: The computer is marked "Security Terminal 2".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		SET pc2, %R9
		N: The passcode is #<pc2>.
		GIVE T802
		END
	@dead
		TAKE H32000
		TAKE M32000
		N: You've run out of air!
		END
	@hasT802
		IF XP, @dead
		N: The computer is marked "Security Terminal 2".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		N: The passcode is #<pc2>.
		END
;=======================================
	SCENE	814	MoonTemple1, SCENE, "Security Terminal 3"
		WAIT	3
		IF	T88, @hasT88
		N: The computer doesn't respond...
		END
	@hasT88
		IF T803, @hasT803
		IF XP, @dead
		N: The computer is marked "Security Terminal 3".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		SET pc3, %R9
		N: The passcode is #<pc3>.
		GIVE T803
		END
	@dead
		TAKE H32000
		TAKE M32000
		N: You've run out of air!
		END
	@hasT803
		IF XP, @dead
		N: The computer is marked "Security Terminal 3".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		N: The passcode is #<pc3>.
		END
;=======================
	SCENE	815	MoonTemple1, SCENE, "Security Terminal 4"
		WAIT	3
		IF	T88, @hasT88
		N: The computer doesn't respond...
		END
	@hasT88
		IF T804, @hasT804
		IF XP, @dead
		N: The computer is marked "Security Terminal 4".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		SET pc4, %R9
		N: The passcode is #<pc4>.
		GIVE T804
		END
	@dead
		TAKE H32000
		TAKE M32000
		N: You've run out of air!
		END
	@hasT804
		IF XP, @dead
		N: The computer is marked "Security Terminal 4".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		N: The passcode is #<pc4>.
		END
;================================
	SCENE	816	MoonTemple1, SCENE, "Security Terminal 5"
		WAIT	3
		IF	T88, @hasT88
		N: The computer doesn't respond...
		END
	@hasT88
		IF T805, @hasT805
		IF XP, @dead
		N: The computer is marked "Security Terminal 5".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		SET pc5, %R9
		N: The passcode is #<pc5>.
		GIVE T805
		END
	@dead
		TAKE H32000
		TAKE M32000
		N: You've run out of air!
		END
	@hasT805
		IF XP, @dead
		N: The computer is marked "Security Terminal 5".
		WAIT	3
		IF XP, @dead
		N: You press a button marked "passcode".
		WAIT	3
		IF XP, @dead
		N: Loading...
		WAIT	6
		IF XP, @dead
		N: The passcode is #<pc5>.
		END
;======================
	SCENE 817	MoonTemple2, SCENE, "Security Terminal - Main"
		IF	T88, @hasT88
		IF	T86, @skip
		N: The computer refuses to respond...
		END
	@hasT88
		IF XP, @dead
		WAIT	3
		IF XP, @dead
;		Fun fact for anyone reading this: Man wol means "Full moon" in Korean, and this is the Moon Temple. ZOMG!
		N: Welcome to the Manwol Security System.
		WAIT	3
		IF XP, @dead
		IF T801+T802+T803+T804+T805, @haspcs
		N: Please activate all security terminals.
		END
	@haspcs
		N: Please enter security code 51342.
		ASK  32000
		IF XP, @dead
		N: Processing...
		WAIT 5
		IF	Q#<pc5>#<pc1>#<pc3>#<pc4>#<pc2>, @correct
		IF XP, @dead
		N: Incorrect passcode.
		END
	@correct
		N: Passcode accepted.
		WAIT	3
		N: Restoring air pressure to interior environment.
		WAIT	3
		COUNTDOWN 0
		TAKE	T88
		GIVE	T86
		N: Proceed to the Pillar area.
		GOTO LINK 139, 0
	@dead
		TAKE	H32000
		TAKE	M32000
		N: You've run out of air!
		END	
;=============================
;	84, "I arrived in the Moon Temple and defeated a powerful monster."
;	85, "I lit the pillar of the Moon."
;	86, "I overcame the puzzles of the Temple.
;	87, "I returned the light to the pillar!"
;	88, "I am in an area with no air."
	SCENE 818 MoonTemple3, SCENE, "Moon Pillar"
		IF	T84, @hasT84
		H: At long last, the Pillar of the Moon.
		H: It seems like it's taken me years to get here...
		H: But finally, I'm here, and can finally continue to the next stage of my journey!
		H: Let's see what kind of guardian this temple has! I'm not afraid!
		MUSIC	Boss10.mid
		FIGHT2 168
		IF	WIN, @hasT84
		H: No... It can't end here...
		END
	@hasT84
		IF	T85, @hasT85
		GIVE	T84
		H: I did it... I brought light to the temple!
		ACTOR	1, "Luna", Elementals, 8, -1, 71
		MOVE	1, 17, 71
		1: Yes, and I thank you for your assistance.
		H: Luna?
		1: It is, indeed. I'm beginning to feel the power of the elements coursing through my body...
		1: You must be nearing the end of your quest to unseal the temples.
		1: Listen to me, %1. 
		1: In order to face the Judgement of Tamriel, you must unseal all 8 temples...
		1: With each temple you unseal, the power of Mana coursing through the world grows ever greater.	
		1: But there is someone behind the scenes... There's a person who wants you to unseal them for another reason.
		1: I sense a dark hand guiding your actions.
		H: Luna... I have a question. When I unsealed the temples on Tolshroud, the elementals didn't appear to me.
		1: Truly? This is harrowing, indeed.
		1: I fear that the hand guiding you has targeted the elementals and is using you to deliver them to it.
		H: Should I not unseal the Temple of Nature, then?
		1: You should unseal it and speak to Dryad.
		1: She may know more than I.
		1: The Moon Temple is designed to be airless, like the Moon it was built to pay homage to.
		1: Leave through the escape chamber in the back and head to the Temple of Nature.
		H: I will... Thank you, Luna.
		GIVE	T85
		END
	@hasT85
		N: You hear the slight hum of computers in the background as the Pillar glows dimly.
		END
;=============================
	SCENE 819, AboardShip, SCENE, "Untaba Docks"
		ACTOR 1, "Bow-Legged Swansoon", josh57, 1, 17, 71
		IF #131, @Renkin
		1: I can take you back to Rinpuu if you'd like.
		1: How about it?
		@menu
		MENU	"Yes=@yes", "No=@no"
		GOTO @menu
		@no
		1: Get outta here, then!
		END
		@yes
		1: Okay, it won't take long.
		WAIT 3
		GOTO LINK  131, 3
		@Renkin
		1: I can take you to Untaba if you'd like.
		1: How about it?
		@menu2
		MENU	"Yes=@yes2", "No=@no"
		GOTO @menu2
		@yes2
		1: Okay, it won't take long.
		WAIT 3
		GOTO LINK  140, 1
;=============================
	SCENE 820, 3, SCENE, "A beach somewhere"
		COMPARE #<activ8>, 1
		IF= @activate
		GOTO @skip	
	@activate
		IF T90, @skip
		H: Ugh... Where am I...?
		H: Guess I'll take a look around...
		GIVE T90
		END
	@skip
		END
;============================
	SCENE 821, MoonTemple2, SCENE, "Escape Passage"
		WAIT	3
		N: Activating automatic escape system. Please wait.
		SET activ8, 1
		WAIT	3
		GOTO LINK	140, 0, 1

	SCENE	822, MoonTemple2, SCENE,	"Airlock Entry"
		WAIT	3
		N: If you continue beyond these gates, you will exit the airlocked area.
		WAIT	3
		N: Would you like to continue?
		@menu
		MENU	"Yes=@yes", "No=@no"
		GOTO	@menu
		@no
		WAIT	3
		N: Action aborted.
		END
		@yes
		WAIT	3
		TAKE	T88
		COUNTDOWN	0
		GOTO	LINK	135, 1
		END
;