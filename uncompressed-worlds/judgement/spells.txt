;
; SPELLS
;
;
;-------------------
; Spells used in the game.  Each line describes one spell
;
; Mandatory arguments are:
;
; 	//	arg0	ID# (1-767)			// unique ID number of spell
;
;		* ID #
;
;		Each spell must have a unique ID number between 1 and 767 (which is a strange value, I know
;		but it has historical significance :-)
;
;  		Spell 0 is a very special entry in the table which is actually a set of algorithm tweaks.
;		Read monsters.txt for more details about algorithm tweaks.
;
;	//	arg1	name[MAX_SPELL_NAME_LEN];  // name of spell, as it appears on screen. 
;
;	//	arg2	ppCost;				// Play Points (Whack Points) required to learn spell 
;									//  -1  means "spell cannot be learned" (can be given as a quest reward only)
;									//   0  means "cost determined automatically from spell's level"
;									//  n>0 Means "cost is n PP" 
;									// An "unlearnable" spell simply doesn't appear in the Learn Spells dialog.
;
;	//	arg3	element;			// Spells element ID#: 0-255.  
;									// A spell will be most effective when cast against the opposing
;									// element.  Actual element definitions are in the ELEMENTS table.
;									// Special Element IDs are:
;									//   0 - HEAL spell (cannot be used to cause damage)
;									//   4 - DEATH spell (affects all elements equally)
;									//  8-255  CHAOS elements (use DEATH element training for effectiveness)
;									// Chaos elements are intended to let you have more variety in monsters, such
;									// that some spells are only cast by one or two monsters.
;									// Chaos spells cannot be learned.  They can only be assigned by table, or
;									// given as quest prizes (and it's a bad idea to give chaos spells to players
;									// since they have no explicit chaos selector on the SPELLs panel.)
;									// In fact, it is ILLEGAL for a human to know a chaos spell.  period
;
;	//	arg4	damage.monsterID		// pure attack strength (before defense filtering)
;									// damage determins nature of spell
;									// optional monsterID is used by SUMMON_SPECIFIC 
;
;					The damage argument is also used to specify certain special effect spells
;					which do not cause regular damage or do one-of-a-kind things
;					Generall, a negative value indicates a specialty spell.  These are
;					called 'diseases' just to give them a general name.
;
;					Damage			// Specialty
;
;						 0			damage computed automatically from level (preferred value if not HEAL spell)
;
;						-1			RESURRECTION		(brings dead to life, with 1 HP)
;
;						-2			DISEASE_POISON		(Makes HP restores ineffective)		
;						-3			DISEASE_SAP			(Makes Spells cost more MP to cast)		
;						-4			DISEASE_STUN		(cannot move, or attack)
;						-5			DISEASE_GAG			(cannot case spells)(* wears off)		
;						-6			DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-7			DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;						-8			DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-9			DISEASE_CHARM		(once charmed, monster fights for you)	
;						-10			DISEASE_TAME		(taming a charmed monster makes it a pet)(NOT IMPLEMENTED YET)	
;						-11			DISEASE_FREEZE		(cannot move)
;						-20			DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-21			DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-22			DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-23			DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-24			DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	
;						-200		SUMMON_SAME			Summon a monster of same type as caster
;						-201		SUMMON_SAME_ELEMENT	Summon a monster of the same element as caster
;						-202		SUMMON_RANDOM		Summon a random monster at or below level of caster (within 10 levels)
;						-203		SUMMON_SPECIFIC		Summon a specific monster (monster id in optional dotted arg)
;						-300		SUMMON_ITEM			Summon an item to be used on the target (item id in dotteg arg)
;
;					Most 'diseases' wear off when you leave the scene, however each disease can be 'cured'
;					by a spell whose 'damage' argument is the disease damage plus -100.  For
;					example, "-102" would cure poisoning (-2), and "-107" would cure sleep (-7)
;					Not all diseases have a logical cure (for example, there is no cure for 
;					resurrection (-1)
;
;					Some diseases(*) wear off after awhile, even if you don't leave the scene.
;
;					The 5 ability diseases are a little different as they lower (or raise, if a cure)
;					your ability a certain percentage with each use.  Hence repeated lowerings has
;					an larger effect.
;
;					Note, except for specialty spells (negative damage value) and LIFE spells, this
;					field is now ignored as regular damage is computed automatically

;	//  arg5	price;				// mp required to cast
;									//  0 = determined automatically, based on spell level
;									// this is now determined automatically and this entry is ignored
;
;	//	arg6	reqLevel+All.flags.minPlayerLevel.tokenNeeded	// minimum required elemental affinity (0-9) to cast it.
;									// [optional]add 100 if it affects ALL enemies
;											105  = level 5 required, and affects all on same team as target
;											(note this means you cannot make a spell require player level 100)
;									// [optional]add a decimal point followed by a spell FLAG
;											5.1	 = level 5 required, and is controlled by flag '1'
;											5.3  = level 5 required, flags 1 AND 2 (1+2 = 3)
;									// SPELL FLAGS: (add together all you want)
;											0 = no flags (in case you want to use minPLayerLevel option)
;											1 = monsters/pets cannot use this spell
;											2 = humans are not offered this spell on LEARN SPELL dialog
;											4 = this spell can only be used on yourself
;									// minPlayerLevel (assumed 0 unless you specify) character must be this level in class (or above)
;											5.0.12  = level 5 element training, no flags, level 12 character required
;									// tokenNeeded (assumed 0, which means no special token needed.  Otherwise spell is only in
;													the learn Spells list if the player has the specified token
;											5.0.0.7  = requires token 7 before it appears in list.
;
;				so... for example, a value of "107.1" would mean:
;
;						* element training level 7 or higher required to cast this spell
;						* when cast, it affects ALL members of the targeted team (following the same leader)
;						* monster/pets cannot learn/use this spell. (but humans can)
;
; the rest are optional and control the 'look' of the effect
;
;	//	arg7	path;				// path style (9 digit decimal number AAABBBCDD)
;
;					DD	'cloud' shape of magic in motion
;					00 - tight random cloud
;					01	- looser random cloud
;					02 - loose random cloud
;					03 - single circle
;					04 - two circles
;
;					C	Accumulation of magic around caster, before it 'launches'
;					0	- accumulate over caster
;					1	- accumulate within caster
;					2	- accumulate under caster
;		
;					BBB Travel Path
;					000 - straight line
;					001 - looser line
;					002 - looser line
;					003 - loose line
;					004 - slow wavy
;					005 - medium wavy
;					006 - faster wavy
;					007 - fast wavy
;					008 - spiral
;					009 - spiral
;					010 - spiral
;					011 - spiral
;					012 - column at foot of target
;					013 - column at head of target
;					014 - column at top of screen (use gravity)
;					015 - column at bottom of screen (use gravity)
;					016 - column at random between foot and head
;					017 - column at random between foot and top of screen
;
;					AAA Attack Path
;					000 - random cloud
;					001	- looser random cloud
;					002 - loose random cloud
;					003 - single circle
;					004 - two circles
;
; For example:   
;
;		004(2circ) + 011(spiral) + 2(under) + 04(2circ) 
;
; would be written:  004011204, or just 4011204
;
;	//	arg8	row; (0-199)		// row of effects table to use
;									// Magical effects are animated from a film-strip of up to 16
;									// images which are played from left to right.  Up to 200 such
;									// film-strips may be defined by the world designer.  Rather than
;									// cram all 200 filmstrips into a single file, they are broken
;									// into groups of ten and placed in files with the name:
;									// "EffectsNN.bmp" where NN runs from 00 to 19 and represents the
;									// first two digits of the effect 'row number' (000-199)
;									// For example, row 45 would be at offset 5 in the file "Effects04.bmp"
;									// Effects files 00 through 09 are stored in the evergreen ART folder
;									// and are reserved for the use of synthetic-reality (feel free to submit
;									// cool effects to be added).  World designers should put their custom
;									// effects into the files "effects10.bmp" through "effects19.bmp" and
;									// place them in the world's private ART folder (this means world designers
;									// control rows 100-199).
; 
;									// effects are 16x16 graphical glyphs stored in a separate
;									// row within the file. Columns represent individual images
;									// of the animation and are generally shown left to right.
;									// It would probably be most useful to open art\effects00.bmp and
;									// look at it for illumination.
;
;	//	arg9	maxCols; (1-31)		// # of columns to use (of the selected row in effects.bmp)
;	//	arg10	maxFx;	(0 - 100)	// # of trails to remember (increases the 'amount' of sparkles)
;	//	arg11	tixPerCol;			// step time between columns (in msec)
;									// controls the speed of the animation through the current row.
;	//	arg12	gravity;			<gravity>.<effects>.<weather>
;									Three 'dotted arguments' specifyng the gravity, special effects, and
;									weather present while the spell is being cast.  The format has changed,
;									but the values are the same as the old style:
;
;									// OLD STYLE ARG12 (pre-A57) was a 9 digit number AAABBBCCC
;									// gravity style controls 'dripping' behaviour(0 is none)
;
;					CCC
;					000	- no gravity
;					001	- magic falls slowly
;					002	- falls faster
;					003	- falls very fast
;					004   - falls way too fast
;					005	- magic RISEs slowly
;					006	- rises faster
;					007	- rises very fast
;					008	- rises way too fast
;
;					BBB
;					xxx	- FX modifier
;		
;					AAA
;					xxx - weather modifier
;			
;	//	arg13	loop;				// loop style (0 is none)
;
; these optional parameters control the SOUND of the spell
;
;	//  arg 14	sfxSummon[80];		// sound of spell being summoned	"summon.wav"
;	//  arg 15	sfxTravel[80];		// sound of spell traveling			"travel.wav"
;	//  arg 16	sfxStrike[80];		// sound of spell striking target	"strike,wav"

;
; SPELL ZERO
;
; There is no real spell zero, this line just sets some percentage tweaks which can be used to adjust the 
; algorithmically derived values.
;
; You can adjust: PP Cost, Damage (painful spells only), and MP Cost
;
	0,	"Algorithm Scale %",100,0,	100,100,0

;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15				16
;	ID	Name				PP	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
; Earth Spells

	1,	Lef,				250,0,	75,	2,	0,		3,		5,	11,	80,	100,	0,		0,		summon1.wav,travel1.wav,magic10.wav,
	2,	Lefna,				500,0,	150,6,	101,	0,		5,	11,	80,	100,	0,		0,		,			,			,			
	3,	Youlef,				1000,0,	500,8,	2,		0,		6,	11,	80,	100,	0,		0,		,			,			,			
	4,	Olef,				2000,0,	750,14,	103,	,		6,	11,	80,	100,	0,		0,		,			,			,			
	5,	Leftome,			4000,0,	1000,17,	4,		203,	8,	11,	80,	100,	0.0.1055,0,		,			,			,			
	6,	Lefnates,			8000,0,	1500,26,	105,	0,		8,	11,	80,	100,	0,		0,		,			,			,			
	7,	Metalef,			16000,0,	3000,32,	6,		203,	16,	11,	80,	100,	0.3.3055,0,		,			,			,			
	8,	Lefteum,			25000,0,	5500,46,	107,	0,		16,	11,	80,	100,	0,		0,		,			,			,			
	9,	Lefgod,				30000,0,	7500,58,	8,		0,		72,	11,	80,	100,	0,		0,		,			,			,			
	10,	Gigaonlefgod,		32000,0,	9999,79,	109,	0,		73,	11,	80,	100,	0,		,		,			,			,			
	11,	Aqu,				250,1,	30,	2,	0,		0,		52,	11,	80,	100,	0.0.1,	,		,			,			,			
	12,	Aquna,				500,1,	35,	6,	101,	0,		53,	11,	80,	100,	0.0.1,	,		,			,			,			
	13,	Youaqu,				1000,1,	57,	8,	2,		0,		50,	11,	80,	100,	0.2.2,	,		,			,			,			
	14,	Oaqu,				2000,1,	75,	14,	103,	0,		11,	11,	80,	100,	0.1.3,	,		,			,			,			
	15,	Aqutome,			4000,1,	94,	17,	4,		0,		10,	11,	80,	100,	0.1.3,	,		,			,			,			
	16,	Aqunates,			8000,1,	110,26,	105,	0,		54,	11,	80,	100,	0.01.3,	,		,			,			,			
	17,	Metaqua,			16000,1,	124,32,	6,		0,		51,	11,	80,	100,	0.0.7,	,		,			,			,			
	18,	Aquteumi,			25000,1,	259,46,	107,	0,		67,	11,	80,	100,	0.0.7,	,		,			,			,			
	19,	Aqugod,				30000,1,	311,58,	8,		0,		13,	11,	80,	100,	0.0.8,	,		,			,			,			
	20,	Kaametaaqugod,		32000,1,	464,79,	109,	0,		51,	11,	80,	100,	0.6.9,	,		,			,			,			
	24,	Soa,				250,2,	30,	2,	0,		0,		3,	11,	80,	100,	5000,	0,		,			,			,			
;						-4			DISEASE_STUN		(cannot move, or attack)
;						-11			DISEASE_PARALYZE	(cannot move)
;
; Death Spells

;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	25,	Soana,				500,2,	35,	6,	101,	3,		12,	11,	80,	100,	0,		0,		,			,			,			
	26,	Yousoa,				1000,2,	57,	8,	2,		0,		15,	11,	80,	100,	0,		0,		,			,			,			
	27,	Osoa,				2000,2,	75,	14,	103,	203,	20,	11,	80,	100,	0,		0,		,			,			,			
	28,	Soatome,			4000,2,	94,	17,	4,		203,	27,	11,	80,	100,	0,		0,		,			,			,			
	29,	Soanates,			8000,2,	110,26,	105,	203,	64,	11,	80,	100,	0,		0,		,			,			,			
	30,	Metasoa,			16000,2,	124,32,	6,		203,	70,	11,	80,	100,	0,		0,		,			,			,			
	31,	Soateum,			25000,2,	259,46,	107,	203,	64,	11,	80,	100,	0.3,	0,		,			,			,			
	32,	Soagod,				30000,2,	311,58,	8,		0,		2,	11,	80,	100,	0.5.5,	0,		,			,			,			
	33,	Gigasoagod,			32000,2,	464,79,	109,	203,	70,	11,	80,	100,	0.5.6,	0,		,			,			,			
	34,	Animus,				45000,0,	-1,	165,9.0.70.4,203,	82,	11,	80,	100,	0.5.6,	0,		,			,			,			0.0,
	35,	Nanikueteyeus,		45000,4,	600,499,9.0.0.5.0,203,	70,	11,	80,	100,	0.5.6,	0,		,			,			,			,	
	36,	Kage,				2500,4,	-203.2000,3,	1.1.12.7.1100.0,9.0.0.5,203,70,	11,	80,		100,	0.5.6,	0,			,			,			,	
	37,	Kagekanpeki,		9000,4,	-203.2001,8,	3.1.37.7.1100.0.2.0,203,	70,	11,	80,	100,	0.5.6,	0,		,			,			,			,	
	38,	Kakusu,				100,4,	50.1,2,	0.1.12.7.0.1100.2.0,203,	70,	11,	80,	100,	0.5.6,	0,		,			,			,			,	
	49,	Pra,				250,3,	30,	2,	0,		0,		60,	11,	80,	100,	5000,	0,		,			,			,			
;						-2			DISEASE_POISON		(saps a little HP every few seconds)		
;						-3			DISEASE_SAP			(saps a little MP every few seconds)		
;						-6			DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-7			DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;
; Wind Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	51,	Prana,				500,3,	35,	6,	101,	4,		60,	11,	80,	100,	0.5,	0,		,			,			,			
	52,	Youpra,				1000,3,	57,	8,	2,		0,		12,	11,	80,	100,	5,		0,		,			,			,			
	53,	Opra,				2000,3,	75,	14,	103,	0,		63,	11,	80,	100,	6,		0,		,			,			,			
	54,	Pratome,			4000,3,	94,	17,	4,		0,		63,	11,	80,	100,	7.5,	0,		,			,			,			
	55,	Pranates,			8000,3,	110,26,	105,	0,		61,	11,	80,	100,	8,		0,		,			,			,			
	56,	Metapra,			16000,3,	124,32,	6,		0,		61,	11,	80,	100,	7.5,	0,		,			,			,			
	57,	Prateum,			25000,3,	259,46,	107,	0,		62,	11,	80,	100,	7,		0,		,			,			,			
	58,	Pragod,				30000,3,	311,58,	8,		0,		62,	11,	80,	100,	7.5,	0,		,			,			,			
	59,	Youprabird,			32000,3,	464,79,	109,	0,		57,	11,	80,	100,	7.5.3,	0,		,			,			,			
	74,	Sere,				250,4,	30,	2,	0,		0,		1,	11,	80,	100,	7,		0,		,			,			,			
;
; Spirit Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	75,	Serena,				500,4,	35,	6,	101,	4,		7,	11,	80,	100,	0,		0,		,			,			,			
	76,	Yousere,			1000,4,	57,	8,	2,		0,		71,	11,	80,	100,	5,		0,		,			,			,			
	77,	Osere,				2000,4,	75,	14,	103,	0,		86,	11,	80,	100,	5,		0,		,			,			,			
	78,	Seretome,			4000,4,	94,	17,	4.0,	0,		87,	11,	80,	100,	5,		0,		,			,			,			,	
	79,	Serenates,			8000,4,	110,26,	105.1,	0,		7,	11,	80,	100,	5.3.7,	0,		,			,			,			
	80,	Metasere,			16000,4,	124,32,	6,		0,		78,	11,	80,	100,	5.3.7,	0,		,			,			,			
	81,	Sereteum,			25000,4,	259,46,	107,	0,		71,	11,	80,	100,	7.3.5,	0,		,			,			,			
	82,	Seregod,			30000,4,	311,58,	8.0,	0,		71,	11,	80,	100,	5.1.9,	0,		,			,			,			
	83,	Kaakixseregod,		32000,4,	464,79,	109,	0,		85,	11,	80,	100,	8.6.9,	0,		,			,			,			
	84,	Ig,					250,5,	30,	2,	0,		4,		58,	11,	80,	100,	0,		0,		boom1.wav,	,			,			
	85,	Igna,				500,5,	35,	6,	101,	4,		58,	11,	80,	100,	0.4,	0,		,			,			,			
	86,	Youig,				1000,5,	57,	8,	2,		4,		55,	11,	80,	100,	0,		0,		,			,			,			
	87,	Oig,				2000,5,	75,	14,	103,	4,		55,	11,	80,	100,	0.4,	0,		,			,			,			
;	93,	"Summon Home",		0,	6,	-300.99,0,7,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	94,	"Summon Specific",	0,	6,	-203.44,0,7,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	95,	"Summon Same",		0,	6,	-200,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	96,	"Summon Element",	0,	6,	-201,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	97,	"Summon Random",	0,	6,	-202,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
	98,	Igtome,				4000,5,	94,	17,	4,		4,		59,	11,	80,	100,	0,		0,		,			,			,			
	99,	Ignates,			8000,5,	110,26,	105,	0,		59,	11,	80,	100,	8.4,	0,		,			,			,			
;						-8			DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-9			DISEASE_CHARM		(once charmed, monster fights for you)	
;						-10			DISEASE_TAME		(taming a charmed monster makes it a pet)	
;						-5			DISEASE_GAG			(cannot case spells)(* wears off)		
;						-21			DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-200		SUMMON_SAME			Summon a monster of same type as caster
;						-201		SUMMON_SAME_ELEMENT	Summon a monster of the same element as caster
;						-202		SUMMON_RANDOM		Summon a random monster at or below level of caster (within 10 levels)
;						-300		SUMMON_ITEM			Summon an item to be used on the target (item id in dotteg arg)
;
; Fire Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	101,Metaig,				16000,5,	124,32,	6,		4000,	18,	11,	80,	100,	0,		0,		,			,			,			
	102,Igteum,				25000,5,	259,46,	107,	5000,	18,	11,	80,	100,	1.4,	0,		,			,			,			
	103,Iggod,				30000,5,	311,58,	8,		0,		14,	11,	80,	100,	0,		0,		,			,			,			
	104,Kaakiiggod,			32000,5,	464,79,	109,	6000,	14,	11,	80,	100,	2.6.3,	0,		,			,			,			
	105,Nihi,				250,6,	30,	2,	0,		7000,	14,	11,	80,	100,	3,		0,		,			,			,			
	106,Nihina,				500,6,	35,	6,	101,	10000,	55,	11,	80,	100,	4,		0,		,			,			,			
	107,Younihi,			1000,6,	57,	8,	2,		0,		24,	11,	80,	100,	0,		0,		,			,			,			
	108,Onihi,				2000,6,	75,	14,	103,	0,		55,	11,	80,	100,	0,		0,		,			,			,			
	109,Nihitome,			4000,6,	94,	17,	4,		0,		58,	11,	80,	100,	0,		0,		,			,			,			
	124,Nihinates,			8000,6,	110,26,	105,	0,		89,	11,	80,	100,	0,		0,		,			,			,			
;
; Water Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	151,Metanihi,			16000,6,	124,32,	6,		8000,	52,	11,	80,	100,	0,		0,		,			,			,			
	152,Nihiteum,			25000,6,	259,46,	107,	9000,	11,	11,	80,	100,	1000,	0,		,			,			,			
	153,Nihigod,			30000,6,	311,58,	8,		10000,	54,	11,	80,	100,	1000,	0,		,			,			,			
	154,Nanihigod,			32000,6,	464,79,	109,	11000,	10,	11,	80,	100,	2000,	0,		,			,			,			
;	155,"Deluge All",		-1,	1,	0,	0,	104,	0,		53,	11,	80,	100,	2000,	0,		,			,			,			,	
	156,Teo,				250,7,	30,	2,	0,		0,		67,	11,	80,	100,	0,		0,		,			,			,			
	157,Teona,				500,7,	35,	6,	101,	0,		50,	11,	80,	100,	9003000,0,		,			,			,			
	158,Youteo,				1000,7,	57,	8,	2,		0,		67,	11,	80,	100,	0,		0,		,			,			,			
	159,Oteo,				2000,7,	75,	14,	103,	0,		51,	11,	80,	100,	9003000,0,		,			,			,			
	160,Teotome,			4000,7,	94,	17,	4,		0,		13,	11,	80,	100,	9003000,0,		,			,			,			
;
; Nature Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	175,Teonates,			8000,7,	110,26,	105,	4000,	15,	11,	80,	100,	0,		0,		,			,			,			
	176,Metateo,			16000,7,	124,32,	6,		5000,	81,	11,	80,	100,	1,		0,		,			,			,			
	177,Teoteum,			25000,7,	259,46,	107,	5000,	88,	11,	80,	100,	1,		0,		,			,			,			
	178,Teogod,				30000,7,	311,58,	8,		5000,	1,	11,	80,	100,	1,		0,		,			,			,			
	179,Okikaateogod,		32000,7,	464,79,	109,	6000,	22,	11,	80,	100,	2,		0,		,			,			,			
	180,Animus,				-1,	0,	-1,	28,	9,		5000,	81,	11,	80,	100,	1,		0,		,			,			,			
	181,Nanikueteyus,		-1,	4,	510,110,109.1,	004014201,85,	11,	80,	100,	005.6.3,0,		chant4.wav,	travel8.wav,magic7.wav,	
	182,Agerudairiki,		250,0,	-120,8,	0,		5000,	84,	11,	80,	100,	1,		0,		,			,			,			
	183,Agerugenki,			500,0,	-122,8,	1,		7000,	84,	11,	80,	100,	3,		0,		,			,			,			
	184,Ageruchie,			1000,0,	-121,8,	2,		5000,	84,	11,	80,	100,	1,		0,		,			,			,			
	185,Agerubinsoku,		2000,0,	-123,8,	3,		5000,	84,	11,	60,	100,	1,		0,		,			,			,			
	186,Agerujoute,			4000,0,	-124,8,	4,		5000,	84,	11,	84,	100,	1,		0,		,			,			,			
	187,Agerudairikigunshuu,8000,0,	-120,25,	105,	17000,	84,	16,	200,300,	3,		0,		,			,			,			
	199,Agerugenkigunshuu,	16000,0,	-122,25,	106,	0,		84,	11,	80,	100,	0,		0,		,			,			,			
	200,Ageruchiegunshuu,	25000,0,	-121,25,	107,	0,		84,	11,	80,	100,	0,		0,		,			,			,			
	201,Agerubinsokugunshuu,30000,0,	-123,25,	108,	0,		84,	11,	80,	100,	0,		0,		,			,			,			
	202,Agerujoutegunshuu,	32000,0,	-124,25,	109,	0,		84,	11,	80,	100,	0,		0,		,			,			,			
	203,Doku,				2000,2,	-2,	8,	3,		0,		3,	11,	80,	100,	0,		0,		,			,			,			
	204,Myuuto,				4000,7,	-5,	12,	4,		0,		7,	11,	80,	100,	0,		0,		,			,			,			
	205,Nousatsu,			4000,2,	-9,	20,	0,		0,		83,	11,	80,	100,	0,		0,		,			,			,			
	206,Kainarasu,			4000,2,	-10,0,	0,		0,		80,	11,	80,	100,	0,		0,		,			,			,			
	207,"Sojib: Holy Slime",-1,	0,	-203.104,10,	0,		0,		27,	11,	80,	100,	0,		0,		,			,			,			,	
	208,Kudasudairiki,		250,2,	-20,8,	0,		0,		81,	11,	80,	100,	0,		0,		,			,			,			
	209,Kudasugenki,		500,2,	-22,8,	1,		0,		81,	11,	80,	100,	0,		0,		,			,			,			
	210,Kudasuchie,			1000,2,	-21,8,	2,		0,		81,	11,	80,	100,	0,		0,		,			,			,			
	211,Kudasubinsoku,		2000,2,	-23,8,	3,		0,		81,	11,	80,	100,	0,		0,		,			,			,			
	212,Kudasujoute,		4000,2,	-24,8,	4,		0,		81,	11,	80,	100,	0,		0,		,			,			,			
	213,Kudasudairikigunshuu,8000,2,	-20,25,	105,	0,		81,	11,	80,	100,	0,		0,		,			,			,			
	214,Kusadugenkigunshuu,	16000,2,	-22,25,	106,	0,		81,	11,	80,	100,	0,		0,		,			,			,			
	215,Kudasuchiegunshuu,	25000,2,	-21,25,	107,	0,		81,	11,	80,	100,	0,		0,		,			,			,			
	216,Kudasubinsokugunshuu,30000,2,	-23,25,	108,	0,		81,	11,	80,	100,	0,		0,		,			,			,			
	217,Kudasujoutegunshuu,	32000,2,	-24,25,	109,	0,		81,	11,	80,	100,	0,		0,		,			,			,			
	218,"Sojib: Blaze Slug",250,2,	-203.112,7,	0.0.0.8,0,		27,	11,	80,	100,	0,		0,		,			,			,			,	
	219,"Sojib: Mujagwi",	500,4,	-202,12,	0.0.0.8,0,		29,	11,	80,	100,	0,		0,		,			,			,			,	
;	220,Nova,				-1,	2,	0,	0,	109,	0,		59,	11,	80,	100,	0,		0,		,			,			,			,	
;	221,Nova,				-1,	2,	0,	0,	109,	0,		59,	11,	80,	100,	0,		0,		,			,			,			,	
;	222,Nova,				-1,	2,	0,	0,	109,	0,		59,	11,	80,	100,	0,		0,		,			,			,			,	
;	223,Nova,				-1,	2,	0,	0,	109,	0,		59,	11,	80,	100,	0,		0,		,			,			,			,	
; Alchemy, Tier I
	224,Heal,				1000,0,	1000,10,	1.1.0.9.1222,0,		16,	11,	80,	100,	0,		0,		summon5.wav,travel7.wav,magic4.wav,	0.0,
	225,Cure,				1000,0,	-102,10,	1.1.0.9.1223,0,		21,	11,	80,	100,	0,		0,		summon4.wav,travel9.wav,CHIRP2.WAV,	,	
	226,Defend,				1000,0,	-122,10,	101.1.0.9.1224,0,		59,	11,	80,	100,	0,		0,		,			,			,			,	
	227,Levitate,			1000,7,	90,	10,	1.1.0.9.1225,0,		4,	11,	80,	100,	0,		0,		chant6.wav,	travel11.wav,thunder1.wav,,	
	228,Speed,				1000,0,	-123,10,	101.1.0.9.1226,0,		27,	11,	80,	100,	0,		0,		summon5.wav,travel11.wav,magic8.wav,	,	
	229,Flash,				1000,5,	90,	10,	1.1.0.9.1227,0,		59,	11,	80,	100,	0,		0,		summon4.wav,travel8.wav,fire1.wav,	,	
	230,"Hard Ball",		1000,3,	90,	10,	1.1.0.9.1228,0,		3,	11,	80,	100,	0,		0,		summon5.wav,travel4.wav,petWhip.wav,,	
	231,"Acid Rain",		1000,1,	85,	10,	101.1.0.9.1229,0,		52,	11,	80,	100,	0,		0,		summon5.wav,travel4.wav,rain1.wav,	,	
; Alchemy, Tier II
	232,Revealer,			5000,7,	-22,2,	102.1.0.9.1230,0,		19,	11,	80,	100,	0,		0,		summon1.wav,travel11.wav,magic3.wav,	,	
	233,Escape,				5000,0,	-123,2,	102.1.0.9.1231,0,		65,	11,	80,	100,	0,		0,		,			,			,			,	
	234,Revive,				5000,0,	1750,20,	102.1.0.9.1232,0,		8,	11,	80,	100,	0,		0,		,			,			,			,	
	235,Barrier,			5000,6,	-21,2,	102.1.0.9.1233,0,		10,	11,	80,	100,	0,		0,		,			,			,			,	
	236,Sting,				5000,2,	180,20,	2.1.0.9.1234,0,		57,	11,	80,	100,	0,		0,		,			,			,			,	
	237,Atlas,				5000,0,	-120,2,	102.1.0.9.1235,0,		73,	11,	80,	100,	0,		0,		,			,			,			,	
	238,Crush,				5000,3,	180,20,	2.1.0.9.1236,0,		61,	11,	80,	100,	0,		0,		,			,			,			,	
	239,Drain,				5000,4,	-3,	20,	2.1.0.9.1237,0,		79,	11,	80,	100,	0,		0,		,			,			,			,	
	240,Fireball,			5000,5,	180,20,	2.1.0.9.1238.0,0,		55,	11,	80,	100,	0,		0,		,			,			,			,	

;	Summoning Spells - Unlimited Reboot
	300,"Sojib: Mujagwi",	500,4,	-203.1030,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	301,"Sojib: Jugeum",	2500,4,	-203.1031,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	302,"Sojib: Gongpo",	5000,4,	-203.1032,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	303,"Sojib: Areumdaum",	15000,4,	-203.1033,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	304,"Sojib: Muljil",	500,1,	-203.1034,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	305,"Sojib: Seori",		2500,1,	-203.1035,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	306,"Sojib: Abunsi",	5000,1,	-203.1036,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	307,"Sojib: Nunbora",	15000,1,	-203.1037,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	308,"Sojib: Namu",		500,2,	-203.1038,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	309,"Sojib: Jandi",		2500,2,	-203.1039,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	310,"Sojib: Kochi",		5000,2,	-203.1040,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	311,"Sojib: Yeou",		15000,2,	-203.1041,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	312,"Sojib: Neugdae",	500,3,	-203.1042,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	313,"Sojib: Jijin",		2500,3,	-203.1043,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	314,"Sojib: Teollim",	5000,3,	-203.1044,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	315,"Sojib: Hwangso",	15000,3,	-203.1045,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	316,"Sojib: Hwajae",	500,5,	-203.1046,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	317,"Sojib: Yulyeong",	2500,5,	-203.1047,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	318,"Sojib: Jiog",		5000,5,	-203.1048,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	319,"Sojib: Pogbal",	15000,5,	-203.1049,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	320,"Sojib: Danggeun",	500,6,	-203.1050,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	321,"Sojib: Dal",		2500,6,	-203.1051,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	322,"Sojib: Sig",		5000,6,	-203.1052,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	323,"Sojib: Hyeseong",	15000,6,	-203.1053,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	324,"Sojib: Peng",		500,7,	-203.1054,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	325,"Sojib: Nabi",		2500,7,	-203.1055,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	326,"Sojib: Sutarg",	5000,7,	-203.1056,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	327,"Sojib: Heolikein",	15000,7,	-203.1057,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	328,"Sojib: Jeongsin",	500,0,	-203.1058,15,	2.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	329,"Sojib: Georughan",	2500,0,	-203.1059,30,	4.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	330,"Sojib: Jeongui",	5000,0,	-203.1060,60,	6.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	
	331,"Sojib: Sinseonghan",15000,0,	-203.1061,120,8.1.0.8,3,		27,	11,	80,	100,	0,		0,		chant1.wav,	,			,			,	


;						-20			DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-22			DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-23			DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-24			DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	
;
; Health Spells
;
; Note: arg4 (DAMAGE) is VERY IMPORTANT HERE.  It is NOT set automatically. Do NOT use 0 (zero)
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15				16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP		summon.wav	travel.wav		strike.wav
;
;	201,"Health Weave",		0,	0,	50,	0,	0,		4000000,16,	11, 80, 100,	0,		0,		"",			"",				""
;	202,"Unparalyze",		0,	0,-111,	0,	0,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
;	203,"Antidote",			0,	0,-102, 0,	1,		0,		17,	11, 80, 100,	0,		0,		"",			"",				""
;	204,"Raise Dexterity",	0,	0,-124,	0,	1,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
;	205,"Raise Agility",	0,	0,-123,	0,	2,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
;	206,"Stop Leak",		0,	0,-103,	0,	2,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
;	207,"Heal",				0,	0, 100, 0,	3,		0,		5,	11, 80, 100,	0,		0,		"",			"",				""
;	208,"Raise Stamina",	0,	0,-122,	0,	3,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
;	209,"Un Gag",			0,	0,-105,	0,	4,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
;	210,"Numb away",		0,	0,-106,	0,	4,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
;	211,"Heal All",			0,	0, 500,	0,	105,	0,		6,	11, 80, 100,	0,		0,		"",			"",				""
;	212,"Awaken",			0,	0,-107,	0,	5,		0,		72,	11, 80, 100,	0,		0,		"",			"",				""
;	213,"Deconfuse",		0,	0,-108,	0,	6,		0,		72,	11, 80, 100,	0,		0,		"",			"",				""
;	214,"Raise Strength",	0,	0,-120,	0,	6,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
;	215,"Un Stun",			0,	0,-104,	0,	7,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
;	216,"Restore",			0,	0,3000, 0,	7,		0,		5,	11, 80, 100,	0,		0,		"",			"",				""
;	217,"Raise Wisdom",		0,	0,-121,	0,	8,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
;	223,"Resurrect",		0,	0,	-1, 0,	8,		3,		82,	11, 80, 100,	0,		0,		"",			"",				""
;	224,"Restore All",		0,	0,3000, 0,	109,	3,		5,	11, 80, 100,	0,		0,		"",			"",				""
;						-102			CURE DISEASE_POISON		(saps a little HP every few seconds)		(NOT WORKING YET)
;						-103			CURE DISEASE_SAP		(saps a little MP every few seconds)		(NOT WORKING YET)
;						-104			CURE DISEASE_STUN		(cannot move, or attack)
;						-105			CURE DISEASE_GAG		(cannot case spells)(* wears off)		
;						-106			CURE DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-107			CURE DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;						-108			CURE DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-111			CURE DISEASE_FREEZE		(cannot move)
;						-120			CURE DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-121			CURE DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-122			CURE DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-123			CURE DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-124			CURE DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	

; Special Element Spells
; These are generally for the use of special element monsters who can only cast within their own special element
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	 summon.wav	travel.wav	strike.wav
;
;	600,"Wyrm Breath",		-1,	100,0,	0,	4,		3,		75,	11,	80,	100,	0,		0,		chant1.wav,	,			,			
;	601,"More Eyes",		0,	254,-203.132,0,6,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
	
; Book Spells
; These are generally spellbound to book-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	 summon.wav	travel.wav	strike.wav
;
	700	"Book Ichi",		-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	701	"Book Ni",			-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	702	"Book San",			-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""
	703	"Book Shi",			-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"chant4.wav",	"",	""
	704	"Book Roku",		-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"chant5.wav",	"",	""
	705	"Book Hachi",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"chant6.wav",	"",	""
	706	"Book Mitsu",		-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"chant7.wav",	"",	""
	707	"Book Chi",			-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	708	"Book Ba",			-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	709	"Book Cho",			-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""

; Summoning Spells
; These are generally spellbound to spirit-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	750	"Summon",			-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	751	"Summon Ash",		-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	752	"Summon Ember",		-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	753	"Summon Coral",		-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	754	"Summon Spirit",	-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	755	"Summon Atmos",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	756	"Summon Chronos",	-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	757	"Summon Pragma",	-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	758	"Summon Arcana",	-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	759	"Summon Ethos",		-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""


;
; Be sure to leave a blank line at the end of this file
