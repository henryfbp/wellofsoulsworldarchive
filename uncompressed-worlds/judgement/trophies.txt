;
; TROPHIES
;
; TROPHIES are carried in the Trophy Bag
;
; They are basically only used for quests.  You can, for example, define specific trophies
; to be dropped by individual monsters.  "Wolf Ear"  "Bear Fang" etc. and then use
; cookies to check if the user has collected what you asked for.
;
; At any given time, the player has exactly one trophy bag, and it has a width and a height (measured in 'slots')
; Players start out with no bag at all, and you must script a scene to give them a bag when you feel they deserve
; one (probably an NPC bag-maker will give or sell them a bag, and possibly upgrade it over time.
;
; Bags look like this, for example:  (width=4, height=3, 12 slots total)
;
;   +----+----+----+----+
;   |    |    |    |    |
;   +----+----+----+----+
;   |    |    |    |    |
;   +----+----+----+----+
;   |    |    |    |    |
;   +----+----+----+----+
;
; Each slot can hold only one sort of trophy at a time, for example a fang in one slot, and a claw 
; in another.
;
; But most trophies (per following table)  'stack' in the sense that you can hold up to.. say.. 10 fangs
; in the same slot, and only when you get your 11th fang do you need another slot.
;
; The user can drag the contents of slots around, or right-click on them to sell or discard a slot's
; contents.  They can only sell when in a shop scene.  The game will automatically pick the 'best slot'
; to use when a new trophy is given to the player (unless you use a slot-specific cookie commands).
;
; But, should the player end up with two slots holding less than a full stack of the same trophy, they
; can 'drag one into the other' to merge them.
;
; If you want to completely remove the bag from the player, set both its width and height to 0.  Note
; that if you make the total number of slots smaller than it was, items will be lost if they cannot all
; fit in the remaining number of slots.  You might want to think about which dimension you want to 
; change first, if that will make a difference.
;
; It is the user's responsibility to keep some empty slots available, or they might not be able
; to get an important trophy.  The world developer can measure how much room is in the bag and
; warn the player if they have not got room for a mission-critical trophy.
;
; Trophies can NOT be shared between players.  Trophies are not ITEMs and fulfill no purpose
; other than as named-quest maguffins.
;
; The world developer has the following commands and cookie controls over the player's Trophy Bag
;
;// COMMANDS  (running out of letters, so 'Z' means trophy only because.. um.. I have no idea :-)
;//  %Znn					- name of trophy nn  (e.g. "Hello, %1, that's a nice %Z23 you have there!")
;//  GIVE Znn.cc				- give cc of trophy nn  (e.g.  HOST_GIVE Z23.11)
;//  TAKE Znn.cc				- take cc of trophy nn
;//  Conditional  Znn.cc		- go to label if host has cc or more of trophy NN  (e.g.  IF Z23.10 @hasTenOrMore)
;//
;// COOKIES
;//  num.TrophyBagOpen			- (r/w) 0 if bag closed, 1 if open  (cannot open 0 - slot bag)
;//  num.TrophyBagWidth			- (r/w) width of bag
;//  num.TrophyBagHeight			- (r/w) height of bag
;//  num.TrophyBagSlots			- (r) total slots in bag
;//  num.TrophyBagSlotsInUse		- (r/w) non-empty slots in bag (WRITING any value, empties bag completely)
;//  num.TrophyBagEmptySlots		- (r) empty slots in bag
;//  num.TrophyNNN				- (r) how many trophyId=NNN hero has, in total, in bag
;//  num.TrophyBagRoomNNN		- (r) how many MORE trophyId=NNN hero could take (filling all stacks and slots)
;//  num.TrophyIdInSlotNNN       - (r/w)  id (or 0) of trophy in slot NNN.  (write REPLACEs slot with a SINGLE trophy)
;//  num.TrophyCountInSlotNNN    - (r/w)  qty (or 0) of trophy in slot NNN.  (write REPLACEs count, without changing id)
;//								  (cannot set count if id=0, cannot set count above 'stackHeight' for id)
;
; TABLE ARGUMENTS:
;
; Each Possible Trophy is defined by one line in this table, with the following 8 arguments per trophy.  Note that
; Trophy images are 32x32 'icons' stored in "filmstrip" files in the ART folder of your world.  These must be
; prepared like any other WoS BMP file, and use the official WoS 256 color palette.  There are assumed to be
; guidelines in the file, so only 31x31 pixels are available for art.  The upper left pixel of the file defines
; the 'transparent' color for the icon.
;
;	//	arg 0	ID# 			(1-4095)
;	//	arg 1	trophy name 	(31 char max)
;	//	arg 2	ImageFileName,  (in ART folder, assumed to have .bmp extension)
;	//	arg 3	ImageIndex, 	(0 for first frame of 'filmstrip' in ImageFile)
;	//	arg 4	stack size 		(how many of these fit in a single Trophy Bag Slot) (0-99)
;	//	arg 5	gpForStack		(How much a shop will pay for a FULL STACK of these)
;	//	arg 6	demonId			(Which monster 'drops' these when killed) (use 0 if not to be found on monsters)
;	//	arg 7	probability, 	(percentage chance 0-100 you will get one for killing that monster)
;
;	Arg0	Arg1									Arg2				Arg3	Arg4	Arg5	Arg6	Arg7
;   ID		Name									ImageFileName		ImgIndx	StackSz StackGP	DemonID	findProb
;   1234     1234567890123456789012345678901     123456789012345
	1,		"Lizard Skin",			        	"trophy",			0,		25,		75,		1,		30
	2,		"Claw of Cancer",			        "trophy",			1,		25,		80,		2,		45
	3,		"Spider Web",						"trophy",			2,		10,		150,	3,		35
	4,		"Stinger",							"trophy",			3,		20,		78,		5,		48
	5,		"Spider Leg",						"trophy",			4,		25,		90,		6,		40
	6,		"Regal Fang",						"trophy",			5,		5,		2570,	7,		100
	7,		"Slime Residue",						"trophy",			6,		25,		100,	8,		45
	8,		"Chitinous Shell",					"trophy",			7,		15,		95,		9,		30
	9,		"Green Apple",						"trophy",			8,		10,		200,	10,		30
	10,		"Impblade Hilt",						"trophy",			9,		5,		500,	11,		25
	11,		"Hotstone",							"trophy",			10,		10,		250,	12,		29
	12,		"Wolf Fang",							"trophy",			11,		30,		150,	13,		30
	13,		"Impblade Edge",						"trophy",			12,		5,		3000,	15,		100
	14,		"Slug Moss",							"trophy",			15,		25,		80,		4,		30
	15,		"Impblade Steel",			    	"trophy",			14,		5,		75,	14,		23
	16,		"Picked Flower",					    "trophy",			13,		20,		400,	16,		35
	17,		"Fish Scale",						"trophy",			16,		25,		710,	17,		30
	18,		"Leecher Fang",						"trophy2",			2,		12,		360,	18,		40
	19,		"Hunk of Steel",						"trophy",			14,		12,		410,	19,		35
	20,		"Leviathan Skin",					"trophy",			16,		1,		1250,	20,		25
	21,		"Octomamu Tentacle",					"trophy",			20,		12,		473,	21,		35
	22,		"Ghastly Fin",						"trophy",			21,		12,		590,	22,		25
	23,		"Lobster Claw",						"trophy",			1,		12,		510,	23,		30
	24,		"Scorpion Claw",						"trophy",			22,		12,		585,	29,		27
	25,		"Wooden Heart",						"trophy",			23,		12,		630,	28,		20
	26,		"Skeleton Ring",						"trophy",			29,		12,		615,	32,		30
	27,		"Mystic Bone",						"trophy",			30,		12,		630,	33,		28
	28,		"Flame Spirit",						"trophy2",			4,		12,		665,	37,		30
	29,		"Arc Beak",							"trophy2",			6,		12,		630,	38,		35
	30,		"Scaldstone",						"trophy2",			5,		12,		600,	39,		45
	31,		"Hell's Cuirass",					"trophy2",			7,		12,		950,	40,		20
	32,		"Phial of Jellyfish Goop",						"trophy4",		0,	12,	725,	44,	18
	33,		"Old Spellbook",							"trophy4",		1,	12,	730,	45,	15
	34,		"Coiled Bull Horn",							"trophy4",		2,	12,	750,	46,	20
	35,		"Bloody Dinosaur Fang",						"trophy4",		3,	12,	795,	47,	17
	36,		"Buried Ore",							"trophy4",		4,	12,	816,	48,	19
	37,		"Infected Nectar",							"trophy4",		5,	12,	675,	151,	22
	38,		"Damn Dirty Pelt",							"trophy4",		6,	12,	660,	153,	18
	39.		"Crude Rawrgre Doll",						"trophy4",		7,	12,	690,	154,	20
;   ID		Name									ImageFileName		ImgIndx	StackSz StackGP	DemonID	findProb
	50,	"Cycloptic Eye",								"trophy3",		0,	12,	1250,	50,	15
	51,	"Rusty Screw",								"trophy3",		1,	12,	1421,	51,	20
	52,	"Flame Claw",								"trophy3",		2,	12,	1470,	52,	15
	53,	" Gnomic Rod",								"trophy3",		3,	12,	1500,	53,	17
	54,	"Reinforced Shell",								"trophy3",		4,	12,	1535,	54,	18
	55,	"Poisoned Fang",								"trophy3",		5,	12,	1580,	55,	20
	56,	"Arcane Stinger",								"trophy3",		6,	12,	1670,	56,	12
	57,	"Goblin Mask",								"trophy3",		7,	12,	1725,	57,	25
	58,	"Phial of Blending Jelly",							"trophy3",		8,	12,	1830,	58,	20
	59,	"Lighteater Stone",								"trophy3",		9,	12,	2500,	59,	5
	60,	"Dactyl Wing",								"trophy3",		10,	12,	1900,	60,	18
	61,	"Shadow Hide",								"trophy3",		11,	12,	1975,	61,	22
	62,	"Ape Fur",								"trophy3",		12,	12,	2100,	62,	15
	63,	"Bone Wand",								"trophy3",		13,	12,	1800,	63,	35
	64,	"Crimson Jelly",								"trophy3",		14,	12,	2140,	64,	20
	65,	"Brainsnake Tentacle",							"trophy3",		15,	12,	2200,	65,	17
	66,	"Draconic Talon",								"trophy3",		16,	12,	2400,	66,	16
	67,	"Ogrish Loincloth",								"trophy3",		17,	12,	5000,	67,	5
	68,	"Ancient Willow",								"trophy3",		18,	12,	2450,	68,	20
	69,	"Demon Signet",								"trophy3",		19,	12,	2550,	69,	18
	70,	"Petrified Eye",								"trophy3",		20,	12,	2620,	70,	20
;   ID		Name									ImageFileName		ImgIndx	StackSz StackGP	DemonID	findProb
	71,	"Misty Bead",								"trophy4",		8,	12,	2750,	156,	15
	72,	"Sanguine Scale",								"trophy4",		9,	12,	2815,	157,	17
	73,	"Lump of Charcoal",							"trophy4",		10,	12,	2900,	158,	16
	74,	"Wyrm Egg",								"trophy4",		11,	12,	2955,	159,	20
	75,	"Stingray Tail",								"trophy4",		12,	12,	3030,	160,	18
	76,	"Rawrgre's Lucky Clover",							"trophy4",		13,	12,	3070,	161,	15
	77,	"Emblem of Knighthood",							"trophy4",		14,	12,	3120,	164,	20
	78,	"Giant Bird Wing",								"trophy4",		15,	12,	3190,	165,	21
	79,	"Moon Leather",								"trophy4",		16,	12,	3230,	166,	14
	80,	"Tremorstone",								"trophy4",		17,	12,	3300,	167,	17
	81,	"Lump of Gold Ore",							"trophy4",		18,	12,	3350,	169,	19
	82,	"Web-covered Leaf",							"trophy4",		19,	12,	3415,	170,	20
	83,	"Electrical Wire",								"trophy4",		20,	12,	3470,	171,	16
	84,	"Astral Combustion",							"trophy4",		21,	12,	3520,	172,	21
	85,	"Paper Lamp",								"trophy4",		22,	12,	3390,	173,	10
	86,	"Broken Impmail",								"trophy4",		23,	12,	3400,	174,	15
	87,	"Broodmother Fangs",							"trophy4",		24,	12,	3480,	175,	20
;	200,	"Resonant Shadow",					"Christmas",		6,		12,		1,		4095,	0,	0,	3
;	243,	"Giant Troll Ear",					"trophy4",			23,		5,		1000,	243,	45
;	255,	"Black Wrath Blood",					"trophy2",			26,		1,		10000,	255,	30
	1000, "Stocking",							"Christmas",		0,		100,	1,		129-133,	85
	1001, "Piece of Candy",						"Christmas",		1,		30,		5000,	130-133,	50
	1002, "Delicious Cookie",					"Christmas",		5,		20,		7500,	131-133,	25
	1003, "Moist Cake",							"Christmas",		2,		10,		9000,	132.133,	15
	1004, "Giftbox",								"Christmas",		3,		5,		15000,	133,	5
	1005,	"10k Bank Token",					Christmas,			4,		1,		10000,	2003,	0,		,		1,		
	1006,	"50k Bank Token",					Christmas,			4,		1,		50000,	2003,	0,		,		1,		
	1007,	"100k Bank Token",					Christmas,			4,		1,		100000,	2003,	0,		,		1,		
	1008,	"250k Bank Token",					Christmas,			4,		1,		250000,	2003,	0,		,		1,		
	1009,	"500k Bank Token",					Christmas,			4,		1,		500000,	2003,	0,		,		1,		
	1010,	"1m Bank Token",					Christmas,			4,		1,		1000000,2003,	0,		,		1,		
	1011, "Prize Token",							"Christmas",		4,		9,		1,			2003,	0
	1100,	"Resonant Shadow",					"Christmas",		6,		99,		1,		4095,	0,	0,	3
;Alchemy-related trophies
	1200,	"Acorn",								"alchemy",			0, 		20,		200,	0,		0
	1201,	"Ash",								"alchemy",			1,		20,		200,	1-4095,		2
	1202,	"Atlas Amulet",						"alchemy",			2,		20,		200,	0,		0
	1203,	"Bone",								"alchemy",			3,		20,		200,	0,		0
	1204,	"Brimstone",							"alchemy",			4,		20,		200,	0,		0
	1205,	"Clay",								"alchemy",			5,		20,		200,	1-4095,		2
	1206,	"Crystal",							"alchemy",			6,		20,		200,	1-4095,		2
	1207,	"Dry Ice",							"alchemy",			7,		20,		200,	0,		0
	1208,	"Ethanol",							"alchemy",			8,		20,		200,	0,		0
	1209,	"Feather",							"alchemy",			9,		20,		200,	0,		0
	1210,	"Grease",							"alchemy",			10,		20,		200,	0,		0
	1211,	"Gunpowder",							"alchemy",			11,		20,		200,	0,		0
	1212,	"Iron",								"alchemy",			12,		20,		200,	0,		0
	1213,	"Limestone",							"alchemy",			13,		20,		200,	0,		0
	1214,	"Meteorite",							"alchemy",			14,		20,		200,	0,		0
	1215,	"Mud Pepper",						"alchemy",			15,		20,		200,	1-4095,		2
	1216,	"Mushroom",							"alchemy",			16,		20,		200,	0,		0
	1217,	"Oil",								"alchemy",			17,		20,		200,	1-4095,		2
	1218,	"Root",								"alchemy",			18,		20,		200,	1-4095,		2
	1219,	"Vinegar",							"alchemy",			19,		20,		200,	0,		0
	1220,	"Wax",								"alchemy",			20,		20,		200,	1-4095,		2
	1221,	"Water",								"alchemy",			21,		20,		200,	1-4095,		3
;Alchemy Spell Trophies
;  ===============Tier I spells==========================
	1222,	"Heal Charge",						"alchemy",			22,		9,		50,		0,		0
;	Recipe: 1 Root+ 1 Water
	1223,	"Cure Charge",						"alchemy",			23,		9,		50,		0,		0
;	Recipe:	2 Root+ 1 Oil
	1224,	"Defend Charge",						"alchemy",			24,		9,		50,		0,		0
;	Recipe:	1 Clay+ 1 Ash
	1225,	"Levitate Charge",					"alchemy",			25,		9,		50,		0,		0
;	Recipe:	1 Mud Pepper + 1 Water
	1226,	"Speed Charge",						"alchemy",			26,		9,		50,		0,		0
;	Recipe:	1 Wax+ 2 Water
	1227,	"Flash Charge",						"alchemy",			27,		9,		50,		0,		0
;	Recipe:	1 Wax+ 2 Oil
	1228,	"Hard Ball Charge",					"alchemy",			28,		9,		50,		0,		0
;	Recipe:	1 Crystal+ 1 Clay
	1229,	"Acid Rain Charge",					"alchemy",			29,		9,		50,		0,		0
;	Recipe:	1 Ash+ 3 Water
;   ================Tier II spells=====================
	1230,	"Revealer Charge",					"alchemy",			30,		9,		50,		0,		0
;	Recipe:	2 Ash + 1 Wax
	1231,	"Escape Charge",						"alchemy",			31,		9,		50,		0,		0
;	Recipe:	1 Wax + 1 Vinegar
	1232,	"Revive Charge",						"alchemy2",			0,		9,		50,		0,		0
;	Recipe: 3 Root + 1 Bone
	1233,	"Barrier Charge",					"alchemy2",			1,		9,		50,		0,		0
;	Recipe: 1 Limestone + 2 Bone
	1234,	"Sting Charge",						"alchemy2",			2,		9,		50,		0,		0
;	Recipe: 2 Water + 1 Vinegar
	1235,	"Atlas Charge",						"alchemy2",			3,		9,		50,		0,		0
;	Recipe: 1 Atlas Amulet + 1 Ash
	1236,	"Crush Charge",						"alchemy2",			4,		9,		50,		0,		0
;	Recipe: 1 Limestone + 1 Wax
	1237,	"Drain Charge",						"alchemy2",			5,		9,		50,		0,		0
;	Recipe: 1 Ethanol + 2 Root
	1238,	"Fireball Charge",					"alchemy2",			6,		9,		50,		0,		0
;	Recipe: 1 Brimstone + 2 Ash