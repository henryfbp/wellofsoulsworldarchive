SCENE 300		stairs, SCENE, "King", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master",master, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0
	IF C8+V99, @remort




	
1: Im sorry but you are not the type of person im looking for
1: im looking for a strong fighter at least one whos been a warlord for a long time

END


@remort

SET a, "25000"
	SET b, "#<num.hostkills>"
	COMPARE #<b>, #<a>
	IF> @remort2
	IF= @remort2

1:Hmm you must have at least 25000 monster kills to contiune.
END


@remort2

SET a, "50"
	SET b, " #<num.hostPKs>"
	COMPARE #<b>, #<a>
	IF> @remort3
	IF= @remort3

1: I see you have the 25000 Monster kills but you still need the 50 Player Kills.

END

@remort3

1: Nice you have the monster kills and the player kills required if you can beat me i will improve your skills.
WAIT 5.0
FIGHT 542
IF ALIVE, @reward
IF DEAD, @dead
END

@dead

1: come back when you are ready to try again.
GOTO EXIT
END

@reward

1: Great Job
 SET num.hostclass 31
GOTO LINK o,0,0
END



SCENE 301		stairs, SCENE, "Ninja", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master",master, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0

	IF C30+V99, @remort


	
1: Im sorry but you are not the type of person im looking for
1: im looking for a skilled fighter of the dark at least someone whos been an assassin for a long time.

END

@remort

SET a, "25000"
	SET b, "#<num.hostkills>"
	COMPARE #<b>, #<a>
	IF> @remort2
	IF= @remort2

1:Hmm you must have at least 25000 monster kills to contiune.
END


@remort2

SET a, "50"
	SET b, " #<num.hostPKs>"
	COMPARE #<b>, #<a>
	IF> @remort3
	IF= @remort3

1: I see you have the 25000 Monster kills but you still need the 50 Player Kills.

END

@remort3

1: Nice you have the monster kills and the player kills required if you can beat me i will improve your skills.
WAIT 5.0
FIGHT 542
IF ALIVE, @reward
IF DEAD, @dead
END

@dead

1: come back when you are ready to try again.
GOTO EXIT
END

@reward

1: Great Job
 SET num.hostclass 32
GOTO LINK o,0,0
END


SCENE 302		stairs, SCENE, "God", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master",master, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0


	IF C83+V99, @remort


	
1: Im sorry but you are not the type of person im looking for
1: im looking for one of gods followers at least someone whos been an angel for a long time

END

@remort

SET a, "25000"
	SET b, "#<num.hostkills>"
	COMPARE #<b>, #<a>
	IF> @remort2
	IF= @remort2

1:Hmm you must have at least 25000 monster kills to contiune.
END


@remort2

SET a, "50"
	SET b, " #<num.hostPKs>"
	COMPARE #<b>, #<a>
	IF> @remort3
	IF= @remort3

1: I see you have the 25000 Monster kills but you still need the 50 Player Kills.

END

@remort3

1: Nice you have the monster kills and the player kills required if you can beat me i will improve your skills.
WAIT 5.0
FIGHT 542
IF ALIVE, @reward
IF DEAD, @dead
END

@dead

1: come back when you are ready to try again.
GOTO EXIT
END

@reward

1: Great Job
 SET num.hostclass 33
GOTO LINK o,0,0
END


SCENE 303		stairs, SCENE, "Devil", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master",master, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0


	IF C13+V99, @remort


	
1: Im sorry but you are not the type of person im looking for
1: im looking for one of satans followers at least someone whos been a demon for awhile

END

@remort

SET a, "25000"
	SET b, "#<num.hostkills>"
	COMPARE #<b>, #<a>
	IF> @remort2
	IF= @remort2

1:Hmm you must have at least 25000 monster kills to contiune.
END


@remort2

SET a, "50"
	SET b, " #<num.hostPKs>"
	COMPARE #<b>, #<a>
	IF> @remort3
	IF= @remort3

1: I see you have the 25000 Monster kills but you still need the 50 Player Kills.

END

@remort3

1: Nice you have the monster kills and the player kills required if you can beat me i will improve your skills.
WAIT 5.0
FIGHT 542
IF ALIVE, @reward
IF DEAD, @dead
END

@dead

1: come back when you are ready to try again.
GOTO EXIT
END

@reward

1: Great Job
 SET num.hostclass 34
GOTO LINK o,0,0
END

SCENE 304		stairs, SCENE, "Grand Wizard", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master",master, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0


	IF C63+V99, @remort

	
1: Im sorry but you are not the type of person im looking for
1: im looking for a strong Mage at least one who has achieved the rank of battlemage for awhile

END


@remort

SET a, "25000"
	SET b, "#<num.hostkills>"
	COMPARE #<b>, #<a>
	IF> @remort2
	IF= @remort2

1:Hmm you must have at least 25000 monster kills to contiune.
END


@remort2

SET a, "50"
	SET b, " #<num.hostPKs>"
	COMPARE #<b>, #<a>
	IF> @remort3
	IF= @remort3

1: I see you have the 25000 Monster kills but you still need the 50 Player Kills.

END

@remort3

1: Nice you have the monster kills and the player kills required if you can beat me i will improve your skills.
WAIT 5.0
FIGHT 542
IF ALIVE, @reward
IF DEAD, @dead
END

@dead

1: come back when you are ready to try again.
GOTO EXIT
END

@reward

1: Great Job
 SET num.hostclass 35
GOTO LINK o,0,0
END








SCENE 305		stairs, SCENE, "Demon Axe", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Master Demon", baphomet, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0
	IF T999	@back
	IF T998 @reward
	IF I301 @back2


	1: Welcome I have a favor to ask of you.
	1: I would like you to defeat a few angels for me.
	1: In return i will give you one of my axes
	1: So will you do this for me.
	ASK 30
	IF YES @yes
	IF NO @no
	END

	@yes

	1: Thank you go to heaven in the lower left corner their is a barracks.
	1: In these barrack there will be 3 Elite Angels you must defeat.
	1: Return here when you do.
	GIVE T999
	GOTO EXIT
	END

	@no
	1: Ok then leave.
	END

	@back

	1: Go kill those angels before Satan gets any more mad at me.
	END

	@reward
	
	1: I see you have defeated them good good.
	1: Satan will be pleased.
	1: Here is your reward %1
	1: The Demon Axe
	GIVE I302
	TAKE T1000
	1: You can leave now.
	GOTO EXIT
	END

	@back2
	
	1: Thank you again Satan was very pleased.
	END






SCENE 306		stairs, SCENE, "Barracks", 0, 0

	MUSIC "ChildrenOfTheGrave.mid"
	THEME	0
	ACTOR	1,	"Jesus", jesus, 1, 20, 60
    SEL		1
	POSE	32,32,32
	MOVE	1,	50, 60
	WAIT	1.0

	IF T999 @fight

	1: Hello.

	END

@fight
	1: An intruder kill him
	FIGHT 534, 532, 531
	IF ALIVE @win
	IF DEAD @dead
END

@win

1: Angels arent what they use to be
N: %1 snickers
TAKE T999
GIVE T998
END

@dead
1: weak demon
END







SCENE 350 shop, SCENE, "Chest", 0, 0

	ACTOR	1,	"roby",	roby, 10, 30, 85
	ACTOR   2,	"",	124, 5, 25, 80


	
	1: Its 10,000 Souls per try.
	1: Just open the chest and see what you find.
	1: Would You Like to try your luck.
	ASK 30
	IF YES @game
	IF No @no

END

@no

	1: Oh Well.
END

@game

SET deposit, "10000"
IF G#<deposit>, @game1
IF -G#<deposit>, @nogo
END


@nogo

1: Sorry you dont seem to have enough Souls.
END

@game1

TAKE G10000
IF R35 @game2

1: Oh you didnt find anything.
1: Maybe next time.

END

@game2
IF R1, @rareitem
IF R1, @rarespell
IF R40, @manaitem
IF R40, @hpitem
IF R10, @rich
IF R30, @gold
IF R40, @mediumgold
IF R50, @lowergold
IF R60, @lowestgold
IF R15, @cocaine
IF R10, @IK


END

@rareitem

N: %1 has found a rare item
GIVE I853
1: Nice.
END

@rarespell

N: %1 has found a rare spell
GIVE S634
1: Nice.
END

@hpitem

N: %1 has found a Beer
GIVE I32
1: Nice.
END

@manaitem

N: %1 has found a Tri-Amber
GIVE I94
1: Nice.
END

@rich

N: %1 has found 300,000 Souls
GIVE G300000
1: Nice.
END

@gold

N: %1 has found 100,000 Souls
GIVE G100000
1: Nice.
END

@mediumgold

N: %1 has found 20,000 Souls
GIVE G10000
1: Nice.
END

@lowergold

N: %1 has found 10,000 Souls
GIVE G5000
1: At least you made your money back.
END

@lowestgold

N: %1 has found 5,000 Souls
GIVE G1000
1: Oh that sucks
END

@cocaine

N: %1 has found a crack rock
GIVE I
1: Nice
END

@IK

N: %1 has found an Immortal Killer
GIVE I28
1: Nice
END




























; Now we are at the extreme end of the file