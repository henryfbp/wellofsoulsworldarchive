@bosslist
SET allymenu1,"0,"
WAIT 0.2
SET allymenu2,"0,"
WAIT 0.2
SET allymenu3,"0,"
WAIT 0.2
SET allymenu4,"0,"
WAIT 0.2
SET allymenu5,"0,"
WAIT 0.2
SET allymenu6,"0,"
WAIT 0.2
SET allymenu7,"0,"
WAIT 0.2
SET allymenu8,"0,"
WAIT 0.2
COMPARE #<num.hostHasPet800>,"0"
IF= ,@give800
@got800
WAIT 0.2
COMPARE #<num.hostHasPet801>,"0"
IF= ,@give801
@got801
WAIT 0.2
COMPARE #<num.hostHasPet802>,"0"
IF= ,@give802
@got802
WAIT 0.2
COMPARE #<num.hostHasPet803>,"0"
IF= ,@give803
@got803
WAIT 0.2
COMPARE #<num.hostHasPet804>,"0"
IF= ,@give804
@got804
WAIT 0.2
COMPARE #<num.hostHasPet805>,"0"
IF= ,@give805
@got805
WAIT 0.2
COMPARE #<num.hostHasPet806>,"0"
IF= ,@give806
@got806
WAIT 0.2
COMPARE #<num.hostHasPet807>,"0"
IF= ,@give807
@got807
WAIT 0.2
N: Allies available will appear in the "shop" menu now...
OFFER #<allymenu1> #<allymenu2> #<allymenu3> #<allymenu4> #<allymenu5> #<allymenu6> #<allymenu7> #<allymenu8>
END
@give800
SET allymenu1,"800,"
GOTO @got800
END
@give801
SET allymenu2,"801,"
GOTO @got801
END
@give802
SET allymenu3,"802,"
GOTO @got802
END
@give803
SET allymenu4,"803,"
GOTO @got803
END
@give804
SET allymenu5,"804,"
GOTO @got804
END
@give805
SET allymenu6,"805,"
GOTO @got805
END
@give806
SET allymenu7,"806,"
GOTO @got806
END
@give807
IF T600,@readyforx
@returnwithx
GOTO @got807
END
@readyforx
SET allymenu8,"807,"
GOTO @returnwithx
END
;
;
;
@parts
COMPARE #<currentcore>,"2"
IF=,@core2
COMPARE #<currentcore>,"3"
IF=,@core3
COMPARE #<currentcore>,"4"
IF=,@core4
COMPARE #<currentcore>,"5"
IF=,@core5
COMPARE #<currentcore>,"6"
IF=,@core6
COMPARE #<currentcore>,"7"
IF=,@core7
COMPARE #<currentcore>,"8"
IF=,@core8
COMPARE #<currentcore>,"9"
IF=,@core9
COMPARE #<currentcore>,"10"
IF=,@core10
COMPARE #<currentcore>,"11"
IF=,@core11
COMPARE #<currentcore>,"12"
IF=,@core12
COMPARE #<currentcore>,"13"
IF=,@core13
COMPARE #<currentcore>,"14"
IF=,@core14
COMPARE #<currentcore>,"15"
IF=,@core15
COMPARE #<currentcore>,"16"
IF=,@core16
COMPARE #<currentcore>,"17"
IF=,@core17
COMPARE #<currentcore>,"18"
IF=,@core18
COMPARE #<currentcore>,"19"
IF=,@core19
COMPARE #<currentcore>,"20"
IF=,@core20
COMPARE #<currentcore>,"21"
IF=,@core21
COMPARE #<currentcore>,"22"
IF=,@core22
COMPARE #<currentcore>,"23"
IF=,@core23
COMPARE #<currentcore>,"24"
IF=,@core24
COMPARE #<currentcore>,"25"
IF=,@core25
COMPARE #<currentcore>,"26"
IF=,@core26
COMPARE #<currentcore>,"27"
IF=,@core27
COMPARE #<currentcore>,"28"
IF=,@core28
COMPARE #<currentcore>,"29"
IF=,@core29
COMPARE #<currentcore>,"30"
IF=,@core30
COMPARE #<currentcore>,"31"
IF=,@core31
COMPARE #<currentcore>,"32"
IF=,@core32
COMPARE #<currentcore>,"33"
IF=,@core33
COMPARE #<currentcore>,"34"
IF=,@core34
COMPARE #<currentcore>,"35"
IF=,@core35
COMPARE #<currentcore>,"36"
IF=,@core36
COMPARE #<currentcore>,"37"
IF=,@core37
COMPARE #<currentcore>,"38"
IF=,@core38
COMPARE #<currentcore>,"39"
IF=,@core39
COMPARE #<currentcore>,"40"
IF=,@core40
COMPARE #<currentcore>,"41"
IF=,@core41
COMPARE #<currentcore>,"42"
IF=,@core42
COMPARE #<currentcore>,"43"
IF=,@core43
COMPARE #<currentcore>,"44"
IF=,@core44
COMPARE #<currentcore>,"45"
IF=,@core45
COMPARE #<currentcore>,"46"
IF=,@core46
COMPARE #<currentcore>,"47"
IF=,@core47
COMPARE #<currentcore>,"48"
IF=,@core48
COMPARE #<currentcore>,"49"
IF=,@core49

COMPARE #<KM.1>,9
IF> , @mon1done
N: Need more %M1 parts to create new core part...
END
@mon1done
COMPARE #<KM.2>,9
IF> , @mon2done
N: Need more %M2 parts to create new core part...
END
@mon2done
COMPARE #<KM.3>,9
IF> , @mon3done
N: Need more %M3 parts to create new core part...
END
@mon3done
COMPARE #<KM.4>,9
IF> , @mon4done
N: Need more %M4 parts to create new core part...
END
@mon4done
COMPARE #<KM.5>,9
IF> , @mon5done
N: Need more %M5 parts to create new core part...
END
@mon5done
COMPARE #<KM.6>,9
IF> , @mon6done
N: Need more %M6 parts to create new core part...
END
@mon6done
HOST_GIVE I450
SET currentcore,"2"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core2
COMPARE #<KM.7>,9
IF> , @mon7done
N: Need more %M7 parts to create new core part...
END
@mon7done
COMPARE #<KM.8>,9
IF> , @mon8done
N: Need more %M8 parts to create new core part...
END
@mon8done
COMPARE #<KM.9>,9
IF> , @mon9done
N: Need more %M9 parts to create new core part...
END
@mon9done
COMPARE #<KM.10>,9
IF> , @mon10done
N: Need more %M10 parts to create new core part...
END
@mon10done
COMPARE #<KM.11>,9
IF> , @mon11done
N: Need more %M11 parts to create new core part...
END
@mon11done
HOST_GIVE I451
SET currentcore,"3"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core3
COMPARE #<KM.12>,9
IF> , @mon12done
N: Need more %M12 parts to create new core part...
END
@mon12done
COMPARE #<KM.13>,9
IF> , @mon13done
N: Need more %M13 parts to create new core part...
END
@mon13done
COMPARE #<KM.14>,9
IF> , @mon14done
N: Need more %M14 parts to create new core part...
END
@mon14done
COMPARE #<KM.15>,9
IF> , @mon15done
N: Need more %M15 parts to create new core part...
END
@mon15done
COMPARE #<KM.16>,9
IF> , @mon16done
N: Need more %M16 parts to create new core part...
END
@mon16done
COMPARE #<KM.17>,9
IF> ,@mon17done
N: Need more %M17 parts to create new core part...
END
@mon17done
HOST_GIVE I452
SET currentcore,"4"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core4
COMPARE #<KM.18>,9
IF> , @mon18done
N: Need more %M18 parts to create new core part...
END
@mon18done
COMPARE #<KM.19>,9
IF> , @mon19done
N: Need more %M19 parts to create new core part...
END
@mon19done
COMPARE #<KM.20>,9
IF> , @mon20done
N: Need more %M20 parts to create new core part...
END
@mon20done
COMPARE #<KM.21>,9
IF> , @mon21done
N: Need more %M21 parts to create new core part...
END
@mon21done
COMPARE #<KM.22>,9
IF> , @mon22done
N: Need more %M22 parts to create new core part...
END
@mon22done
COMPARE #<KM.23>,9
IF> ,@mon23done
N: Need more %M23 parts to create new core part...
END
@mon23done
HOST_GIVE I453
SET currentcore,"5"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core5
COMPARE #<KM.24>,9
IF> , @mon24done
N: Need more %M24 parts to create new core part...
END
@mon24done
COMPARE #<KM.25>,9
IF> , @mon25done
N: Need more %M25 parts to create new core part...
END
@mon25done
COMPARE #<KM.26>,9
IF> , @mon26done
N: Need more %M26 parts to create new core part...
END
@mon26done
COMPARE #<KM.27>,9
IF> , @mon27done
N: Need more %M27 parts to create new core part...
END
@mon27done
COMPARE #<KM.28>,9
IF> , @mon28done
N: Need more %M28 parts to create new core part...
END
@mon28done
COMPARE #<KM.29>,9
IF> ,@mon29done
N: Need more %M29 parts to create new core part...
END
@mon29done
HOST_GIVE I454
SET currentcore,"6"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core6
COMPARE #<KM.30>,9
IF> , @mon30done
N: Need more %M30 parts to create new core part...
END
@mon30done
COMPARE #<KM.31>,9
IF> , @mon31done
N: Need more %M31 parts to create new core part...
END
@mon31done
COMPARE #<KM.32>,9
IF> , @mon32done
N: Need more %M32 parts to create new core part...
END
@mon32done
COMPARE #<KM.33>,9
IF> , @mon33done
N: Need more %M33 parts to create new core part...
END
@mon33done
COMPARE #<KM.34>,9
IF> , @mon34done
N: Need more %M34 parts to create new core part...
END
@mon34done
COMPARE #<KM.35>,9
IF> ,@mon35done
N: Need more %M35 parts to create new core part...
END
@mon35done
HOST_GIVE I455
SET currentcore,"7"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core7
COMPARE #<KM.36>,9
IF> , @mon36done
N: Need more %M36 parts to create new core part...
END
@mon36done
COMPARE #<KM.37>,9
IF> , @mon37done
N: Need more %M37 parts to create new core part...
END
@mon37done
COMPARE #<KM.38>,9
IF> , @mon38done
N: Need more %M38 parts to create new core part...
END
@mon38done
COMPARE #<KM.39>,9
IF> , @mon39done
N: Need more %M39 parts to create new core part...
END
@mon39done
COMPARE #<KM.40>,9
IF> , @mon40done
N: Need more %M40 parts to create new core part...
END
@mon40done
HOST_GIVE I456
SET currentcore,"8"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core8
COMPARE #<KM.41>,9
IF> , @mon41done
N: Need more %M41 parts to create new core part...
END
@mon41done
COMPARE #<KM.42>,9
IF> , @mon42done
N: Need more %M42 parts to create new core part...
END
@mon42done
COMPARE #<KM.43>,9
IF> , @mon43done
N: Need more %M43 parts to create new core part...
END
@mon43done
COMPARE #<KM.44>,9
IF> , @mon44done
N: Need more %M44 parts to create new core part...
END
@mon44done
COMPARE #<KM.45>,9
IF> , @mon45done
N: Need more %M45 parts to create new core part...
END
@mon45done
COMPARE #<KM.46>,9
IF> , @mon46done
N: Need more %M46 parts to create new core part...
END
@mon46done
HOST_GIVE I457
SET currentcore,"9"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core9
COMPARE #<KM.47>,9
IF> , @mon47done
N: Need more %M47 parts to create new core part...
END
@mon47done
COMPARE #<KM.48>,9
IF> , @mon48done
N: Need more %M48 parts to create new core part...
END
@mon48done
COMPARE #<KM.49>,9
IF> , @mon49done
N: Need more %M49 parts to create new core part...
END
@mon49done
COMPARE #<KM.50>,9
IF> , @mon50done
N: Need more %M50 parts to create new core part...
END
@mon50done
HOST_GIVE I458
SET currentcore,"10"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core10
COMPARE #<KM.51>,9
IF> , @mon51done
N: Need more %M51 parts to create new core part...
END
@mon51done
COMPARE #<KM.52>,9
IF> , @mon52done
N: Need more %M52 parts to create new core part...
END
@mon52done
COMPARE #<KM.53>,9
IF> , @mon53done
N: Need more %M53 parts to create new core part...
END
@mon53done
COMPARE #<KM.54>,9
IF> , @mon54done
N: Need more %M54 parts to create new core part...
END
@mon54done
HOST_GIVE I459
SET currentcore,"11"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core11
COMPARE #<KM.55>,9
IF> , @mon55done
N: Need more %M55 parts to create new core part...
END
@mon55done
COMPARE #<KM.56>,9
IF> , @mon56done
N: Need more %M56 parts to create new core part...
END
@mon56done
COMPARE #<KM.57>,9
IF> , @mon57done
N: Need more %M57 parts to create new core part...
END
@mon57done
COMPARE #<KM.58>,9
IF> , @mon58done
N: Need more %M58 parts to create new core part...
END
@mon58done
HOST_GIVE I460
SET currentcore,"12"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core12
COMPARE #<KM.59>,9
IF> , @mon59done
N: Need more %M59 parts to create new core part...
END
@mon59done
COMPARE #<KM.60>,9
IF> , @mon60done
N: Need more %M60 parts to create new core part...
END
@mon60done
COMPARE #<KM.61>,9
IF> , @mon61done
N: Need more %M61 parts to create new core part...
END
@mon61done
COMPARE #<KM.62>,9
IF> , @mon62done
N: Need more %M62 parts to create new core part...
END
@mon62done
HOST_GIVE I461
SET currentcore,"13"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core13
COMPARE #<KM.63>,9
IF> , @mon63done
N: Need more %M63 parts to create new core part...
END
@mon63done
COMPARE #<KM.64>,9
IF> , @mon64done
N: Need more %M64 parts to create new core part...
END
@mon64done
COMPARE #<KM.65>,9
IF> , @mon65done
N: Need more %M65 parts to create new core part...
END
@mon65done
COMPARE #<KM.66>,9
IF> , @mon66done
N: Need more %M66 parts to create new core part...
END
@mon66done
HOST_GIVE I462
SET currentcore,"14"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core14
COMPARE #<KM.67>,9
IF> , @mon67done
N: Need more %M67 parts to create new core part...
END
@mon67done
COMPARE #<KM.68>,9
IF> , @mon68done
N: Need more %M68 parts to create new core part...
END
@mon68done
COMPARE #<KM.69>,9
IF> , @mon69done
N: Need more %M69 parts to create new core part...
END
@mon69done
COMPARE #<KM.70>,9
IF> , @mon70done
N: Need more %M70 parts to create new core part...
END
@mon70done
HOST_GIVE I463
SET currentcore,"15"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core15
COMPARE #<KM.71>,9
IF> , @mon71done
N: Need more %M71 parts to create new core part...
END
@mon71done
COMPARE #<KM.72>,9
IF> , @mon72done
N: Need more %M72 parts to create new core part...
END
@mon72done
COMPARE #<KM.73>,9
IF> , @mon73done
N: Need more %M73 parts to create new core part...
END
@mon73done
COMPARE #<KM.74>,9
IF> , @mon74done
N: Need more %M74 parts to create new core part...
END
@mon74done
HOST_GIVE I464
SET currentcore,"16"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core16
COMPARE #<KM.75>,9
IF> , @mon75done
N: Need more %M75 parts to create new core part...
END
@mon75done
COMPARE #<KM.76>,9
IF> , @mon76done
N: Need more %M76 parts to create new core part...
END
@mon76done
COMPARE #<KM.77>,9
IF> , @mon77done
N: Need more %M77 parts to create new core part...
END
@mon77done
COMPARE #<KM.78>,9
IF> , @mon78done
N: Need more %M78 parts to create new core part...
END
@mon78done
HOST_GIVE I465
SET currentcore,"17"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core17
COMPARE #<KM.79>,9
IF> , @mon79done
N: Need more %M79 parts to create new core part...
END
@mon79done
COMPARE #<KM.80>,9
IF> , @mon80done
N: Need more %M80 parts to create new core part...
END
@mon80done
COMPARE #<KM.81>,9
IF> , @mon81done
N: Need more %M81 parts to create new core part...
END
@mon81done
COMPARE #<KM.82>,9
IF> , @mon82done
N: Need more %M82 parts to create new core part...
END
@mon82done
COMPARE #<KM.83>,9
IF> , @mon83done
N: Need more %M83 parts to create new core part...
END
@mon83done
COMPARE #<KM.84>,9
IF> , @mon84done
N: Need more %M84 parts to create new core part...
END
@mon84done
HOST_GIVE I466
SET currentcore,"18"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core18
COMPARE #<KM.85>,9
IF> , @mon85done
N: Need more %M85 parts to create new core part...
END
@mon85done
COMPARE #<KM.86>,9
IF> , @mon86done
N: Need more %M86 parts to create new core part...
END
@mon86done
COMPARE #<KM.87>,9
IF> , @mon87done
N: Need more %M87 parts to create new core part...
END
@mon87done
COMPARE #<KM.88>,9
IF> , @mon88done
N: Need more %M88 parts to create new core part...
END
@mon88done
COMPARE #<KM.89>,9
IF> , @mon89done
N: Need more %M89 parts to create new core part...
END
@mon89done
HOST_GIVE I467
SET currentcore,"19"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core19
COMPARE #<KM.90>,9
IF> , @mon90done
N: Need more %M90 parts to create new core part...
END
@mon90done
COMPARE #<KM.91>,9
IF> , @mon91done
N: Need more %M91 parts to create new core part...
END
@mon91done
COMPARE #<KM.92>,9
IF> , @mon92done
N: Need more %M92 parts to create new core part...
END
@mon92done
COMPARE #<KM.93>,9
IF> , @mon93done
N: Need more %M93 parts to create new core part...
END
@mon93done
COMPARE #<KM.94>,9
IF> , @mon94done
N: Need more %M94 parts to create new core part...
END
@mon94done
HOST_GIVE I468
SET currentcore,"20"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core20
COMPARE #<KM.95>,9
IF> , @mon95done
N: Need more %M95 parts to create new core part...
END
@mon95done
COMPARE #<KM.96>,9
IF> , @mon96done
N: Need more %M96 parts to create new core part...
END
@mon96done
COMPARE #<KM.97>,9
IF> , @mon97done
N: Need more %M97 parts to create new core part...
END
@mon97done
COMPARE #<KM.98>,9
IF> , @mon98done
N: Need more %M98 parts to create new core part...
END
@mon98done
COMPARE #<KM.99>,9
IF> , @mon99done
N: Need more %M99 parts to create new core part...
END
@mon99done
COMPARE #<KM.100>,9
IF> , @mon100done
N: Need more %M100 parts to create new core part...
END
@mon100done
HOST_GIVE I469
SET currentcore,"21"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core21
COMPARE #<KM.101>,9
IF> , @mon101done
N: Need more %M101 parts to create new core part...
END
@mon101done
COMPARE #<KM.102>,9
IF> , @mon102done
N: Need more %M102 parts to create new core part...
END
@mon102done
COMPARE #<KM.103>,9
IF> , @mon103done
N: Need more %M103 parts to create new core part...
END
@mon103done
COMPARE #<KM.104>,9
IF> , @mon104done
N: Need more %M104 parts to create new core part...
END
@mon104done
COMPARE #<KM.105>,9
IF> , @mon105done
N: Need more %M105 parts to create new core part...
END
@mon105done
HOST_GIVE I470
SET currentcore,"22"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core22
COMPARE #<KM.106>,9
IF> , @mon106done
N: Need more %M106 parts to create new core part...
END
@mon106done
COMPARE #<KM.107>,9
IF> , @mon107done
N: Need more %M107 parts to create new core part...
END
@mon107done
COMPARE #<KM.108>,9
IF> , @mon108done
N: Need more %M108 parts to create new core part...
END
@mon108done
COMPARE #<KM.109>,9
IF> , @mon109done
N: Need more %M109 parts to create new core part...
END
@mon109done
COMPARE #<KM.110>,9
IF> , @mon110done
N: Need more %M110 parts to create new core part...
END
@mon110done
HOST_GIVE I471
SET currentcore,"23"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core23
COMPARE #<KM.111>,9
IF> , @mon111done
N: Need more %M111 parts to create new core part...
END
@mon111done
COMPARE #<KM.112>,9
IF> , @mon112done
N: Need more %M112 parts to create new core part...
END
@mon112done
COMPARE #<KM.113>,9
IF> , @mon113done
N: Need more %M113 parts to create new core part...
END
@mon113done
COMPARE #<KM.114>,9
IF> , @mon114done
N: Need more %M114 parts to create new core part...
END
@mon114done
COMPARE #<KM.115>,9
IF> , @mon115done
N: Need more %M115 parts to create new core part...
END
@mon115done
COMPARE #<KM.116>,9
IF> , @mon116done
N: Need more %M116 parts to create new core part...
END
@mon116done
HOST_GIVE I472
SET currentcore,"24"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core24
COMPARE #<KM.117>,9
IF> , @mon117done
N: Need more %M117 parts to create new core part...
END
@mon117done
COMPARE #<KM.118>,9
IF> , @mon118done
N: Need more %M118 parts to create new core part...
END
@mon118done
COMPARE #<KM.119>,9
IF> , @mon119done
N: Need more %M119 parts to create new core part...
END
@mon119done
COMPARE #<KM.120>,9
IF> , @mon120done
N: Need more %M120 parts to create new core part...
END
@mon120done
COMPARE #<KM.121>,9
IF> , @mon121done
N: Need more %M121 parts to create new core part...
END
@mon121done
HOST_GIVE I473
SET currentcore,"25"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core25
COMPARE #<KM.122>,9
IF> , @mon122done
N: Need more %M122 parts to create new core part...
END
@mon122done
COMPARE #<KM.123>,9
IF> , @mon123done
N: Need more %M123 parts to create new core part...
END
@mon123done
COMPARE #<KM.124>,9
IF> , @mon124done
N: Need more %M124 parts to create new core part...
END
@mon124done
COMPARE #<KM.125>,9
IF> , @mon125done
N: Need more %M125 parts to create new core part...
END
@mon125done
HOST_GIVE I474
SET currentcore,"26"
N: You have helped Dr Light create a new core for you!
MUSIC won
END
END

@core26
COMPARE #<KM.126>,9
IF> , @mon126done
N: Need more %M126 parts to create new core part...
END
@mon126done
COMPARE #<KM.127>,9
IF> , @mon127done
N: Need more %M127 parts to create new core part...
END
@mon127done
COMPARE #<KM.128>,9
IF> , @mon128done
N: Need more %M128 parts to create new core part...
END
@mon128done
COMPARE #<KM.129>,9
IF> , @mon129done
N: Need more %M129 parts to create new core part...
END
@mon129done
HOST_GIVE I475
SET currentcore,"27"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core27
COMPARE #<KM.130>,9
IF> , @mon130done
N: Need more %M130 parts to create new core part...
END
@mon130done
COMPARE #<KM.131>,9
IF> , @mon131done
N: Need more %M131 parts to create new core part...
END
@mon131done
COMPARE #<KM.132>,9
IF> , @mon132done
N: Need more %M132 parts to create new core part...
END
@mon132done
COMPARE #<KM.133>,9
IF> , @mon133done
N: Need more %M133 parts to create new core part...
END
@mon133done
HOST_GIVE I476
SET currentcore,"28"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core28
COMPARE #<KM.134>,9
IF> , @mon134done
N: Need more %M134 parts to create new core part...
END
@mon134done
COMPARE #<KM.135>,9
IF> , @mon135done
N: Need more %M135 parts to create new core part...
END
@mon135done
COMPARE #<KM.136>,9
IF> , @mon136done
N: Need more %M136 parts to create new core part...
END
@mon136done
COMPARE #<KM.137>,9
IF> , @mon137done
N: Need more %M137 parts to create new core part...
END
@mon137done
HOST_GIVE I477
SET currentcore,"29"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core29
COMPARE #<KM.138>,9
IF> , @mon138done
N: Need more %M138 parts to create new core part...
END
@mon138done
COMPARE #<KM.139>,9
IF> , @mon139done
N: Need more %M139 parts to create new core part...
END
@mon139done
COMPARE #<KM.140>,9
IF> , @mon140done
N: Need more %M140 parts to create new core part...
END
@mon140done
COMPARE #<KM.141>,9
IF> , @mon141done
N: Need more %M141 parts to create new core part...
END
@mon141done
HOST_GIVE I478
SET currentcore,"30"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core30
COMPARE #<KM.142>,9
IF> , @mon142done
N: Need more %M142 parts to create new core part...
END
@mon142done
COMPARE #<KM.143>,9
IF> , @mon143done
N: Need more %M143 parts to create new core part...
END
@mon143done
COMPARE #<KM.144>,9
IF> , @mon144done
N: Need more %M144 parts to create new core part...
END
@mon144done
COMPARE #<KM.145>,9
IF> , @mon145done
N: Need more %M145 parts to create new core part...
END
@mon145done
HOST_GIVE I479
SET currentcore,"31"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core31
COMPARE #<KM.146>,9
IF> , @mon146done
N: Need more %M146 parts to create new core part...
END
@mon146done
COMPARE #<KM.147>,9
IF> , @mon147done
N: Need more %M147 parts to create new core part...
END
@mon147done
COMPARE #<KM.148>,9
IF> , @mon148done
N: Need more %M148 parts to create new core part...
END
@mon148done
COMPARE #<KM.149>,9
IF> , @mon149done
N: Need more %M149 parts to create new core part...
END
@mon149done
HOST_GIVE I480
SET currentcore,"32"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core32
COMPARE #<KM.150>,9
IF> , @mon150done
N: Need more %M150 parts to create new core part...
END
@mon150done
COMPARE #<KM.151>,9
IF> , @mon151done
N: Need more %M151 parts to create new core part...
END
@mon151done
COMPARE #<KM.152>,9
IF> , @mon152done
N: Need more %M152 parts to create new core part...
END
@mon152done
COMPARE #<KM.153>,9
IF> , @mon153done
N: Need more %M153 parts to create new core part...
END
@mon153done
HOST_GIVE I481
SET currentcore,"33"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core33
COMPARE #<KM.154>,9
IF> , @mon154done
N: Need more %M154 parts to create new core part...
END
@mon154done
COMPARE #<KM.155>,9
IF> , @mon155done
N: Need more %M155 parts to create new core part...
END
@mon155done
COMPARE #<KM.156>,9
IF> , @mon156done
N: Need more %M156 parts to create new core part...
END
@mon156done
HOST_GIVE I482
SET currentcore,"34"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core34
COMPARE #<KM.157>,9
IF> , @mon157done
N: Need more %M157 parts to create new core part...
END
@mon157done
COMPARE #<KM.158>,9
IF> , @mon158done
N: Need more %M158 parts to create new core part...
END
@mon158done
HOST_GIVE I483
SET currentcore,"35"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core35
COMPARE #<KM.159>,9
IF> , @mon159done
N: Need more %M159 parts to create new core part...
END
@mon159done
COMPARE #<KM.160>,9
IF> , @mon160done
N: Need more %M160 parts to create new core part...
END
@mon160done
COMPARE #<KM.161>,9
IF> , @mon161done
N: Need more %M161 parts to create new core part...
END
@mon161done
HOST_GIVE I484
SET currentcore,"36"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core36
COMPARE #<KM.162>,9
IF> , @mon162done
N: Need more %M162 parts to create new core part...
END
@mon162done
COMPARE #<KM.163>,9
IF> , @mon163done
N: Need more %M163 parts to create new core part...
END
@mon163done
HOST_GIVE I485
SET currentcore,"37"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core37
COMPARE #<KM.164>,9
IF> , @mon164done
N: Need more %M164 parts to create new core part...
END
@mon164done
COMPARE #<KM.165>,9
IF> , @mon165done
N: Need more %M165 parts to create new core part...
END
@mon165done
COMPARE #<KM.166>,9
IF> , @mon166done
N: Need more %M166 parts to create new core part...
END
@mon166done
HOST_GIVE I486
SET currentcore,"38"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core38
COMPARE #<KM.167>,9
IF> , @mon167done
N: Need more %M167 parts to create new core part...
END
@mon167done
COMPARE #<KM.168>,9
IF> , @mon168done
N: Need more %M168 parts to create new core part...
END
@mon168done
HOST_GIVE I487
SET currentcore,"39"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core39
COMPARE #<KM.169>,9
IF> , @mon169done
N: Need more %M169 parts to create new core part...
END
@mon169done
COMPARE #<KM.170>,9
IF> , @mon170done
N: Need more %M170 parts to create new core part...
END
@mon170done
COMPARE #<KM.171>,9
IF> , @mon171done
N: Need more %M171 parts to create new core part...
END
@mon171done
HOST_GIVE I488
SET currentcore,"40"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core40
COMPARE #<KM.172>,9
IF> , @mon172done
N: Need more %M172 parts to create new core part...
END
@mon172done
COMPARE #<KM.173>,9
IF> , @mon173done
N: Need more %M173 parts to create new core part...
END
@mon173done
HOST_GIVE I489
SET currentcore,"41"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core41
COMPARE #<KM.174>,9
IF> , @mon174done
N: Need more %M174 parts to create new core part...
END
@mon174done
COMPARE #<KM.175>,9
IF> , @mon175done
N: Need more %M175 parts to create new core part...
END
@mon175done
COMPARE #<KM.176>,9
IF> , @mon176done
N: Need more %M176 parts to create new core part...
END
@mon176done
HOST_GIVE I490
SET currentcore,"42"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core42
COMPARE #<KM.177>,9
IF> , @mon177done
N: Need more %M177 parts to create new core part...
END
@mon177done
COMPARE #<KM.178>,9
IF> , @mon178done
N: Need more %M178 parts to create new core part...
END
@mon178done
HOST_GIVE I491
SET currentcore,"43"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core43
COMPARE #<KM.179>,9
IF> , @mon179done
N: Need more %M179 parts to create new core part...
END
@mon179done
COMPARE #<KM.180>,9
IF> , @mon180done
N: Need more %M180 parts to create new core part...
END
@mon180done
COMPARE #<KM.181>,9
IF> , @mon181done
N: Need more %M181 parts to create new core part...
END
@mon181done
HOST_GIVE I492
SET currentcore,"44"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core44
COMPARE #<KM.182>,9
IF> , @mon182done
N: Need more %M182 parts to create new core part...
END
@mon182done
COMPARE #<KM.183>,9
IF> , @mon183done
N: Need more %M183 parts to create new core part...
END
@mon183done
HOST_GIVE I493
SET currentcore,"45"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core45
COMPARE #<KM.184>,9
IF> , @mon184done
N: Need more %M184 parts to create new core part...
END
@mon184done
COMPARE #<KM.185>,9
IF> , @mon185done
N: Need more %M185 parts to create new core part...
END
@mon185done
COMPARE #<KM.186>,9
IF> , @mon186done
N: Need more %M186 parts to create new core part...
END
@mon186done
HOST_GIVE I494
SET currentcore,"46"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core46
COMPARE #<KM.187>,9
IF> , @mon187done
N: Need more %M187 parts to create new core part...
END
@mon187done
COMPARE #<KM.188>,9
IF> , @mon188done
N: Need more %M188 parts to create new core part...
END
@mon188done
HOST_GIVE I495
SET currentcore,"47"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core47
COMPARE #<KM.189>,9
IF> , @mon189done
N: Need more %M189 parts to create new core part...
END
@mon189done
COMPARE #<KM.190>,9
IF> , @mon190done
N: Need more %M190 parts to create new core part...
END
@mon190done
COMPARE #<KM.191>,9
IF> , @mon191done
N: Need more %M191 parts to create new core part...
END
@mon191done
HOST_GIVE I496
SET currentcore,"48"
N: You have helped Dr Light create a new core for you!
MUSIC won
END

@core48
COMPARE #<KM.192>,9
IF> , @mon192done
N: Need more %M192 parts to create new core part...
END
@mon192done
COMPARE #<KM.193>,9
IF> , @mon193done
N: Need more %M193 parts to create new core part...
END
@mon193done
HOST_GIVE I499
HOST_GIVE T307
SET currentcore,"49"
N: You have helped Dr Light create the ULTIMATE power core !!!!
MUSIC won
END

@core49
N: You have obtained the ultimate power core already.
END

@ability
OFFER 110
N: Ability upgrades are now being offered via the SHOP menu.
END
;