;---------------MISC. SCENES-----------
;------------------JURAI---------------

SCENE 200		jurai, SCENE, "Jurai"
	ACTOR 1,	"The Master", master, 0, 15, 50
	MUSIC "jurai.mid"
	SEL		1
	IF C5, @knight
	' Ah, hello young %1.
	' I am surprised you found me out here.
	' This is the planet Jurai.
	' I can train you to become a different class.
	' The cost: A mere 100,000 GP to morph your class once.
	' Is this as you wish?(WARNING: Class changing is still relatively new and may permanently damage your character!  You have been warned!)
	ASK 20
	IF -YES, @skipmenu
	IF -G100000, @notenough
	MOVE	1, 30, 80
	' Ahh, excellent, what class do you wish to become?
	WAIT 2.0
	SOUND wisely.WAV
	IF V50+H10+G500000, @juraiknight
	MENU "Light Jedi=@ljedi", "Dark Jedi=@djedi", "Cop=@cop", "Boxer=@boxer", "Magician=@magic"
@skip
	' Oh, you like being a %C?
	END
@notenough
	' You don't have enough money.
	END
@juraiknight
	MENU "Light Jedi=@ljedi", "Dark Jedi=@djedi", "Cop=@cop", "Boxer=@boxer", "Magician=@magic", "Jurai Knight=@jknight"
	GOTO @skip
@ljedi
	' So you wish to become a jedi of the light side, eh?
	' That'll cost you 100K GP!
	' Is that okay?
	ASK 20
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G100000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 1
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Light Jedi!
	END
@djedi
	' So you wish to become a jedi of the dark side, eh?
	' That'll cost you 100K GP!
	' Is that okay?
	ASK 20
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G100000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 2
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Dark Jedi!
	END
@cop
	' So you wish to become a cop, eh?
	' That'll cost you 100K GP!
	' Is that okay?
	ASK 20
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G100000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 3
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Cop!
	END
@boxer
	' So you wish to become a boxer, eh?
	' That'll cost you 100K GP!
	' Is that okay?
	ASK 20
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G100000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 4
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Boxer!
	END
@magic
	' So you wish to become a magician, eh?
	' That'll cost you 100K GP!
	' Is that okay?
	ASK 20
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G100000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 6
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Magician!
	END
@jknight
	' So you wish to become a jurai knight, eh?
	' That'll cost you 500,000 Credits!
	' It's a very special class.
	' Offered only to those above level 50 that have played this character for at least 10 hours.
	' It's also a lot more expensive.
	' And, the training it takes is so intense, you will be pushed back down to level 20!
	' Is this as you wish?  (Remember, 500K G, reduce to level 20)
	ASK 20
	IF -G500000, @notenough
	IF -YES, @skip
	' Okay, here it goes!
	TAKE G500000
	POSE 1
	WAIT 2.0
	POSE 2
	SOUND thunder1.wav
	WAIT 1.0
	SET num.hostclass, 5
	POSE 0
	' Whew!  That takes a lot out of you!
	' Congratulations!  You are now a Jurai Knight!
	END
@knight
	' Hi %1.
	' Now that you are a Jurai Knight, I cannot change your class anymore.
	' Sorry.
	END

;------------------THE BANK--------------------

SCENE 201		bank, SCENE, "Universal Bank"
	MUSIC bank.mid
	ACTOR 1, "Bank Guard", actorskins, 3, 15, 70
@continue
	1: Welcome to the bank, %1.
	COMPARE #<a>, "1"
	IF= @normal
	1: Here, you can deposit any amount of money.
	SET money, "0"
	SET a, "0"
	1: When you enter the bank after this, you will have a few choices.
	1: The first is to deposit.  You merely choose "Deposit" To give money to the bank.
	1: The second option is to "Withdraw".  This allows you to take money *out* of the bank.
	SET a, "1"
@normal
	1: Your current balance is #<money> Gold.
	1: Would you like to "DEPOSIT" or "WITHDRAW" or "EXIT"?
	MENU "Deposit=@deposit", "Withdraw=@withdraw", "Exit=@exit"
	GOTO @normal
	END
@deposit
	1: How much money would you like to deposit? (Enter the amount)
	SET deposit, "0"
	ASK 9999999
	SET deposit, "#<lastAsk>"
	IF G#<deposit>, @continued
	1: You do not have that much money.
	GOTO @deposit
	END
@continued
	1: You would like to deposit #<deposit>? (YES/NO)
	IF NO, @no
	IF -G#<deposit>, @nogo
	1: Okay, please wait a moment..
	TAKE G#<deposit>
	ADD money, "#<deposit>"
	1: Thank you for your deposit.
	GOTO @normal
	END
@withdraw
	1: How much money would you like to withdraw? (Enter the amount)
	SET withdraw, "0"
	ASK 99999
	SET withdraw, "#<lastAsk>"
	COMPARE #<money>, #<withdraw>
	IF> @continuew
	IF= @continuew
	1: You do not have that much money deposited.  Your current balance is #<money>.
	GOTO @withdraw
	END
@continuew
	1: You would like to withdraw #<withdraw> Gold? (YES/NO)
	IF No, @no
	1: Okay, one moment please.
	GIVE G#<withdraw>
	SUB money, #<withdraw>
	1: Thank you for using our bank!
	GOTO @normal
	END 
@no
	1: No? Okay.
	GOTO @normal
	END
@nogo
	1: You no longer have enough money.
	GOTO @normal
	END
@exit
	1: Thank you for using the bank.
	END

;--------------GOLD SAUCER----------------

;   GAME 1	- The slobber slots machine.
;   GAME 2	- The Monster Racer
;   GAME 3	- The Search for Pi at Home
;   GAME 4	- Blackjack 
;   GAME 5  - Pokegatchi Training Center (same as 'train pet' on Equip Screen)
;   GAME 6  - Asteroids
;   GAME 7  - Stock Market Game
;   GAME 8  - Tetris

SCENE 211		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 1
	END

SCENE 212		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 2
	END

SCENE 213		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 4
	END

SCENE 214		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 6
	END

SCENE 215		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 7
	END

SCENE 216		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 8
	END

SCENE 217		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 3
	END

SCENE 218		arcade, 0, 0
	N: Loudspeaker: Welcome to the Gold Saucer!
	GAME 5
	END

;-------------THE INFORMATION KIOSK--------------

SCENE 220		jurai
	MUSIC jurai.mid
	ACTOR	1,	"The Kiosk Guy",	joshtownsfolk,	1,	10,	85
	SEL	1
	' Welcome to Beyond Sol!
	' I'm here to tell you about the game.
	' You may have seen various tickets on sale in the item shops...
	' Arena Town Ticket: Takes you to arena town, where you can enter the PK and pet arenas and teleport to three locations in the Sol System.
	' Gold Saucer Ticket: Takes you to the Gold Saucer, an arcade where you can play all the games!
	' You are currently in the Sol system, which is probably where you live in real life.
	' There are three other systems:
	' The Drengi system, the Arcea System, and the Sirius Binary System.
	' You can only reach them through use of a deep-space travel ship.
	' So...
	' Enjoy Beyond Sol and recommend it to your friends!
	END

;-----------------WELL OF SOULS-----------------

SCENE 221		temple, WELL, "Well Of Souls", 2, 7
	MUSIC intro.mid
	ACTOR	1,	"The Blind Sage",	BlindSage,	2,	10, 85
	SEL 1
	' I'm the Blind Sage.
	' From Evergreen.
	' Remember...?
	ASK 20
	IF -YES, @attack
	' Oh, good.
	' And I thought everyone hated me.
	END
@attack
	' Oh Really...
	' ATTACK!!!!!
	FIGHT	1, 62, 62, 62, 62, 62
	END