SCENE 60		cloudymountains2
	ACTOR	1,	"Farmer",	joshtownsfolk,	17,	10,	80
	SEL	1
	IF T62,	@givedoll	
	IF T61,	@stillgone
	IF T60, @ask
	
	' Please Help!
	' All my sheep have been disappearing!
	' In fact, everyone's sheep have been going missing
	' I suspect Space Wolves, but i can't find a launch pad nearby!
	' Please Help!
	GIVE T60
	END
@ask
	' Have you found out anything?
	' Those space wolves are causing you that much trouble?
	' I'll erase your memory of me if you can't fight them just yet.
	ASK 20
	IF YES, @take
	' Oh good.  Get out there and fight!
	END
@take
	TAKE T60
	' I do hope you'll come back and help.
	END
@stillgone
	' So you killed the wolves?
	' Did you find my sheep?
	' No?
	' Well please look for them.
	END
@givedoll
	ACTOR 2,	"Sheep",	joshanimals,	8,	10,	50
	ACTOR 3,	"Sheep",	joshanimals,	8,	50,	50
	ACTOR 4,	"Sheep",	joshanimals,	8,	20,	30
	ACTOR 5,	"Sheep",	joshanimals,	8,	80,	80
	2: Baaaa...
	3: Baaaa...
	4: Baaaa...
	5: Baaaa...
	1: Baaaa...
	1: I mean, Thank you for rescuing my sheep!
	IF T63, @skip
	1: Have this sheep doll
	GIVE T63
@skip
	END

;--------------------------------------------------------

SCENE 61		bgIceWall
	IF T61-T62, @wolf
	END
@wolf
	ACTOR 1,	"Sheep",	joshanimals,	8,	10,	50
	ACTOR 2,	"Sheep",	joshanimals,	8,	50,	50
	ACTOR 3,	"Sheep",	joshanimals,	8,	20,	30
	ACTOR 4,	"Sheep",	joshanimals,	8,	80,	80
	MUSIC seymour.mid
	2: Baaaa...
	3: Baaaa...
	4: Baaaa...
	1: Baaaa...
	WAIT 4.0
	FIGHT	1, 61
	IF WON, @hewon
	4:Baaaaaaa.....
	MOVE	4,	-15,	80,	1
	END
@hewon
	2: Baaaaa...
	N: You Won!  Now take the sheep back to Sirius A II.
	GIVE T62
	END