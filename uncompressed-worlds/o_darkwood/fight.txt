IF R10, @noRun
FIGHT
IF WIN, @bleghh
IF LOSE, @dead

@noRun
N: Trapped! Can't run!
FIGHT2

@bleghh
N: Victory!
END
@dead
N: You lost...
N: Would you like to go to the nearest inn?
ASK 30
IF YES, @ok
END

@ok
GOTO SCENE 1
END
