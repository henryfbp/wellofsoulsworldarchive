; Made by Silver for proxbeta
;
; To soul ID protect this file simply add in your soul ID.
; Right now there is a standard option to protect proxbeta with several soul ID's, but you can just copy/paste the lines and 
; expand it if needed.
; To soul ID protect this file simply add in your soul ID.
;
; Please do not forget to remove the semicons before the other string compare commands.
; To give a golden soul access fill in between the "..." SOUL <GS number>. I standardly put in my own soul ID as an example.
; To give a normal soul access simply fill in the persons entire soul number.
; For example:
; STRCMP #<str.soul>, "1234AB56"
;

	STRCMP #<str.soul>, "669C1452"
	IF= @gotoproxbetaactor
	STRCMP #<str.soul>, "76B7F513"
	IF= @gotoproxbetaactor 
	STRCMP #<str.soul>, "SOUL 2662"
	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 
;	STRCMP #<str.soul>, "..."
;	IF= @gotoproxbetaactor 

GOTO @proxbetasoulIDend

@proxbetamenuadmin
; 
; IMPORTANT! 
;
; Put here only the soul ID's you want to allow to use the admin functions in proxbeta.
; Other people who are allowed to use proxbeta but not these admin restrictions will not be able to use them.
;
; Please also note that admins can only use their priveleges if they can also use proxbeta itself.
; If there is a high demand for it to change then I'll try to change that.
;


	STRCMP #<str.soul>, "669C1452"
	IF= @gotoproxbetaactor
	STRCMP #<str.soul>, "76B7F513"
	IF= @gotoproxbetaactor 
	STRCMP #<str.soul>, "SOUL 2662"
	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 
;	STRCMP #<str.soul>, "..."
;	IF= @proxbetaadmininimenu 

	N: You are not authorized to use this function!
	END

	GOTO @proxbetaadmininimenu

@proxbetasoulIDend

; This file is copyright protected 2004 - 2005.  