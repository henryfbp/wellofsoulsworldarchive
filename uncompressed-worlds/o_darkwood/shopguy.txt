COMPARE #<num.mapNum>, 4
IF= @regret
COMPARE #<num.mapNum>, 3
IF= @bleakforest
@regret
Actor 1, "Shop Guy", Josh1, 1, 70, 50
POSE 1, 4, 3
#include update.txt
#include linkstyles.txt
1: Welcome to my generic shop!
1: You'll find everything you need here.
1: I promised my parole officer I wouldn't feast on the flesh from my customers' bones again, so you're safe.
GOTO @sell

@bleakforest
ACTOR 1, "Sh0pk33pr", JoshTownsfolk, 2, -10, 75
MOVE 1, 40, 77
POSE 2, 3
1: Hey there. 
1: I have all t3h l33t w4r3z for 4ll y0ur 4dv3ntur1ng n33dz!!
1: What exactly do you need, hon?
GOTO @sell


;This is the neat part of the script that will choose which items are sold
@eventActorClick1
@sell
MENU "Potions, Tonics, and Foods=@items","Helmets, Armor and Boots=@armor","Other Equipment=@other","Cancel=@end"
GOTO @end

@items
OFFER2 0, 99, 0
GOTO @end

@armor
COMPARE #<num.hostLevel>, 10
IF< @armor1
IF= @armor1

COMPARE #<num.hostLevel>, 20
IF< @armor2
IF= @armor2

COMPARE #<num.hostLevel>, 30
IF< @armor3
IF= @armor3

COMPARE #<num.hostLevel>, 40
IF< @armor4
IF= @armor4

COMPARE #<num.hostLevel>, 50
IF< @armor5
IF= @armor5

COMPARE #<num.hostLevel>, 60
IF< @armor6
IF= @armor6

COMPARE #<num.hostLevel>, 70
IF< @armor7
IF= @armor7

COMPARE #<num.hostLevel>, 80
IF< @armor8
IF= @armor8

COMPARE #<num.hostLevel>, 90
IF< @armor9
IF= @armor9
IF> @armor10

@armor1
OFFER2 1, 10, 10, 1, 10, 11, 1, 10, 20, 1, 10, 21
GOTO @end

@armor2
OFFER2 11, 20, 10, 11, 20, 11, 11, 20, 20, 11, 20, 21
GOTO @end

@armor3
OFFER2 21, 30, 10, 21, 30, 11, 21, 30, 20, 21, 30, 21
GOTO @end

@armor4
OFFER2 31, 40, 10, 31, 40, 11, 31, 40, 20, 31, 40, 21
GOTO @end

@armor5
OFFER2 41, 50, 10, 41, 50, 11, 41, 50, 20, 41, 50, 21
GOTO @end

@armor6
OFFER2 51, 60, 10, 51, 60, 11, 51, 60, 20, 51, 60, 21
GOTO @end

@armor7
OFFER2 61, 70, 10, 61, 70, 11,61, 70, 20, 61, 70, 21
GOTO @end

@armor8
OFFER2 71, 80, 10, 71, 80, 11, 71, 80, 20, 71, 80, 21
GOTO @end

@armor9
OFFER2 81, 90, 10, 81, 90, 11, 81, 90, 20, 81, 90, 21
GOTO @end

@armor10
OFFER2 91, 99, 10, 91, 99, 11, 91, 99, 20, 91, 99, 21
GOTO @end

@other
COMPARE #<num.hostLevel>, 10
IF< @other1
IF= @other1

COMPARE #<num.hostLevel>, 20
IF< @other2
IF= @other2

COMPARE #<num.hostLevel>, 30
IF< @other3
IF= @other3

COMPARE #<num.hostLevel>, 40
IF< @other4
IF= @other4

COMPARE #<num.hostLevel>, 50
IF< @other5
IF= @other5

COMPARE #<num.hostLevel>, 60
IF< @other6
IF= @other6

COMPARE #<num.hostLevel>, 70
IF< @other7
IF= @other7

COMPARE #<num.hostLevel>, 80
IF< @other8
IF= @other8

COMPARE #<num.hostLevel>, 90
IF< @other9
IF= @other9
IF> @other10

@other1
OFFER2 0, 10, 22, 0, 10, 23
GOTO @end

@other2
OFFER2 11, 20, 22, 11, 20, 23
GOTO @end

@other3
OFFER2 21, 30, 22, 21, 30, 23
GOTO @end

@other4
OFFER2 31, 40, 22, 31, 40, 23
GOTO @end

@other5
OFFER2 41, 50, 22, 41, 50, 23
GOTO @end

@other6
OFFER2 51, 60, 22, 51, 60, 23
GOTO @end

@other7
OFFER2 61, 70, 22, 61, 70, 23
GOTO @end

@other8
OFFER2 71, 80, 22, 71, 80, 23
GOTO @end

@other9
OFFER2 81, 90, 22, 81, 90, 23
GOTO @end

@other10
OFFER2 91, 99, 22, 91, 99, 23
GOTO @end

@end
N: Right-click if anything else is needed.
END