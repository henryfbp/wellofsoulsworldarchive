world\HTML folder
-----------------

This folder contains HTML pages and related artwork you might use in your world.

You can open an HTML page as part of a scripted command "HTML"

as in:

	SCENE...

	1: Please read the Book of the Dead
	HTML	"dead.htm"

	1: Isn't that shocking?
	END

In this case, it would open the file:  c:\WoS\Worlds\YourWorldName\HTML\dead.htm

The script would then pause until the host closed the web window.

-----

* If you start with "http://" then it will fetch the page from somewhere on the web instead.

* You can have sub-folders in the HTML folder of your world.