;EDGER
;----------
;Weapon Trader
SCENE 40  shop
	ACTOR 1, "Weapon Trader", josh51, 1, 18,79
	' I carry ONLY the FINEST imports from Paradyne
	' Buy something or get out!
	OFFER2 0,50,12, 0,50,16, 0,50,13
END

;----------
;Ranged Weapons
SCENE 41  shop
	ACTOR 1, "Nearsighted Mike", josh92, 2, 20,80
	' You like my bows?
	' I make them myself...
	OFFER2 0,50,17, 0,50,14
END

;----------
;Arcane Library
SCENE 42  bglibrary
	ACTOR 1, "Professor Faust", TownPeople, 21, 30,90
	' WHAT?!
	' WHAT!?
	' READING is FUNDAMENTAL!
	OFFER2 0,50,18, 0,50,19
END

;----------
;Panpipes
SCENE 43  Bazaar
	ACTOR 1, "Ace Of Cups", Royals, 30,30,80
	' Fashion Police? Never heard of 'em...
	' I just make music...
	IF T9, @stone
	GOTO @business
@stone
	IF T13 @business
	IF T12 @gotstone
	' Have you retrieved my father's stone yet?
	' he kept it locked away in a very secret place.
	' The entrance is just north of the Western Temple, right on the coast.
	' But I dont know where the key is... 
	' I'm sure you can find it.
	' If you need any help, talk to the High Priest.
	' I'd better get back to business as usual.
	GOTO @business
@gotstone
	' You FOUND IT
	' Oh! Thank you SO much!
	' Here! take this music box as a symbol of my gratitude!
	GIVE I343
	' Well I'd best get back to business...
	GIVE T13
@business
	' See anything you like?
	OFFER2 0,50,15
END

;----------
;Guild of The Wand
SCENE 44 bgStage2
	ACTOR 1, "Ace Of Wands", Royals, 21, 20,75
	IF T13, @waterdone
	IF T9, @waterstone
	IF T4, @sawpriest
	' You need to see the priest in the western temple before i can have a use for you.
	' Go there, to the north....
	GOTO @end
@sawpriest
	' Good, you have seen the High Priest.
	' I have a use for you.
	' We fear the terrible Gap Demon is soon to return to our realm.
	' In order to stop it, we will need to activate all four of the sacred stones.
	' The knowledge of how to activate their power
	' has been passed between generations;
	' Their secrets closely Guarded within four families.
 	' You will need to find the Sacred Stone of The Water...
	' And bring it to the Ace of Cups at Panpipes, just across from these Guilds.
	' It was his father who last weilded its power.
	' You may wish to speak with him before you mske your journey.
	' Go Now, Make Haste!
	GIVE T9
	GOTO @end
@waterstone
	' Have you found it yet?
	' You must deliver it to the Ace of Cups as soon as possible!
	GOTO @end
@waterdone
	' Good Work.
	' I may have another assignment for you in the future.
@end
END

;----------
;Guild Of The Sword
SCENE 45 bgStage2
	IF T7, @end
ACTOR 1, "Ace Of Swords", Josh19, 2, 20,85
		IF T6, @done
	IF T5, @notdone
	IF T4, @sawpriest
	' You have no business here until you have seen the High Priest!
	' Out with you!
	GOTO @end
@sawpriest
	' Good.. you have met the priest
	' I have work for you...
	' A long time ago, my grandfather was entrusted with the location of an artifact...
	' An artifact very sacred to this guild...
	' The sacred stone of the Air.
	' Upon his death several years ago, the knowledge was lost
	' We need its power to battle the evil Gap Demon...
	' You must find out Where the stone is....
	' All I can tell you is that we THINK it is FAR to the NORTH...
	' You will be rewarded for your efforts.
	' Go now, travel swiftly.
	GIVE T5
	GOTO @end
@notdone 
	' You have returned, but where is the Stone?
	GOTO @end
@done
	' Good Work!
	' It's in the east Is it?
	' Thank you for your effort...
	' I must  travel to find it...
	' What are you still standing around for?
	' Oh. Payment.
	WAIT 5
	' I'll tell you how to get to Paradyne!
	' Just Follow the river... 
	' South into the mountains.
	' There is a cave hidden there.
	GIVE T7
	' Get out of here.
@end
END

;------------
;CASTLE
SCENE 46 bgstage2
	ACTOR 1, "The High Guildmaster", Josh51, 1, 20,85
	' Welcome to my Kingdom...
	IF T8 @dockhere
	' You may have my permission to dock your ship in our port.
	GIVE T8
	' If you have a ship...
@dockhere
	IF T7 @swordgone
	' If you are seeking work, try the guilds to the south.
	GOTO @end
@swordgone
	' The Ace of swords seems to have left the city.
	' But you may still find work at the Guild of the Wand.
@end
END

;
;