;
; MONSTERS
;
;-------------------
; Monsters

;	//	arg 0	ID# (1-4095)
;	//	arg 1	name (16 char)
;	//	arg 2	skin name,
;				Set this to "mirror" to make the monster look just like YOU! 
;	//	arg 3   scale.flags.colors.xpar	<-- up to four arguments, separated by dots
;				scale factor (enlarges skin) legal values are -8 t0 +8
;					0 - normal size
;					1 - 125% normal
;					2 - 150% normal
;					3 - 175% normal
;					4 - 200% normal
;					5 - 225% normal
;					6 - 250% normal
;					7 - 275% normal
;					8 - 300% normal
;					-1 - 90% of normal
;					-2 - 80% of normal
;					...
;					-8 - 20% of normal (or some minimum size)
;					// note: resizing skins makes them ugly and pixellated and makes baby Josh cry.
;
;				[optional] MONSTER FLAGS 
;					add a period followed by the monster FLAG you want
;						example: 0.1  = normal size with FLAG = 1
;					// AVAILABLE MONSTER FLAGS: (add together to combine: 3 = 1+2)
;					1 - monster cannot be tamed (cannot capture as a pet)
;					2 - monster can only cast spells in his or her own element (no matter how wise)
;
;				[optional] COLORTABLE 0-255
;					add yet another period after the flag if you want this
;						example: 0.0.3 = normal size, no flags, color table 3
;					Color tables dynamically recolor the monster's skin without having to come up with
;					new art files (saves download size, at the expense of less artistry)
;					Here are the conversions as of A57, unimplemented color tables will have no effect 
;						0: no change. (standard palette order) 
;						These next ones swap two pigments of the RGB triplet. 
;						1: R-G swap
;						2: R-B Swap
;						3: G-B swap 
;						The following overright one pigment with another (so that 2 of the 3 are then the same) My color description is 'what happens to a pinkish human skin tone' 
;						4: G<-R 'yellow-greens' 
;						5: B<-R 'purples'
;						6: R<-G 'khakis'
;						7: B<-G 'reddens'
;						8: R<-B 'green-hulks'
;						9: G<-B 'pinkens' 
;						In the following, I reduce the intensity of one of the pigments. 
;						10: cut red slightly
;						11: cut green slightly
;						12: cut blue slightly 
;						In the following, I dim all three pigments the same amount, the image gets darker, but is 
;						still colored. 
;						13: dim little <-- good for 'low light'
;						14: dim medium
;						15: dim high 
;						Same deal here, only I brighten, pretty much to incandescence :-) 
;						16: brighten little
;						17: brighten medium
;						18: brighten lots
;						19: brighten even more
;						20: brighten super much 
;						The following remove all color and turn image to grayscale, then dim or brighten it. 
;						21: gray out colors and dim it lots
;						22: gray out colors and dim it a litte
;						23: gray out colors but keep intensity
;						24: gray out colors and brighten
;						25: gray out colors and brighten more 
;						The next ones are the psychedelic ones and definitely mess up the purity of the artistic vision. But they have a sort of x-ray look which might be useful somewhere 
;						26: invert red
;						27: invert green
;						28: invert blue
;						29: invert red and green
;						30: invert red and blue
;						31: invert blue and green
;						32: invert all
;
;				[optional] XPARENT [0/1]
;					If you set this to '1' then the monster will be ghostly transparent, sort of.

;
;
;	//	arg 4	element			[0-7, following elements defined in ELEMENTS table.  8-255 are "special" elements which may or may not work (as of A52)]
;				element.alignment  Optionally you can provide an 'alignment' to further categorize your monsters.
;								an alignment is a short (15 character max) word, using no punctuation or spaces
;								For example:  "4.evil"  <-- element 4, alignment 'evil'
;								The actual use of the alignment is hidden in the intricacies of the #<cookie> system.
;	//	arg 5	hp (also maxHP)	[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//	arg 6	mp (also maxMP)	[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//	arg 7	defense, 		[Set to 0 (zero) and it will be set automatically from LEVEL]

;	//	arg 8	offense, 		[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//	arg 9	exp pts (that you win by killing it)
;								// NOTE: a value of 0 is HIGHLY recommended (to set automatically from LEVEL]
;	//	arg 10	gold (that you win by killing it)
;								// NOTE: a value of 0 is HIGHLY recommended (to set automatically from LEVEL]
;	//  arg 11  level 			(many things can be computed automatically based on level, which approximates human player levels)
;
;	// the following fields are optional (default values will be used if you don't assign them)
;
;	//  arg 12	strength		[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//  arg 13	stamina			[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//  arg 14	agility			[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//  arg 15	dexterity		[Set to 0 (zero) and it will be set automatically from LEVEL]
;	//  arg 16	wisdom			[Set to -1 and it will be set automatically from LEVEL, 0 really means no wisdom, and therefore no magic-casting]
;								in general, a higher wisdom allows the monster to cast higher level spells (level <= wis/8)
;								but a high wisdom also allows monster to use spells "near" it's own element (about 40 wisdom points per step on the spell wheel)
;
; Monsters can have a couple custom sound effects.  If none is specified, they 
; will choose randomly from the 'standard' sounds.
;
;	//  arg 17  growl wave file		"snarl.wav" for example
;	//	arg 18	pain wave file		"ouch.wav" for example
;	//  arg 19	optional attack path  (see ITEMS table, argument 16 for details)
;				this controls additional animation during a monster's physical (not magical) attack.
;				-1		select one at random at world load
;				0		no attack path (default)
;				Nxx		path N, argument xx (see ITEMS table documentation)

;							skin		scale		el	hp		mp	def	off	exp	gld	lev	str	sta	agi	dex	wis		growl.wav				pain.wav		attack path
;	0	1................	2			3		4	5		6	7	8	9	10	11	12	13	14	15	16		17						18			19
;BY ELEMENT
;LIFE
	1,	"White jelly",		josh3,		0.2.24.0,0,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
;WATER
	21,	"Water jelly",		josh2,		0.2.3.0,1,		0,	0,	0,	0,	0,	0,	2,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	22,	"Land Crab",		josh15,		0.2.0.0,1,		0,	0,	0,	0,	0,	0,	12,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	23,	"Maneater",			josh12,		0.2.13.0,1,		0,	0,	0,	0,	0,	0,	22,	0,	0,	0,	0,	0,		petChew.wav,			petDead.WAV,	-1,				
	24,	"Water Sprite",		Efaerie,		0.3.0.0,1,		0,	0,	0,	0,	0,	0,	32,	0,	0,	0,	0,	-1,		Okiepain1.wav,			growl16.wav,	-1,				
	25,	"Gator",			josh18,		0.2.11.0,1,		0,	0,	0,	0,	0,	0,	42,	0,	0,	0,	0,	0,		petChew.wav,			pain4.wav,		-1,				
	26,	"Ocean Leech",		josh61,		0.2.5.0,1,		0,	0,	0,	0,	0,	0,	52,	0,	0,	0,	0,	0,		growl17.wav,			pain2.wav,		-1,				
	27,	"White Shark",		josh12,		1.2.20.0,1,		0,	0,	0,	0,	0,	0,	62,	0,	0,	0,	0,	0,		petChew.wav,			petDead.WAV,	-1,				
	28,	"Death at Sea",		josh91,		0.2.29.0,1,		0,	0,	0,	0,	0,	0,	72,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		3,				
	29,	"Sea Serpent",		josh14,		0.2.11.0,1,		0,	0,	0,	0,	0,	0,	82,	0,	0,	0,	0,	0,		growl23.wav,			pain3.wav,		-1,				
	30,	"Shipwrecker",		josh13,		1.2.14.0,1,		0,	0,	0,	0,	0,	0,	92,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
;NATURE
	41,	"Tree jelly",		josh2,		0.2.27.0,2,		0,	0,	0,	0,	0,	0,	3,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	42,	"Slug",				josh20,		0.2.30.0,2,		0,	0,	0,	0,	0,	0,	13,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	43,	"Giant Lizard",		Salamander,	1.2.1.0,2,		0,	0,	0,	0,	0,	0,	23,	0,	0,	0,	0,	0,		Hiss.wav,				Okiepain1.wav,	-1,				
	44,	"Tree Sprite",		Efaerie2,	0.2.1.0,2,		0,	0,	0,	0,	0,	0,	33,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	45,	"Raptor",				josh27,		0.2.3.0,2,		0,	0,	0,	0,	0,	0,	53,	0,	0,	0,	0,	0,		growl23.wav,			pain9.wav,		-1,				
	46,	"Giant Scorpion",	josh28,		0.2.23.0,2,		0,	0,	0,	0,	0,	0,	43,	0,	0,	0,	0,	0,		Hiss.wav,				pain2.wav,		-1,				
	47, "Paracite",			josh83,		0.2.0.0,2,		0,	0,	0,	0,	0,	0,	63,	0,	0,	0,	0,	0,		Okiepain1.wav,			pain13.WAV,		-1,				
	48,	"Jungle Tiger",		kitten,		0.2.2.0,2,		0,	0,	0,	0,	0,	0,	73,	0,	0,	0,	0,	0,		growl11.wav,			pain4.wav,		-1,				
	49,	"Mountaineer",		josh94,		0.2.0.0,2,		0,	0,	0,	0,	0,	0,	83,	0,	0,	0,	0,	0,		sword4.wav,				pain1.WAV,		-1,				
	50,	"Manticore",			Manticore,	0.2.0.0,2,		0,	0,	0,	0,	0,	0,	93,	0,	0,	0,	0,	-1,		growl12.wav,			pain17.WAV,		-1,				
;EARTH
	61,	"Rock jelly",		josh2,		0.2.25.0,3,		0,	0,	0,	0,	0,	0,	4,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	62,	"Wolf Cub",			josh35,		-1.0.18.0,3,	0,	0,	0,	0,	0,	0,	14,	0,	0,	0,	0,	0,		growl8.wav,				petPoke.wav,	-1,				
	63,	"Wolf",				josh35,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	24,	0,	0,	0,	0,	0,		growl13.wav,			pain14.wav,		-1,				
	64,	"Dire Wolf",		josh35,		1.1.0.0,3,		0,	0,	0,	0,	0,	0,	34,	0,	0,	0,	0,	0,		growl13.wav,			pain14.wav,		-1,				
	65,	"White Wolf",		josh36,		1.1.0.0,3,		0,	0,	0,	0,	0,	0,	44,	0,	0,	0,	0,	0,		growl13.wav,			pain14.wav,		-1,			
	66,	Goblin,				josh26,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	54,	0,	0,	0,	0,	0,		growl7.WAV,				pain13.WAV,		-1,				
	67,	"Goblin Warrior",	josh32,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	64,	0,	0,	0,	0,	0,		growl7.WAV,				pain13.WAV,		-1,
	68,	"Goblin Soldier",	josh33,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	74,	0,	0,	0,	0,	0,		growl7.WAV,				pain13.WAV,		-1,				
	69,	"Goblin Cavalry",	josh53,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	84,	0,	0,	0,	0,	0,		growl7.WAV,				pain13.WAV,		-1,				
	70,	"Ogre",				josh82,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	94,	0,	0,	0,	0,	0,		growl7.WAV,				pain13.WAV,		-1,				
	71,	"Ogre Mage",		josh88,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	104,0,	0,	0,	0,	-1,		growl7.WAV,				pain13.WAV,		-1,				
	72,	"Troll",			josh98,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	114,0,	0,	0,	0,	-1,		growl7.WAV,				pain13.WAV,		-1,				
	73,	"Goblin Catapult",	josh99,		0.1.0.0,3,		0,	0,	0,	0,	0,	0,	124,0,	0,	0,	0,	0,		arrow1.wav,				boom2.wav,		0,				
;DEATH
	81,	"Black jelly",		josh4,		0.2.31.0,4,		0,	0,	0,	0,	0,	0,	5,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	82,	"Undead Bones",		josh84,		0.0.32.0,4,		0,	0,	0,	0,	0,	0,	15,	0,	0,	0,	0,	0,		growl7.WAV,				pain2.wav,		-1,				
	83,	"Death Stalker",	josh9,		0.0.12.0,4,		0,	0,	0,	0,	0,	0,	25,	0,	0,	0,	0,	0,		petChew.wav,			flee.wav,		-1,				
	84,	"Restless Soul",	josh56,		0.0.2.1,4,		0,	0,	0,	0,	0,	0,	35,	0,	0,	0,	0,	0,		bow.wav,				pain15.WAV,		-1,				
	85,	"Demon Bones",		josh85,		0.2.32.0,4,		0,	0,	0,	0,	0,	0,	45,	0,	0,	0,	0,	-1,		growl7.WAV,				pain2.wav,		-1,				
	86,	"Ghoul",			josh1,		0.0.3.0,4,		0,	0,	0,	0,	0,	0,	55,	0,	0,	0,	0,	0,		growl7.WAV,				pain2.wav,		-1,				
	87,	"Zombie",			Zombie,		0.0.0,	4,		0,	0,	0,	0,	0,	0,	65,	0,	0,	0,	0,	0,		fist.wav,				pain13.WAV,		0,				
	88,	"Vampire",			josh65,		0.1.0.0,4,		0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,		growl9.wav,				pain15.WAV,		-1,				
	89,	"Giant Tarantula",	josh60,		0.0.0.0,4,		0,	0,	0,	0,	0,	0,	85,	0,	0,	0,	0,	0,		pain2.wav,				pain2.wav,		-1,				
	90,	"Undead Sorceror",	josh7,		0.1.0.0,4,		0,	0,	0,	0,	0,	0,	95,	0,	0,	0,	0,	0,		chant4.wav,				pain15.WAV,		-1,				
;FIRE
	101,"Fire jelly",		josh30,		0.2.0.0,5,		0,	0,	0,	0,	0,	0,	6,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	102,"Living Flame",		josh31,		0.2.0.0,5,		0,	0,	0,	0,	0,	0,	16,	0,	0,	0,	0,	-1,		fire1.wav,				summon6.wav,	-1,				
	103,"Pyrocite",			josh83,		0.2.9.0,5,		0,	0,	0,	0,	0,	0,	26,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
	104,"Fire Sprite",		Efaerie2,	0.3.3.0,1,		0,	0,	0,	0,	0,	0,	32,	0,	0,	0,	0,	-1,		Okiepain1.wav,			growl16.wav,	-1,				
	105,"Pyrosaur",			josh27,		0.2.7.0,5,		0,	0,	0,	0,	0,	0,	36,	0,	0,	0,	0,	-1,		growl23.wav,			pain3.wav,		-1,				
	106,"PyroManiac",		wtf,			0.3.3.0,5,		0,	0,	0,	0,	0,	0,	46,	0,	0,	0,	0,	-1,		haunt.wav,				pain14.wav,		-1,				
	107,"Draconian Warrior",draggyb,		0.3.0.0,5,		0,	0,	0,	0,	0,	0,	56,	0,	0,	0,	0,	-1,		magic1.WAV,				pain1.WAV,		-1,				
	108,Salamander,			Salamander,	0.2.0.0,5,		0,	0,	0,	0,	0,	0,	66,	0,	0,	0,	0,	-1,		Hiss.wav,				pain8.wav,		-1,				
	109,"Flame Toad",		josh69,		0.2.1.0,5,		0,	0,	0,	0,	0,	0,	76,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	110,"Blaze",			josh70,		-1.3.0.0,5,		0,	0,	0,	0,	0,	0,	86,	0,	0,	0,	0,	-1,		growl9.wav,				pain4.wav,		-1,				
	111,"ScissorJaw",		josh73,		0.2.2.0,5,		0,	0,	0,	0,	0,	0,	96,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,		

;ETHER
	121,"Ghost jelly",		josh3,		0.2.24.1,6,		0,	0,	0,	0,	0,	0,	7,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		-1,				
;AIR
	141,"Flying jelly",		josh30,		0.2.32.0,7,		0,	0,	0,	0,	0,	0,	8,	0,	0,	0,	0,	0,		haunt.wav,				pain13.WAV,		3,				
;SPECIAL/QUEST
;<FICHER LAKE>
	201,	"Guppie",			josh11,		-3.1.2,	0,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,		growl17.wav,			pain7.wav,		-1,				
	202,	"Keeper",			josh11,		0.1.13,	0,		0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	0,		growl17.wav,			pain7.wav,		-1,				
	203,	"Reeler",			josh11,		1.1.11,	0,		0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	0,		growl17.wav,			pain7.wav,		-1,				
	204,	"Grass Snake",		josh38,		0.0.17.0,2,		0,	0,	0,	0,	0,	0,	5,	0,	0,	0,	0,	0,		Hiss.wav,				Okiepain1.wav,	-1,				
;<FIRE BOOTS>
	205,	"Fire Wolf",		josh36,		0.2.31,	5,		0,	0,	0,	0,	0,	0,	25,	0,	0,	0,	0,	-1,		growl9.wav,				pain14.wav,		-1,				
;<AIR STONE>
	206,"Buster",			josh25,		2.3.5,	7,	0,		0,	0,	0,	0,	0, 50,	0,	0,	0,	0,	50,		"growl1.wav",			"pain5.wav",	-1
;<PASSPORT>
	207,"Roger",			pet06,			0.1.3,	2,		0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,		growl5.wav,				pain9.wav,		-1,				
	208,"Bouncer",			Bouncer,			0.1.0.0,8,		0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,		bow.wav,				pain1.WAV,		-1,				
	209,"Psychobouncer",		DM_PsychoBouncer,2,0.1.0.1,8,	0,	0,	0,	0,	0,	0,	75,	0,	0,	0,	0,	-1,		bow.wav,				pain1.wav,		-1,				
;<GUARDIANS>
	210,"Water Guardian",	josh41,		0.3.29.1,1.elemental,0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		DisIncarnate.wav,		Incarnate.wav,	-1,				
	211,"Earth Guardian",	josh41,		0.3.30.1,1.elemental,0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		DisIncarnate.wav,		Incarnate.wav,	-1,				
	212,"Fire Guardian",	josh41,		0.3.31.1,1.elemental,0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		DisIncarnate.wav,		Incarnate.wav,	-1,				
	213,"Air Guardian",		josh41,		0.3.28.1,1.elemental,0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		DisIncarnate.wav,		Incarnate.wav,	-1,				
;<BOXING ARENA>
	501,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	10,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	502,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	20,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	503,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	30,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	504,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	40,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	505,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	50,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	506,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	60,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	507,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	70,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	508,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	80,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	509,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	90,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
	510,"Contender",			mirror,		0.1.0.0,8,		0,	0,	0,	0,	0,	0,	100,	0,	0,	0,	0,	-1,		fist.wav,				pain1.WAV,		-1,				
;
;PETS
	1001,	"Roger",			pet06,		0.0.3,	2,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		growl5.wav,				pain9.wav,		-1,				
	1002,	"White jelly",		josh3,		0.2.24.0,0,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1003,	"Water jelly",		josh2,		0.2.3.0,1,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1004,	"Tree jelly",		josh2,		0.2.27.0,2,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1005,	"Rock jelly",		josh2,		0.2.25.0,3,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1006,	"Black jelly",		josh4,		0.2.31.0,4,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1007,	"Fire jelly",		josh30,		0.2.0.0,5,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1008,	"Ghost jelly",		josh3,		0.2.24.1,6,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		-1,				
	1009,	"Flying jelly",		josh30,		0.2.32.0,7,		0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	-1,		haunt.wav,				pain13.WAV,		3,				
