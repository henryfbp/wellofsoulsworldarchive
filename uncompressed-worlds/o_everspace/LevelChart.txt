Earth Class:
HP: 200
MP: 200

Pirate Class:
HP: 160
MP: 200

Salvage:
HP: 225
MP: 200

Fighter:
HP: 160
MP: 200

Alien:
HP: 200
MP: 200

Tanker:
HP: 225
MP: 200

Cruiser:
HP: 180
MP: 200

  	   | strength | wisdom | stamina | agility | dexterity | TOTAL |   Total HP

Earth (DONEX):	150,	150,	   150,	     150,	150	  750  0    2000
Pirate(DONEX):	150,	150,	   150,	     200,	150	  800  --   1600
Fighter(DONEX):	125,	150,	   125,	     200,	175	  775  --   1800
Alien(DONEX):	200,	150,	   125,	     100,	150    	  725  0    2200

Each (25 points) of Ability worth X HP.
Start at 2000, +150 for each loss, -200 for each gain.