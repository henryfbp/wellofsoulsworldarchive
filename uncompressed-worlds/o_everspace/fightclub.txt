; switches arena1-35 here
SCENE 59, are, SCENE, "Ultimate Fight"
MUSIC are.mid
ACTOR 1, "Moderator", la1, 1, 30, 50
1: Welcome to the Arena.
1: Here, for a small price, you can fight as many monsters as you will for a prize.
1: However, you can only win each prize once.
@reask
1: How many monsters would you like to attempt? (1-30)
ASK 9999999
set attempt,"#<lastAsk>"
COMPARE "#<attempt>", "1"
IF= @1
COMPARE "#<attempt>", "2"
IF= @2
COMPARE "#<attempt>", "3"
IF= @3
COMPARE "#<attempt>", "4"
IF= @4
COMPARE "#<attempt>", "5"
IF= @5
COMPARE "#<attempt>", "6"
IF= @6
COMPARE "#<attempt>", "7"
IF= @7
COMPARE "#<attempt>", "8"
IF= @8
COMPARE "#<attempt>", "9"
IF= @9
COMPARE "#<attempt>", "10"
IF= @10
COMPARE "#<attempt>", "11"
IF= @11
COMPARE "#<attempt>", "12"
IF= @12
COMPARE "#<attempt>", "13"
IF= @13
COMPARE "#<attempt>", "14"
IF= @14
COMPARE "#<attempt>", "15"
IF= @15
COMPARE "#<attempt>", "16"
IF= @16
COMPARE "#<attempt>", "17"
IF= @17
COMPARE "#<attempt>", "18"
IF= @18
COMPARE "#<attempt>", "19"
IF= @19
COMPARE "#<attempt>", "20"
IF= @20
COMPARE "#<attempt>", "21"
IF= @21
COMPARE "#<attempt>", "22"
IF= @22
COMPARE "#<attempt>", "23"
IF= @23
COMPARE "#<attempt>", "24"
IF= @24
COMPARE "#<attempt>", "25"
IF= @25
COMPARE "#<attempt>", "26"
IF= @26
COMPARE "#<attempt>", "27"
IF= @27
COMPARE "#<attempt>", "28"
IF= @28
COMPARE "#<attempt>", "29"
IF= @29
COMPARE "#<attempt>", "30"
IF= @30
GOTO @reask
END
@no
1: No?  Return when you wish to.
END
@nomon
1: I'm afraid you don't have enough money
END
@doned
1: You have allready completed this challenge.
END
@1
COMPARE "#<arena1>", "1"
IF= @doned
1: To fight one monster it costs $10,000 Credits.
IF -G10000, @nomon
1: Do you wish to spend 10,000 Credits to attempt to kill this monster?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G10000, @nomon
TAKE G10000
WAIT 1.0
1: GO!
FIGHT 250
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated one monster.
SET arena1, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
@2
COMPARE "#<arena2>", "1"
IF= @doned
1: To fight two monsters it costs $25,000 Credits.
IF -G25000, @nomon
1: Do you wish to spend 25,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G25000, @nomon
TAKE G25000
WAIT 1.0
1: GO!
FIGHT 250, 253
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated two monsters.
SET arena2, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@3
COMPARE "#<arena3>", "1"
IF= @doned
1: To fight three monsters it costs $50,000 Credits.
IF -G50000, @nomon
1: Do you wish to spend 50,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G50000, @nomon
TAKE G50000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated three monsters.
SET arena3, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@4
COMPARE "#<arena4>", "1"
IF= @doned
1: To fight four monsters it costs $75,000 Credits.
IF -G75000, @nomon
1: Do you wish to spend 75,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G75000, @nomon
TAKE G75000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated four monsters.
SET arena4, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@5
COMPARE "#<arena5>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $100,000 Credits.
IF -G100000, @nomon
1: Do you wish to spend 100,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G100000, @nomon
TAKE G100000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@6
COMPARE "#<arena6>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $125,000 Credits.
IF -G125000, @nomon
1: Do you wish to spend 125,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G125000, @nomon
TAKE G125000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@7
COMPARE "#<arena7>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $150,000 Credits.
IF -G150000, @nomon
1: Do you wish to spend 150,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G150000, @nomon
TAKE G150000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@8
COMPARE "#<arena8>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $175,000 Credits.
IF -G175000, @nomon
1: Do you wish to spend 175,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G175000, @nomon
TAKE G175000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@9
COMPARE "#<arena9>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $200,000 Credits.
IF -G200000, @nomon
1: Do you wish to spend 200,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G200000, @nomon
TAKE G200000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@10
COMPARE "#<arena10>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $225,000 Credits.
IF -G225000, @nomon
1: Do you wish to spend 225,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G225000, @nomon
TAKE G225000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@11
COMPARE "#<arena11>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $250,000 Credits.
IF -G250000, @nomon
1: Do you wish to spend 250,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G250000, @nomon
TAKE G250000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@12
COMPARE "#<arena12>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $275,000 Credits.
IF -G275000, @nomon
1: Do you wish to spend 275,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G275000, @nomon
TAKE G275000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;---------
@13
COMPARE "#<arena13>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $300,000 Credits.
IF -G300000, @nomon
1: Do you wish to spend 300,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G300000, @nomon
TAKE G300000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;---------
@14
COMPARE "#<arena14>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $325,000 Credits.
IF -G325000, @nomon
1: Do you wish to spend 325,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G325000, @nomon
TAKE G325000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;---------
@15
COMPARE "#<arena15>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $350,000 Credits.
IF -G350000, @nomon
1: Do you wish to spend 350,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G350000, @nomon
TAKE G350000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;---------
@16
COMPARE "#<arena16>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $375,000 Credits.
IF -G375000, @nomon
1: Do you wish to spend 375,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G375000, @nomon
TAKE G375000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@17
COMPARE "#<arena17>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $400,000 Credits.
IF -G400000, @nomon
1: Do you wish to spend 400,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G400000, @nomon
TAKE G400000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@18
COMPARE "#<arena18>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $425,000 Credits.
IF -G425000, @nomon
1: Do you wish to spend 425,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G425000, @nomon
TAKE G425000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@19
COMPARE "#<arena19>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $450,000 Credits.
IF -G450000, @nomon
1: Do you wish to spend 450,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G450000, @nomon
TAKE G450000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@20
COMPARE "#<arena20>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $475,000 Credits.
IF -G475000, @nomon
1: Do you wish to spend 475,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G475000, @nomon
TAKE G475000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@21
COMPARE "#<arena21>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $500,000 Credits.
IF -G500000, @nomon
1: Do you wish to spend 500,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G500000, @nomon
TAKE G500000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@22
COMPARE "#<arena22>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $525,000 Credits.
IF -G525000, @nomon
1: Do you wish to spend 525,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G525000, @nomon
TAKE G525000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@23
COMPARE "#<arena23>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $550,000 Credits.
IF -G550000, @nomon
1: Do you wish to spend 550,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G550000, @nomon
TAKE G550000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@24
COMPARE "#<arena24>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $575,000 Credits.
IF -G575000, @nomon
1: Do you wish to spend 550,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G575000, @nomon
TAKE G575000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@25
COMPARE "#<arena25>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $600,000 Credits.
IF -G600000, @nomon
1: Do you wish to spend 600,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G600000, @nomon
TAKE G600000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@26
COMPARE "#<arena26>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $625,000 Credits.
IF -G625000, @nomon
1: Do you wish to spend 625,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G625000, @nomon
TAKE G625000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293, 294
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;--------
@27
COMPARE "#<arena27>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $650,000 Credits.
IF -G650000, @nomon
1: Do you wish to spend 650,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G650000, @nomon
TAKE G650000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293, 294, 296
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;-------
@28
COMPARE "#<arena28>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $700,000 Credits.
IF -G700000, @nomon
1: Do you wish to spend 700,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G700000, @nomon
TAKE G700000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293, 294, 296, 297
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;------
@29
COMPARE "#<arena29>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $850,000 Credits.
IF -G850000, @nomon
1: Do you wish to spend 850,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G850000, @nomon
TAKE G850000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293, 294, 296, 297, 298
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;------
@30
COMPARE "#<arena30>", "1"
IF= @doned
1: To fight #<attempt> monsters it costs $1,000,000 Credits.
IF -G1000000, @nomon
1: Do you wish to spend 1,000,000 Credits to attempt to kill these monsters?
1: If you win, the prize is worth your while... (YES/NO)
ask 999999
IF NO, @no
1: Yes? Okay, please wait a moment while we fix you up and prepare you for the battle..
GIVE L1
WAIT 1.0
GIVE H6000
GIVE M6000
1: 3...
WAIT 1.0
1: 2....
WAIT 1.0
1: 1....
IF -G1000000, @nomon
TAKE G1000000
WAIT 1.0
1: GO!
FIGHT 250, 253, 254, 255, 256, 257, 258, 259, 260, 270, 271, 272, 275, 277, 278
FIGHT 279, 280, 281, 282, 283, 289, 290, 291, 292, 293, 294, 296, 297, 298, 302
IF DEAD, @dead
PARTY V#<num.hostLevel>
PARTY C#<num.hostClass> 
PARTY G#<num.hostGP>
PARTY KM1.#<KM.jft>
1: Excellent, %1, you have defeated #<attempt> monsters.
SET arena#<attempt>, "1"
GIVE I104
1: Please inspect your inventory, you should see the nanites there.
END
;---
@dead
1: Good try, %1.
GIVE L1
GIVE M6000
GIVE H6000
1: Please return and try again.
END
;DONE!
;FINALLY DONE!
