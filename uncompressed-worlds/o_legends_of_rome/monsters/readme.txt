world MONSTERS Folder

Each world has its own MONSTERS folder which contains the skin files for monsters, villagers, etc. (everything but HEROES which are always taken from the WoS 'root' SKINS directory)

Logically, monsters are associated with a particular world and so their skin files belong in this directory.

However, I suspect monster art to get re-used a lot and so it is a download savings to have a common monster in the root MONSTERS folder where it is available to all worlds.

Anyway, the game will look here first, and then in the root if the monster is not found here.