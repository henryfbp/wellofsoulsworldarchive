;---------
;Castle Maze's End
;
SCENE 11		manzemaze, SCENE, "Castle Maze End", 0, 0
	THEME	0
	MUSIC	end.mid
	ACTOR	1,	"Gentleman", ronnie,	9,	25,	90
	POSE	11,4
	SEL		1
	IF XP, @lose
	IF T602, @wrong

	COUNTDOWN 0
	IF DEAD, @dead
	IF PARTY T1001, @wait
	' Congrats! You win today's hot item, boots!
	' Please try again in the future!
	' From now on, it will cost you 500 gold, so be careful when you decide to try!
	' Remember! You could win another great item!
	WAIT 2
	GIVE I508
	GIVE T601
	GIVE T1001
	GIVE T1000
	GOTO LINK 3, 9, 0
END

@wait
	' Please wait for your leader.
END

@lose
	' Too bad, you lose! Better luck next time!
	WAIT 1
	GIVE T1000
	GIVE T600
	TAKE T602
	GOTO LINK 3, 9, 0
END

@wrong
	' I am happy to inform you that...
	WAIT 2
	' this isn't the right spot.
END

@dead
	' Oh, I'm sorry! You must be alive to recieve the prize!
	' But, if you want to try again for 100 gold, I'll put 200 gold in your bank account!
	' You will also be revived for free when you accept the offer.
	' Or you could leave... but would you really want to do that?
	' So, would you want to go again?
	ASK 99999
	IF YES, @goagain
	' Oh, I see...
	' Good-bye!
	WAIT 1
	GOTO LINK 3, 9, 0
END

@goagain
	IF -G100, @cantgo
	GIVE L1
	GIVE H9999
	GIVE M9999
	1: Ok!
	TAKE G100
	ADD money, 200
	1: There, you're current balence is #<money>.
	1: Starting the timer...
	1: GO!
	COUNTDOWN 60
	GOTO LINK 5,0,0
END

@cantgo
	1: Sorry, but you don't have enough money.
	1: Come back when you do!
	GIVE L1
	WAIT 1
	GOTO LINK 3, 9, 0
END
	

;---------
;Castle Maze's Start
;
SCENE 12 	manzemaze, SCENE, "Maze's Start", 0, 0
	THEME	0
	MUSIC	funk1.mid
	ACTOR	1,	"Euclid", ronnie,	9,25,90
	ACTOR	2,	"Sign",		 		sign,   9,-1,-1
	POSE	11,4
	SEL		1
	IF T1002+T1055, @hesgone
	IF T1055, @round2
	IF T1002, @needgnow
	IF T602, @wait
	IF T601, @round2
	IF T600, @choice
	IF XP, @choice

	' Welcome to the Manze Castle Maze Challenge! I'm the owner, Elucid!
	' Here, you can win great prizes for small fees.
	' In return, you get prizes and a lot of fun!
	' Since this is your first time, you may go for free.
	' In this round, you must run to the exit!
	' You will know which one is the exit because it will have a name on it.
	' The countdown has begun! Hurry!
	COUNTDOWN 60
	WAIT 1
	GOTO LINK 5, 0, 0
END

@choice
	' Looks like you're out of time.
	' If you want to try again, it will cost you 100 gold.
	' Or you could just leave... but that wouldn't be any fun!
	' So, What will it be?
	' Yes = Try again
	' No = Leave challenge
	ASK 20
	IF YES, @againrnd1
	IF NO, @notnow
END

@notnow
	H: I'll come back later.
	' Alright, bye!
	WAIT 0.5
	GOTO LINK 3, 9, 0
END

@againrnd1
	H: Let me go again!
	IF G100, @enoughrnd1
	' Hey, don't try to rip me off!
	WAIT 2
	GOTO LINK 3,9,0
END

@enoughrnd1
	TAKE G100
	' Alright! I'll start the clock...
	WAIT 2
	COUNTDOWN 60
	' GO!
	GOTO LINK 5, 0, 0
END

@round2
	GIVE T601
	GIVE T1001
	GIVE T1000
	' Ahh, I see you are back.
	' So, would you like to try the next round for 500 gold pieces?
	ASK 20
	IF YES, @yesrnd2
	IF NO, @notnow
END

@yesrnd2
	' Alright, hand over the money...
	IF G500, @enoughrnd2
	' You don't have enough!
	' Scat!
	WAIT 0.5
	GOTO LINK 3,9,0
END

@enoughrnd2
	TAKE G500
	GIVE T602
	' Have fun!
	' In this round, you must go to the right exit!
	' You only have 30 seconds, so hurry!
	' Starting...
	' NOW!!!
	COUNTDOWN 30
	GIVE L1
	GIVE H9999
	GIVE M9999
	GOTO LINK 5, 0, 0
END

@wait
	' Hey, your not out of time!
	'... or do you want to quit?
	' If you don't want to quit, exit NOW!
	WAIT 2.5
	' Ok, I'll send you out!
	COUNTDOWN 0
	TAKE T602
	GOTO LINK 3, 9, 0
END

@needgnow
	IF I508, @redo
	' Sorry, but we've ran out of business...
	' I spent all my money on this prize, but some idiot cheated and got it...
	' It's about time for me to leave, too.
	H: Then what are you waiting for?
	' For this stick to sale... with it's profits, I'll be able to start a new game!
	IF T1050, @usefulitmightbe
	H: Great... well, good luck!
	WAIT 2
	GOTO LINK 3, 9, 0
END

@usefulitmightbe
	H: How much are you asking?
	' I dunno, I guess all I want for it is 50k.
	' You want it?
	WAIT 1
@usefulitmightbemenu
	MENU "Alright! It's a deal!=@ok50k", "What, are you crazy?=@lower50k"
	WAIT 1
	GOTO @usefulitmightbemenu
END

@ok50k
	' Alright! Hand over the money...
	IF G50000, @ok50k2
	' C'mon, don't rip me off!
END

@ok50k2
	TAKE G50000
	GOTO @boughtstick
END

@lower50k
	' How about if I lowered it to 25k?
	WAIT 1.5
@lower50kmenu
	MENU "Alright=@ok25k", "That's still too high!=@lower25k"
	WAIT 1
GOTO @lower50kmenu
END

@ok25k
	' Alright! Hand over the money...
	IF G25000, @ok25k2
	' C'mon, don't rip me off!
END

@ok25k2
	TAKE G25000
	GOTO @boughtstick
END

@lower25k
	' Grr... 10k, my final offer!
@lower25kmenu
	MENU "Alright=@ok10k", "Forget it!=@nonono"
	WAIT 1
GOTO @lower25kmenu
END

@ok10k
	' Alright! Hand over the money...
	IF G10000, @ok10k2
	' C'mon, don't rip me off!
END

@ok10k2
	TAKE G10000
	GOTO @boughtstick
END

@nonono
	' Fine! Goodbye!
END

@boughtstick
	IF PARTY T1055, @wait
	GIVE I452
	GIVE T1055
	GIVE T1003
	' Thanks a ton!
	' I can open my new store now!
	' It'll be in Sitae!
	' See you there!
	MOVE 1,-100,70
END

@hesgone
	IF #5, @gotolink
	MOVE	2,25,90,1
	MOVE	1,0,0,1
	IF I508+I509, @redo
	TAKE T1003
	N: Right click the sign to read!
END

@hesgone2
	' Hey, sorry about the bug problem, I'll now send you to the right link.
	GIVE T1003
	WAIT 2
	GOTO @hesgone
END

@eventActorClick2
	2: I'm now in Sitae running my new shop!
	2: Come by for a try some time!
	2:  - Euclid
	GOTO @end
END	

@end
	WAIT 1
	N: If you want to read the sign again, right-click it!
END

@redo
	IF PARTY -I509-T1052-T1003, @wait
	' Looks like you met a bug!
	' I will now take the token that allowed you to get here!
	' I'm sorry for any trouble that this caused, but it's the only way to fix it!
	' Thank you!
	TAKE I509
	TAKE T1002
	TAKE T1003
END

@gotolink
	GOTO LINK 3, 9, 0
END

@wait
	' Please wait for your leader.
END

;---------
;Castle Maze's Wrong Stop!
;
SCENE 13 manzemaze, SCENE, "End of Maze", 0, 0
	THEME	0
	MUSIC	end.mid
	ACTOR	1,	"Gentleman", ronnie,	9,	25,	90
	POSE	11,4
	SEL		1
	IF XP, @lose

	' I am happy to inform you that...
	WAIT 3
	' this isn't the right spot.
END

@lose
	' Too bad, you lose! Better luck next time!
	WAIT 1
	GIVE T600
	TAKE T602
	GOTO LINK 3, 9, 0
END

 

;---------
;Castle Maze's Right Stop!
;
SCENE 14		manzemaze, SCENE, "End of Maze", 0, 0
	THEME	0
	MUSIC	end.mid
	ACTOR	1,	"Gentleman", ronnie,	9,	25,	90
	POSE	11,4
	SEL		1
	IF XP, @lose2

	' I am happy to inform you that...
	WAIT 3
	' this is the right spot.
	COUNTDOWN 0
	' What? How did you do that?
	' That's impossible!
	' NOOOOO!!!
	SOUND manzemaze1.wav
	' Ugg, GUARDS!
	FIGHT 501, 501
	IF WIN, @win
	IF DEAD, @lose1
END

@win
	' No!
	SOUND manzemaze1.wav
	' Fine, I'll upgrade those boots you have...
	TAKE I508
	GIVE I509
	GIVE T1002
	TAKE T602
	' Now, get out of here!
	' Thanks to you, we're no longer open!
	' Leave NOW!
	WAIT 2
	GOTO LINK 3,9,0
END

@lose1
	' AHAHA!
	TAKE T602
	SOUND lose1.wav
	COUNTDOWN 0
	' Ahem, please join us again!
	GIVE L1
	GIVE H10000
	GIVE M10000
	WAIT 1.1
	GOTO LINK 3,9,0

@lose2
	' Too bad, you lose! Better luck next time!
	TAKE T602
	GOTO SCENE 12
END

;-------
; Wand Shop
;
SCENE 17 gate,	 SCENE, "Graveyard", 0, 0
	THEME	0
	MUSIC   wandshop.mid
	COMPARE #<num.peopleinscene>, 1
	IF> @cantserve
	ACTOR	1,	"Dark Wanderer?", Wandshop,	1,25,90
	ACTOR   2,  "Wand Shop Owner", wandshop, 1,-1,-1
	POSE	11,4
	SEL		1
	IF T1050, @appraise1

	H: Are you...
	' One of them? No.
	H: Then, who are you?
	' I am just a figment of your imagination...
	H: What?
	' You know... I had some wands and all.. but...
	WAIT 0.1
	H: I SAID WHO ARE YOU!
	' ... I am the Wand Shop Owner.
	WAIT 1
	MOVE 1,0,0,1
	MOVE 2,25,90,1
	H: I've heard of you before!
	2: Yes, but after Erson was destoryed, I was turned into this horrible state...
	2: Even worse, all of my wands were collected by Omega!
	H: Aww... and I wanted a wand...
	2: Stop your whining you %3!
	2: All you can do now is collect the wands that were spread across the island.
	2: Whenever you get a wand, or a stick, bring it to me, and I'll appraise it.
	H: Ok, I'll remember that.
	H: Bye!
	WAIT 0.5
	GIVE T1050
	GOTO SCENE 250
END

@cantserve
	N: It seems like nothing is here, but
	N: you have the feeling that you should come back...
	N: ... alone...
END

@eventactorclick2
	LOCK 1
	COMPARE #<wandshoprobe>, 1
	IF= @stop
	2: Stop it! I DON'T LIKE TO BE CLICKED!
	2: Besides, don't you have anything better to do?
	2: Scat! Or I'm calling in some guards!
	WAIT 5
	2: Alright, you asked for it!
	FIGHT 402,402,300,300
	IF DEAD, @hedied
	IF WIN, @helived
END

@stop
	2: That's enough, now.
	LOCK 0
END

@hedied
	2: That'll teach you!
	2: Get!
	WAIT 0.5
	GOTO EXIT
END

@helived
	2: I can't believe it!
	2: You deserve this...
	GIVE I700
	SET wandshoprobe, 1
	2: You are a great warrior, my friend!
	2: Goodbye!
	LOCK 0
END

@yousuck
	2: Look! I don't like cheaters! If you want to cheat, go to Evergreen!
	2: Leave!
	WAIT 0.5
	GOTO EXIT
END

@youhaveit
	N: You already got this wand, and you can't get it again!
	WAIT 1.5
	2: Wait until your leader's done, then follow him!
END

@appraise1
	LOCK 1
	MOVE 1,0,0,1
	MOVE 2,25,90,1
	2: So, do you have anything?
	GOTO @appraise2
END

@appraise2
	IF I450, @appraiseaxl1
	IF I451, @appraiseerson1
	IF I452, @appraisemaze1
	IF I453, @appraiseerson2
	IF I454, @appraisebank1
	H: Nope, sorry, nothing.
	2: Well get out there and find something, you %3!
END

@appraise3
	2: So, do you have anything else?
	WAIT 1
	GOTO @appraise2
END

@appraiseaxl1
	IF PARTY T1052, @youhaveit
	IF T1052, @takeitem1
	H: Axl gave me a stick!
	2: Wow, a stick...
	H: Will you just appraise it?
	2: Ok, let me see here...
	2: This appears to be...
	WAIT 3
	2: A deconfusion wand!
	2: Here you go!
	TAKE I450
	GIVE I401
	GIVE T1052
	WAIT 1
	GOTO @appraise3
END

@takeitem1
	2: Oh my, it seems you got an extra wand.
	2: I'll just take it...
	TAKE I450
	GOTO @appraise3
END

@appraiseerson1
	IF PARTY T1054, @youhaveit
	IF T1054, @takeitem2
	H: The guy at the gate found a stick!
	2: Hmm, ok, let me take a look at it...
	2: This seems to be...
	WAIT 3
	2: A cracked healing wand!
	2: Here you go!
	TAKE I451
	GIVE I400
	GIVE T1054
	GOTO @appraise3
END

@takeitem2
	2: Oh my, it seems you got an extra wand.
	2: I'll just take it...
	TAKE I451
	GOTO @appraise3
END

@appraisemaze1
	IF PARTY T1056, @youhaveit
	IF T1056, @takeitem3
	H: I bought a stick from the guy who worked at Manze Castle Maze!
	2: Great, let me see it!
	2: This would be...
	WAIT 3
	2: The Wand of Stunning!
	2: Here you go!
	TAKE I452
	GIVE I405
	GIVE T1056
	GOTO @appraise3
END

@takeitem3
	2: Oh my, it seems you got an extra wand.
	2: I'll just take it...
	TAKE I452
	GOTO @appraise3
END

@appraiseerson2
	IF PARTY T1058, @youhaveit
	IF T1058, @takeitem4
	H: The beggar traded me a stick!
	2: You listened to him?
	2: I'll just appraise it...
	2: This would be...
	WAIT 3
	2: A stick!
	H: What???
	2: Yep, an ordinary stick.
	2: From now on, don't mess with beggars, ok?
	H: NOOOOOOO!
	TAKE I453
	GIVE I407
	GIVE T1058
	GOTO @appraise3
END

@takeitem4
	2: Oh my, it seems you got an extra wand.
	2: I'll just take it...
	TAKE I453
	GOTO @appraise3
END

@appraisebank1
	IF PARTY T1060, @youhaveit
	IF T1060, @takeitem5
	H: I got a stick for depositing #<money> gold at the bank!
	2: Cool! Let's see what it is..
	2: It looks like..
	WAIT 3
	2: A wide-range healing wand!
	2: Here you go!
	TAKE I454
	GIVE I403
	GIVE T1060
	GOTO @appraise3
END

@takeitem5
	2: Oh my, it seems you got an extra wand.
	2: I'll just take it...
	TAKE I454
	GOTO @appraise3
END

;--------------------------------------------
;

;----------------------------
; Hal's Mini-Games!
;
SCENE 30, fight, SCENE, "Hal's Mini-Games!"
@start
ACTOR 1, "Hal", joshtownsfolk, 9, 30, 70
MUSIC mayor.mid
	1: Welcome to my wonderful collection of mini-games and whatnot!
	1: Simmen Stenil will be adding all of my mini-scripts as I complete them.
	1: For now, we have the following..
	WAIT 0.5
	N: Say the number you wish to play.
	N: 1: Number Guessing Game
	N: 2: Calculator
@askgame
	ASK 99999
	IF Q1, @beginng
	IF Q2, @begincl
	N: Please say 1, 2, or 3. (1/2/3)
	GOTO @askgame
END
;
;--------------------------------------
;
@beginng
	@musicng
	IF R1, @music1ng
	IF R1, @music2ng
	GOTO @musicng
	END
	@music1ng
	MUSIC predance.mid
	GOTO @beginng0
	END
	@music2ng
	MUSIC wiseman2.mid
	GOTO @beginng0
	END
@beginng0
	ACTOR 1, "The Life of Dark", joshtownsfolk, 9, 30, 70
	1: Hello %1, I am The Life of Dark, the guy who runs this number guessing game.
	1: In this game, you must guess a number from 1-25.
	1: If you win, you get 150GP.
	1: You could also win an EXTRA 50GP, depending on how many tries it takes you to guess the number.
	1: It costs 100GP to play; wanna try? (YES/NO)
ASK 30
IF YES, @startng
IF NO, @leave
	1: #<lastask>? C'mon! Choose a real option!
	GOTO @beginng
END

@leave
	1: Ok, come back soon!
	WAIT 1
	GOTO SCENE 30
END

@startng
		SET winlost, "0"
		SET didwin, "0"
		SET chancewin, "0%"
		SET looptime, "0"
@newgame
	1: I'll just take the 100GP now...
IF -G100, @cantplay
TAKE G100
		SUB winlost, "100"
		SET numguess, %R25
		SET guessleft, "5"
@guess
	1: Well, guess away!
ASK 99999
	COMPARE #<lastAsk>, #<numguess>
IF> @greatercheck
IF< @toosmall
IF= @won
END

@guess2
		COMPARE #<guessleft>, "0"
IF= @youlose
ASK 99999
	COMPARE #<lastAsk>, #<numguess>
IF> @greatercheck
IF< @toosmall
IF= @won
END

@youlose
	1: You took too many guesses.
	1: Want to try again?
	ASK 99999
	IF YES, @newgame
	1: Ok, you can come back later!
	WAIT 1
	GOTO SCENE 30
END

@greatercheck
		SUB guessleft, "1"
		COMPARE #<lastAsk>, "25"
IF> @gwerido
IF< @toobig
IF= @toobig
@gwerido
	1: Sheesh weirdo!
	1: You guessed above 25!
	1: I said to guess from 1 to 25!
	1: Nothing higher!
	1: Guess again!
GOTO @guess2

@toobig
	1: You guessed too HIGH.
	1: Guess LOWER!
GOTO @guess2

@toosmall
		SUB guessleft, "1"
	1: You guessed too LOW.
	1: Guess HIGHER!
GOTO @guess2

@won
		COMPARE #<guessleft>, "5"
IF> @setone
IF= @settwo
		COMPARE #<guessleft>, "3"
IF> @setthree
IF= @setfour
IF< @setfive

@setone
		SET looptime, "1"
		SET chancewin, "50%"
GOTO @fpol

@settwo
		SET looptime, "2"
		SET chancewin, "25%"
GOTO @fpol

@setthree
		SET looptime, "3"
		SET chancewin, "12.5%"
GOTO @fpol

@setfour
		SET looptime, "4"
		SET chancewin, "6.25%"
GOTO @fpol

@setfive
		SET looptime, "5"
		SET chancewin, "3.125%"

@fpol
		SET aname, %R2
		COMPARE #<aname>, 1
IF= @nope

@going
		SUB looptime, "1"
		COMPARE #<looptime>, "0"
IF= @bonus
GOTO @fpol

@nope
		SET didwin, "0"
GOTO @talk

@bonus
		SET didwin, "1"
@talk
		ADD winlost, "150"
	1: You got it!
	1: Here's your gold!
GIVE G150	
	1: You have a #<chancewin> chance of winning an extra 50GP!
	COMPARE didwin, "0"
IF= @nobonus
IF> @getbonus

@nobonus
	1: Sorry, you didn't get a bonus.
GOTO @finishtalk

@getbonus
		ADD winlost, "50"
	1: Here's your bonus gold!
	1: Congratulations!
GIVE G50

@finishtalk
COMPARE #<winlost>, 0
IF< @loser
	1: Wow, that's alot of gold, #<winlost>GP, you should watch out for theives!
	GOTO @truend

@loser
	1: Man, oh man, you won #<winlost>.
	1: WAIT, that number was NEGATIVTE!
	1: That means, you LOST gold!
	1: Oi!
@truend
	1: Well, want to play again?
ASK 30
IF YES, @startng
IF QNO, @leave
	1: #<lastask>? C'mon! Choose a real option!
	GOTO @trueend
END

@cantplay
	1: Sorry, but it looks like you don't have enough money.
	1: Come again soon!
	WAIT 1.5
	GOTO SCENE 30
END
;
;-----------------------------------------------------
;
@begincl
	ACTOR 1, "Simmen Stenil", ss, 9, 30, 70
	POSE 1
	SET answer, "0"
	MUSIC greenery.mid
	1: Welcome to the calculator, made by Simmen Stenil!
	COMPARE #<didpayforclacuse>, 1
	IF= @begincl2
	1: Before we begin, I ask that you pay a one time fee of 500 gold.
	1: Is this OK? (YES/NO)
	ASK 99999
	IF YES, @beginclcheck
	1: Ok, come back any time!
	GOTO @start
END

@beginclcheck
	IF -G500, @cantplay
	1: Ok! I'll take the money now..
	TAKE G500
	SET didpayforclacuse, "1"
	1: Have fun!
@begincl2
	1: Here you may do math!
	1: Please use whole numbers!
	1: Now specify if you wish to add (ADD), subtract (SUB), multiply (MUL), divide (DIV), or leave (LEAVE).
	@askthingcl
	ASK 99999
	IF Qadd, @add
	IF Qsub, @sub
	IF Qmul, @mul
	IF Qdiv, @div
	IF Qleave, @leave
	1: Please say ADD, SUB, MUL, or DIV.
	GOTO @askthingcl
END

@add
	1: Okay, please specify the first number.
	ASK 99999
	SET num1, #<lastask>
	1: Okay, so what do you want to add to #<num1>?
	ASK 99999
	SET num2, #<lastask>
	1: Now let's see what #<num1> + #<num2> is...
	ADD num1, #<num2>
	SET answer, #<num1>
	1: The answer is #<answer>!
	GOTO @domore
END

@sub
	1: Okay, please specify the first number.
	ASK 99999
	SET num1, #<lastask>
	1: Okay, so what do you want to subtract from #<num1>?
	ASK 99999
	SET num2, #<lastask>
	1: Now let's see what #<num1> - #<num2> is...
	SUB num1, #<num2>
	SET answer, #<num1>
	1: The answer is #<answer>!
	GOTO @domore
END

@mul
	1: Okay, please specify the first number.
	ASK 99999
	SET num1, #<lastask>
	1: Okay, so what do you want to multiply #<num1> by?
	ASK 99999
	SET num2, #<lastask>
	1: Now let's see what #<num1> x #<num2> is...
	MUL num1, #<num2>
	SET answer, #<num1>
	1: The answer is #<answer>!
	GOTO @domore
END

@div
	1: Okay, please specify the first number.
	ASK 99999
	SET num1, #<lastask>
	1: Okay, so what do you want to divide #<num1> by?
	ASK 99999
	SET num2, #<lastask>
	1: Now let's see what #<num1> / #<num2> is...
	DIV num1, #<num2>
	SET answer, #<num1>
	1: The answer is #<answer>!
	GOTO @domore
END

@domore
	1: Want to do anything more to this number? (ADD/SUB/MUL/DIV/NO)
	ASK 99999
	IF Qadd, @add2
	IF Qsub, @sub2
	IF Qmul, @mul2
	IF Qdiv, @div2
	IF Qno, @begincl
	1: Please say ADD, SUB, MUL, DIV, or NO (NO lets you start over).
END

@add2
	1: How much do you want to add to #<answer>?
	ASK 99999
	SET num3, #<lastask>
	ADD answer, #<num3>
	1: And the total is #<answer>!
	GOTO @domore
END

@sub2
	1: How much do you want to subtract from #<answer>?
	ASK 99999
	SET num3, #<lastask>
	SUB answer, #<num3>
	1: And the total is #<answer>!
	GOTO @domore
END

@mul2
	1: What do you want to multiply #<answer> by?
	ASK 99999
	SET num3, #<lastask>
	MUL answer, #<num3>
	1: And the total is #<answer>!
	GOTO @domore
END

@div2
	1: What do you want to divide #<answer> by?
	ASK 99999
	SET num3, #<lastask>
	DIV answer, #<num3>
	1: And the total is #<answer>!
	GOTO @domore
END

;
;---------------------------------------------
;
SCENE 52 fight, SCENE, "Hot or Cold?"
	MUSIC hoc.mid
	ACTOR 1, "Simmen Stenil", ss, 1, 30, 70
	1: Welcome to Hot or Cold!
	COMPARE #<toldhotcold>, 1
	IF= @askplay
	1: In a moment, I will disappear.
	1: Then you must find me.
	1: You do this by moving to the location that you think I am, then saying any letter.
	1: I will tell you if you are getting warmer or colder every step you take.
	1: You must be within two coordinates of me to win.
	1: You will be allowed 5 guesses before I show you where I was.
	1: If you win, you recieve twice the amount you bet.
	1: Also, if you keep a good record, you just might get something in return, soon enough...
	SET toldhotcold, 1
	SET hcwin, 0
	SET hclose, 0
	SET hctotal, 0
@askplay
	1: Would you like to play?
	ASK 99999
	IF YES, @starthc
	1: Ok, come back any time!
	WAIT 2
	GOTO EXIT
END

@starthc
	SEL 1
	FACE 0
	COMPARE #<num.hostGP>, 0
	IF 0, @cantplay
	SET hcbetamt, 0
	1: How much would you like to bet?
	ASK 9999
	SET hcbetamt, #<lastask>
	COMPARE #<hcbetamt>, #<num.hostGP>
	IF> @cantafford
	COMPARE #<hcbetamt>, 0
	IF< @notpossible
	HOST_TAKE G#<hcbetamt>
	1: Alright then #<hcbetamt> gold is on the line!
	GOTO @tellbetamt2
END

@cantafford
1: You don't have that much money.
GOTO @starthc

@notpossible
1: Hey! You can't bet #<hcbetamt>!!!
1: Do it over again!
GOTO @starthc

@tellbetamt2
1: Find me!
MOVE 1,-100,-100,1
SET guesses, 5
SET guesses2, guesses
SET xcoord, %R100
SET ycoord, %R100
SET xname, left
SET yname, up

@hcnormal
COMPARE #<guesses>, 1
IF= @setguesses
@hcnormal2
1: You have #<guesses> #<guesses2> left.
COMPARE #<guesses>, 0
IF= @hcgo
1: Guess away.
ASK 9999
SET hxcoord, #<num.hostX>
SET hycoord, #<num.hostY>

@checkx
SUB hxcoord, #<xcoord>
SET coord1, #<hxcoord>
COMPARE #<coord1>, 0
IF< @negativex
SET xname, left

@checky
SUB hycoord, #<ycoord>
SET coord2, #<hycoord>
COMPARE #<coord2>, 0
IF< @negativey
SET yname, up

@addemboth
COMPARE #<coord1>, #<coord2>
IF> @movex
IF< @movey
IF= @movex

@addemboth2
ADD coord1, #<coord2>
SET missamt, #<coord1>
DIV missamt, 2
@compare0
SUB guesses, 1


@compare0
COMPARE #<missamt>, 50
IF< @compare1
1: You are very cold. Move #<movexy>.
GOTO @hcnormal

@compare1
COMPAPRE #<missamt>, 25
IF< @compare2
1: You are cold. Move #<movexy>.
GOTO @hcnormal

@compare2
COMPARE #<missamt>, 15
IF< @compare3
1: You are warmly cold. Move #<movexy>.
GOTO @hcnormal

@compare3
COMPARE #<missamt>, 10
IF< @compare4
1: You're getting warmer... Move #<movexy>.
GOTO @hcnormal

@compare4
COMPARE #<missamt>, 5
IF< @compare5
1: You're hot! Move #<movexy> a bit...
GOTO @hcnormal

@compare5
COMPARE #<missamt>, 3
IF< @compare6
1: You're on FIRE! Move #<movexy> just a bit!
GOTO @hcnormal

@compare6
ADD hcwin, 1
ADD hctotal, 1
MOVE 1, #<xcoord>, #<ycoord>, 1
MOVE 1,30,70
1: You've won!
MUL hcbetamt, 2
1: Here's #<hcbetamt> gold for winning!
1: Wow! You've won #<hcwin>/#<hctotal> times!
HOST_GIVE G#<hcbetamt>
1: Want to play again?
ASK 9999
IF YES, @starthc
1: Okay, maybe some other time!
WAIT 2
GOTO EXIT
END

@negativex
SUB coord1, #<hxcoord>
SUB coord1, #<hxcoord>
SET xname, right
GOTO @checky

@negativey
SUB coord2, #<hycoord>
SUB coord2, #<hycoord>
SET yname, down
GOTO @addemboth

@hcgo
ADD hclose, 1
ADD hctotal, 1
MOVE 1, #<xcoord>, #<ycoord>, 1
MOVE 1,30,70
1: Oh! Looks like you're out of turns!
1: I was hiding back there.
1: You've lost #<hclose>/#<hctotal> times.
1: Want to play again?
SEL 1
FACE 0
ASK 9999
IF YES, @starthc
1: Okay, maybe some other time!
WAIT 2
GOTO EXIT
END

@movex
SET movexy, #<xname>
GOTO @addemboth2

@movey
SET movexy, #<yname>
GOTO @addemboth2
END

@cantplay
	1: Sorry, but it looks like you don't have enough money.
	1: At least 1 gold is required to play this game.
	1: Come again soon!
	WAIT 1.5
	GOTO EXIT
END

@setguesses
	SET guesses2, guess
	GOTO @hcnormal2
END
