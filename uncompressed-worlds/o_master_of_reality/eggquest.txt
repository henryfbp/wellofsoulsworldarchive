; There are six reward classes for each of the six eggs.
;
; Worst Reward - 		 1. 1,000 Gold
;				 2. 100,000 Gold
;				 3. 500,000 Gold
;				 4. Inferno Stick
;				 5. All Are Holy Stick
; Best Reward -  		 6. Easter Egg Bow
;
; Items 230-235
;
SCENE 7101 fight, SCENE, "Easter Egg Shop"
ACTOR 1, "Salesman", ronnie, 1,20,70
COMPARE #<num.peopleinscene>, 1
IF> @cannotwork
COMPARE #<eggscomplete>, 1
IF= @finished
COMPARE #<toldeasterquest>, 1
IF= @normal
FACE 1,4
1: Where could they be? I had them here yesterday...
1: Could someone have stolen them?
H: Excuse me, but what are you talking about?
FACE 1,1
1: Oh! Welcome to my store.
1: Normally I'd have some easter eggs to sell you around this time of year, but, as you can see, we're out.
1: It seems that someone has stolen them...
H: Hmm... this is bad news indeed.
H: Do you think that I could help you find them?
1: Oh, that would be great!
1: I had six when I last checked.
1: I don't know, but for some reason I think that the easter eggs are spread out all across the world.
1: Why don't you check in every place to try and find them?
1: If you help me, I'll give you some great rewards!
1: Whenever you find an easter egg, bring it here and I will give you a reward.
1: Thank you! I will see you again soon!
SET toldeasterquest, 1
SET totaleastereggs, 0
END

@cannotwork
1: I can only have one of you in here at a time.
END

@normal
1: Hello again %2. Do you have any easter eggs yet?
@checkeggs
IF I230, @hasegg
IF I231, @hasegg2
IF I232, @hasegg3
IF I233, @hasegg4
IF I234, @hasegg5
IF I235, @hasegg6
H: No, I don't have any new ones yet.
1: Oh, I see... Well, with just #<totaleastereggs> eggs, I can't reopen yet.
1: Please find the rest!
END

@hasegg
COMPARE #<foundI230>, 1
IF= @foundI230
H: Yes, I have one right here.
SET temp, "I230"
SET foundI230, 1
GOTO @give#<totaleastereggs>
END

@foundI230
TAKE I230
GOTO @checkeggs
END

@hasegg2
COMPARE #<foundI231>, 1
IF= @foundI231
H: Yes, here's an easter egg.
SET temp, "I231"
SET foundI231, 1
GOTO @give#<totaleastereggs>
END

@foundI231
TAKE I231
GOTO @checkeggs
END

@hasegg3
COMPARE #<foundI232>, 1
IF= @foundI232
H: Here's one for you.
SET temp, "I232"
SET foundI232, 1
GOTO @give#<totaleastereggs>
END

@foundI232
TAKE I232
GOTO @checkeggs
END

@hasegg4
COMPARE #<foundI233>, 1
IF= @foundI233
H: I found one!
SET temp, "I233"
SET foundI233, 1
GOTO @give#<totaleastereggs>
END

@foundI233
TAKE I233
GOTO @checkeggs
END

@hasegg5
COMPARE #<foundI234>, 1
IF= @foundI234
H: Yes, here's an easter egg.
SET temp, "I234"
SET foundI234, 1
GOTO @give#<totaleastereggs>
END

@foundI234
TAKE I234
GOTO @checkeggs
END

@hasegg6
COMPARE #<foundI235>, 1
IF= @foundI235
H: Yes, here's an easter egg.
SET temp, "I235"
SET foundI235, 1
GOTO @give#<totaleastereggs>
END

@foundI235
TAKE I235
GOTO @checkeggs
END

@give0
1: Thank you, %2. Here, let me give you 1,000 gold for this first easter egg.
GIVE G1000
TAKE #<temp>
ADD totaleastereggs, 1
1: Remember, if you bring me another back, I'll give you something even better!
1: Do you have any more?
GOTO @checkeggs
END

@give1
1: Thanks %2! Since this is the second egg you brought to me, here's 100,000 gold!
GIVE G100000
TAKE #<temp>
ADD totaleastereggs, 1
1: If you bring me another, I'll make the prize even better!
1: Are there any more easter eggs that you have?
GOTO @checkeggs
END

@give2
1: Thanks! That makes the total number of eggs you've found three.
1: That means you get 500,000 gold!
GIVE G500000
TAKE #<temp>
ADD totaleastereggs, 1
1: The next prize will be even better!
1: Are there any more eggs you have for me?
GOTO @checkeggs
END

@give3
1: Nice. So that means that I now have four of my eggs back.
1: Here, take this Inferno Stick!
GIVE I227
TAKE #<temp>
ADD totaleastereggs, 1
1: I bet you want to have the next prize!
1: Well, do you have any more easter eggs?
GOTO @checkeggs
END

@give4
1: Wow, so that means that you've found five eggs so far.
1: Please, take this All Are Holy stick.
GIVE I228
TAKE #<temp>
ADD totaleastereggs, 1
1: I hope you play online a lot!
1: Do you have the last egg?
GOTO @checkeggs
END

@give5
1: Thank you! That means I now have all of my eggs back.
1: Here, I was planning to sell this in the shop, but since you've helped me so much, I think you deserve it.
H: What is it?
1: It's an easter egg bow. It shoots easter eggs at your enemies!
H: That's cool.
1: Thank you so much for all of your help!
GIVE I229
TAKE #<temp>
SET eggscomplete, 1
END

@finished
1: Hey %2! Thanks again for all of your help!
END