;-------------------
; character levels
;
; You can define various character 'classes' which control the
; basic limits of a character (what weapon they can hold in their
; right hand, whether they get magic points, the rate at which
; their levels increase, and some other things)
;
; But a character also has a LEVEL which starts at '1' and goes up along
; with experience points.
;
; This section lets you specify the experience points needed to attain each
; level, and lets you associate a name with each level, so your character
; can appear to evolve through various stages (mage, wizard, warlock, sorceror, etc.)
; even though REALLY only the actual level number is important.
;
; The first argument is a number which indicates the class and the level.  
;
;    arg0:		Class+Level (103 = warrior level 3, 203 = wizard level 3)
;    arg1:		[NO LONGER USED] Additional Experience points to attain level from previous level
;	 arg2:		Additional max HP earned when level is attained
;	 arg3:		Additional max MP earned when level is attained
;    arg4:      Level Name (use the same name for several levels in a row)
;
; These arguments are only valid for level 0 in each character class.  They control
; common aspects of the class
;
;	 arg5		(level 0 only)  magic-ratio
;	 arg6		(level 0 only)  right-hand weapon (1 - 8, or 0 if none (will use start_hand_training instead))
;
; Levels run from 1 to 99 maximum.  A bonus level "100" is supplied by the game, but it uses the same
; table entry as level 99.  That is to say, you cannot provide a custom level name for level 100.
;
; You only need to have entries for levels where you want the hp, mp, new spell, or
; level name to change.  Minimum experience levels are now computed automatically
; for you.  If you give an 'empty' level name (except for level 1 where it is
; mandatory) the previous name will be used for that level
;
; Note that LEVEL ZERO (100,200, etc) is a SPECIAL ENTRY which names the actual
; character class, and is not a level in of itself.  I know this is horribly
; confusing, but it is just so dang CONVENIENT to do it this way!
; So, for LEVEL 0 ONLY:  maxHP and maxMP are the starting levels of a new character
; in that class, level_name is the 'character class name' which appears in the
; new soul dialog box.  
;
; LEVEL ZERO is where you define a characters magic-ration and right-hand weapon.
; Their 'magic ratio' determines what percentage of their attack points are used for magic versus sword attack;
;
; Hence a MAGIC-USER would have a high magic ratio (80-90) while a SWORD-USER
; would have lower one (10-20), and some other characters might be more 50-50
; A character needs to be more than 50% in the respective sense in order to 
; equip swords or staffs.  For example a magic ratio > 50 would allow staff,
; < 50 would allow sword, and == 50 would allow both.
;
; The right-hand weapon controls which class of EQUIP item they can hold in their
; right hand (each class is hard-wired to a single right-hand item, but the
; world designer can define what those actual items are).  Between the combination
; of magic-ratio and right-hand you can create some interesting classes.  For example
; an ARCHER and a MAGIC-ARCHER might both have right-hand set to 'BOW' but different
; magic-ratios.  The magic-archer (with the higher magic-ratio) would be better at
; casting spells, but his actual bow-use would be weaker.
;
; The default (used in Evergreen) right-hand weapons are:
;
;	1	Sword
;	2	Staff
;	3	Bow
;	4	Musical Instrument
;   5   Fist
;   6   Dart
;   7   Book
;   8   Spirit
; 
; if you set arg6 to a value between 1 and 8, then characters of that class will always
; be born with level 5 training in that hand, no matter what the start_hand_pp option
; is for that hand. 
;
; But if you set this field to 0 (Version A57 and later, only), then no hand will be
; pre-trained unless you specifically use the start_hand_pp option.
;
; Setting this field to zero in versions of WoS before A57 will lead to a bug which can
; render the character unusable.  If you publish your world later than July 2001, you can
; probably ignore this comment :)
;
; OPTIONAL CHARACTER LIMITATIONS
;
; If you (as the world designer) like, you can apply certain special commands to each
; character class, generally to limit its access to element and hand training,  You
; place these commands (one per line) just after the introductory line of the class
; For example:
;
; The class name
;	100,	0,			20,		0,		"Dingle-User",   10,				1
;    DESCRIPTION	"Dingle Users are known for their mad dingling skillz...."
;    MAGIC_RATIO	10
;    HAND_RATIO		90
;    MAX_ABILITY 100,100,50,255,0
;    START_ELEMENT_PP 556,1251,2143,3334,5001,7501,11667,20001		<-- no particular sense to these example values, just levels in order
;    MAX_ELEMENT_PP 1251,2143,3334,5001,7501,11667,20001,45001
;    START_HAND_PP 557,1251,2143,3334,5001,7501,11667,20001
;    MAX_HAND_PP 1251,2143,3334,5001,7501,11667,20001,45001
; now the class level names
;	101,	0,			1,		0,		"Dinglet"
;   etc.
;	198,	0,			1,		0,		"El Dinglo"
;	199,	0,			1,		0,		"Dingle King"
;
; Each command is optional and applies only to the class within which is is embedded.  See how
; all the commands appear between line 100 and line 101? 
; You must provide the information in a fixed syntax, as shown.
;
; Here are the available commands:
;
;   DESCRIPTION	 "This class sucks.  People who pick it are losers"		
;	A short text which appears on character creation dialog.  Put text in double quotes
;
;	MAGIC_RATIO	n		
;	Where n is a number from 0 to 100, specifies effectiveness with magical attacks
;
;   HAND_RATIO	n
;	Where n is a number from 1-100, specifies effectiveness of non-magical attacks.  
;   If you set to '0' it will use "100 - magic ratio" for "balanced" classes.
;
;   MAX_ABILITY str, wis, sta, agi, dex  
;	Provide all 5 ability values in order, giving the maximum level allowed for each.
;   Valid max levels are 0 to 255.
;
;   START_ELEMENT_PP  0,1,2,3,4,5,6,7
;	Must have 8 values, in order of +ELEMENTS table.  Sets initial elemental training.
;   Values range from 0 to 1000000 as described later (PP Levels).
;
;   MAX_ELEMENT_PP 0,1,2,3,4,5,6,7 
;	Must have 8 values, in order of +ELEMENTS table.  Sets max possible elemental training
;   Values range from 0 to 1000000 as described later (PP Levels).
;
;   START_HAND_PP 0,1,2,3,4,5,6,7  
;	Must have 8 values, in order of +HANDS table. Sets initial right-hand training
;   Values range from 0 to 1000000 as described later (PP Levels).
;
;   MAX_HAND_PP 0,1,2,3,4,5,6,7   
;	Must have 8 values, in order of +HANDS table. Sets maximum right-hand training.
;   Values range from 0 to 1000000 as described later (PP Levels).
;
;   AUTO_MAX minHP, maxHP, minMP, maxMP, startMPLevel 
;	Must have all five values.  Sets the max HP and MP levels for each level of the character.
;   That is to say, you are setting the maxHP and maxMP for level 1 and level 100, and the intermediate levels are
;   then computed automatically to span those maximums "appropriately" (semi-logarithmic algorithm)
;	The initial level 1 HP will be minHP, final level 100 HP will be maxHP
;	The fifth arg (startMPLevel) lets you postpone first MP (maxMP will stay 0 until that level).
;	NOTE: Actual maximums will be a little lower than you specify because of curve-shaping
;	If you use this command, the HP and MP entries in your level table for that class will be ignored.
;
;	START_LOCATION  <map>, <link>  
;	Lets you set where a newly created character will appear on their first
;	incarnation, based on class.  (so all elves can be born in Elf Town, for example).  Otherwise, they
;	will appear above link 0 on map 0.  It is your responsibility to make sure the map and link specified
;	actually exist and 'make sense' (it would be cruel to incarnate a newbie in a tough spot)
;
;   MAX_WALLET n 
; 	Further limits wallet size to n (0 to 1000000). (cannot make wallets BIGGER than
;	normal game restrictions.)  Normally characters start off with a wallet holding 20K GP at level 1, with
;   an additional 10K added for each level, until they reach one million at level 99.
;
;   NO_GIFTS
;	(No arguments). Indicates that class cannot receive money or items from other players.  (but from scripts is OK)
;
;   START_ITEMS 1, 2, 3, ...
;	Lets you give character up to 8 items right at start of game.  
;	Use valid Item numbers only
;
;	START_SPELLS 1, 2, 3, .. 
;	Lets you give characters up to 8 spells right at start of game
;	 Use valid Spell IDs only.
;
;   START_TOKENS 1, 2, 3...  
;	Lets you give character up to 8 tokens right at start of game.  
;	Use valid token numbers only
;
;	HIDDEN_CLASS
;   (no Arguments). Class doesn't show up on New Character dialog as an available choice.
;   You have to be morphed into such a class via "SET num.hostClass, <class#>" script
;   command
;
; Note that the 'preferred right hand training' of each class will always be level 5 at start, no 
; matter what START_HAND_PP says.  This is for compatibility with existing worlds.  Sorry.
;
; PP Levels
;
; The PP required for each level grows according to a nasty equation, but here are the PP
; limits as of the time of this writing.
;
; level 0		PP 0 - 554
; level 1		PP 555 - 1249
; level 2		PP 1250 - 2141
; level 3		PP 2142 - 3332
; level 4		PP 3333 - 4999
; level 5		PP 5000 - 7499
; level 6		PP 7500 - 11665
; level 7		PP 11666 - 19999
; level 8		PP 20000 - 44999
; level 9		PP 45000 - 1000000 (one million)(you approach level 9.9999999 asymptotically)
;
; Note that is the total PP which affects your skill and the level numbers are somewhat arbitrary
; so a training level 3 with 3333 PP will hit harder than a training level 3 with 2143 PP
;
; To protect yourself against floating point round off issues from one version of windows to
; another, I recommend not using these exact values, but include a safety buffer.  So, for example
; if you wanted a STARTING level of 4, use 3350 instead of 3333.  And if you want a MAXIMUM level
; of 4, use 4970, not 4999.  A 20 unit buffer should not be noticeable by the user and might make
; your world more reliable.
;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; First the Warrior levels (100 + level #)
;  CCLL	    ???????.	MaxHP	maxMP	Level_name		Magic Ratio		Right-Hand
;	0		1			2		3		4				5				6
;
; The class name (sword user, magic ratio of 10.. ergo hand ratio of 90.. preferred right-hand weapon is #1 (sword))
	100,	0,			20,		0,		"Swordsman",   10,				1

; Next the special commands for the sword-user class.  These come between line nn00 and line nn01
	DESCRIPTION	"Swordsmen are world-renowned for their feirce fighting abilities and vast knowledge of battle tactics."
	AUTO_MAX	20, 3759,  10, 1186,  15	
	START_LOCATION 11,0,0
	START_ITEMS 200,201,202,135	

; now the class level names.. note that since I used the AUTO_MAX command, the maxHP and maxMP values shown in this
; table are actually ignored.  If I weren't so lazy, I would change them all to 0.

	101,	0,			1,		2,		"Barbarian"
	105,	0,			3,		2,		"Fighter"
	110,	0,			5,		2,		"Soldier"
	120,	0,			8,		10,		"Warrior"
	130,	0,			10,		0,		"Ranger"
	140,	0,			14,		10,		"Dragoon"
	150,	0,			22,		0,		"Knight"
	160,	0,			28,		20,		"Templar"
	170,	0,			36,		0,		"Paladin"
	180,	0,			50,		20,		"Conqueror"
	190,	0,			65,		0,		"Emperor"

;
;------------
; Now the Wizard levels (200 + level #)
;  CCLL	    ???			MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	200,	0,			15,		5,		"Magician",		90,				4
	DESCRIPTION	"Magicians are skilled scroll readers and spell casters. They have the ability to read scrolls."
	AUTO_MAX	15, 3321,  10, 2184,  0
	START_LOCATION 11,0,0
	START_ITEMS 200,201,202,535

; the class level names for the magic-user class
	201,	0,			1,		2,		"Shaman"
	205,	0,			2,		5,		"Enchanter",		
	210,	0,			4,		10,		"Magician",			
	220,	0,			7,		15,		"Conjuror",			
	230,	0,			8,		15,		"Mage",				
	240,	0,			11,		20,		"Wizard",			
	250,	0,			18,		25,		"Sorcerer",			
	260,	0,			22,		30,		"Thaumaturge",		
	270,	0,			28,		35,		"Demonologist",		
	280,	0,			40,		40,		"Necromancer"
	290,	0,			55,		55,		"Deity",
;			
;--------------
; Now the Archer levels (300 + level #)
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
;
; The class name
	300,	0,			18,		2,		"Bow-User",			40,				3

	DESCRIPTION	"Archers tend to be aloof, as their elegant weapons do not require such closed quarters. But only a narrow shaft stands between them and their doom."
	AUTO_MAX	20, 3656,  10, 1435,  5
	START_LOCATION 11,0,0
	START_ITEMS 200,201,202,84	

; the class levels
	301,	0,			2,		2,		"Stringer"			
	305,	0,			4,		3,		"Shafter",		
	310,	0,			6,		6,		"Bowman",			
	320,	0,			9,		9,		"Arrow Head",			
	330,	0,			12,		11,		"Archer",				
	340,	0,			15,		16,		"Long Arrow",			
	350,	0,			20,		20,		"Boltman",			
	360,	0,			25,		22,		"Master Bowman",		
	370,	0,			32,		27,		"Arbelest",		
	380,	0,			48,		32,		"Master Archer"
	390,	0,			65,		40,		"Quiver Lord",

; Be sure to leave this comment or a blank line at the very end