	0, "Pillow Gauntlet.jpg",	"PillowGauntlet", "Pillow Gauntlet",		   	   32
	1, "alpha.jpg",				"Alpha",		  "Deep within the Castle",	   96
	2, "manze.jpg",				"Manze",	      "Manze",						8
	3, "castle2.jpg",			"Castle2"		  "Manze Castle",				8
	4, "Greenery.jpg",			"Greenery"		  "Manze Castle - Outside",		0
	5, "manzemaze.jpg",   		"manzemaze"		  "Manze Castle Maze",	 	 8264
	6, "erson.jpg",				"erson",		  "Erson's Remains",				0
	7, "port.jpg",				"port",			  "Manze Port",			   	   40
	8, "delta1.jpg",      		"delta1",         "Delta's Labyrinth",	  		0
	9, "Sitae.jpg",				"sitae",		  "Sitae",				   	   72
	10,"Sitae2.jpg",			"sitae2",		  "Under Sitae", 	   	   	   64
	11,"void.jpg",				"void",			  "The Eternal Void",	  	  104
	13,"pkarena.jpg",			"pkarena",		  "PK Arena",		   	   	   54
	14,"euphoria.jpg",			"euphoria",		  "Euphoria", 					8
	15,"mountains1.jpg",		"mountains1",     "Gabonasia Mountain-Out",		0
	16,"mountains2.jpg",		"mountains2",	  "Gabonasia Mountain-In", 	   64
	17,"mountains3.jpg",		"mountains3",	  "Secret Area",					0
	18,"mountains4.jpg",		"mountains4",	  "Hidden Lake",					0
	19,"vendetta.jpg",			"vendetta",	      "Vendetta - Outside",			0
	20,"vendetta1.jpg",			"vendetta1",	  "Underlings' Chamber",			0
	21,"vendetta2.jpg",			"vendetta2",	  "Imprisonment Chamber",		0
	22,"joecastle.jpg",			"kingjoe1",		  "King Joe's Castle",			0
	23,"joecastle.jpg",			"kingjoe2",		  "Castle - Morning",			0
	24,"mysteriousforest.jpg",	"mystfrst",		  "Mysterious Forest",			0
	25,"masterscamp.jpg",		"masters",		  "Masters' Camp",				0
	26,"oblian.jpg",			"oblian",		  "Oblian Forest",				0
	27,"euphund.jpg",			"euphund",		  "Benieth Euphoria"	,			0
	28,"cultelson.jpg",			"cultelson",	  "Omegaian Occult-Elson",		0
	29,"shadowfaks.jpg",		"shadowfaks",	  "Shadowfaks",					0
	30,"castle2.jpg",			"manzecastle2",	  "Manze Castle-Night",			0
	31,"donationvillage.jpg"	"donationvillage","Donation Village",		   40
	32,"mirrormaze.jpg",		"mirrormaze1",		"Shadowfaks Maze",			0
	33,"mirrormaze2.jpg",		"mirrormaze2",		"Shadowfaks Mirror Maze",	0