; Player will have 5000G taken away from him
; When player starts, there will be 6 squares to choose
; Frames 1-6 are the OFF colors, and 7-12 are the ON colors
; All frames are in their respective locations
; The player will get 100G for every frame he correctly chooses
; The player will begin with ZERO lives.
; For every TEN rounds the player clears, he will get a LIFE.
; This LIFE can be used to start again when you lose.
; A RECORD TABLE will be kept and can be displayed when choosing what you want to do
; The options for the opening will be as follows
;
;	       Instructions---Records
;	       \                    /
;		\                  /
;		 \                /
;		  \              /
;                  \  Options	/
;		    \	       /
;		     \        /
;		      \      /
;		       \    /
; 			\  /
;                        \/
;			Start
;
; When the game starts, there will be no BETTING as of right now
; At a later date, I might include difficulty levels

SCENE 7000 fight, CUT, "Simon", 0,0
MUSIC simon.mid
@beginning
COMPARE #<setrecords>, "1"
IF= @normal
SET recordnum1, 50
SET recordnum1name, "Simmen Stenil"
SET recordnum2, 45
SET recordnum2name, "Gandalf the White"
SET recordnum3, 40
SET recordnum3name, "Marva"
SET recordnum4, 35
SET recordnum4name, "Crazy Keyes"
SET recordnum5, 20
SET recordnum5name, "Can Not"
SET recordnum6, 15
SET recordnum6name, "Samsyn"
SET recordnum7, 10
SET recordnum7name, "King Axl"
SET recordnum8, 8
SET recordnum8name, "Euclid"
SET recordnum9, 6
SET recordnum9name, "Ruffles"
SET recordnum10, 4
SET recordnum10name, "King Joe"
SET setrecords, 1
SET simonsound, "a"
SET simoncost, 5000
SET simongive, 100
SET simonwait, 0.25
SET simonmode, Normal
SET simongametotal, 0
SET simonlives, 3
@normal
N: Welcome to Simon! Right-click an option to begin!
ACTOR 13, "Start Game", simon, 13,50,55
ACTOR 14, "Records", simon, 14,70,40
ACTOR 6, "Rules", simon, 15,30,40
ACTOR 15, "Options", simon, 16,50,80
END

@eventactorclick13
COMPARE #<num.hostGP>, 5000
IF< @cantplay
GOTO @begingame
END

@cantplay
n: Sorry, you don't have enough money to play!
n: Come back when you have 5000 gold!
END

@eventactorclick14
N: Top ten records:
N: Record:   Name:
N: #<recordnum1>     #<recordnum1name>
N: #<recordnum2>     #<recordnum2name>
N: #<recordnum3>     #<recordnum3name>
N: #<recordnum4>     #<recordnum4name>
N: #<recordnum5>     #<recordnum5name>
N: #<recordnum6>     #<recordnum6name>
N: #<recordnum7>     #<recordnum7name>
N: #<recordnum8>     #<recordnum8name>
N: #<recordnum9>     #<recordnum9name>
N: #<recordnum10>   #<recordnum10name>
END

@eventactorclick6
6: Welcome to Simon!
6: This game is much like the classic Simon, which invloves repeating what the compter does.
6: When you start a game, six circles will appear.
6: Then, they will glow for a moment.
6: You must click the same ones in the same pattern.
6: You begin with 3 lives.
6: For every 5 rounds that you pass, you get another life.
6: This life can be used allow you to start again if you mess up.
6: Be careful! If you click the wrong thing and DON'T have a life, the game's over!
6: I hope you have fun!
END

@eventactorclick15
MENU "Game Mode=@gameselection","Timing=@timing","Profile=@profile","#<bonusoption1>=@#<bonusoption1lbl>","Sound Selection=@soundselection","Cancel=@end"
END

@adjustlives
N: How many lives would you like to start with?
MENU "One=@al1","Two=@al2","Three=@al3","Four=@al4","Five=@al5","Six=@al6","Seven=@al7","Eight=@al8","Nine=@al9","Ten=@al10","Cancel=@end"
END

@al1
SET simonlives, 1
N: You will now start with one life.
END

@al2
SET simonlives, 2
n: You will now start with two lives.
END

@al3
SET simonlives, 3
n: You will now start with three lives.
END

@al4
SET simonlives, 4
n: You will now start with four lives.
END

@al5
SET simonlives, 5
n: You will now start with five lives.
END

@al6
SET simonlives, 6
N: You will now start with six lives.
END

@al7
SET simonlives, 7
N: You will now start with seven lives.
END

@al8
SET simonlives, 8
N: You will now start with eight lives.
END

@al9
SET simonlives, 9
N: You will now start with nine lives.
END

@al10
SET simonlives, 10
N: You will now start with ten lives.
END

@end
END

@gameselection
MENU "Normal=@gamenormal","#<bonusgame1>=@game#<bonusgame1lbl>","#<bonusgame2>=@game#<bonusgame2lbl>","#<bonusgame3>=@game#<bonusgame3lbl>","#<bonusgame4>=@game#<bonusgame4lbl>","#<bonusgame5>=@game#<bonusgame5lbl>","Cancel=@end"
END

@gamedm
SET simoncost, 10000
SET simongive, 200
SET simonlives, 3
SET simonmode, "Double Money"
N: You are now playing on Double Money mode.
END

@gametm
SET simoncost, 15000
SET simongive, 300
SET simonlives, 3
SET simonmode, "Triple Money"
N: You are now playing on Triple Money mode.
END

@gameqm
SET simoncost, 20000
SET simongive, 400
SET simonlives, 3
SET simonmode, "Quadruple Money"
N: You are now playing on Quadruple Money mode.
END

@gamefp
SET simoncost, 0
SET simongive, 0
SET simonlives, 3
SET simonmode, "Free Play"
N: You are now playing on Free Play mode.
END

@gameil
SET simoncost, 0
SET simongive, 0
SET simonlives, 999
SET simonmode, "Infinate Lives"
N: You are now playing on Infinate Lives mode.

@gamenormal
SET simoncost, 5000
SET simongive, 100
SET simonlives, 3
SET simonmode, Normal
N: You are now playing on Normal mode.
END

@timing
N: How long would you like the buttons to flash?
MENU "1 second=@timing1","0.5 seconds=@timing2","0.25 seconds=@timing3","0.1 seconds=@timing4","0.05 seconds=@timing5"
END

@timing1
N: Buttons will now flash for 1 second.
SET simonwait, 1
END

@timing2
N: Buttons will now flash for 0.5 seconds.
SET simonwait, 0.5
END

@timing3
N: Buttons will now flash for 0.25 seconds.
SET simonwait, 0.25
END

@timing4
N: Buttons will now flash for 0.1 seconds.
SET simonwait, 0.1
END

@timing5
N: Buttons will now flash for 0.05 seconds.
SET simonwait, 0.05
END

@profile
N: Times played: #<simongames>
N: Total points scored: #<simontotalpts>
n: Max combo: #<simonmaxcombo>
n: Extra modes unlocked: #<simongametotal>/5
N: Current button spacing: #<simonwait> seconds
N: Current game mode: #<simonmode>
END

@soundselection
MENU "Piano=@sspiano","#<ss1>=@sstechno","Cancel=@end"
END

@sspiano
SOUND simon2a.wav
N: Every time a button flashes,
N: This instrument will be played.
SET simonsound, "a"
END

@sstechno
SOUND simon2b.wav
n: Every time a button flashes,
n: This instrument will be played.
SET simonsound, "b"
END





;--------------------------------------------------------------------------------------

@begingame
ADD simongames, 1
TAKE G#<simoncost>
SET maxcombo, 0
SUB maxcombo, 1
SET tempmaxcombo, 0
SET totalrounds, 0
SET lives, #<simonlives>
SET tempround, 0
SET lifepart, 0
SUB lifepart, 1
MOVE 13, -100,-100,1
MOVE 14, -100,-100,1
MOVE 6, -100,-100,1
MOVE 15, -100,-100,1
ACTOR 0, "", simon, 1,40,40
ACTOR 1, "", simon, 2,60,40
ACTOR 2, "", simon, 3,70,60
ACTOR 3, "", simon, 4,60,80
ACTOR 4, "", simon, 5,40,80
ACTOR 5, "", simon, 6,30,60
GOTO @roundbegin
END

@roundbegin
SET tempround, 0
WAIT 1
ADD maxcombo, 1
ADD lifepart, 1
COMPARE #<lifepart>, 5
IF= @newlife
@roundbeginnorm
COMPARE #<tempround>, #<totalrounds>
IF= @roundnew
ADD tempround, 1
SET temp1, "circle#<tempround>"
SET temp2, "circle#<tempround>frame2"
SET temp3, "circle#<tempround>frame"
SOUND simon*#<temp1>#<simonsound>
SEL *#<temp1>
POSE *#<temp2>
WAIT #<simonwait>
SEL *#<temp1>
POSE *#<temp3>
WAIT #<simonwait>
GOTO @roundbeginnorm
END

@newlife
ADD lives, 1
SET lifepart, 0
N: You now have #<lives> lives!
GOTO @roundbeginnorm
END



@roundnew
ADD totalrounds, 1
SET tempround, #<totalrounds>
SET tempcookie, %r6
GOTO @circle#<tempcookie>

@circle1
; Actor number first
SET circle#<totalrounds>, 0
; Then NORMAL frame
SET circle#<tempround>frame, 1
; Then the frame it CHANGES to
SET circle#<tempround>frame2, 7
GOTO @playerinput
END

@circle2
SET circle#<totalrounds>, 1
SET circle#<totalrounds>frame, 2
SET circle#<totalrounds>frame2, 8
GOTO @playerinput
END

@circle3
SET circle#<totalrounds>, 2
SET circle#<totalrounds>frame, 3
SET circle#<totalrounds>frame2, 9
GOTO @playerinput
END

@circle4
SET circle#<totalrounds>, 3
SET circle#<totalrounds>frame, 4
SET circle#<totalrounds>frame2, 10
GOTO @playerinput
END

@circle5
SET circle#<totalrounds>, 4
SET circle#<totalrounds>frame, 5
SET circle#<totalrounds>frame2, 11
GOTO @playerinput
END

@circle6
SET circle#<totalrounds>, 5
SET circle#<totalrounds>frame, 6
SET circle#<totalrounds>frame2, 12
GOTO @playerinput
END


;----------------------------------------------------------------

@playerinput
SET temp1, "circle#<totalrounds>"
SET temp2, "circle#<totalrounds>frame2"
SET temp3, "circle#<totalrounds>frame"
SOUND simon*#<temp1>#<simonsound>
SEL *#<temp1>
POSE *#<temp2>
WAIT #<simonwait>
SEL *#<temp1>
POSE *#<temp3>
WAIT #<simonwait>
SET playerrounds, #<totalrounds>
SET playerstart, 0
END

@eventactorclick0
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 0
IF< @incorrect
IF> @incorrect
SOUND simon0#<simonsound>
SEL 0
POSE 7
WAIT #<simonwait>
SEL 0
POSE 1
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@eventactorclick1
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 1
IF< @incorrect
IF> @incorrect
SOUND simon1#<simonsound>
SEL 1
POSE 8
WAIT #<simonwait>
SEL 1
POSE 2
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@eventactorclick2
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 2
IF< @incorrect
IF> @incorrect
SOUND simon2#<simonsound>
SEL 2
POSE 9
WAIT #<simonwait>
SEL 2
POSE 3
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@eventactorclick3
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 3
IF< @incorrect
IF> @incorrect
SOUND simon3#<simonsound>
SEL 3
POSE 10
WAIT #<simonwait>
SEL 3
POSE 4
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@eventactorclick4
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 4
IF< @incorrect
IF> @incorrect
SOUND simon4#<simonsound>
SEL 4
POSE 11
WAIT #<simonwait>
SEL 4
POSE 5
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@eventactorclick5
ADD playerstart, 1
SET temp1, "circle#<playerstart>"
COMPARE *#<temp1>, 5
IF< @incorrect
IF> @incorrect
SOUND simon5#<simonsound>
SEL 5
POSE 12
WAIT #<simonwait>
SEL 5
POSE 6
GIVE G#<simongive>
COMPARE #<playerstart>, #<totalrounds>
IF= @roundbegin
END

@incorrect
SUB playerstart, 1
SUB lives, 1
n: Sorry, that's not right!
COMPARE #<maxcombo>, #<simonmaxcombo>
IF> @setnewcombo
@combocheck
COMPARE #<maxcombo>, #<tempmaxcombo>
IF> @setcombo
@incorrectcheck
COMPARE #<lives>, 0
IF= @gameover
IF< @gameover
SET maxcombo, 0
n: You have #<lives> lives left.
n: Good luck!
END

@setcombo
SET tempmaxcombo, #<maxcombo>
GOTO @incorrectcheck

@setnewcombo
N: New max combo record!
SET simonmaxcombo, #<maxcombo>
GOTO @combocheck
END

;------------------------------------------

@gameover
MOVE 0,-100,-100,1
MOVE 2,-100,-100,1
MOVE 3,-100,-100,1
MOVE 4,-100,-100,1
MOVE 5,-100,-100,1
ACTOR 1, "Attendant", ronnie, 1,50,60
1: Oh no! You're out of lives!
SET maxcombo, #<tempmaxcombo>
SUB totalrounds, 1
COMPARE #<totalrounds>, #<simonhigh>
IF> @setnewhigh
@gameover2
SET recordrounds, #<totalrounds>
SUB recordrounds, 1
1: Well, today your score was #<totalrounds>, with a max combo of #<maxcombo>.
ADD simontotalpts, #<totalrounds>
COMPARE #<totalrounds>, #<recordnum1>
IF> @newrecord1
COMPARE #<totalrounds>, #<recordnum2>
IF> @newrecord2
COMPARE #<totalrounds>, #<recordnum3>
IF> @newrecord3
COMPARE #<totalrounds>, #<recordnum4>
IF> @newrecord4
COMPARE #<totalrounds>, #<recordnum5>
IF> @newrecord5
COMPARE #<totalrounds>, #<recordnum6>
IF> @newrecord6
COMPARE #<totalrounds>, #<recordnum7>
IF> @newrecord7
COMPARE #<totalrounds>, #<recordnum8>
IF> @newrecord8
COMPARE #<totalrounds>, #<recordnum9>
IF> @newrecord9
COMPARE #<totalrounds>, #<recordnum10>
IF> @newrecord10
1: Not bad, but it doesn't qualify you for a high score.
GOTO @checkbonuses
END

@finish
1: Thanks for playing!
WAIT 2
MOVE 1,-100,-100,1
GOTO @normal
END

@setnewhigh
SET simonhigh, #<totalrecords>
GOTO @gameover2
END

@newrecord1
1: WOW! YOU GOT THE TOP SCORE!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<recordnum5>
SET recordnum5, #<recordnum4>
SET recordnum4, #<recordnum3>
SET recordnum3, #<recordnum2>
SET recordnum2, #<recordnum1>
SET recordnum1, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
SET recordnum6name, #<recordnum5name>
SET recordnum5name, #<recordnum4name>
SET recordnum4name, #<recordnum3name>
SET recordnum3name, #<recordnum2name>
SET recordnum2name, #<recordnum1name>
@newrecord1ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum1name, #<lastask>
ASK 9999
IF Qn, @newrecord1ask
1: Alright, #<recordnum1name>! You've got a high score of #<recordnum1>!
GOTO @checkbonuses
END

@newrecord2
1: Awesome! You got second place!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<recordnum5>
SET recordnum5, #<recordnum4>
SET recordnum4, #<recordnum3>
SET recordnum3, #<recordnum2>
SET recordnum2, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
SET recordnum6name, #<recordnum5name>
SET recordnum5name, #<recordnum4name>
SET recordnum4name, #<recordnum3name>
SET recordnum3name, #<recordnum2name>
@newrecord2ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum2name, #<lastask>
ASK 9999
IF Qn, @newrecord2ask
1: Alright, #<recordnum2name>! You've got a high score of #<recordnum2>!
GOTO @checkbonuses
END

@newrecord3
1: Wow, you've got third place!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<recordnum5>
SET recordnum5, #<recordnum4>
SET recordnum4, #<recordnum3>
SET recordnum3, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
SET recordnum6name, #<recordnum5name>
SET recordnum5name, #<recordnum4name>
SET recordnum4name, #<recordnum3name>
@newrecord3ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum3name, #<lastask>
ASK 9999
IF Qn, @newrecord3ask
1: Alright, #<recordnum3name>! You've got a high score of #<recordnum3>!
GOTO @checkbonuses
END

@newrecord4
1: Whoa, that's the fourth highest score!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<recordnum5>
SET recordnum5, #<recordnum4>
SET recordnum4, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
SET recordnum6name, #<recordnum5name>
SET recordnum5name, #<recordnum4name>
@newrecord4ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum4name, #<lastask>
ASK 9999
IF Qn, @newrecord4ask
1: Alright, #<recordnum4name>! You've got a high score of #<recordnum4>!
GOTO @checkbonuses
END

@newrecord5
1: Nice, that's the fifth highest score!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<recordnum5>
SET recordnum5, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
SET recordnum6name, #<recordnum5name>
@newrecord5ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum5name, #<lastask>
ASK 9999
IF Qn, @newrecord5ask
1: Alright, #<recordnum5name>! You've got a high score of #<recordnum5>!
GOTO @checkbonuses
END

@newrecord6
1: Cool, that puts you in the sixth high score spot!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<recordnum6>
SET recordnum6, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
SET recordnum7name, #<recordnum6name>
@newrecord6ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum6name, #<lastask>
ASK 9999
IF Qn, @newrecord6ask
1: Alright, #<recordnum6name>! You've got a high score of #<recordnum6>!
GOTO @checkbonuses
END

@newrecord7
1: Nice, that's the seventh highest score!
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<recordnum7>
SET recordnum7, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
SET recordnum8name, #<recordnum7name>
@newrecord7ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum7name, #<lastask>
ASK 9999
IF Qn, @newrecord7ask
1: Alright, #<recordnum7name>! You've got a high score of #<recordnum7>!
GOTO @checkbonuses
END

@newrecord8
1: Cool, that's the 8th highest score.
SET recordnum10, #<recordnum9>
SET recordnum9, #<recordnum8>
SET recordnum8, #<totalrounds>
SET recordnum10name, #<recordnum9name>
SET recordnum9name, #<recordnum8name>
@newrecord8ask
1: What would you like the name to be for this record?
ASK 9999
SET recordnum8name, #<lastask>
1: So you'd like it to be #<lastask>?
ASK 9999
IF Qn, @newrecord8ask
1: Alright, #<recordnum8name>! You've got a high score of #<recordnum8>!
GOTO @checkbonuses
END

@newrecord9
1: Nice, that's the 9th highest score.
SET recordnum10, #<recordnum9>
SET recordnum9, #<totalrounds>
SET recordnum10name, #<recordnum9name>
@newrecord9ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum9name, #<lastask>
ASK 9999
IF Qn, @newrecord9ask
1: Alright, #<recordnum9name>! You've got a high score of #<recordnum9>!
GOTO @checkbonuses
END

@newrecord10
1: Nice, that's the 10th highest score.
SET recordnum10, #<totalrounds>
@newrecord10ask
1: What would you like the name to be for this record?
ASK 9999
1: So you'd like it to be #<lastask>?
SET recordnum10name, #<lastask>
ASK 9999
IF Qn, @newrecord10ask
1: Alright, #<recordnum10name>! You've got a high score of #<recordnum10>!
GOTO @checkbonuses
END

@checkbonuses
COMPARE #<simontotalpts>, 100
IF= @bonusnumber1
IF> @bonusnumber1
@checkbonuses2
COMPARE #<simonhigh>, 15
IF= @bonusnumber2
IF> @bonusnumber2
@checkbonuses3
COMPARE #<simonhigh>, 50
IF= @bonusnumber3
IF> @bonusnumber3
@checkbonuses4
COMPARE #<simonmaxcombo>, 25
IF= @bonusnumber4
IF> @bonusnumber4
@checkbonuses5
COMPARE #<simontotalpts>, 200
IF= @bonusnumber5
IF> @bonusnumber5
@checkbonuses6
COMPARE #<simontotalpts>, 300
IF= @bonusnumber6
IF> @bonusnumber6
@checkbonuses7
COMPARE #<simontotalpts>, 50
IF= @bonusnumber7
IF> @bonusnumber7
GOTO @finish
END

@bonusnumber1
COMPARE #<bonusnumber1>, 1
IF= @checkbonuses2
1: You have unlocked "Double Money" mode!
SET bonusgame1, "Double Money"
SET bonusgame1lbl, "dm"
SET bonusnumber1, 1
ADD simongametotal, 1
GOTO @checkbonuses2
END

@bonusnumber2
COMPARE #<bonusnumber2>, 1
IF= @checkbonuses3
1: You have unlocked "Free Play" mode!
SET bonusgame2, "Free Play"
SET bonusgame2lbl, "fp"
SET bonusnumber2, 1
ADD simongametotal, 1
GOTO @checkbonuses3
END

@bonusnumber3
COMPARE #<bonusnumber3>, 1
IF= @checkbonuses4
1: You have unlocked "Infinate Lives" mode!
SET bonusgame3, "Infinate Lives"
SET bonusgame3lbl, "il"
SET bonusnumber3, 1
ADD simongametotal, 1
GOTO @checkbonuses4
END

@bonusnumber4
COMPARE #<bonusnumber4>, 1
IF= @checkbonuses5
1: You can now adjust how many lives you start with!
SET bonusoption1, "Adjust Lives"
SET bonusoption1lbl, "adjustlives"
SET bonusnumber4, 1
GOTO @finish
END

@bonusnumber5
COMPARE #<bonusnumber5>, 1
IF= @checkbonuses6
1: You have unlocked "Triple Money" mode!
SET bonusgame4, "Triple Money"
SET bonusgame4lbl, "tm"
SET bonusnumber5, 1
ADD simongametotal, 1
GOTO @checkbonuses6
END

@bonusnumber6
COMPARE #<bonusnumber6>, 1
IF= @checkbonuses7
1: You have unlocked "Quadruple Money" mode!
SET bonusgame5, "Quadruple Money"
SET bonusgame5lbl, "qm"
SET bonusnumber6, 1
ADD simongametotal, 1
GOTO @checkbonuses7
END

@bonusnumber7
COMPARE #<bonusnumber7>, 1
IF= @finish
1: You just got the techno sound! Select it from the sound options!
SOUND simon2b.wav
SET bonusnumber7, 1
SET ss1, "Techno Pad"
ADD simonsoundtotal, 1
GOTO @finish
END