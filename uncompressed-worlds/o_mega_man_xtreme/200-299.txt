;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX	  BOSS FIGHTS    200 - 299	 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
;
;
SCENE 200 1ArmoredArmadillo
#include all.txt
IF T200,@t200
MUSIC Boss
WAIT 5.0
FIGHT 1
IF DEAD,@dead
MUSIC Boss
FIGHT 2
IF DEAD,@dead
MUSIC Boss
FIGHT 3
IF DEAD,@dead
MUSIC Boss
FIGHT 4
IF DEAD,@dead
MUSIC Bpss
FIGHT 5
IF DEAD,@dead
MUSIC Boss
FIGHT 6
IF DEAD,@dead
MUSIC Boss
FIGHT2 2000
IF WON,@give200
GOTO @dead
END
@give200
MUSIC BossDead
GIVE T200
GIVE S300
GIVE I400
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t200
END
@dead
SOUND Death
END
;
;
SCENE 201 1BoomerKuwanger
#include all.txt
IF T201,@t201
MUSIC Boss
WAIT 5.0
FIGHT 7
IF DEAD,@dead
MUSIC Boss
FIGHT 8
IF DEAD,@dead
MUSIC Boss
FIGHT 9
IF DEAD,@dead
MUSIC Boss
FIGHT 10
IF DEAD,@dead
MUSIC Boss
FIGHT 11
IF DEAD,@dead
MUSIC Boss
FIGHT2 2001
IF WON,@give201
GOTO @dead
END
@give201
MUSIC BossDead
GIVE T201
GIVE S400
GIVE I401
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t201
END
@dead
SOUND Death
END
;
;
SCENE 202 1ChillPenguin
#include all.txt
IF T202,@t202
MUSIC Boss
WAIT 5.0
FIGHT 12
IF DEAD,@dead
MUSIC Boss
FIGHT 13
IF DEAD,@dead
MUSIC Boss
FIGHT 14
IF DEAD,@dead
MUSIC Boss
FIGHT 15
IF DEAD,@dead
MUSIC Boss
FIGHT 16
IF DEAD,@dead
MUSIC Boss
FIGHT 17
IF DEAD,@dead
MUSIC Boss
FIGHT2 2002
IF WON,@give202
GOTO @dead
END
@give202
MUSIC BossDead
GIVE T202
GIVE S100
GIVE I402
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t202
END
@dead
SOUND Death
END
;
;
SCENE 203 1FlameMammoth
#include all.txt
IF T203,@t203
MUSIC Boss
WAIT 5.0
FIGHT 18
IF DEAD,@dead
MUSIC Boss
FIGHT 19
IF DEAD,@dead
MUSIC Boss
FIGHT 20
IF DEAD,@dead
MUSIC Boss
FIGHT 21
IF DEAD,@dead
MUSIC Boss
FIGHT 22
IF DEAD,@dead
MUSIC Boss
FIGHT 23
IF DEAD,@dead
MUSIC Boss
FIGHT2 2004
IF WON,@give203
GOTO @dead
END
@give203
MUSIC BossDead
GIVE T203
GIVE S500
GIVE I403
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t203
END
@dead
SOUND Death
END
;
;
SCENE 204 1LaunchOctopus
#include all.txt
IF T204,@t204
MUSIC Boss
WAIT 5.0
FIGHT 24
IF DEAD,@dead
MUSIC Boss
FIGHT 25
IF DEAD,@dead
MUSIC Boss
FIGHT 26
IF DEAD,@dead
MUSIC Boss
FIGHT 27
IF DEAD,@dead
MUSIC Boss
FIGHT 28
IF DEAD,@dead
MUSIC Boss
FIGHT 29
IF DEAD,@dead
MUSIC Boss
FIGHT2 2005
IF WON,@give204
GOTO @dead
END
@give204
MUSIC BossDead
GIVE T204
GIVE S1
GIVE I404
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t204
END
@dead
SOUND Death
END
;
;
SCENE 205 1SparkMandrill
#include all.txt
IF T205,@t205
MUSIC Boss
WAIT 5.0
FIGHT 30
IF DEAD,@dead
MUSIC Boss
FIGHT 31
IF DEAD,@dead
MUSIC Boss
FIGHT 32
IF DEAD,@dead
MUSIC Boss
FIGHT 33
IF DEAD,@dead
MUSIC Boss
FIGHT 34
IF DEAD,@dead
MUSIC Boss
FIGHT 35
IF DEAD,@dead
MUSIC Boss
FIGHT2 2006
IF WON,@give205
GOTO @dead
END
@give205
MUSIC BossDead
GIVE T205
GIVE S700
GIVE I405
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t205
END
@dead
SOUND Death
END
;
;
SCENE 206 1StingChameleon
#include all.txt
IF T206,@t206
MUSIC Boss
WAIT 5.0
FIGHT 36
IF DEAD,@dead
MUSIC Boss
FIGHT 37
IF DEAD,@dead
MUSIC Boss
FIGHT 38
IF DEAD,@dead
MUSIC Boss
FIGHT 39
IF DEAD,@dead
MUSIC Boss
FIGHT 40
IF DEAD,@dead
MUSIC Boss
FIGHT2 2008
IF WON,@give206
GOTO @dead
END
@give206
MUSIC BossDead
GIVE T206
GIVE S200
GIVE I406
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t206
END
@dead
SOUND Death
END
;
;
SCENE 207 1StormEagle
#include all.txt
IF T207,@t207
MUSIC Boss
WAIT 5.0
FIGHT 41
IF DEAD,@dead
MUSIC Boss
FIGHT 42
IF DEAD,@dead
MUSIC Boss
FIGHT 43
IF DEAD,@dead
MUSIC Boss
FIGHT 44
IF DEAD,@dead
MUSIC Boss
FIGHT 45
IF DEAD,@dead
MUSIC Boss
FIGHT 46
IF DEAD,@dead
MUSIC Boss
FIGHT2 2009
IF WON,@give207
GOTO @dead
END
@give207
MUSIC BossDead
GIVE T207
GIVE S600
GIVE I407
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t207
END
@dead
SOUND Death
END
;
;
SCENE 208 1Bosses
#include all.txt
IF T208,@t208
MUSIC Boss
FIGHT2 2007			;SPIDER
IF WON,@give208
GOTO @dead
END
@give208
MUSIC BossDead
GIVE T208
END
@t208
END
@dead
SOUND Death
END
;
;
SCENE 209 1Bosses
#include all.txt
IF T209,@t209
MUSIC Boss
FIGHT2 2003			;DEATH FACE
IF WON,@give209
GOTO @dead
END
@give209
MUSIC BossDead
GIVE T209
END
@t209
END
@dead
SOUND Death
END
;
;
SCENE 210 1Bosses
#include all.txt
IF T210,@t210
MUSIC Boss
FIGHT2 2012			;VILE WALKER
IF WON,@give210
GOTO @dead
END
@give210
MUSIC BossDead
GIVE T210
END
@t210
END
@dead
SOUND Death
END
;
;
SCENE 211 1Bosses
#include all.txt
IF T211,@t211
MUSIC Boss
FIGHT2 2011			;VILE
IF WON,@give211
GOTO @dead
END
@give211
MUSIC BossDead
GIVE T211
END
@t211
END
@dead
SOUND Death
END
;
;
SCENE 212 1Bosses
#include all.txt
IF T212,@t212
MUSIC Boss
FIGHT2 2010			;VELGUANUNDER
IF WON,@give212
GOTO @dead
END
@give212
MUSIC BossDead
GIVE T212
END
@t212
END
@dead
SOUND Death
END
;
;
SCENE 213 1Bosses
#include all.txt
IF T213,@t213
MUSIC MainBoss
WAIT 3.0
FIGHT 2000
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2001
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2002
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2004
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2005
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2006
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2008
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2009
IF DEAD,@dead
MUSIC MainBoss
FIGHT2 2013			;SIGMA VER.1
IF WON,@give213
GOTO @dead
END
@give213
MUSIC MainBossDead
GIVE T213
@t213
N: ............................... ZONE CLEAR !!! ..................................... 
N: .........................CONGRATULATIONS !!! ................................
N: YOU MAY NOW USE A NEW DR LIGHT "OPTION"
N: TO REVIVE BOSSES AND REPLAY AREAS.
END
@dead
SOUND Death
END
;
;
SCENE 215 2BubbleCrab
#include all.txt
IF T214,@t214
MUSIC Boss
WAIT 5.0
FIGHT 47
IF DEAD,@dead
MUSIC Boss
FIGHT 48
IF DEAD,@dead
MUSIC Boss
FIGHT 49
IF DEAD,@dead
MUSIC Boss
FIGHT 50
IF DEAD,@dead
MUSIC Boss
FIGHT2 2020
IF WON,@give214
GOTO @dead
END
@give214
MUSIC BossDead
GIVE T214
GIVE S101
GIVE I408
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t214
END
@dead
SOUND Death
END
;
;
SCENE 216 2CrystalSnail
#include all.txt
IF T215,@t215
MUSIC Boss
WAIT 5.0
FIGHT 51
IF DEAD,@dead
MUSIC Boss
FIGHT 52
IF DEAD,@dead
MUSIC Boss
FIGHT 53
IF DEAD,@dead
MUSIC Boss
FIGHT 54
IF DEAD,@dead
MUSIC Boss
FIGHT2 2021
IF WON,@give215
GOTO @dead
END
@give215
MUSIC BossDead
GIVE T215
GIVE S201
GIVE I409
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t215
END
@dead
SOUND Death
END
;
;
SCENE 217 2FlameStag
#include all.txt
IF T216,@t216
MUSIC Boss
WAIT 5.0
FIGHT 55
IF DEAD,@dead
MUSIC Boss
FIGHT 56
IF DEAD,@dead
MUSIC Boss
FIGHT 57
IF DEAD,@dead
MUSIC Boss
FIGHT 58
IF DEAD,@dead
MUSIC Boss
FIGHT2 2022
IF WON,@give216
GOTO @dead
END
@give216
MUSIC BossDead
GIVE T216
GIVE S501
GIVE I410
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t216
END
@dead
SOUND Death
END
;
;
SCENE 218 2MagnaCentipede
#include all.txt
IF T217,@t217
MUSIC Boss
WAIT 5.0
FIGHT 59
IF DEAD,@dead
MUSIC Boss
FIGHT 60
IF DEAD,@dead
MUSIC Boss
FIGHT 61
IF DEAD,@dead
MUSIC Boss
FIGHT 62
IF DEAD,@dead
MUSIC Boss
FIGHT2 2023
IF WON,@give217
GOTO @dead
END
@give217
MUSIC BossDead
GIVE T217
GIVE S2
GIVE I411
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t217
END
@dead
SOUND Death
END
;
;
SCENE 219 2MorphMoth
#include all.txt
IF T218,@t218
MUSIC Boss
WAIT 5.0
FIGHT 63
IF DEAD,@dead
MUSIC Boss
FIGHT 64
IF DEAD,@dead
MUSIC Boss
FIGHT 65
IF DEAD,@dead
MUSIC Boss
FIGHT 66
IF DEAD,@dead
MUSIC Boss
FIGHT2 2024
IF WON,@give218
GOTO @dead
END
@give218
MUSIC BossDead
GIVE T218
GIVE S601
GIVE I412
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t218
END
@dead
SOUND Death
END
;
;
SCENE 220 2OverdriveOstrich
#include all.txt
IF T219,@t219
MUSIC Boss
WAIT 5.0
FIGHT 67
IF DEAD,@dead
MUSIC Boss
FIGHT 68
IF DEAD,@dead
MUSIC Boss
FIGHT 69
IF DEAD,@dead
MUSIC Boss
FIGHT 70
IF DEAD,@dead
MUSIC Boss
FIGHT2 2025
IF WON,@give219
GOTO @dead
END
@give219
MUSIC BossDead
GIVE T219
GIVE S401
GIVE I413
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t219
END
@dead
SOUND Death
END
;
;
SCENE 221 2WheelGator
#include all.txt
IF T220,@t220
MUSIC Boss
WAIT 5.0
FIGHT 71
IF DEAD,@dead
MUSIC Boss
FIGHT 72
IF DEAD,@dead
MUSIC Boss
FIGHT 73
IF DEAD,@dead
MUSIC Boss
FIGHT 74
IF DEAD,@dead
MUSIC Boss
FIGHT2 2026
IF WON,@give220
GOTO @dead
END
@give220
MUSIC BossDead
GIVE T220
GIVE S301
GIVE I414
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t220
END
@dead
SOUND Death
END
;
;
SCENE 222 2WireSponge
#include all.txt
IF T221,@t221
MUSIC Boss
WAIT 5.0
FIGHT 75
IF DEAD,@dead
MUSIC Boss
FIGHT 76
IF DEAD,@dead
MUSIC Boss
FIGHT 77
IF DEAD,@dead
MUSIC Boss
FIGHT 78
IF DEAD,@dead
MUSIC Boss
FIGHT2 2027
IF WON,@give221
GOTO @dead
END
@give221
MUSIC BossDead
GIVE T221
GIVE S701
GIVE I415
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t221
END
@dead
SOUND Death
END
;
;

SCENE 223 2Bosses
#include all.txt
IF T222,@t222
MUSIC Boss
FIGHT2 2028			
IF WON,@give222
GOTO @dead		;vile walker
END
@give222
MUSIC BossDead
GIVE T222
END
@t222
END
@dead
SOUND Death
END
;
;
SCENE 224 2Bosses
#include all.txt
IF T223,@t223
MUSIC Boss
FIGHT2 2029			
IF WON,@give223		;vile
GOTO @dead
END
@give223
MUSIC BossDead
GIVE T223
END
@t223
END
@dead
SOUND Death
END
;
;
SCENE 225 2Bosses
#include all.txt
IF T224,@t224
MUSIC Boss
FIGHT2 2030			;AGILE
IF WON,@give224
GOTO @dead
END
@give224
MUSIC BossDead
GIVE T224
END
@t224
END
@dead
SOUND Death
END
;
;
SCENE 226 2Bosses
#include all.txt
IF T225,@t225
MUSIC Boss
FIGHT2 2031			;SERGES
IF WON,@give225
GOTO @dead
END
@give225
MUSIC BossDead
GIVE T225
END
@t225
END
@dead
SOUND Death
END
;
;
SCENE 227 2Bosses
#include all.txt
IF T226,@t226
MUSIC Boss
FIGHT2 2032			;VIOLEN
IF WON,@give226
GOTO @dead
END
@give226
MUSIC BossDead
GIVE T226
END
@t226
END
@dead
SOUND Death
END
;
;
SCENE 228 2Bosses
#include all.txt
IF T227,@t227
MUSIC MainBoss
WAIT 3.0
FIGHT 2020
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2021
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2022
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2023
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2024
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2025
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2026
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2027
IF DEAD,@dead
MUSIC MainBoss
FIGHT2 2033			;SIGMA
IF WON,@give227
GOTO @dead
END
@give227
MUSIC MainBossDead
GIVE T227
@t227
N: ............................... ZONE CLEAR !!! ..................................... 
N: .........................CONGRATULATIONS !!! ................................
END
@dead
SOUND Death
END
;
;
SCENE 230 3BlastHornet
#include all.txt
IF T228,@t228
MUSIC Boss
WAIT 5.0
FIGHT 79
IF DEAD,@dead
MUSIC Boss
FIGHT 80
IF DEAD,@dead
MUSIC Boss
FIGHT 81
IF DEAD,@dead
MUSIC Boss
FIGHT 82
IF DEAD,@dead
MUSIC Boss
FIGHT 83
IF DEAD,@dead
MUSIC Boss
FIGHT 84
IF DEAD,@dead
MUSIC Boss
FIGHT2 2040
IF WON,@give228
GOTO @dead
END
@give228
MUSIC BossDead
GIVE T228
GIVE S3
GIVE I416
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t228
END
@dead
SOUND Death
END
;
;
SCENE 231 3BlizzardBuffalo
#include all.txt
IF T229,@t229
MUSIC Boss
WAIT 5.0
FIGHT 85
IF DEAD,@dead
MUSIC Boss
FIGHT 86
IF DEAD,@dead
MUSIC Boss
FIGHT 87
IF DEAD,@dead
MUSIC Boss
FIGHT 88
IF DEAD,@dead
MUSIC Boss
FIGHT 89
IF DEAD,@dead
MUSIC Boss
FIGHT2 2041
IF WON,@give229
GOTO @dead
END
@give229
MUSIC BossDead
GIVE T229
GIVE S102
GIVE I417
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t229
END
@dead
SOUND Death
END
;
;
SCENE 232 3CrushCrawfish
#include all.txt
IF T230,@t230
MUSIC Boss
WAIT 5.0
FIGHT 90
IF DEAD,@dead
MUSIC Boss
FIGHT 91
IF DEAD,@dead
MUSIC Boss
FIGHT 92
IF DEAD,@dead
MUSIC Boss
FIGHT 93
IF DEAD,@dead
MUSIC Boss
FIGHT 94
IF DEAD,@dead
FIGHT2 2042
IF WON,@give230
GOTO @dead
END
@give230
MUSIC BossDead
GIVE T230
GIVE S202
GIVE I418
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t230
END
@dead
SOUND Death
END
;
;
SCENE 233 3GravityBeetle
#include all.txt
IF T231,@t231
MUSIC Boss
WAIT 5.0
FIGHT 95
IF DEAD,@dead
MUSIC Boss
FIGHT 96
IF DEAD,@dead
MUSIC Boss
FIGHT 97
IF DEAD,@dead
MUSIC Boss
FIGHT 98
IF DEAD,@dead
MUSIC Boss
FIGHT 99
IF DEAD,@dead
MUSIC Boss
FIGHT 100
IF DEAD,@dead
MUSIC Boss
FIGHT2 2043
IF WON,@give231
GOTO @dead
END
@give231
MUSIC BossDead
GIVE T231
GIVE S402
GIVE I419
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t231
END
@dead
SOUND Death
END
;
;
SCENE 234 3NeonTiger
#include all.txt
IF T232,@t232
MUSIC Boss
WAIT 5.0
FIGHT 101
IF DEAD,@dead
MUSIC Boss
FIGHT 102
IF DEAD,@dead
MUSIC Boss
FIGHT 103
IF DEAD,@dead
MUSIC Boss
FIGHT 104
IF DEAD,@dead
MUSIC Boss
FIGHT 105
IF DEAD,@dead
MUSIC Boss
FIGHT2 2044
IF WON,@give232
GOTO @dead
END
@give232
MUSIC BossDead
GIVE T232
GIVE S302
GIVE I420
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t232
END
@dead
SOUND Death
END
;
;
SCENE 235 3ToxicSeahorse
#include all.txt
IF T233,@t233
MUSIC Boss
WAIT 5.0
FIGHT 106
IF DEAD,@dead
MUSIC Boss
FIGHT 107
IF DEAD,@dead
MUSIC Boss
FIGHT 108
IF DEAD,@dead
MUSIC Boss
FIGHT 109
IF DEAD,@dead
MUSIC Boss
FIGHT 110
IF DEAD,@dead
MUSIC Boss
FIGHT2 2045
IF WON,@give233
GOTO @dead
END
@give233
MUSIC BossDead
GIVE T233
GIVE S602
GIVE I421
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t233
END
@dead
SOUND Death
END
;
;
SCENE 236 3TunnelRhino
#include all.txt
IF T234,@t234
MUSIC Boss
WAIT 5.0
FIGHT 111
IF DEAD,@dead
MUSIC Boss
FIGHT 112
IF DEAD,@dead
MUSIC Boss
FIGHT 113
IF DEAD,@dead
MUSIC Boss
FIGHT 114
IF DEAD,@dead
MUSIC Boss
FIGHT 115
IF DEAD,@dead
MUSIC Boss
FIGHT 116
IF DEAD,@dead
MUSIC Boss
FIGHT2 2046
IF WON,@give234
GOTO @dead
END
@give234
MUSIC BossDead
GIVE T234
GIVE S502
GIVE I422
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t234
END
@dead
SOUND Death
END
;
;
SCENE 237 3VoltCatfish
#include all.txt
IF T235,@t235
MUSIC Boss
WAIT 5.0
FIGHT 117
IF DEAD,@dead
MUSIC Boss
FIGHT 118
IF DEAD,@dead
MUSIC Boss
FIGHT 119
IF DEAD,@dead
MUSIC Boss
FIGHT 120
IF DEAD,@dead
MUSIC Boss
FIGHT 121
IF DEAD,@dead
MUSIC Boss
FIGHT2 2047
IF WON,@give235
GOTO @dead
END
@give235
MUSIC BossDead
GIVE T235
GIVE S702
GIVE I423
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t235
END
@dead
SOUND Death
END
;
;
SCENE 238 3Bosses
#include all.txt
IF T236,@t236
MUSIC Boss
FIGHT2 2048			;VILE 3
IF WON,@give236
GOTO @dead
END
@give236
MUSIC BossDead
GIVE T236
END
@t236
END
@dead
SOUND Death
END
;
;
SCENE 239 3Bosses
#include all.txt
IF T237,@t237
MUSIC Boss
FIGHT2 2049			;BIT
IF WON,@give237
GOTO @dead
END
@give237
MUSIC BossDead
GIVE T237
END
@t237
END
@dead
SOUND Death
END
;
;
SCENE 240 3Bosses
#include all.txt
IF T238,@t238
MUSIC Boss
FIGHT2 2050			;BYTE
IF WON,@give238
GOTO @dead
END
@give238
MUSIC BossDead
GIVE T238
END
@t238
END
@dead
SOUND Death
END
;
;
SCENE 241 3Bosses
#include all.txt
IF T239,@t239
MUSIC Boss
FIGHT2 2051			;GIGABYTE
IF WON,@give239
GOTO @dead
END
@give239
MUSIC BossDead
GIVE T239
END
@t239
END
@dead
SOUND Death
END
;
;
SCENE 242 3Bosses
#include all.txt
IF T240,@t240
MUSIC Boss
FIGHT2 2052			;DR DOPPLER
IF WON,@give240
GOTO @dead
END
@give240
MUSIC BossDead
GIVE T240
END
@t240
END
@dead
SOUND Death
END
;
;
SCENE 243 3Bosses
#include all.txt
IF T241,@t241
MUSIC MainBoss
WAIT 3.0
FIGHT 2040
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2041
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2042
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2043
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2044
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2045
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2046
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2047
IF DEAD,@dead
MUSIC MainBoss
FIGHT2 2053			;SIGMA VER.3
IF WON,@give241
GOTO @dead
END
@give241
MUSIC MainBossDead
GIVE T241
@t241
N: ............................... ZONE CLEAR !!! ..................................... 
N: .........................CONGRATULATIONS !!! ................................
END
@dead
SOUND Death
END
;
;
SCENE 245 4CyberPeacock
#include all.txt
IF T242,@t242
MUSIC Boss
WAIT 5.0
FIGHT 122
IF DEAD,@dead
MUSIC Boss
FIGHT 123
IF DEAD,@dead
MUSIC Boss
FIGHT 124
IF DEAD,@dead
MUSIC Boss
FIGHT 125
IF DEAD,@dead
MUSIC Boss
FIGHT2 2060
IF WON,@give242
GOTO @dead
END
@give242
MUSIC BossDead
GIVE T242
GIVE S303
GIVE I424
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t242
END
@dead
SOUND Death
END
;
;
SCENE 246 4FrostWalrus
#include all.txt
IF T243,@t243
MUSIC Boss
WAIT 5.0
FIGHT 126
IF DEAD,@dead
MUSIC Boss
FIGHT 127
IF DEAD,@dead
MUSIC Boss
FIGHT 128
IF DEAD,@dead
MUSIC Boss
FIGHT 129
IF DEAD,@dead
MUSIC Boss
FIGHT2 2061
IF WON,@give243
GOTO @dead
END
@give243
MUSIC BossDead
GIVE T243
GIVE S103
GIVE I425
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t243
END
@dead
SOUND Death
END
;
;
SCENE 247 4JetStingray
#include all.txt
IF T244,@t244
MUSIC Boss
WAIT 5.0
FIGHT 130
IF DEAD,@dead
MUSIC Boss
FIGHT 131
IF DEAD,@dead
MUSIC Boss
FIGHT 132
IF DEAD,@dead
MUSIC Boss
FIGHT 133
IF DEAD,@dead
MUSIC Boss
FIGHT2 2062
IF WON,@give244
GOTO @dead
END
@give244
MUSIC BossDead
GIVE T244
GIVE S4
GIVE I426
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t244
END
@dead
SOUND Death
END
;
;
SCENE 248 4MagmaDragoon
#include all.txt
IF T245,@t245
MUSIC Boss
WAIT 5.0
FIGHT 134
IF DEAD,@dead
MUSIC Boss
FIGHT 135
IF DEAD,@dead
MUSIC Boss
FIGHT 136
IF DEAD,@dead
MUSIC Boss
FIGHT 137
IF DEAD,@dead
MUSIC Boss
FIGHT2 2063
IF WON,@give245
GOTO @dead
END
@give245
MUSIC BossDead
GIVE T245
GIVE S503
GIVE I427
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t245
END
@dead
SOUND Death
END
;
;
SCENE 249 4SlashBeast
#include all.txt
IF T246,@t246
MUSIC Boss
WAIT 5.0
FIGHT 138
IF DEAD,@dead
MUSIC Boss
FIGHT 139
IF DEAD,@dead
MUSIC Boss
FIGHT 140
IF DEAD,@dead
MUSIC Boss
FIGHT 141
IF DEAD,@dead
MUSIC Boss
FIGHT2 2064
IF WON,@give246
GOTO @dead
END
@give246
MUSIC BossDead
GIVE T246
GIVE S403
GIVE I428
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t246
END
@dead
SOUND Death
END
;
;
SCENE 250 4SplitMushroom
#include all.txt
IF T247,@t247
MUSIC Boss
WAIT 5.0
FIGHT 142
IF DEAD,@dead
MUSIC Boss
FIGHT 143
IF DEAD,@dead
MUSIC Boss
FIGHT 144
IF DEAD,@dead
MUSIC Boss
FIGHT 145
IF DEAD,@dead
MUSIC Boss
FIGHT2 2065
IF WON,@give247
GOTO @dead
END
@give247
MUSIC BossDead
GIVE T247
GIVE S203
GIVE I429
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t247
END
@dead
SOUND Death
END
;
;
SCENE 251 4StormOwl
#include all.txt
IF T248,@t248
MUSIC Boss
WAIT 5.0
FIGHT 146
IF DEAD,@dead
MUSIC Boss
FIGHT 147
IF DEAD,@dead
MUSIC Boss
FIGHT 148
IF DEAD,@dead
MUSIC Boss
FIGHT 149
IF DEAD,@dead
MUSIC Boss
FIGHT2 2066
IF WON,@give248
GOTO @dead
END
@give248
MUSIC BossDead
GIVE T248
GIVE S603
GIVE I430
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t248
END
@dead
SOUND Death
END
;
;
SCENE 252 4WebSpider
#include all.txt
IF T249,@t249
MUSIC Boss
WAIT 5.0
FIGHT 150
IF DEAD,@dead
MUSIC Boss
FIGHT 151
IF DEAD,@dead
MUSIC Boss
FIGHT 152
IF DEAD,@dead
MUSIC Boss
FIGHT 153
IF DEAD,@dead
MUSIC Boss
FIGHT2 2067
IF WON,@give249
GOTO @dead
END
@give249
MUSIC BossDead
GIVE T249
GIVE S703
GIVE I431
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t249
END
@dead
SOUND Death
END
;
;
SCENE 253 4Bosses
#include all.txt
IF T250,@t250
MUSIC Boss
FIGHT2 2068			;DOUBLE
IF WON,@give250
GOTO @dead
END
@give250
MUSIC BossDead
GIVE T250
END
@t250
END
@dead
SOUND Death
END
;
;
SCENE 254 4Bosses
#include all.txt
IF T251,@t251
MUSIC Boss
FIGHT2 2069			;IRIS
IF WON,@give251
GOTO @dead
END
@give251
MUSIC BossDead
GIVE T251
END
@t251
END
@dead
SOUND Death
END
;
;
SCENE 255 4Bosses
#include all.txt
IF T252,@t252
MUSIC Boss
FIGHT2 2070			;COLONEL
IF WON,@give252
GOTO @dead
END
@give252
MUSIC BossDead
GIVE T252
END
@t252
END
@dead
SOUND Death
END
;
;
SCENE 256 4Bosses
#include all.txt
IF T253,@t253
MUSIC Boss
FIGHT2 2071			;GENERAL
IF WON,@give253
GOTO @dead
END
@give253
MUSIC BossDead
GIVE T253
END
@t253
END
@dead
SOUND Death
END
;
;
SCENE 257 4Bosses
#include all.txt
IF T254,@t254
MUSIC Boss
FIGHT2 2072			;REAPER
IF WON,@give254
GOTO @dead
END
@give254
MUSIC BossDead
GIVE T254
END
@t254
END
@dead
SOUND Death
END
;
;
SCENE 258 4Bosses
#include all.txt
IF T255,@t255
MUSIC MainBoss
WAIT 3.0
FIGHT 2060
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2061
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2062
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2063
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2064
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2065
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2066
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2067
IF DEAD,@dead
MUSIC MainBoss
FIGHT2 2073			;SIGMA VER.4
IF WON,@give255
GOTO @dead
END
@give255
MUSIC MainBossDead
GIVE T255
@t255
N: ............................... ZONE CLEAR !!! ..................................... 
N: .........................CONGRATULATIONS !!! ................................
END
@dead
SOUND Death
END
;
;
SCENE 260 5AxleTheRed
#include all.txt
IF T256,@t256
MUSIC Boss
WAIT 5.0
FIGHT 154
IF DEAD,@dead
MUSIC Boss
FIGHT 155
IF DEAD,@dead
MUSIC Boss
FIGHT 156
IF DEAD,@dead
MUSIC Boss
FIGHT 157
IF DEAD,@dead
MUSIC Boss
FIGHT 158
IF DEAD,@dead
MUSIC Boss
FIGHT2 2074
IF WON,@give256
GOTO @dead
END
@give256
MUSIC BossDead
GIVE T256
GIVE S5
GIVE I432
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t256
END
@dead
SOUND Death
END
;
;
SCENE 261 5DarkDizzy
#include all.txt
IF T257,@t257
MUSIC Boss
WAIT 5.0
FIGHT 159
IF DEAD,@dead
MUSIC Boss
FIGHT 160
IF DEAD,@dead
MUSIC Boss
FIGHT 161
IF DEAD,@dead
MUSIC Boss
FIGHT 162
IF DEAD,@dead
MUSIC Boss
FIGHT 163
IF DEAD,@dead
MUSIC Boss
FIGHT2 2075
IF WON,@give257
GOTO @dead
END
@give257
MUSIC BossDead
GIVE T257
GIVE S404
GIVE I433
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t257
END
@dead
SOUND Death
END
;
;
SCENE 262 5DuffMcWhalen
#include all.txt
IF T258,@t258
MUSIC Boss
WAIT 5.0
FIGHT 164
IF DEAD,@dead
MUSIC Boss
FIGHT 165
IF DEAD,@dead
MUSIC Boss
FIGHT 166
IF DEAD,@dead
MUSIC Boss
FIGHT 167
IF DEAD,@dead
MUSIC Boss
FIGHT 168
IF DEAD,@dead
MUSIC Boss
FIGHT2 2076
IF WON,@give258
GOTO @dead
END
@give258
MUSIC BossDead
GIVE T258
GIVE S104
GIVE I434
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t258
END
@dead
SOUND Death
END
;
;
SCENE 263 5SlashGrizzly
#include all.txt
IF T259,@t259
MUSIC Boss
WAIT 5.0
FIGHT 179
IF DEAD,@dead
MUSIC Boss
FIGHT 180
IF DEAD,@dead
MUSIC Boss
FIGHT 181
IF DEAD,@dead
MUSIC Boss
FIGHT 182
IF DEAD,@dead
MUSIC Boss
FIGHT 183
IF DEAD,@dead
MUSIC Boss
FIGHT2 2077
IF WON,@give259
GOTO @dead
END
@give259
MUSIC BossDead
GIVE T259
GIVE S304
GIVE I435
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t259
END
@dead
SOUND Death
END
;
;
SCENE 264 5IzzyGlow
#include all.txt
IF T260,@t260
MUSIC Boss
WAIT 5.0
FIGHT 169
IF DEAD,@dead
MUSIC Boss
FIGHT 170
IF DEAD,@dead
MUSIC Boss
FIGHT 171
IF DEAD,@dead
MUSIC Boss
FIGHT 172
IF DEAD,@dead
MUSIC Boss
FIGHT 173
IF DEAD,@dead
MUSIC Boss
FIGHT2 2078
IF WON,@give260
GOTO @dead
END
@give260
MUSIC BossDead
GIVE T260
GIVE S204
GIVE I436
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t260
END
@dead
SOUND Death
END
;
;
SCENE 265 5Mattrex
#include all.txt
IF T261,@t261
MUSIC Boss
WAIT 5.0
FIGHT 174
IF DEAD,@dead
MUSIC Boss
FIGHT 175
IF DEAD,@dead
MUSIC Boss
FIGHT 176
IF DEAD,@dead
MUSIC Boss
FIGHT 177
IF DEAD,@dead
MUSIC Boss
FIGHT 178
IF DEAD,@dead
MUSIC Boss
FIGHT2 2079
IF WON,@give261
GOTO @dead
END
@give261
MUSIC BossDead
GIVE T261
GIVE S504
GIVE I437
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t261
END
@dead
SOUND Death
END
;
;
SCENE 266 5SquidAdler
#include all.txt
IF T262,@t262
MUSIC Boss
WAIT 5.0
FIGHT 184
IF DEAD,@dead
MUSIC Boss
FIGHT 185
IF DEAD,@dead
MUSIC Boss
FIGHT 186
IF DEAD,@dead
MUSIC Boss
FIGHT 187
IF DEAD,@dead
MUSIC Boss
FIGHT 188
IF DEAD,@dead
MUSIC Boss
FIGHT2 2080
IF WON,@give262
GOTO @dead
END
@give262
MUSIC BossDead
GIVE T262
GIVE S704
GIVE I438
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t262
END
@dead
SOUND Death
END
;
;
SCENE 267 5TheSkiver
#include all.txt
IF T263,@t263
MUSIC Boss
WAIT 5.0
FIGHT 189
IF DEAD,@dead
MUSIC Boss
FIGHT 190
IF DEAD,@dead
MUSIC Boss
FIGHT 191
IF DEAD,@dead
MUSIC Boss
FIGHT 192
IF DEAD,@dead
MUSIC Boss
FIGHT 193
IF DEAD,@dead
MUSIC Boss
FIGHT2 2081
IF WON,@give263
GOTO @dead
END
@give263
MUSIC BossDead
GIVE T263
GIVE S604
GIVE I439
N: Special Weapon aquired!!!
N: Sensor Component aquired!!!
END
@t263
END
@dead
SOUND Death
END
;
;
SCENE 268 5Bosses
#include all.txt
IF T264,@t264
MUSIC Boss
FIGHT2 2281			;DARK EYE
IF WON,@give264
GOTO @dead
END
@give264
MUSIC BossDead
GIVE T264
END
@t264
END
@dead
SOUND Death
END
;
;
SCENE 269 5Bosses
#include all.txt
IF T265,@t265
MUSIC Boss
FIGHT2 2282			;DYNAMO
IF WON,@give265
GOTO @dead
END
@give265
MUSIC BossDead
GIVE T265
END
@t265
END
@dead
SOUND Death
END
;
;
SCENE 270 5Bosses
#include all.txt
IF T266,@t266
MUSIC MainBoss
WAIT 3.0
FIGHT 2273
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2274
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2275
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2276
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2277
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2278
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2279
IF DEAD,@dead
MUSIC MainBoss
FIGHT 2280
IF DEAD,@dead
MUSIC MainBoss
FIGHT2 2283		;SIGMA VER.5
IF WON,@give266
GOTO @dead
END
@give266
MUSIC MainBossDead
GIVE T266
@t266
N: ............................... ZONE CLEAR !!! ..................................... 
N: .........................CONGRATULATIONS !!! ................................
END
@dead
SOUND Death
END
;
;