;-------------------
; character levels
;    arg0:		Class+Level (103 = warrior level 3, 203 = wizard level 3)
;    arg1:		[NO LONGER USED] Additional Experience points to attain level from previous level
;	 arg2:		Additional max HP earned when level is attained
;	 arg3:		Additional max MP earned when level is attained
;    arg4:      Level Name (use the same name for several levels in a row)
;
; These arguments are only valid for level 0 in each character class.  They control
; common aspects of the class
;
;	 arg5		(level 0 only)  magic-ratio
;	 arg6		(level 0 only)  right-hand weapon (1 - 8, or 0 if none (will use start_hand_training instead))
;
; Levels run from 1 to 99 maximum.  A bonus level "100" is supplied by the game, but it uses the same
; table entry as level 99.  That is to say, you cannot provide a custom level name for level 100.
;
; You only need to have entries for levels where you want the hp, mp, new spell, or
; level name to change.  Minimum experience levels are now computed automatically
; for you.  If you give an 'empty' level name (except for level 1 where it is
; mandatory) the previous name will be used for that level
;
; Note that LEVEL ZERO (100,200, etc) is a SPECIAL ENTRY which names the actual
; character class, and is not a level in of itself.  I know this is horribly
; confusing, but it is just so dang CONVENIENT to do it this way!
; So, for LEVEL 0 ONLY:  maxHP and maxMP are the starting levels of a new character
; in that class, level_name is the 'character class name' which appears in the
; new soul dialog box.  
;
; LEVEL ZERO is where you define a characters magic-ration and right-hand weapon.
; Their 'magic ratio' determines what percentage of their attack points are used for magic versus sword attack;
;
; The right-hand weapon controls which class of EQUIP item they can hold in their
; right hand (each class is hard-wired to a single right-hand item, but the
; world designer can define what those actual items are).  
;
; The default (used in Evergreen) right-hand weapons are:
;	1	Sword                 Rifle
;	2	Staff                 Blunt
;  	3	Bow                   Sniper rifle
;	4	Musical Instrument    Loudspeaker
;   5   Fist                  Knife
;   6   Dart                  Bombs\Missiles
;   7   Book                  Manual
;   8   Spirit                Mortar
; if you set arg6 to a value between 1 and 8, then characters of that class will always
; be born with level 5 training in that hand, no matter what the start_hand_pp option
; is for that hand. 
; But if you set this field to 0 (Version A57 and later, only), then no hand will be
; pre-trained unless you specifically use the start_hand_pp option.
; Setting this field to zero in versions of WoS before A57 will lead to a bug which can
; render the character unusable.  If you publish your world later than July 2001, you can
; probably ignore this comment :)
;
; OPTIONAL CHARACTER LIMITATIONS
;
; If you (as the world designer) like, you can apply certain special commands to each
; character class, generally to limit its access to element and hand training,  You
; place these commands (one per line) just after the introductory line of the class
; Each command is optional and applies only to the class within which is is embedded.  See how
; all the commands appear between line 100 and line 101? 
; You must provide the information in a fixed syntax, as shown.
;
; Here are the available commands:
;    DESCRIPTION	 "This class sucks.  People who pick it are losers"		
;	A short text which appears on character creation dialog.  Put text in double quotes
;	MAGIC_RATIO	n		
;	Where n is a number from 0 to 100, specifies effectiveness with magical attacks
;   HAND_RATIO	n
;	Where n is a number from 1-100, specifies effectiveness of non-magical attacks.  
;   If you set to '0' it will use "100 - magic ratio" for "balanced" classes.
;   MAX_ABILITY str, wis, sta, agi, dex  
;	Provide all 5 ability values in order, giving the maximum level allowed for each.
;   Valid max levels are 0 to 255.
;   START_ELEMENT_PP  0,1,2,3,4,5,6,7
;	Must have 8 values, in order of +ELEMENTS table.  Sets initial elemental training.
;   Values range from 0 to 1000000 as described later (PP Levels).
;   MAX_ELEMENT_PP 0,1,2,3,4,5,6,7 
;	Must have 8 values, in order of +ELEMENTS table.  Sets max possible elemental training
;   Values range from 0 to 1000000 as described later (PP Levels).
;   START_HAND_PP 0,1,2,3,4,5,6,7  
;	Must have 8 values, in order of +HANDS table. Sets initial right-hand training
;   Values range from 0 to 1000000 as described later (PP Levels).
;   MAX_HAND_PP 0,1,2,3,4,5,6,7   
;	Must have 8 values, in order of +HANDS table. Sets maximum right-hand training.
;   Values range from 0 to 1000000 as described later (PP Levels).
;   AUTO_MAX minHP, maxHP, minMP, maxMP, startMPLevel 
;	Must have all five values.  Sets the max HP and MP levels for each level of the character.
;   That is to say, you are setting the maxHP and maxMP for level 1 and level 100, and the intermediate levels are
;   then computed automatically to span those maximums "appropriately" (semi-logarithmic algorithm)
;	The initial level 1 HP will be minHP, final level 100 HP will be maxHP
;	The fifth arg (startMPLevel) lets you postpone first MP (maxMP will stay 0 until that level).
;	NOTE: Actual maximums will be a little lower than you specify because of curve-shaping
;	If you use this command, the HP and MP entries in your level table for that class will be ignored.
;	START_LOCATION  <map>, <link>  
;	Lets you set where a newly created character will appear on their first
;	incarnation, based on class.  (so all elves can be born in Elf Town, for example).  Otherwise, they
;	will appear above link 0 on map 0.  It is your responsibility to make sure the map and link specified
;	actually exist and 'make sense' (it would be cruel to incarnate a newbie in a tough spot)
;   MAX_WALLET n 
; 	Further limits wallet size to n (0 to 1000000). (cannot make wallets BIGGER than
;	normal game restrictions.)  Normally characters start off with a wallet holding 20K GP at level 1, with
;   an additional 10K added for each level, until they reach one million at level 99.
;   NO_GIFTS
;	(No arguments). Indicates that class cannot receive money or items from other players.  (but from scripts is OK)
;   START_ITEMS 1, 2, 3, ...
;	Lets you give character up to 8 items right at start of game.  
;	Use valid Item numbers only
;	START_SPELLS 1, 2, 3, .. 
;	Lets you give characters up to 8 spells right at start of game
;	 Use valid Spell IDs only.
;   START_TOKENS 1, 2, 3...  
;	Lets you give character up to 8 tokens right at start of game.  
;	Use valid token numbers only
;	HIDDEN_CLASS  <startLevel>
;   (startLevel optional). Class doesn't show up on New Character dialog as an available choice.
;   You have to be morphed into such a class via "SET num.hostClass, <class#>" script
;   command. If the <startlevel> argument is included, then the character will be changed to that
;   level (so long as it is less than their current level).  Hence "HIDDEN_CLASS 20" would lower
;   a level 40 character to level 20 in the new class, but would NOT raise a level 5 character to
;   level 20 in the new class.
; Note that the 'preferred right hand training' of each class will always be level 5 at start, no 
; matter what START_HAND_PP says.  This is for compatibility with existing worlds.  Sorry.
;
; PP Levels
;
; The PP required for each level grows according to a nasty equation, but here are the PP
; limits as of the time of this writing.
;
; level 0		PP 0 - 554
; level 1		PP 555 - 1249
; level 2		PP 1250 - 2141
; level 3		PP 2142 - 3332
; level 4		PP 3333 - 4999
; level 5		PP 5000 - 7499
; level 6		PP 7500 - 11665
; level 7		PP 11666 - 19999
; level 8		PP 20000 - 44999
; level 9		PP 45000 - 1000000 (one million)(you approach level 9.9999999 asymptotically)
;
; Note that is the total PP which affects your skill and the level numbers are somewhat arbitrary
; so a training level 3 with 3333 PP will hit harder than a training level 3 with 2143 PP
; To protect yourself against floating point round off issues from one version of windows to
; another, I recommend not using these exact values, but include a safety buffer.  So, for example
; if you wanted a STARTING level of 4, use 3350 instead of 3333.  And if you want a MAXIMUM level
; of 4, use 4970, not 4999.  A 20 unit buffer should not be noticeable by the user and might make
; your world more reliable.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  CCLL	    ???????.	MaxHP	maxMP	Level_name		Magic Ratio		Right-Hand
;	0		1			2		3		4				5				6
	100,	0,			20,		0,		"Marine",   10,				1
	
    DESCRIPTION	"Marines are often the first to battle, and last to leave.  Few stand to fight against the Marines"
	AUTO_MAX	20, 3759,  10, 1186,  20
    START_HAND_PP 5000,2000,2000,2000,2000,2000,2000,2000
    MAX_HAND_PP 1000000,900000,900000,900000,900000,900000,900000,900000		

	101,	0,			1,		0,		"Maggot Puke"
	105,	0,			2,		0,		"Snowball"
	110,	0,			5,		0,		"Rookie"
	120,	0,			8,		10,		"Soldier"
	130,	0,			10,		0,		"Seargent"
	140,	0,			14,		10,		"Drill Seargent"
	150,	0,			22,		0,		"Squad Leader"
	160,	0,			28,		20,		"Lieutenant"
	170,	0,			36,		0,		"Spec Ops Recruit"
	180,	0,			50,		20,		"Spec Ops Soldier"
	190,	0,			65,		0,		"Spec Ops Leader"

;  CCLL	    ???			MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	200,	0,			15,		5,		"Navy",		90,				2
	
    DESCRIPTION	"These guys have been at sea for too long.  The built up frustration has had the added benefit of making them brutal with blunt objects"
	AUTO_MAX	15, 3321,  10, 2184,  0
    START_HAND_PP 2000,5000,2000,2000,2000,2000,2000,2000
    MAX_HAND_PP 900000,1000000,900000,900000,900000,900000,900000,900000	

	201,	0,			1,		2,		"Swabbie"
	205,	0,			2,		5,		"Landlubber",		
	210,	0,			4,		10,		"Rookie",			
	220,	0,			7,		15,		"Squinky",			
	230,	0,			8,		15,		"Seaman",				
	240,	0,			11,		20,		"Seaman Elite",			
	250,	0,			18,		25,		"Team Leader",			
	260,	0,			22,		30,		"Squad Leader",		
	270,	0,			28,		35,		"Platoon Leader",		
	280,	0,			40,		40,		"Seal Trainee"
	290,	0,			55,		55,		"Elite Seal",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	300,	0,			18,		2,		"Army",			40,				3

	DESCRIPTION	"Prefer to engage the enemy from afar via sniper rifles and artillary"
	AUTO_MAX	20, 3656,  10, 1435,  5	
    START_HAND_PP 2000,2000,5000,2000,2000,2000,2000,2000
    MAX_HAND_PP 900000,900000,1000000,900000,900000,900000,900000,900000

  	301,	0,			2,		2,		"Ball lint",			
	305,	0,			4,		3,		"Douche Bag",		
	310,	0,			6,		6,		"Scum",			
	320,	0,			9,		9,		"Rookie",			
	330,	0,			12,		11,		"Soldier",				
	340,	0,			15,		16,		"Squad Leader",			
	350,	0,			20,		20,		"Platoon Leader",			
	360,	0,			25,		22,		"Wannabe Ranger",		
	370,	0,			32,		27,		"Ranger Trainee",		
	380,	0,			48,		32,		"Ranger",
	390,	0,			65,		40,		"Ranger Leader",
	
;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	400,	0,			18,		2,		"Psy-Ops",		60,				4
	
    DESCRIPTION	"These special operations units use psychological warfare tactics to make their enemies tremble in fear."
	AUTO_MAX	20, 2954,  10, 1435,  5	
    START_HAND_PP 2000,2000,2000,5000,2000,2000,2000,2000
    MAX_HAND_PP 900000,900000,900000,1000000,900000,900000,900000,900000

	401,	0,			1,		2,		"Moron",			
	405,	0,			2,		5,		"Idiot",		
	410,	0,			5,		10,		"Learner",			
	420,	0,			8,		15,		"Recruit",			
	430,	0,			10,		15,		"Understudy",				
	440,	0,			12,		20,		"Instructor",			
	450,	0,			15,		25,		"Game Player",			
	460,	0,			20,		30,		"Confuser",		
	470,	0,			30,		35,		"Headtwister",		
	480,	0,			50,		40,		"Mindbender",
	490,	0,			65,		55,		"Mindslayer",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	500,	0,			20,		0,		"Black Ops",		5,				5

	DESCRIPTION	"One thought runs through their minds constantly..Take out the target!  Will get up close and personal with their enemies with a minimum of firepower."
	AUTO_MAX	20, 3549,  10, 1285,  18	
    START_HAND_PP 2000,2000,2000,2000,5000,2000,2000,2000
    MAX_HAND_PP 900000,900000,900000,900000,1000000,900000,900000,900000

	501,	0,			2,		1,		"Pooter",			
	505,	0,			3,		2,		"Fool",		
	510,	0,			7,		4,		"Apprentice",			
	520,	0,			10,		7,		"Rookie",			
	530,	0,			12,		9,		"Assassin",				
	540,	0,			14,		11,		"Feared",			
	550,	0,			22,		15,		"Hated",			
	560,	0,			28,		18,		"Loathed",		
	570,	0,			36,		21,		"Dispised",		
	580,	0,			50,		24,		"Unstoppable",
	590,	0,			65,		27,		"Heartless",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	600,	0,			18,		0,		"Air Force",		20,				6

	DESCRIPTION	"Death from above!! Air Force fighters call on their peers to rain down fear on their enemies."
	AUTO_MAX	20, 4217,  10, 1467,  10
    START_HAND_PP 2000,2000,2000,2000,2000,5000,2000,2000
    MAX_HAND_PP 900000,900000,900000,900000,900000,1000000,900000,900000	

	601,	0,			1,		1,		"Turkey",			
	605,	0,			2,		2,		"Duck",		
	610,	0,			5,		4,		"Goose",			
	620,	0,			8,		6,		"Dove",			
	630,	0,			10,		8,		"Robin",				
	640,	0,			12,		11,		"Finch",			
	650,	0,			15,		15,		"Buzzard",			
	660,	0,			20,		20,		"Owl",		
	670,	0,			30,		25,		"Hawk",		
	680,	0,			50,		30,		"Falcon",
	690,	0,			65,		35,		"Eagle",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	700,	0,			10,		8,		"Intelligence",		70,				7

	DESCRIPTION	"Education is key with these fighters. Their vast knowledge of the enemy gives them an advantage in the field."
	AUTO_MAX	20, 2823,  10, 1954,  0	
    START_HAND_PP 2000,2000,2000,2000,2000,2000,5000,2000
    MAX_HAND_PP 900000,900000,900000,900000,900000,900000,1000000,900000

	701,	0,			1,		2,		"Flunky"			
	705,	0,			2,		5,		"Diletante",		
	710,	0,			5,		8,		"Pupil",			
	720,	0,			8,		12,		"Student",			
	730,	0,			10,		14,		"Tutor",				
	740,	0,			12,		16,		"Instructor",			
	750,	0,			15,		19,		"Teacher",			
	760,	0,			20,		23,		"Professor",		
	770,	0,			30,		30,		"Scholar",		
	780,	0,			50,		35,		"Researcher"
	790,	0,			65,		55,		"Genius",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	800,	0,			8,		20,		"Artillary",		80,				8

	DESCRIPTION	"Blast your enemies from far away with this explosive class.  (skin appropriatly please)"
	AUTO_MAX	20, 3081,  10, 1954,  0	
    START_HAND_PP  2000,2000,2000,2000,2000,2000,2000,5000
    MAX_HAND_PP  900000,900000,900000,900000,900000,900000,900000,1000000

	801,	0,			1,		5,		"Dweeb",			
	805,	0,			2,		8,		"Putz",		
	810,	0,			4,		15,		"Dork",			
	820,	0,			6,		20,		"Rookie",			
	830,	0,			8,		25,		"Lobber",				
	840,	0,			10,		30,		"Tosser",			
	850,	0,			13,		35,		"Shooter",			
	860,	0,			16,		40,		"Bomber",		
	870,	0,			19,		45,		"Boomer",		
	880,	0,			24,		50,		"Sighter",
	890,	0,			65,		55,		"Blaster",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				6
	900,	0,			8,		20,		"Mercenary",			5,				1

	DESCRIPTION	"Depend on themselves for support.  They use only what they earn for themselves.  They are Hard-Core! " 
	AUTO_MAX	20, 2407,  10, 1954,  0	
	NO_GIFTS
	MAGIC_RATIO	45
	HAND_RATIO	45
	START_HAND_PP 5000,5000,5000,5000,5000,5000,5000,5000
	MAX_HAND_PP 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000
	
	901,	0,			1,		5,		"Fool"			
	905,	0,			2,		8,		"Prude",		
	910,	0,			4,		15,		"Oppressor",			
	920,	0,			6,		20,		"Steward",			
	930,	0,			8,		25,		"Tyrant",				
	940,	0,			10,		30,		"Stoic",			
	950,	0,			13,		35,		"Disciple",			
	960,	0,			16,		40,		"Purist",		
	970,	0,			19,		45,		"Ascetic",		
	980,	0,			24,		50,		"Spartan"
	990,	0,			65,		55,		"Hardcore",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				0
	1000,	0,			8,		20,		"Defector",		50,				1

	DESCRIPTION	"Iraqi defectors, these guys won't take your help when you offer.  They wish to take Saddam out on their own."
	AUTO_MAX	20, 2688,  10, 1954,  0	
	NO_GIFTS
	MAGIC_RATIO	50
	HAND_RATIO	50
    START_HAND_PP 5000,5000,5000,5000,5000,5000,5000,5000
    MAX_HAND_PP 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000
	
    1001,	0,			1,		5,		"Hobo"			
	1005,	0,			2,		8,		"Transient",		
	1010,	0,			4,		15,		"Stricken",			
	1020,	0,			6,		20,		"Pauper",			
	1030,	0,			8,		25,		"Leech",				
	1040,	0,			10,		30,		"Tramp",			
	1050,	0,			13,		35,		"Pleader",			
	1060,	0,			16,		40,		"Supplicant",		
	1070,	0,			19,		45,		"Indigent",		
	1080,	0,			24,		50,		"Vagabond"
	1090,	0,			65,		55,		"Mendicant",

;  CCLL	    Exp_Pts.	MaxHP	maxMP	Level_name			Magic-ratio		Right-Hand
;	0		1			2		3		4					5				0

; Be sure to leave this comment or a blank line at the very end
