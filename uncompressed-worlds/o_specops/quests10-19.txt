SCENE 10 tentcity

    ACTOR	1,	"Bomb Master",	"sceneactors", 5, 25, 90
	OFFER2  11, 99, 17 
	' Order you air suppor readied here.
END

;   Bullhorns
;
SCENE 11 tentcity

    ACTOR   1,  "Bullhorn Master", "sceneactors", 6, 25, 90
    OFFER2  11, 99, 15
    '  Fear is on your side soldier.
END

;   Volumes
;
SCENE 12 tentcity
 
    ACTOR   1,  "Librarian", "sceneactors", 6, 25, 90
    OFFER2  11, 99, 18
    '  Exploit the weaknesses of the human mind to obtain victory.
END

;   Mortars
;
SCENE 13 tentcity
     
    ACTOR   1,  "Mortar Master", "sceneactors", 1, 25, 90
    OFFER2  11, 99, 19
    '  Don't worry pal, it'll be over quick for them!

END

;    Headgear
;
SCENE 14 tentcity
 
    ACTOR   1,  "Helmet Master", "sceneactors", 5, 25, 90
    OFFER2  11,99,10
    ' Don't forget to cover your melon!

END

SCENE 15 tentcity
 
    ACTOR   1,  "Clothing Master", "sceneactors", 1, 25, 90
    OFFER2  11,99,11
    ' You can't go out naked! Get some suitable clothing.

END

SCENE 16 tentcity

    ACTOR   1,  "Boot Master", "sceneactors", 7, 25, 90
    OFFER2  11,99,20
    ' Protect your feet from the burning desert sands.

END

SCENE 17 tentcity
      
    ACTOR  1,   "Body Armor Master", "sceneactors", 2, 25, 90
    OFFER2 11,99,21
    ' Hope you never have to see if this stuff works.

END

SCENE 18 tentcity

    ACTOR  1,  "Device Master", "sceneactors", 3, 25, 90
   OFFER2 11,99,22
    ' I have the tools of modern warfare right here.

END

SCENE 19 tentcity

   ACTOR 1,  "Comm. Master", "sceneactors", 11, 25, 90
  OFFER2 11,99,23
   ' Stay informed on the battlefield, you'll live longer that way.

END
