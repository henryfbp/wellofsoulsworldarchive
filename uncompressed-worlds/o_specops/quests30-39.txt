SCENE 30  carlin
MUSIC nothing
WAIT 3.0
SOUND "Carlininnocent.wav"
WAIT 18.0
SOUND "Carlinmillions.wav"
WAIT 21.0
END

SCENE 31  tentcity
ACTOR 1, "Trainer", "sceneactors", 3, 25, 90
1: Would you like to train in a simulation to enhance your skills?
1: It's gonna cost ya 60,000 bucks to train.
IF G60000, @good
1: You don't seem to have the dough, come back later.
END
@good
1:What do you say?
ASK 15.0
IF YES, @saidyes
END
@saidyes
TAKE G60000
COUNTDOWN 300.0
IF XP, @timesup
FIGHT 3
IF WON, @winner
GOTO SCENE 31
END
@timesup
GOTO EXIT
END
@winner
COUNTDOWN 0
N: Your Prestige Points have improved!
WAIT 2.0
GOTO SCENE 31
END

; This scene allows people to replay adventures
;
SCENE 32 tentcity,
    THEME	
    ACTOR	1,	"Therapist", "sceneactors", 9, 25, 80

	1: Welcome %2.
	1: I can help take away the painful memories of war. 
	1: Be warned, once treated, you will have no memories of this war!
    1: Would you like me to make you forget your experiences?
    ASK 15.0
    IF -YES, @nevermind
    1: Are you sure you want to forget everything?
    ASK 15.0
    IF -YES, @nevermind
    1: Fine...here we go.
    THEME spit
    WAIT 4.0
    TAKE T2
    TAKE T3
    TAKE T4
    TAKE T5
    TAKE T6
    TAKE T7
    TAKE T8
    TAKE T9
    TAKE T10
    TAKE T11
    TAKE T12
    TAKE T13
    TAKE T14
    TAKE T18
    1: Welcome to the gulf soldier...
    END
    @nevermind
    1: Come again if your memories keep you awake at night.
    END

SCENE 34 giza5
    IF T14, @go
    END
    @go
    THEME largeblast
    N: You have used explosives to open the way...
    WAIT 5.0
    GOTO LINK 9,0
    END

SCENE 35 bgLibrary
    ACTOR 1, "Osama Bin Laden", osama, 1, 30, 90
    1: You have come...
    1: You will die...
    1: I will live forever...
    1: You cannot kill me...
    1: If I die there will be 100 more to take my place...
    1: I have filled the whole in their hearts...
    1: They need me...
    1: They will never allow you to kill me...
    1: Give up now...
    1: You can never truly win...
    1: I am eternal in the heart of my people...
    1: Do you understand fool...
    1: Do you speak English...
    1: YOU CAN NOT WIN
    1: I CANNOT DIE
    1: EVER
    1: I AM ETERNAL
    1: YOU WILL NOW SEE MY POWER
    1: WITNESS THE POWER OF MY FAITH!!
    MOVE 1, -50, 50
    WAIT 5.0
    MUSIC Baghdad.mid
    FIGHT 243
    IF WON, @winner
    MOVE 1, 30, 50
    1: I am eternal.........
    END
    @winner
    GIVE T18
    GIVE T19
    MOVE 1, 30, 50
    1: I cannot... I ...  can never..
    1: NOOOOOOO!
    THEME wusswail
    THEME pain1 
    THEME pain2
    THEME pain3
    THEME pain4
    THEME pain5
    MOVE 1, -100, 50
    WAIT 1.0
    N: %1 has terminated Osama Bin Laden!!
    N: %1 is among your most elite!!
    END
  
SCENE 36 bridge
ACTOR 1, "Admiral Combs", "sceneactors", 11, 25, 90
IF T22, @again
IF T21, @beaten
1: Battle Stations!!! Quickly now, Incoming 3 o' clock high!
1: Take them out NOW!
GOTO SCENE 37
END
@beaten
GIVE T22
1: Great work soldier.  This ships crew owes you their lives.
1: We are thankful for your efforts and would like to reward you.
1: Allow me to enhance your living conditions...
1: You will now be able to restore your health in your camp.
@again
1: Thank you again.
END

SCENE 37 kittyhawk
FIGHT 16,16,16,16,16
IF WON,@winner
WAIT 2.0
GOTO EXIT
@winner
GIVE T21 
GOTO SCENE 36
END

SCENE 38 mypic, cut
WAIT 2.0
N: Welcome to my world.  You are in for the fight of your life soldier.
WAIT 1.0
N: I suggest you listen to Greaves as if your life depends on his words.
WAIT 1.0
N: Accomplish your missions, defeat your enemies...
WAIT 1.0
N: And rid the world of Saddam, once and for all!
WAIT 1.0
N: Good luck
END

SCENE 39 pumpHouse
IF T23, @finished
ACTOR 1, "Spy", joshTownsfolk, 9,25,90
1: Help..
1: I'm with you!
MOVE 1, -50,-50
WAIT 3.0
FIGHT 114,114,114
IF WON, @wonagain
END
@wonagain
HOST_GIVE I191
GIVE T23
MOVE 1, 25, 90
WAIT 4.0
1: Thank you so much.
1: I was sent to gather intelligence on this refinery when rumors of sabotage surfaced.
1: My mission was to confirm that the enemy was using sabotage tactics.
1: Obviously, I was captured.
1: I did manage to get word back to base, but I allowed myself to be caught.
1: I must return to be debriefed now, Thank you again.
1: Please accept my body armor as a reward for your efforts.
@finished
END