SCENE 50 barbedwireatnight
IF T38,@t39
H: Something doesn't feel right.
H: I'll wait here for a bit, set up camp.
WAIT 7.0
H: There, what's that?
WAIT 3.0
FIGHT 242
IF WON, @beat1
END
@beat1
FIGHT 242
IF WON, @beat2
END
@beat2
FIGHT 242
IF WON, @beat3
END
@beat3
H: Whew! Wait what's this?
HOST_GIVE I197
H: It's the %I197!
HOST_GIVE T39
@t39
END

SCENE 51 instructions
END