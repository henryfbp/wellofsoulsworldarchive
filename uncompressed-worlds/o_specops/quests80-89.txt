SCENE 80 ranking CUT
ACTOR 1, ,blankmonsterskin, 1, 52, 38
1: %1
WAIT 3.0

SET addedfinal, 0

GOTO @checklevel
@gotlevel
ACTOR 2, ,stars, #<levstars>, 67, 49
2: #<num.hostLevel>
WAIT 2.0

GOTO @checkenemyks
@gotenemyks
ACTOR 3, ,stars, #<enemyks>, 67, 55
3: #<num.hostKills>
WAIT 2.0

GOTO @checkpks
@gotpks
ACTOR 4, ,stars, #<yourpks>, 67, 61
4: #<num.hostPKs>
WAIT 2.0

GOTO @checkGP
@gotGP
ACTOR 5, ,stars, #<allGP>, 67, 67
5: #<addedGP>
WAIT 2.0

GOTO @checkage
@gotage
ACTOR 6, ,stars, #<hostage>, 67, 73
6: #<num.hostAge>
WAIT 2.0

GOTO @checkkilled
@gotkilled
ACTOR 7, ,stars, #<timeskilled>, 67, 79
7: #<num.hostDeaths>
WAIT 2.0

GOTO @checksaddam
@gotsaddam
ACTOR 8, ,stars, #<saddamks>, 67, 85
8: #<KM.255>
WAIT 2.0

GOTO @checkosama
@gotosama
ACTOR 9, ,stars, #<osamaks>, 67, 91
9: #<KM.243>
WAIT 2.0
9:

GOTO @checkpet
@gotpet
ACTOR 10, ,stars, #<pet>, 67, 97
WAIT 2.0

GOTO @checkbrave
@gotbrave
ACTOR 11, ,stars, #<brave>, 67, 103
WAIT 2.0

GOTO @checkmissions
@gotmissions
ACTOR 12, ,stars, #<missions>, 67, 109
WAIT 2.0

GOTO @checkside
@gotside
ACTOR 13, ,stars, #<mytokens>, 67, 115
WAIT 2.0

GOTO @checkhunt
@gothunt
ACTOR 14, ,stars, #<hunt>, 67, 121
WAIT 2.0

GOTO @checkfinalscore
@gotfinalscore
ACTOR 15, ,stars, #<finalscore>, 67, 132
15: #<addedfinal>
WAIT 2.0

COMPARE #<addedfinal>,65
IF= , @bonus
END
@bonus
IF T29, @donewhop
GIVE T29
HOST_GIVE I194
N: You've unlocked the %I194
N: Great Work Soldier!!!
@donewhop
END

;---------------------------

@checklevel
SET levelno, "#<num.hostLevel>"
COMPARE #<levelno>,20
IF> , @over20
SET levstars, 6
ADD addedfinal, 0
GOTO @gotlevel
END
@over20
COMPARE #<levelno>,40
IF> , @over40
SET levstars, 1
ADD addedfinal, 1
GOTO @gotlevel
END
@over40
COMPARE #<levelno>,60
IF>, @over60
SET levstars, 2
ADD addedfinal, 2
GOTO @gotlevel
END
@over60
COMPARE #<levelno>,80
IF>, @over80
SET levstars, 3
ADD addedfinal, 3
GOTO @gotlevel
END
@over80
COMPARE #<levelno>,99
IF>, @over99
SET levstars, 4
ADD addedfinal, 4
GOTO @gotlevel
END
@over99
SET levstars, 5
ADD addedfinal, 5
GOTO @gotlevel
END
;---------------------------

@checkenemyks
SET kilz, "#<num.hostKills>"
COMPARE #<kilz>,500
IF> , @over500
SET enemyks, 6
ADD addedfinal, 0
GOTO @gotenemyks
END
@over500
COMPARE #<kilz>,1000
IF> , @over1000
SET enemyks, 1
ADD addedfinal, 1
GOTO @gotenemyks
END
@over1000
COMPARE #<kilz>,1500
IF>, @over1500
SET enemyks, 2
ADD addedfinal, 2
GOTO @gotenemyks
END
@over1500
COMPARE #<kilz>,2000
IF>, @over2000
SET enemyks, 3
ADD addedfinal, 3
GOTO @gotenemyks
END
@over2000
COMPARE #<kilz>,2500
IF>, @over2500
SET enemyks, 4
ADD addedfinal, 4
GOTO @gotenemyks
END
@over2500
SET enemyks, 5
ADD addedfinal, 5
GOTO @gotenemyks
END

;---------------------------

@checkpks
SET mypks, "#<num.hostPKs>"
COMPARE #<mypks>,4
IF> , @over10pks
SET yourpks, 6
ADD addedfinal, 0
GOTO @gotpks
END
@over10pks
COMPARE #<mypks>,9
IF> , @over20pks
SET yourpks, 1
ADD addedfinal, 1
GOTO @gotpks
END
@over20pks
COMPARE #<mypks>,14
IF>, @over30pks
SET yourpks, 2
ADD addedfinal, 2
GOTO @gotpks
END
@over30pks
COMPARE #<mypks>,19
IF>, @over40pks
SET yourpks, 3
ADD addedfinal, 3
GOTO @gotpks
END
@over40pks
COMPARE #<mypks>,24
IF>, @over50pks
SET yourpks, 4
ADD addedfinal, 4
GOTO @gotpks
END
@over50pks
SET yourpks, 5
ADD addedfinal, 5
GOTO @gotpks
END

;---------------------------

@checkGP
SET addedGP, #<money>
COMPARE #<addedGP>,1000000
IF> , @over1mil
SET allGP, 6
ADD addedfinal, 0
GOTO @gotGP
END
@over1mil
COMPARE #<addedGP>,2500000
IF> , @over2mil
SET allGP, 1
ADD addedfinal, 1
GOTO @gotGP
END
@over2mil
COMPARE #<addedGP>,5000000
IF>, @over5mil
SET allGP, 2
ADD addedfinal, 2
GOTO @gotGP
END
@over5mil
COMPARE #<addedGP>,7500000
IF>, @over7mil
SET allGP 3
ADD addedfinal, 3
GOTO @gotGP
END
@over7mil
COMPARE #<addedGP>,9999998
IF>, @over10mil
SET allGP, 4
ADD addedfinal, 4
GOTO @gotGP
END
@over10mil
SET allGP, 5
ADD addedfinal, 5
GOTO @gotGP
END

;---------------------------

@checkage
SET myage, "#<num.hostAge>"
COMPARE #<myage>, 300
IF> , @over5hours
SET hostsage, 6
ADD addedfinal, 0
GOTO @gotage
END
@over5hours
COMPARE #<myage>,600
IF> , @over10hours
SET hostage, 1
ADD addedfinal, 1
GOTO @gotage
END
@over10hours
COMPARE #<myage>,900
IF>, @over15hours
SET hostage, 2
ADD addedfinal, 2
GOTO @gotage
END
@over15hours
COMPARE #<myage>,1200
IF>, @over20hours
SET hostage, 3
ADD addedfinal, 3
GOTO @gotage
END
@over20hours
COMPARE #<myage>,1500
IF>, @over25hours
SET hostage, 4
ADD addedfinal, 4
GOTO @gotage
END
@over25hours
SET hostage, 5
ADD addedfinal, 5
GOTO @gotage
END

;---------------------------

@checkkilled
SET kild, "#<num.hostDeaths>"
COMPARE #<kild>,500
IF< , @over500d
SET timeskilled, 6
ADD addedfinal, 0
GOTO @gotkilled
END
@over500d
COMPARE #<kild>,450
IF< , @over450d
SET timeskilled, 1
ADD addedfinal, 1
GOTO @gotkilled
END
@over450d
COMPARE #<kild>,400
IF< , @over400d
SET timeskilled, 2
ADD addedfinal, 2
GOTO @gotkilled
END
@over400d
COMPARE #<kild>,300
IF< , @over300d
SET timeskilled, 3
ADD addedfinal, 3
GOTO @gotkilled
END
@over300d
COMPARE #<kild>,200
IF< , @over200d
SET timeskilled, 4
ADD addedfinal, 4
GOTO @gotkilled
END
@over200d
SET timeskilled, 5
ADD addedfinal, 5
GOTO @gotkilled
END

;---------------------------

@checksaddam
SET soda, "#<KM.255>"
COMPARE #<soda>,0
IF> , @none
SET saddamks, 6
ADD addedfinal, 0
GOTO @gotsaddam
END
@none
COMPARE #<soda>,1
IF> , @one
SET saddamks, 1
ADD addedfinal, 1
GOTO @gotsaddam
END
@one
COMPARE #<soda>,2
IF>, @two
SET saddamks, 2
ADD addedfinal, 2
GOTO @gotsaddam
END
@two
COMPARE #<soda>,5
IF>, @four
SET saddamks, 3
ADD addedfinal, 3
GOTO @gotsaddam
END
@four
COMPARE #<soda>,9
IF>, @five
SET saddamks, 4
ADD addedfinal, 4
GOTO @gotsaddam
END
@five
SET saddamks, 5
ADD addedfinal, 5
GOTO @gotsaddam
END

;---------------------------

@checkosama
SET samaz, "#<KM.243>"
COMPARE #<samaz>,0
IF> , @move1
SET osamaks, 6
ADD addedfinal, 0
GOTO @gotosama
END
@move1
COMPARE #<samaz>,1
IF> , @move2
SET osamaks, 1
ADD addedfinal, 1
GOTO @gotosama
END
@move2
COMPARE #<samaz>,2
IF>, @move3
SET osamaks, 2
ADD addedfinal, 2
GOTO @gotosama
END
@move3
COMPARE #<samaz>,5
IF>, @move4
SET osamaks, 3
ADD addedfinal, 3
GOTO @gotosama
END
@move4
COMPARE #<samaz>,9
IF>, @move5
SET osamaks, 4
ADD addedfinal, 4
GOTO @gotosama
END
@move5
SET osamaks, 5
ADD addedfinal, 5
GOTO @gotosama
END

;---------------------------

@checkpet
SET petz, "#<num.hostPetLevel>"
COMPARE #<petz>,20
IF> , @ov20
SET pet, 6
ADD addedfinal, 0
GOTO @gotpet
END
@ov20
COMPARE #<petz>,40
IF> , @ov40
SET pet, 1
ADD addedfinal, 1
GOTO @gotpet
END
@ov40
COMPARE #<petz>,60
IF>, @ov60
SET pet, 2
ADD addedfinal, 2
GOTO @gotpet
END
@ov60
COMPARE #<petz>,80
IF>, @ov80
SET pet, 3
ADD addedfinal, 3
GOTO @gotpet
END
@ov80
COMPARE #<petz>,98
IF>, @ov99
SET pet, 4
ADD addedfinal, 4
GOTO @gotpet
END
@ov99
SET pet, 5
ADD addedfinal, 5
GOTO @gotpet
END

;---------------------------

@checkbrave
SET bra, "#<num.hostBravery>"
COMPARE #<bra>,0
IF> , @o0
SET brave, 6
ADD addedfinal, 0
GOTO @gotbrave
END
@o0
COMPARE #<bra>,100
IF> , @o100
SET brave, 1
ADD addedfinal, 1
GOTO @gotbrave
END
@o100
COMPARE #<bra>,250
IF>, @o250
SET brave, 2
ADD addedfinal, 2
GOTO @gotbrave
END
@o250
COMPARE #<bra>,500
IF>, @o500
SET brave, 3
ADD addedfinal, 3
GOTO @gotbrave
END
@o500
COMPARE #<bra>,1000
IF>, @o1000
SET brave, 4
ADD addedfinal, 4
GOTO @gotbrave
END
@o1000
SET brave, 5
ADD addedfinal, 5
GOTO @gotbrave
END

;---------------------------

@checkmissions

SET totalmissions, 0
IF -T3, @noT3
ADD totalmissions, 1
@noT3
IF -T5, @noT5
ADD totalmissions, 1
@noT5
IF -T7, @noT7
ADD totalmissions, 1
@noT7
IF -T9, @noT9
ADD totalmissions, 1
@noT9
IF -T11, @noT11
ADD totalmissions, 1
@noT11
IF -T13, @noT13
ADD totalmissions, 1
@noT13

SET missionz, "#<totalmissions>"
COMPARE #<missionz>,1
IF> , @ove1
SET missions, 6
ADD addedfinal, 0
GOTO @gotmissions
END
@ove1
COMPARE #<missionz>,2
IF> , @ove2
SET missions, 1
ADD addedfinal, 1
GOTO @gotmissions
END
@ove2
COMPARE #<missionz>,3
IF>, @ove3
SET missions, 2
ADD addedfinal, 2
GOTO @gotmissions
END
@ove3
COMPARE #<missionz>,4
IF>, @ove4
SET missions, 3
ADD addedfinal, 3
GOTO @gotmissions
END
@ove4
COMPARE #<missionz>,5
IF>, @ove5
SET missions, 4
ADD addedfinal, 4
GOTO @gotmissions
END
@ove5
SET missions, 5
ADD addedfinal, 5
GOTO @gotmissions
END

;---------------------------

@checkside

SET sidemissions, 0
IF -T15, @noT15
ADD sidemissions, 1
@noT15
IF -T16, @noT16
ADD sidemissions, 1
@noT16
IF -T17, @noT17
ADD sidemissions, 1
@noT17
IF -T18, @noT18
ADD sidemissions, 1
@noT18
IF -T19, @noT19
ADD sidemissions, 1
@noT19
IF -T20, @noT20
ADD sidemissions, 1
@noT20
IF -T21, @noT21
ADD sidemissions, 1
@noT21
IF -T22, @noT22
ADD sidemissions, 1
@noT22
IF -T23, @noT23
ADD sidemissions, 1
@noT23
IF -T25, @noT25
ADD sidemissions, 1
@noT25
IF -T26, @noT26
ADD sidemissions, 1
@noT26
IF -T27, @noT27
ADD sidemissions, 1
@noT27
IF -T28, @noT28
ADD sidemissions, 1
@noT28

SET tokens, 0
SET tokens, "#<sidemissions>"
COMPARE #<tokens>,4
IF> , @overtwo
SET mytokens, 6
ADD addedfinal, 0
GOTO @gotside
END
@overtwo
COMPARE #<tokens>,6
IF> , @overfour
SET mytokens, 1
ADD addedfinal, 1
GOTO @gotside
END
@overfour
COMPARE #<tokens>,8
IF>, @oversix
SET mytokens, 2
ADD addedfinal, 2
GOTO @gotside
END
@oversix
COMPARE #<tokens>,10
IF>, @overeight
SET mytokens, 3
ADD addedfinal, 3
GOTO @gotside
END
@overeight
COMPARE #<tokens>,12
IF>, @overten
SET mytokens, 4
ADD addedfinal, 4
GOTO @gotside
END
@overten
SET mytokens, 5
ADD addedfinal, 5
GOTO @gotside
END

;---------------------------

@checkhunt
SET huntz, "#<num.hostHuntSkill>"
COMPARE #<huntz>,3
IF> , @hunt3
SET hunt, 6
ADD addedfinal, 0
GOTO @gothunt
END
@hunt3
COMPARE #<huntz>,4
IF> , @hunt4
SET hunt, 1
ADD addedfinal, 1
GOTO @gothunt
END
@hunt4
COMPARE #<huntz>,5
IF>, @hunt5
SET hunt, 2
ADD addedfinal, 2
GOTO @gothunt
END
@hunt5
COMPARE #<huntz>,6
IF>, @hunt6
SET hunt, 3
ADD addedfinal, 3
GOTO @gothunt
END
@hunt6
COMPARE #<huntz>,7
IF>, @hunt7
SET hunt, 4
ADD addedfinal, 4
GOTO @gothunt
END
@hunt7
SET hunt, 5
ADD addedfinal, 5
GOTO @gothunt
END

;---------------------------

@checkfinalscore
SET strz, "#<addedfinal>"
COMPARE #<strz>,13
IF> , @skippit1
SET finalscore, 6
GOTO @gotfinalscore
END
@skippit1
COMPARE #<strz>,25
IF> , @skippit2
SET finalscore, 1
GOTO @gotfinalscore
END
@skippit2
COMPARE #<strz>,38
IF>, @skippit3
SET finalscore, 2
GOTO @gotfinalscore
END
@skippit3
COMPARE #<strz>,51
IF>, @skippit4
SET finalscore, 3
GOTO @gotfinalscore
END
@skippit4
COMPARE #<strz>,64
IF>, @skippit5
SET finalscore, 4
GOTO @gotfinalscore
END
@skippit5
SET finalscore, 5
GOTO @gotfinalscore
END


;------------------------------

SCENE 81 desandground CUT
ACTOR 1, ,blankmonsterskin, 1, 30, 90

COMPARE #<KM.1>,9
IF> , @mon1done
1: Need more %M1 dog tags...
END
@mon1done

COMPARE #<KM.4>,9
IF> , @mon2done
1: Need more %M4 dog tags...
END
@mon2done

COMPARE #<KM.7>,9
IF> , @mon3done
1: Need more %M7 dog tags...
END
@mon3done

COMPARE #<KM.9>,9
IF> , @mon4done
1: Need more %M9 dog tags...
END
@mon4done

COMPARE #<KM.14>,9
IF> , @mon5done
1: Need more %M14 dog tags...
END
@mon5done

COMPARE #<KM.15>,9
IF> , @mon6done
1: Need more %M15 dog tags...
END
@mon6done

COMPARE #<KM.16>,9
IF> , @mon7done
1: Need more %M16 dog tags...
END
@mon7done

COMPARE #<KM.17>,9
IF> , @mon8done
1: Need more %M17 dog tags...
END
@mon8done

COMPARE #<KM.18>,9
IF> , @mon9done
1: Need more %M18 dog tags...
END
@mon9done

COMPARE #<KM.52>,9
IF> , @mon10done
1: Need more %M52 dog tags...
END
@mon10done

COMPARE #<KM.54>,9
IF> , @mon11done
1: Need more %M54 dog tags...
END
@mon11done

COMPARE #<KM.57>,9
IF> , @mon12done
1: Need more %M57 dog tags...
END
@mon12done

COMPARE #<KM.59>,9
IF> , @mon13done
1: Need more %M59 dog tags...
END
@mon13done

COMPARE #<KM.60>,9
IF> , @mon14done
1: Need more %M60 dog tags...
END
@mon14done

COMPARE #<KM.65>,9
IF> , @mon15done
1: Need more %M65 dog tags...
END
@mon15done

COMPARE #<KM.66>,9
IF> , @mon16done
1: Need more %M66 dog tags...
END
@mon16done

COMPARE #<KM.67>,9
IF> , @mon17done
1: Need more %M67 dog tags...
END
@mon17done

COMPARE #<KM.68>,9
IF> , @mon18done
1: Need more %M68 dog tags...
END
@mon18done

COMPARE #<KM.69>,9
IF> , @mon19done
1: Need more %M69 dog tags...
END
@mon19done

COMPARE #<KM.70>,9
IF> , @mon20done
1: Need more %M70 dog tags...
END
@mon20done

COMPARE #<KM.100>,9
IF> , @mon21done
1: Need more %M100 dog tags...
END
@mon21done

COMPARE #<KM.103>,9
IF> , @mon22done
1: Need more %M103 dog tags...
END
@mon22done

COMPARE #<KM.105>,9
IF> , @mon23done
1: Need more %M105 dog tags...
END
@mon23done

COMPARE #<KM.107>,9
IF> , @mon24done
1: Need more %M107 dog tags...
END
@mon24done

COMPARE #<KM.109>,9
IF> , @mon25done
1: Need more %M109 dog tags...
END
@mon25done

COMPARE #<KM.111>,9
IF> , @mon26done
1: Need more %M111 dog tags...
END
@mon26done

COMPARE #<KM.113>,9
IF> , @mon27done
1: Need more %M113 dog tags...
END
@mon27done

COMPARE #<KM.115>,9
IF> , @mon28done
1: Need more %M115 dog tags...
END
@mon28done

COMPARE #<KM.117>,9
IF> , @mon29done
1: Need more %M117 dog tags...
END
@mon29done

COMPARE #<KM.119>,9
IF> , @mon30done
1: Need more %M119 dog tags...
END
@mon30done

COMPARE #<KM.121>,9
IF> , @mon31done
1: Need more %M121 dog tags...
END
@mon31done

COMPARE #<KM.123>,9
IF> , @mon32done
1: Need more %M123 dog tags...
END
@mon32done

COMPARE #<KM.125>,9
IF> , @mon33done
1: Need more %M125 dog tags...
END
@mon33done

COMPARE #<KM.127>,9
IF> , @mon34done
1: Need more %M127 dog tags...
END
@mon34done

COMPARE #<KM.129>,9
IF> , @mon35done
1: Need more %M129 dog tags...
END
@mon35done

COMPARE #<KM.168>,9
IF> , @mon36done
1: Need more %M168 dog tags...
END
@mon36done

COMPARE #<KM.169>,9
IF> , @mon37done
1: Need more %M169 dog tags...
END
@mon37done

COMPARE #<KM.170>,9
IF> , @mon38done
1: Need more %M170 dog tags...
END
@mon38done

COMPARE #<KM.171>,9
IF> , @mon39done
1: Need more %M171 dog tags...
END
@mon39done

COMPARE #<KM.172>,9
IF> , @mon40done
1: Need more %M172 dog tags...
END
@mon40done

COMPARE #<KM.173>,9
IF> , @mon41done
1: Need more %M173 dog tags...
END
@mon41done

COMPARE #<KM.174>,9
IF> , @mon42done
1: Need more %M174 dog tags...
END
@mon42done

COMPARE #<KM.175>,9
IF> , @mon43done
1: Need more %M175 dog tags...
END
@mon43done

COMPARE #<KM.176>,9
IF> , @mon44done
1: Need more %M176 dog tags...
END
@mon44done

COMPARE #<KM.177>,9
IF> , @mon45done
1: Need more %M177 dog tags...
END
@mon45done

COMPARE #<KM.178>,9
IF> , @mon46done
1: Need more %M178 dog tags...
END
@mon46done

COMPARE #<KM.206>,9
IF> , @mon47done
1: Need more %M206 dog tags...
END
@mon47done

COMPARE #<KM.207>,9
IF> , @mon48done
1: Need more %M207 dog tags...
END
@mon48done

COMPARE #<KM.208>,9
IF> , @mon49done
1: Need more %M208 dog tags...
END
@mon49done

COMPARE #<KM.209>,9
IF> , @mon50done
1: Need more %M209 dog tags...
END
@mon50done

COMPARE #<KM.210>,9
IF> , @mon51done
1: Need more %M210 dog tags...
END
@mon51done

COMPARE #<KM.212>,9
IF> , @mon52done
1: Need more %M212 dog tags...
END
@mon52done

COMPARE #<KM.213>,9
IF> , @mon53done
1: Need more %M213 dog tags...
END
@mon53done

COMPARE #<KM.240>,9
IF> , @mon54done
1: Need more %M240 dog tags...
END
@mon54done

COMPARE #<KM.241>,9
IF> , @mon55done
1: Need more %M241 dog tags...
END
@mon55done

COMPARE #<KM.242>,9
IF> , @mon56done
1: Need more %M242 dog tags...
END
@mon56done
1:  
IF T30, @finishedit
GIVE T30
GIVE I195
N: You have obtained all dog tags...
N: Congratulations, here is your %I195
@finishedit
END

SCENE 82 tentcity
TAKE T38 
GOTO LINK 8, 3
END

SCENE 83 urbantrainingbuilding
ACTOR 1, "Drill Instructor", "oldintellisoldier", 1, 20, 70
SET train, "1"
FLAGS	1024	
1: Wanna train your support units?
1: This is the place to do it!
1: Well...(Yes or No)
ASK 9999999
IF YES, @drillem
END


@keepgoing
1: Wanna pump it up a little? (Yes or No)
ASK 3
IF YES, @harder
@drillem
@upped
FIGHT "#<train>"
IF WON, @keepgoing
1: Nice work!
END


@harder

SET traino, "#<train>"

COMPARE #<traino>,"1"
IF=,@qwas1
;COMPARE #<traino>,"2"
;IF-,@quas2
COMPARE #<traino>,"4"
IF=,@qwas4
COMPARE #<traino>,"7"
IF=,@qwas7
COMPARE #<traino>,"9"
IF=,@qwas9
COMPARE #<traino>,"14"
IF=,@qwas14
COMPARE #<traino>,"15"
IF=,@qwas15
COMPARE #<traino>,"16"
IF=,@qwas16
COMPARE #<traino>,"17"
IF=,@qwas17
COMPARE #<traino>,"18"
IF=,@qwas18
COMPARE #<traino>,"52"
IF=,@qwas52
COMPARE #<traino>,"54"
IF=,@qwas54
COMPARE #<traino>,"57"
IF=,@qwas57
COMPARE #<traino>,"59"
IF=,@qwas59
COMPARE #<traino>,"60"
IF=,@qwas60
COMPARE #<traino>,"65"
IF=,@qwas65
COMPARE #<traino>,"66"
IF=,@qwas66
COMPARE #<traino>,"67"
IF=,@qwas67
COMPARE #<traino>,"68"
IF=,@qwas68
COMPARE #<traino>,"69"
IF=,@qwas69
COMPARE #<traino>,"70"
IF=,@qwas70
COMPARE #<traino>,"100"
IF=,@qwas100
COMPARE #<traino>,"103"
IF=,@qwas103
COMPARE #<traino>,"105"
IF=,@qwas105
COMPARE #<traino>,"107"
IF=,@qwas107
COMPARE #<traino>,"109"
IF=,@qwas109
COMPARE #<traino>,"111"
IF=,@qwas111
COMPARE #<traino>,"113"
IF=,@qwas113
COMPARE #<traino>,"115"
IF=,@qwas115
COMPARE #<traino>,"117"
IF=,@qwas117
COMPARE #<traino>,"119"
IF=,@qwas119
COMPARE #<traino>,"121"
IF=,@qwas121
COMPARE #<traino>,"123"
IF=,@qwas123
COMPARE #<traino>,"125"
IF=,@qwas125
COMPARE #<traino>,"127"
IF=,@qwas127
COMPARE #<traino>,"129"
IF=,@qwas129
COMPARE #<traino>,"168"
IF=,@qwas168
COMPARE #<traino>,"169"
IF=,@qwas169
COMPARE #<traino>,"170"
IF=,@qwas170
COMPARE #<traino>,"171"
IF=,@qwas171
COMPARE #<traino>,"172"
IF=,@qwas172
COMPARE #<traino>,"173"
IF=,@qwas173
COMPARE #<traino>,"174"
IF=,@qwas174
COMPARE #<traino>,"175"
IF=,@qwas175
COMPARE #<traino>,"176"
IF=,@qwas176
COMPARE #<traino>,"177"
IF=,@qwas177
COMPARE #<traino>,"178"
IF=,@qwas178
COMPARE #<traino>,"206"
IF=,@qwas206
COMPARE #<traino>,"207"
IF=,@qwas207
COMPARE #<traino>,"208"
IF=,@qwas208
COMPARE #<traino>,"209"
IF=,@qwas209
COMPARE #<traino>,"210"
IF=,@qwas210
COMPARE #<traino>,"212"
IF=,@qwas212
COMPARE #<traino>,"213"
IF=,@qwas213
COMPARE #<traino>,"240"
IF=,@qwas240
COMPARE #<traino>,"241"
IF=,@qwas241
COMPARE #<traino>,"242"
IF=,@qwas242
END

@qwas1
SET train,"4"
GOTO @upped
END
,@qwas2
,SET train,"4"
,GOTO @upped
,END
@qwas4
SET train,"7"
GOTO @upped
END 
@qwas7
SET train,"9"
GOTO @upped
END
@qwas9
SET train,"14"
GOTO @upped
END
@qwas14
SET train,"15"
GOTO @upped
END
@qwas15
SET train,"16"
GOTO @upped
END
@qwas16
SET train,"17"
GOTO @upped
END
@qwas17
SET train,"18"
GOTO @upped
END
@qwas18
SET train,"52"
GOTO @upped
END
@qwas52
SET train,"54"
GOTO @upped
END
@qwas54
SET train,"57"
GOTO @upped
END
@qwas57
SET train,"59"
GOTO @upped
END
@qwas59
SET train,"60"
GOTO @upped
END
@qwas60
SET train,"65"
GOTO @upped
END
@qwas65
SET train,"66"
GOTO @upped
END
@qwas66
SET train,"67"
GOTO @upped
END
@qwas67
SET train,"68"
GOTO @upped
END
@qwas68
SET train,"69"
GOTO @upped
END
@qwas69
SET train,"70"
GOTO @upped
END
@qwas70
SET train,"100"
GOTO @upped
END
@qwas100
SET train,"103"
GOTO @upped
END
@qwas103
SET train,"105"
GOTO @upped
END
@qwas105
SET train,"107"
GOTO @upped
END
@qwas107
SET train,"109"
GOTO @upped
END
@qwas109
SET train,"111"
GOTO @upped
END
@qwas111
SET train,"113"
GOTO @upped
END
@qwas113
SET train,"115"
GOTO @upped
END
@qwas115
SET train,"117"
GOTO @upped
END
@qwas117
SET train,"119"
GOTO @upped
END
@qwas119
SET train,"121"
GOTO @upped
END
@qwas121
SET train,"123"
GOTO @upped
END
@qwas123
SET train,"125"
GOTO @upped
END
@qwas125
SET train,"127"
GOTO @upped
END
@qwas127
SET train,"129"
GOTO @upped
END
@qwas129
SET train,"168"
GOTO @upped
END
@qwas168
SET train,"169"
GOTO @upped
END
@qwas169
SET train,"170"
GOTO @upped
END
@qwas170
SET train,"171"
GOTO @upped
END
@qwas171
SET train,"172"
GOTO @upped
END
@qwas172
SET train,"173"
GOTO @upped
END
@qwas173
SET train,"174"
GOTO @upped
END
@qwas174
SET train,"175"
GOTO @upped
END
@qwas175
SET train,"176"
GOTO @upped
END
@qwas176
SET train,"177"
GOTO @upped
END
@qwas177
SET train,"178"
GOTO @upped
END
@qwas178
SET train,"206"
GOTO @upped
END
@qwas206
SET train,"207"
GOTO @upped
END
@qwas207
SET train,"208"
GOTO @upped
END
@qwas208
SET train,"209"
GOTO @upped
END
@qwas209
SET train,"210"
GOTO @upped
END
@qwas210
SET train,"212"
GOTO @upped
END
@qwas212
SET train,"213"
GOTO @upped
END
@qwas213
SET train,"240"
GOTO @upped
END
@qwas240
SET train,"241"
GOTO @upped
END
@qwas241
SET train,"242"
GOTO @upped
END
@qwas242
GOTO @upped
END
