; made by Silver for RoT
;
; smuggler quest

SCENE 830, bkroon,SCENE,"",0,0

	ACTOR 1, "Haspeltro", a61, 4,-15,80
	THEME 0
	POSE 1,4

	IF  T294, @notkama
	IF  T130, @notkama

	MOVE 1, 35,90
	WAIT 2.0
	1: Who are you young #<g.warrior>?
	H: I'm %1
	H: So are you the one called Kamatari?
	1: This cave is named after his grandfather, but I am not him.
	1: Where do you require him for?
	H: I need to get his support in the coming RoT war.
	1: War... it's always about war...
	1: Have you never questioned why war is so vital to the survival of the Empire or the rebels?
	H: Not really...
	H: They only go on war to take out the other side so they'll be sure to survive.
	1: War only brings death, chaos and destruction, what if you win the war but loose the people you love?
	1: Isn't life more valuable then all of that?
	H: Maybe, but if we don't fight the others will and they will take us out if we don't watch our back.
	1: Maybe they would, but if both stopped fighting you wouldn't have anything to worry about anymore.
	1: I have fought in several wars in the past, I gained nothing except a few medals, 
	1: instead I lost everything I loved.
	H: If I don't fight I would loose what I love anyway.
	1: Not always, often there is another way, you just have to find it.
	GIVE T294
	1: Think about it...
	MOVE 1,-15,95
	WAIT 6.0
	H: Bah...
	H: Now I still don't know where to find Kamatari...
	H: Who was that guy anyway?
	H: I might as well get going again...
	WAIT 4.0
	GOTO EXIT
@notkama
	N: You only hear the wind...
	SOUND "WIND2.wav"
	END

SCENE 831, bkroon,SCENE,"Healing Zone",0,2

	THEME 2
	ACTOR 1, "Leprodah",a70,4,20,80

	1: You should rest...

	IF ALIVE,@start
	HOST_GIVE L1

@start
	IF -V20, @stop1
	SET hpnew, "#<num.hostMaxHP>"
	SET mpnew, "#<num.hostMaxMP>"

	SUB hpnew, "#<num.hostHP>"
	SUB mpnew, "#<num.hostMP>"

	SET hpnews, #<hpnew>
	SET mpnews, #<mpnew>

	SET hptime, "#<num.hostHP>"
	MUL hptime, 100
	DIV hptime,"#<num.hostMaxHP>"

	SET hptimes,"100"
	SUB hptimes,"#<hptime>"  

	DIV hpnew, "#<hptimes>"
	DIV mpnew, "#<hptimes>"

	COMPARE #<hpnew>, 1
	IF< @stop1
	COMPARE #<mpnew>, 1
	IF< @stop1

@heal
	WAIT 0.5
	HOST_GIVE H#<hpnew>
	HOST_GIVE M#<mpnew>
	COMPARE #<num.hostMaxHP> ,#<num.hostHP>  
	IF= @mp
@mp
	COMPARE #<num.hostMaxMP> ,#<num.hostMP>
	IF= @hp
	GOTO @heal
@hp
	COMPARE #<num.hostMaxHP> ,#<num.hostHP>  
	IF= @end
	GOTO @heal
@end
	DIV hptimes, "2"
;	N: You healed #<hpnew> HP and #<mpnew> MP every second for #<hptimes> seconds.
	END
@stop1
	HOST_GIVE H#<num.hostMaxHP>
	HOST_GIVE M#<num.hostMaxMP>
;	N: You healed #<hpnews> HP and #<mpnews> MP.
	END

SCENE 832, bkroon,SCENE,"",0,0

	THEME 4

	COMPARE #<batatir>, "0"
	IF> @end
	ACTOR 1, "Batati",pet025,4,30,90

	POSE 1,4
	WAIT 60.0
	1: I'm bored...
	MOVE 1,20,90
	SET batatir, "1"
@end
	END
; XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
; THE FINAL BATTLE #1
; XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

SCENE 840, aaafuji,SCENE,"????",0,0

	MUSIC "Armageddon.mid"
	LOCK 1
	ACTOR	1,	"Silver",Silver, 4, -10, 80

	IF DEAD, @gback2
	IF T433, @goodend 
	IF T430, @evil
	IF T431, @good
	N: There seems to be nothing here right now...
	END
@goodend
	N: There seems to be nothing here...
	END
@good
	IF T438,@gback
	MOVE 1,20,85
	WAIT 4.0
	1: Ah... it's %1.	
	1: We have been expecting you for quite some time now. 
	H: Do I know you?
	IF T170, @metbefore
	1: No...
	1: We have never met before yet.
	1: But we knew you were coming.
	GOTO @cont

@metbefore
	1: Yes you do.
	1: Remember the fight in which a mysterious stranger showed up and helped you fighting 3 monsters?
	ASK 30
	IF YES, @myes
	H: Not really...
	1: Hmmm...  
	1: To bad you forgot.
	1: Anyway that mysterious ally was me.
	GOTO @cont
@myes
	H: You mean that mysterious stranger was you?
	1: Yes, I help out in very rare occasions.
@cont
	HOST_GIVE T438
	IF -V99, @notstrongenough
@cont2
	1: Come, let me bring you to Atsumori %1.
	WAIT 4.0
	PARTY T172
	GOTO SCENE 841

@gback
	IF -V99, @gback2 
	MOVE 1,20,90
	WAIT 4.0
	GOTO @cont2
@gback2
	N: There seems to be nothing here right now...
	END

@notstrongenough
	1: I'm afraid that you're not strong enough yet.
	1: Return to this place once you reached level 99.
	1: I shall bring you to Atsumori then.
	WAIT 3.0
	MOVE 1,-20,80
	END
@evil
	IF ALIVE, @starte
@evilend
	N: There seems to be nothing here right now...
	END
@starte
	IF -T432, @evilend
	WAIT 2.0
	H: ! ATSUMORI!?
	H: Where are you!?
	MOVE 1, 20,90
	WAIT 4.0
	1: So you are Devils new puppet...
	1: If you're looking for Atsumori you'll first have to beat Atsumori's soldiers and me!
	1: DIE EVIL PUPPETS!
	WAIT 5.0
	PARTY T432
	GOTO SCENE 842

SCENE 841 bathtemp, CUT, "Atsumori's Temple",0,0

	LOCK 1
	FLAGS 16384
	MUSIC "ThingsWeSaidToday.mid"

	ACTOR	1,	"Atsumori",Atsumori, 3, 35,79
	ACTOR	2,	"Silver",Silver, 4, 110, 94
	ACTOR 3,	"Jennifer",stealth,1,48,75
	ACTOR 4,	"Carmen",pet014,4,-10,95
	ACTOR 5,	"Emma",pet010,4,69,80
	ACTOR 6,	"Kitty", kitty3, 4,20,95
	ACTOR 7,	"#<str.name>",mirror,1,110,95

	FACE 5,4
	SEL 4
	POSE 4,1
	SEL 1
	POSE 3,1

	6: Prrr
	3: Atsu darling could you give me the soap?
	1: Sure sugar.
	5: Are you still not done Jennifer?
	5: You're sitting in there for 3 hours already!
	3: You have a problem honey?
	1: Girls, no fighting.
	6: Meow!
	SEL 6
	POSE 2
	SOUND meow.wav
	WAIT 1.0
	POSE 4
	WAIT 1.0
	1: Emma go feed the cat, it's hungry.
	5: Yes Atsu.
	MOVE 5,50,91
	SEL 5
	POSE 4,1
	WAIT 3.0
	5: Come kitty, let's get you some food.
	SEL 6
	POSE 3
	MOVE 6,-20,97
	MOVE 5,-20,99
	WAIT 10.0
	1: Where were we...
	WAIT 2.0
	MOVE 2, 74,94
	WAIT 3.0
	2: Wherever you were going you won't be going there right now.
	2: The time has come...
	1: Right now?
	2: Yes, the chosen one is here.
	2: %1, show yourself.
	WAIT 2.0
	MOVE 7,90,95
	WAIT 3.0
	MOVE 2,58,77
	WAIT 3.0
	2: Hi Jennifer, still bathing I see?
	3: Hi Silver.
	1: So... you are the one they call %1?
	SEL 7
	POSE 4
	7: Yeah that is me.
	1: Hmmm... 
	1: I thought you would be taller.
	1: I guess you'll have to do.
	1: We have chosen you and several other people to fight Devil and Daemonium.
	7: Why won't you just fight him yourself?
	7: You won the last time, didn't you?
	1: Yes I won, but Silver and I decided that it would be better to let the humans save themselves for a change.
	1: Silver and I have been working on a plan to rebuild Kagoshima to what it used to be before the nukes hit the island.
	1: Silver has also been scouting for recruits, people who could be strong enough to take Devil out.
	FACE 2,4
	2: We believe that you are one of the people that has what it takes to take Devil out.
	1: To make it easier I have blessed special equipment to help you in the fight.
	WAIT 5.0
	HOST_GIVE I180
	HOST_GIVE I181
	HOST_GIVE I190
	HOST_GIVE I191
	HOST_GIVE I192
	HOST_GIVE I193
	N: You received the blessed equipment.
	1: We used to have specially blessed weapons too, 
	1: unfortunately that little rat Daemonium stole them and gave it to Devil.
	2: If you want that special equipment you'll have to beat Devil first.
	MOVE 4,18,98
	WAIT 4.0
	4: Hi %1.
	7: Eh.. hi.
	7: Who are you?
	4: I'm Silvers wife.
	7: Eh... Silver weren't you married with Carina?
	2: Yes... how do you know?
	7: Flashbacks...
	2: Huh?
	7: It's an euh.. long story.
	2: After chasing down Daemonium for over 250 years I decided to settle down and learn to live with Carina's death.
	2: A few months later a girl was in need and I saved her.
	2: Turned out we shared many interests and Atsumori didn't mind giving one more mortal immortality,
	2: especially not after all I did for him.
	4: Yup, that's pretty much what happened.
	2: Good luck with your fight %1.
	1: May I suggest having a team of at least 5 soul mates?
	1: Devil is going to throw everything he has at you.
	2: I have some other business to take care off. 
	2: Maybe some day we'll meet again %1.
	7: Thanks for the help.
	MOVE 2,20,99
	MOVE 4, -10,99
	1: It's your holy duty as a %C to end the nightmare and to assassinate Devil.
	WAIT 2.0
	MOVE 2,-10,99
	1: To help you on your quest I have a special summon spell for you.
	WAIT 3.0
	HOST_GIVE S36
	N: You received %S36.
	1: This should be all that you need to take Devil out and change the future for the better.
	HOST_GIVE T433
	WAIT 7.0
	GOTO EXIT

SCENE 842 aaadirty, SCENE, "The Final Battle",0,0
	LOCK 1
	FLAGS 16384
	MUSIC "FF8.mid"	 		
	WAIT 3.0
	ACTOR 8, "", explosion,1, -150,100
	ACTOR 9, "", explosion,1, -150,100

;
; explosion script, this baby will rock you all!!
;
	MOVE 8,50,50,1
	SOUND explosion

	SEL 8
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 8,-150,100,1

	MOVE 8,30,80,1
	SOUND explosion

	SEL 8
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 8,-150,100,1

	MOVE 9,80,70,1
	SOUND explosion

	SEL 9
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 9,-150,100,1
	
	FIGHT2 1300,1302,1304,1306,1308,1310,1312,1314
	IF LOSE, @lost
	GOTO SCENE 850
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END

SCENE 850,aaadirty, SCENE,"The Final Battle",0,0
	LOCK 1
	FLAGS 16384
	EJECT T430

	MUSIC "metallica-master_of_puppets.mid"
	FIGHT2 1301,1303,1305,1307,1309,1311,1313,1315
	IF LOSE, @lost
	GOTO SCENE 851
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END

SCENE 851,aaadirty,SCENE, "The Final Battle",0,0
	LOCK 1
	FLAGS 16384
	EJECT T430

	MUSIC "maser.mid"
	FIGHT2 610,611,613,614,615,616,618,619
	IF LOSE, @lost
	GOTO SCENE 852
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END

SCENE 852,aaadirty,SCENE, "The Final Battle",0,0
	LOCK 1
	FLAGS 16384
	EJECT T430

	MUSIC "xfilestrancemix.mid"
	FIGHT2 630,631,632,633,634,635,636,637,638,639
	IF LOSE, @lost
	GOTO SCENE 853
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END

SCENE 853,battlefeildx,SCENE, "The Final Battle",5,0
	LOCK 1
	FLAGS 16384
	EJECT T430

	STRCMP #<str.soul>, "SOUL 1791"
	IF= @jforever

	MUSIC "black_sabbath.mid"
	FIGHT2 702,704
	IF LOSE, @lost
	GOTO SCENE 854
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END
@jforever
	MUSIC enclosure.mid
	ACTOR	1, "Silver",Silver, 4, -10, 80
	MOVE 1, 20, 80
	WAIT 6.0
	1: I...
	WAIT 5.0
	1: I can't fight you %1...
	WAIT 5.0
	1: You may pass...
	WAIT 5.0
	1: Here, take this;
	WAIT 3.0
	SET jaieX, #<num.hostX>
	SET jaieY, #<num.hostY>
	SUB jaieY, 10
	ACTOR 2,	"",rotm2,14,-20,-20
	FX 9
	WEATHER 3
	SOUND "travel1.wav"
	MOVE 2,#<jaieX>,#<jaieY>,1
	SUB juliaX, 15
	ADD juliaY, 20
	MOVE 2,#<jaieX>,#<jaieY>
	WAIT 1.0
	SOUND "travel9.wav"
	WAIT 1.0
	SOUND "travel8.wav"
	WAIT 1.0
	SOUND "travel9.wav"
	WAIT 1.0
	ADD jaieX, 15
	ADD jaieY, 5
	MOVE 2,#<jaieX>,#<jaieY>
	SOUND "travel8.wav"
	WAIT 1.0
	SOUND "summon1.wav"
	WAIT 2.0
	SUB jaieX, 30
	SUB jaieY, 35
	MOVE 2,#<jaieX>,#<jaieY>
	WAIT 2.0
	MOVE 2,-100,80,1
	FX 0
	WEATHER 0
 	GIVE L1
	GIVE H25000
	GIVE M25000
	1: I restored everybodies HP and MP...
	1: Now go fight your battle with Atsumori...
	WAIT 3.0
	MOVE 1,-20,80
	WAIT 5.0
	GOTO SCENE 854


SCENE 854 ,nightmare,"The Final Battle",1,9998
	BKGND nightmare
	FX 1
	WEATHER 4
	EJECT T430

	MUSIC "doom3.mid"
	FIGHT 700
	IF LOSE, @lost

	MUSIC "Ecuador.mid"


	ACTOR	1,	"Atsumori",Atsumori, 3, -10,90
	
	SEL 1
	POSE 3,1
	IF -R20, @skipkey
	GIVE I205%R5
@skipkey
	IF T434,@defeated2

	MOVE 1,20,95	
	WAIT 3.0
	1: Ughh...
	SOUND pain1.wav
	WAIT 2.0
	1: You have beaten me.
	1: Take your stupid equipment, I don't have any use for it.
	GIVE I162
	GIVE I163
	GIVE I164
	GIVE I165
	GIVE I166
	GIVE I167
	GIVE I168
	GIVE T434
	MOVE 1,-30,99
	END
@defeated2
	IF T436, @defeated3
	MOVE 1,20,95
	WAIT 3.0	
	1: Ughh...
	SOUND pain1.wav
	WAIT 2.0
	1: You have beaten me.
	1: You already have all dark equipment, what more could you want?
	H: Teach us your %S36.
	1: Very well...
	WAIT 5.0
	GIVE S36
	GIVE T436
	N: You learned %S36.
	WAIT 2.0
	MOVE 1,-30,99
	END
@defeated3		
	MOVE 1,40,95
	WAIT 3.0	
	1: Ughh...
	SOUND pain1.wav
	WAIT 2.0
	1: You have beaten me.
	WAIT 3.0
	1: BUT I SHALL NEVER DIE!!
	ACTOR 8, "", explosion,1, -150,100
	ACTOR 9, "", explosion,1, -150,100

;
; explosion script, this baby will rock you all!!
;
	MOVE 8,50,95,1
	SOUND explosion

	SEL 8
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 8,-150,100,1

	MOVE 8,30,70,1
	SOUND explosion

	SEL 8
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 8,-150,100,1

	MOVE 1,-30,80,1

	MOVE 9,#<num.hostX>,#<num.hostY>,1
	SOUND explosion

	SEL 9
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 9,-150,100,1

	MOVE 9,#<num.hostY>,#<num.hostX>,1
	SOUND explosion

	SEL 9
	POSE 1
	WAIT 0.2
	POSE 2
	WAIT 0.2
	POSE 3
	WAIT 0.2
	POSE 4
	WAIT 0.2
	MOVE 9,-150,100,1

	TAKE H25000
	TAKE M25000
	END
@lost
	ACTOR	1,	"Atsumori",Atsumori, 4, -10,95
	WAIT 5.0
	MOVE 1,20,95
	WAIT 5.0
	1: Nice try...
	1: But it looks like you don't have what it takes to take me down.
	1: Better luck next time...
	MOVE 1,-30,95
	END

; XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
; THE FINAL BATTLE #2
; XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

SCENE 845,	aaabright,SCENE,"????",4,2

	LOCK 1
	MUSIC "enclavesong(old).mid"

	ACTOR	1,	"Daemonium",Daemonium, 4, -10, 90

	IF DEAD, @empty
	IF T432, @empty2
	IF T439, @backe
	IF T430, @evil
	IF T431, @good
@empty
	N: There seems to be nothing here right now...
	END
@empty2
	N: there seems to be nothing here...
	END
@evil
	MOVE 1,20,90
	WAIT 3.0
	1: It's about time you got here...
	H: Have we met..?
	IF T168, @killeddae
	IF T169, @gotkilled
	1: No we haven't met yet, but I have been paying close attention to your actions.
	GOTO @cont
@killeddae
	1: We met alright...
	1: Remember that stealth monster that attacked you?
	H: Yeah I kicked it's ass.
	IF T169, @kicked
	1: Yeah, I can still feel the bruises of that fight...
	H: You mean that was you?
	1: Yup, you sure put up one hell of a fight.
	GOTO @cont

@kicked
	1: Yeah you did, but I got the better out of you too.
	H: You mean that creep was you?
	1: Ey, being a bit more respectful doesn't hurt.
	1: But I was that stealth monster yeah.
	GOTO @cont
@cont
	IF -V99, @notgood
	1: You seem to be what Devil is looking for.
	1: Let me bring you to him.
	WAIT 4.0
	PARTY T172
	GOTO SCENE 846

@backe
	IF -V99, @empty
	MOVE 1,20,90
	WAIT 3.0
	1: Ah you're back and level #<num.hostLevel>.
	GOTO @cont
	END

@notgood
	1: Anyways, return here when you're level 99.
	1: You seem to be exactly what Devil and I are looking for, but right now you're still to weak.
	HOST_GIVE T439
	H: Sounds good to me.
	MOVE 1,-10,90
	END

@goodend
	N: There seems to be nothing here right now...
	END
@good
	IF -T433, @goodend
	ACTOR	2,	"Daemonium",Daemonium, 4, -10, 90
	WAIT 2.0
	H: DEVIL!
	H: Where are you hiding!?
	MOVE 1,20,95
	1: You really think you'll ever beat Devil?
	1: I bet you won't even be able to get past me or my soldiers.
	1: But heck...
	1: Let's try it anyway.
	1: PREPARE TO DIE!
	WAIT 5.0
	PARTY T433
	GOTO SCENE 847	


SCENE 846,	silverplace, CUT,"Devils Temple",0,0

	LOCK 1
	FLAGS 16384
	MUSIC "escape.mid"

	ACTOR	1,	"Devil",Devil, 1, 60, 83
	ACTOR	2,	"Daemonium",Daemonium, 4, 110, 80
	ACTOR 3,	"Euzinha",pet019, 1,74,77
	ACTOR 4,	"Beatrice",pet029,4,85,92
	ACTOR 5,	"Carolle",pet021,4,45,87
	ACTOR 6,	"Kitty",kitty4, 4,18,90
	ACTOR 7,	"#<str.name>",mirror,1,110,95

	FACE 3,4
	SEL 4
	POSE 4,1
	FACE 4,4
	SEL 5
	POSE 4,1

	1: Alright, Beatrice gets tonight,
	1: Euzinha tomorrow and Carolle the day after.
	1: Now will you girls finally stop argueing about me!
	4: Devil you know I would do everything for you...
	4: It's just that Carolle had to start it all again.
	5: That's not true!
	5: Euzinha started again!
	1: Argh...
	1: WILL YOU 3 FINALLY STOP IT!?
	6: Meow!
	SOUND "meow.wav"
	SEL 6
	POSE 2
	WAIT 1.0
	POSE 4
	WAIT 1.0
	1: See?
	1: Even Kitty agrees with me.
	6: Meow!
	SOUND "meow.wav"
	SEL 6
	POSE 2
	WAIT 1.0
	POSE 4
	WAIT 1.0
	1: Now what!?
	4: I think Kitty is hungry...
	1: Well then give that cat something to eat!
	1: Do I have to do everything around here myself?
	1: I'm already busy enough terrorizing the world.
	SEL 4
	POSE 4,1
	4: Come Kitty, let's get you something to eat.
	6: Meow!
	SOUND "meow.wav"
	SEL 6
	POSE 2
	WAIT 1.0
	POSE 4
	WAIT 2.0
	POSE 3
	MOVE 4,-10,90
	MOVE 6,-10,90	
	WAIT 8.0
	MOVE 2,72,93
	2: Devil I have somebody here who you probably want to meet.
	MOVE 2,25,98
	WAIT 3.0
	1: Who is it?
	WAIT 3.0
	FACE 2,4
	2: %1, you're allowed to enter now.
	MOVE 7,90,98
	1: %1 huh?
	1: Hmmm... isn't that the #<g.guy> who helped us creating all the chaos in Kagoshima?
	FACE 2,4
	2: Sure is.
	SEL 7
	POSE 4
	7: So you're the big boss huh?
	7: Just how many girls do you have?
	1: About 4 in total, one is shopping right now.
	2: Can we get to the point...
	2: I don't have all day either you know.
	1: Alright, %1, the reason Daemonium brought you here is because we need somebody to kill Atsumori for us.
	7: Why don't you do it yourself?
	1: Cause I already have my hands full on these girls here.
	1: Not to mention I'm working on a master plan to turn Kagoshima in the darkest place you have ever seen.
	7: You're sure it's not because you lost the last time..?
	1: No of course not!
	7: '*cough* chicken	
	1: ...
	1: I'll pretend I didn't hear that.
	2: Really though, we have been working on a new master plan, only Silver keeps interfering each time.
	2: He may not have been actively hunting me the last few years but he still causes us a lot of trouble.
	2: And who knows what Atsumori is working on.
	2: Besides don't think you're *that* special, we have recruited several other people already.
	2: If you want to stand a chance you'll need to team up with at least 5 other people,
	2: else you don't even stand a chance of defeating Atsumori and Silver.
	1: I have been preparing a special dark equipment. 
	1: We used to have weapons too, but Silver found out about those and stole them when Daemonium was chasing girls again.
	2: Hey I can't help I like to chase girls, so far it got me 3 nice women.
	1: We'll talk about that later...
	1: Anyways, here is your special equipment, you will probably need it.
	WAIT 3.0
	HOST_GIVE I160
	HOST_GIVE I161
	HOST_GIVE I170
	HOST_GIVE I171
	HOST_GIVE I172
	HOST_GIVE I173
	N: You received your dark equipment.
	1: To make it all even easier I will also teach you my %S136 technique.
	WAIT 3.0
	HOST_GIVE S136
	N: You learned %S136.
	2: It's probably best if you team up with several soul mates, it's the only way to take out Silver and Atsumori.
	1: In the meanwhile we'll be completing our scheme.
	1: If you're lucky you'll live long enough to see it in action.
	HOST_GIVE T432
	2: Perhaps we'll meet again %1.
	2: Now if you two excuse me, I have other matters to attend to.
	MOVE 2,-10,90
	1: Yes... I better get back to work too...
	WAIT 5.0
	GOTO EXIT

SCENE 847	gateoftime&space,SCENE,"The Final Battle",2,0
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "FF8.mid"	 		
	WAIT 3.0
	FIGHT2 1300,1302,1304,1306,1308,1310,1312,1314
	IF LOSE, @lost
	GOTO SCENE 855
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END

SCENE 855	gateoftime&space,SCENE,"The Final Battle",2,0
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "Vood.mid"	 		
	WAIT 3.0
	FIGHT2 1301,1303,1305,1307,1309,1311,1313,1315
	IF LOSE, @lost
	GOTO SCENE 856
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END	

SCENE 856	gateoftime&space,SCENE,"The Final Battle",2,0
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "doom6.mid"	 		
	WAIT 3.0
	FIGHT2 610,611,613,614,615,616,618,619
	IF LOSE, @lost
	GOTO SCENE 857
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END	

SCENE 857	gateoftime&space,SCENE,"The Final Battle",2,0
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "mother.mid"	 		
	WAIT 3.0
	FIGHT2 630,631,632,633,634,635,636,637,638,639
	IF LOSE, @lost
	GOTO SCENE 858
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END

SCENE 858	glasfire&water,SCENE,"The Final Battle",5,0
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "freak.mid"	 		
	WAIT 3.0
	FIGHT2 703,705
	IF LOSE, @lost
	GOTO SCENE 859
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END

SCENE 859	Iceskull,SCENE,"The Final Battle",2,9799
	LOCK 1
	FLAGS 16384
	EJECT T431

	MUSIC "enclavesong(old).mid"
	WAIT 3.0
	FIGHT2 701
	IF LOSE, @lost
	
	MUSIC "Ecuador.mid"

	ACTOR	1,	"Devil",Devil, 4, -10, 80

	IF -R20, @skipkey
	GIVE I205%R5
@skipkey

	IF T435, @beaten2
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0
	1: Ugggh..
	SOUND pain1.wav
	WAIT 2.0
	1: How could this be?
	1: Beaten by mortals?
	1: Argh!
	1: Here take these useless weapons.
	1: I won't be needing them anyway.
	WAIT 5.0
	GIVE I182
	GIVE I183
	GIVE I184
	GIVE I185
	GIVE I186
	GIVE I187
	GIVE I188
	GIVE T435
	N: You received all holy weapons.
	MOVE 1,-30,99
	END
@beaten2
	IF T437, @beaten3
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0
	1: Ugggh..
	SOUND pain1.wav
	WAIT 2.0	
	1: You foolish mortals.
	1: What do you want this time?
	H: Teach us your %S136!
	1: If that's the only way to get rid of you...
	WAIT 5.0
	HOST_GIVE S136
	HOST_GIVE T437
	N: You learned %S136.
	MOVE 1,-30,99
	END
@beaten3
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0
	1: Ugggh..
	SOUND pain1.wav
	WAIT 2.0	
	1: YOU AGAIN!?
	1: You know you're beginning to become a real pain in my royal ass.
	1: ...
	1: Remember this sound...?

	FX 0
	WEATHER 0
	MOVE 1,-50,80,1
	SOUND "nuke.wav"
	WAIT 5.0
	BKGND nucfireball
	WAIT 3.0
	BKGND nucbomb
	WAIT 3.0
	BKGND Icehellskull
	FX 2
	WEATHER 4
	WAIT 4.0 
	SOUND "evillaugh.wav"
	1: Muhahaha
	1: I SHALL NEVER DIE!!
	TAKE H25000
	TAKE M25000
	END	
@lost
	ACTOR	1,	"Devil",Devil, 4, -10, 80
	WAIT 3.0
	MOVE 1,20,90
	WAIT 3.0	
	1: I can't believe that Atsumori send *you* to beat me...
	SOUND "evillaugh.wav"
	1: Muhahahaha
	WAIT 2.0
	MOVE 1,-30,99
	END




; Comment: pfff... alsof de bedrijfsleiding er ooit achter zou komen dat je die appel meenam.