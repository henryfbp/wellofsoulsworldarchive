; Made by Silver for RoT
;
; This file will allow you to skip flashbacks by clicking in the right upper corner.


	COMPARE #<scene.id>, "1175"
	IF= @endflash

	IF T442, @skiptotal2
	IF T443, @endflash

	MENU "Skip Flashback=@startskip","Skip All Flashbacks=@skiptotal","View Flashback=@endflash","View Flashbacks Always=@viewall"
	GOTO @endflash

@startskip
	COMPARE #<scene.id>, "1176"
	IF= @skip1176
	COMPARE #<scene.id>, "1177"
	IF= @skip1177
	COMPARE #<scene.id>, "1178"
	IF= @skip1178
	COMPARE #<scene.id>, "1179"
	IF= @skip1179
	COMPARE #<scene.id>, "1180"
	IF= @skip1180
	COMPARE #<scene.id>, "1181"
	IF= @skip1181
	COMPARE #<scene.id>, "1182"
	IF= @skip1182
	COMPARE #<scene.id>, "1183"
	IF= @skip1183
	COMPARE #<scene.id>, "1184"
	IF= @skip1184
	COMPARE #<scene.id>, "1185"
	IF= @skip1185
	COMPARE #<scene.id>, "1186"
	IF= @skip1186
	COMPARE #<scene.id>, "1187"
	IF= @skip1187
	COMPARE #<scene.id>, "1188"
	IF= @skip1188
	COMPARE #<scene.id>, "1189"
	IF= @skip1189

	GOTO @nosupport

@skip1176
	HOST_GIVE T201
	GOTO SCENE 2
@skip1177
	HOST_GIVE T202
	GOTO SCENE 2
@skip1178
	HOST_GIVE T203
	GOTO SCENE 2
@skip1179
	HOST_GIVE T204
	GOTO SCENE 2
@skip1180
	HOST_GIVE T205
	GOTO SCENE 2
@skip1181
	HOST_GIVE T206
	GOTO SCENE 2
@skip1182
	HOST_GIVE T207
	GOTO SCENE 2
@skip1183
	HOST_GIVE T208
	GOTO SCENE 2
@skip1184
	HOST_GIVE T209
	GOTO SCENE 2
@skip1185
	HOST_GIVE T213
	GOTO SCENE 2
@skip1186
	HOST_GIVE T214
	GOTO SCENE 2
@skip1187
	HOST_GIVE T215
	HOST_GIVE T216
	GOTO SCENE 2
@skip1188
	HOST_GIVE T217
	GOTO SCENE 2
@skip1189
	HOST_GIVE T218
	GOTO SCENE 2

@viewall
	HOST_GIVE T443
	GOTO @endflash
@skiptotal
	HOST_GIVE T442
	HOST_GIVE T200
	HOST_GIVE T201
	HOST_GIVE T202
	HOST_GIVE T203
	HOST_GIVE T204
	HOST_GIVE T205
	HOST_GIVE T206
	HOST_GIVE T207
	HOST_GIVE T208
	HOST_GIVE T209
	HOST_GIVE T213
	HOST_GIVE T214
	HOST_GIVE T215
	HOST_GIVE T216
	HOST_GIVE T217
	HOST_GIVE T218
	GOTO SCENE 2

@skiptotal2
	GOTO SCENE 2

@nosupport
	N: Scene skipping is not supported.
@endflash