;
; SPELLS
;
;
;-------------------
; Spells used in the game.  Each line describes one spell
;
; Mandatory arguments are:
;
; 	//	arg0	ID# (1-767)			// unique ID number of spell
;
;		* ID #
;
;		Each spell must have a unique ID number between 1 and 767 (which is a strange value, I know
;		but it has historical significance :-)
;
;  		Spell 0 is a very special entry in the table which is actually a set of algorithm tweaks.
;		Read monsters.txt for more details about algorithm tweaks.
;
;	//	arg1	name[MAX_SPELL_NAME_LEN];  // name of spell, as it appears on screen. 
;
;	//	arg2	ppCost;				// Play Points (Whack Points) required to learn spell 
;									//  -1  means "spell cannot be learned" (can be given as a quest reward only)
;									//   0  means "cost determined automatically from spell's level"
;									//  n>0 Means "cost is n PP" 
;									// An "unlearnable" spell simply doesn't appear in the Learn Spells dialog.
;
;	//	arg3	element;			// Spells element ID#: 0-255.  
;									// A spell will be most effective when cast against the opposing
;									// element.  Actual element definitions are in the ELEMENTS table.
;									// Special Element IDs are:
;									//   0 - HEAL spell (cannot be used to cause damage)
;									//   4 - DEATH spell (affects all elements equally)
;									//  8-255  CHAOS elements (use DEATH element training for effectiveness)
;									// Chaos elements are intended to let you have more variety in monsters, such
;									// that some spells are only cast by one or two monsters.
;									// Chaos spells cannot be learned.  They can only be assigned by table, or
;									// given as quest prizes (and it's a bad idea to give chaos spells to players
;									// since they have no explicit chaos selector on the SPELLs panel.
;
;	//	arg4	damage.monsterID		// pure attack strength (before defense filtering)
;									// damage determins nature of spell
;									// optional monsterID is used by SUMMON_SPECIFIC 
;
;					The damage argument is also used to specify certain special effect spells
;					which do not cause regular damage or do one-of-a-kind things
;					Generall, a negative value indicates a specialty spell.  These are
;					called 'diseases' just to give them a general name.
;
;					Damage			// Specialty
;
;						 0			damage computed automatically from level (preferred value if not HEAL spell)
;
;						-1			RESURRECTION		(brings dead to life, with 1 HP)
;
;						-2			DISEASE_POISON		(Makes HP restores ineffective)		
;						-3			DISEASE_SAP			(Makes Spells cost more MP to cast)		
;						-4			DISEASE_STUN		(cannot move, or attack)
;						-5			DISEASE_GAG			(cannot case spells)(* wears off)		
;						-6			DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-7			DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;						-8			DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-9			DISEASE_CHARM		(once charmed, monster fights for you)	
;						-10			DISEASE_TAME		(taming a charmed monster makes it a pet)(NOT IMPLEMENTED YET)	
;						-11			DISEASE_FREEZE		(cannot move)
;						-20			DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-21			DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-22			DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-23			DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-24			DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	
;						-200		SUMMON_SAME			Summon a monster of same type as caster
;						-201		SUMMON_SAME_ELEMENT	Summon a monster of the same element as caster
;						-202		SUMMON_RANDOM		Summon a random monster at or below level of caster (within 10 levels)
;						-203		SUMMON_SPECIFIC		Summon a specific monster (monster id in optional dotted arg)
;						-300		SUMMON_ITEM			Summon an item to be used on the target (item id in dotteg arg)
;
;					Most 'diseases' wear off when you leave the scene, however each disease can be 'cured'
;					by a spell whose 'damage' argument is the disease damage plus -100.  For
;					example, "-102" would cure poisoning (-2), and "-107" would cure sleep (-7)
;					Not all diseases have a logical cure (for example, there is no cure for 
;					resurrection (-1)
;
;					Some diseases(*) wear off after awhile, even if you don't leave the scene.
;
;					The 5 ability diseases are a little different as they lower (or raise, if a cure)
;					your ability a certain percentage with each use.  Hence repeated lowerings has
;					an larger effect.
;
;					Note, except for specialty spells (negative damage value) and LIFE spells, this
;					field is now ignored as regular damage is computed automatically

;	//  arg5	price;				// mp required to cast
;									//  0 = determined automatically, based on spell level
;									// this is now determined automatically and this entry is ignored
;
;	//	arg6	reqLevel+All.flags.minPlayerLevel.tokenNeeded	// minimum required elemental affinity (0-9) to cast it.
;									// [optional]add 100 if it affects ALL enemies
;											105  = level 5 required, and affects all on same team as target
;											(note this means you cannot make a spell require player level 100)
;									// [optional]add a decimal point followed by a spell FLAG
;											5.1	 = level 5 required, and is controlled by flag '1'
;											5.3  = level 5 required, flags 1 AND 2 (1+2 = 3)
;									// SPELL FLAGS: (add together all you want)
;											0 = no flags (in case you want to use minPLayerLevel option)
;											1 = monsters/pets cannot use this spell
;											2 = humans are not offered this spell on LEARN SPELL dialog
;									// minPlayerLevel (assumed 0 unless you specify) character must be this level in class (or above)
;											5.0.12  = level 5 element training, no flags, level 12 character required
;									// tokenNeeded (assumed 0, which means no special token needed.  Otherwise spell is only in
;													the learn Spells list if the player has the specified token
;											5.0.0.7  = requires token 7 before it appears in list.
;
;				so... for example, a value of "107.1" would mean:
;
;						* element training level 7 or higher required to cast this spell
;						* when cast, it affects ALL members of the targeted team (following the same leader)
;						* monster/pets cannot learn/use this spell. (but humans can)
;
; the rest are optional and control the 'look' of the effect
;
;	//	arg7	path;				// path style (9 digit decimal number AAABBBCDD)
;
;					DD	'cloud' shape of magic in motion
;					00 - tight random cloud
;					01	- looser random cloud
;					02 - loose random cloud
;					03 - single circle
;					04 - two circles
;
;					C	Accumulation of magic around caster, before it 'launches'
;					0	- accumulate over caster
;					1	- accumulate within caster
;					2	- accumulate under caster
;		
;					BBB Travel Path
;					000 - straight line
;					001 - looser line
;					002 - looser line
;					003 - loose line
;					004 - slow wavy
;					005 - medium wavy
;					006 - faster wavy
;					007 - fast wavy
;					008 - spiral
;					009 - spiral
;					010 - spiral
;					011 - spiral
;
;					AAA Attack Path
;					000 - random cloud
;					001	- looser random cloud
;					002 - loose random cloud
;					003 - single circle
;					004 - two circles
;
; For example:   
;
;		004(2circ) + 011(spiral) + 2(under) + 04(2circ) 
;
; would be written:  004011204, or just 4011204
;
;	//	arg8	row; (0-199)		// row of effects table to use
;									// Magical effects are animated from a film-strip of up to 16
;									// images which are played from left to right.  Up to 200 such
;									// film-strips may be defined by the world designer.  Rather than
;									// cram all 200 filmstrips into a single file, they are broken
;									// into groups of ten and placed in files with the name:
;									// "EffectsNN.bmp" where NN runs from 00 to 19 and represents the
;									// first two digits of the effect 'row number' (000-199)
;									// For example, row 45 would be at offset 5 in the file "Effects04.bmp"
;									// Effects files 00 through 09 are stored in the evergreen ART folder
;									// and are reserved for the use of synthetic-reality (feel free to submit
;									// cool effects to be added).  World designers should put their custom
;									// effects into the files "effects10.bmp" through "effects19.bmp" and
;									// place them in the world's private ART folder (this means world designers
;									// control rows 100-199).
; 
;									// effects are 16x16 graphical glyphs stored in a separate
;									// row within the file. Columns represent individual images
;									// of the animation and are generally shown left to right.
;									// It would probably be most useful to open art\effects00.bmp and
;									// look at it for illumination.
;
;	//	arg9	maxCols; (1-31)		// # of columns to use (of the selected row in effects.bmp)
;	//	arg10	maxFx;	(0 - 100)	// # of trails to remember (increases the 'amount' of sparkles)
;	//	arg11	tixPerCol;			// step time between columns (in msec)
;									// controls the speed of the animation through the current row.
;	//	arg12	gravity;			<gravity>.<effects>.<weather>
;									Three 'dotted arguments' specifyng the gravity, special effects, and
;									weather present while the spell is being cast.  The format has changed,
;									but the values are the same as the old style:
;
;									// OLD STYLE ARG12 (pre-A57) was a 9 digit number AAABBBCCC
;									// gravity style controls 'dripping' behaviour(0 is none)
;
;					CCC
;					000	- no gravity
;					001	- magic falls slowly
;					002	- falls faster
;					003	- falls very fast
;					004   - falls way too fast
;					005	- magic RISEs slowly
;					006	- rises faster
;					007	- rises very fast
;					008	- rises way too fast
;
;					BBB
;					xxx	- FX modifier
;		
;					AAA
;					xxx - weather modifier
;			
;	//	arg13	loop;				// loop style (0 is none)
;
; these optional parameters control the SOUND of the spell
;
;	//  arg 14	sfxSummon[80];		// sound of spell being summoned	"summon.wav"
;	//  arg 15	sfxTravel[80];		// sound of spell traveling			"travel.wav"
;	//  arg 16	sfxStrike[80];		// sound of spell striking target	"strike,wav"

;
; SPELL ZERO
;
; There is no real spell zero, this line just sets some percentage tweaks which can be used to adjust the 
; algorithmically derived values.
;
; You can adjust: PP Cost, Damage (painful spells only), and MP Cost
;
	0,	"Algorithm Scale %",100,0,	100,100,0

;   	0	1				2	3	4	5	6	7		8		9	10	11	12	13		14			15				16
;	ID	Name				PP	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
; Earth Spells

	1,	"Dirt",				-1,	3,	0,	0,	0,		3,		63,	11,	80,	100,	0,		0,		"",			"",				""
	2,	"Small Stones",			-1,	3,	0,	0,	1,		0,		3,	11, 	120,	100,	0,		0,		"",			"",				""
	3,	"Paralyze",			-1,	3,	-11,	0,	2,		0,		7,	11,	80,	100,	0,		0,		"",			"",				""
	4,	"Rock",				-1,	3,	0,	0,	3,		3,		61,	11,	80,	100,	0,		0,		"",			"",				""
	5,	Sand,				-1,	3,	0,	0,	4,		203,		81,	11,	80,	100,	0.0.1055,0,		,			,			,			
	6,	"Rock All",			-1,	3,	0,	0,	105,		0,		61,	11,	80,	100,	0,		0,		"",			"",				""
	7,	Powder,				-1,	3,	0,	0,	6,		203,		60,	11,	80,	100,	0.3.3055,0,		,			,			,			
	8,	"Stun",				-1, 	3,	-4,	0,	7,		0,		23,	11,	80,	100,	0,		0,		"",			"",				""
	9,	"Boulder",			-1,	3,	0,	0,	8,		0,		62,	11,	80,	100,	0,		0,		"",			"",				""
	24,	"Quake",			-1,	3,	0,	0,	109,		0,		57,	11, 80, 100,	5000,	0,		"",			"",				""
;						-4			DISEASE_STUN		(cannot move, or attack)
;						-11			DISEASE_PARALYZE	(cannot move)
;
; Death Spells

;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	25,	"Bones",			-1,	4,	0,	0,	0,		3,		87,	11,	80,	100,	0,		0,		"",			"",				""
	26,	"Poison",			-1,	4,	-2,	0,	1,		0,		76,	11,	80,	100,	0,		0,		"",			"",				""
	27,	"Skulls",			-1,	4,	0,	0,	2,		203,	85,	11,	80,	100,	0,		0,		"",			"",				""
	28,	"Tomb",				-1,	4,	0,	0,	3,		203,	86,	11,	80,	100,	0,		0,		"",			"",				""
	29,	"Sap",				-1,	4,	-3,	0,	4,		203,	23,	11,	80,	100,	0,		0,		"",			"",				""
	30,	"Skulls All",			-1,	4,	0,	0,	105,	203,	85,	11,	80,	100,	0,		0,		"",			"",				""
	31,	"Numb",				-1,	4,	-6, 0,	6,		0,		7,	11, 120,100,	0,		0,		"",			"",				""
	32,	"Sleep",			-1,	4,	-7, 0,	7,		0,		26,	11, 120,100,	0,		0,		"",			"",				""
	33,	"Gravedigger",			-1,	4,	0,	0,	8,		203,	79,	11,	80,	100,	0,		0,		"",			"",				""
	49,	"Armageddon",			-1,	4,	0,	0,	109,	0,		74,	11, 80, 100,	5000,	0,		"",			"",				""
;						-2			DISEASE_POISON		(saps a little HP every few seconds)		
;						-3			DISEASE_SAP			(saps a little MP every few seconds)		
;						-6			DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-7			DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;
; Wind Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	51,	"Short Breath",			-1,	7,	0,  0,	0,		4,		4,	11, 80, 100,	0,		0,		"",			"",				""
	52,	"Breeze",			-1,	7,	0,	0,	1,		0,		12,	11, 80, 100,	5,		0,		"",			"",				""
	53,	"Breeze All",			-1,	7,	0,  0,	102,	0,		12,	11, 120,100,	6,		0,		"",			"",				""
	54,	"Wind",				-1,	7,	0,	0,	3,		0,		19,	11, 80, 100,	7,		0,		"",			"",				""
	55,	"Wind All",			-1,	7,	0,	0,	104,	0,		69,	11, 120,100,	8,		0,		"",			"",				""
	56,	"Sturm",			-1,	7,	0,	0,	5,		0,		21,	11, 80, 100,	7,		0,		"",			"",				""
	57,	"Sturm All",			-1,	7,	0,	0,	106,	0,		68,	11, 80, 100,	7,		0,		"",			"",				""
	58,	"Gale",				-1,	7,	0,	0,	7,		0,		77,	11, 80, 100,	7,		0,		"",			"",				""
	59,	"Hurricane",			-1,	7,	0,	0,	8,		0,		65,	11, 80, 100,	7,		0,		"",			"",				""
	74,	"Tempest",			-1,	7,	0,	0,	109,	0,		66,	11, 80, 100,	7,		0,		"",			"",				""
;
; Spirit Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	75,	"Whisper",			-1,	6,	0,	0,	0,		4,		70,	11, 80, 100,	0,		0,		"",			"",				""
	76,	"Stupefy",			-1,	6,	-21,0,	1,		0,		81,	11, 80, 100,	5,		0,		"",			"",				""
	77,	"Bid",				-1,	6,	0, 	0,	2,		0,		26,	11, 80, 100,	5,		0,		"",			"",				""
	78,	"Confuse",			-1,	6,	-8, 0,	3.2,	0,		25,	11, 80, 100,	5,		0,		"",			"",				""
	79,	"Enchant",			0,	6,	-9, 0,	4.1,	0,		83,	11, 80, 100,	5,		0,		"",			"",				""
	80,	"Mum",				-1,	6,	-5, 0,	5,		0,		78,	11, 80, 100,	5,		0,		"",			"",				""
	81,	"Ecto",				-1,	6,	0, 	0,	6,		0,		71,	11, 80, 100,	7,		0,		"",			"",				""
	82,	"Tame",				0,	6,	-10, 0,	7.1,	0,		80,	11, 80, 100,	5,		0,		"",			"",				""
	83,	"Ecto All",			-1,	6,	0,	0,	108,	0,		71,	11, 120,100,	8,		0,		"",			"",				""
	84,	"Slime",			-1,	6,	0,	0,	1,		4,		71,	11,	80,	100,	0,		0,		boom1.wav,	,			,			
	85,	"Curse All",			-1,	6,	0,	0,	103,	4,		73,	11,	80,	100,	0,		0,		,			,			,			
	86,	"Wil-o-wisp",			-1,	6,	0,	0,	4,		4,		72,	11,	80,	100,	0,		0,		,			,			,			
	87,	"Banshee Wail",			-1,	6,	0,	0,	105,	4,		75,	11,	80,	100,	0,		0,		,			,			,			
;	93,	"Summon Home",			-1,	6,	-300.99,0,7,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	94,	"Summon Specific",		-1,	6,	-203.44,0,7,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	95,	"Summon Same",			-1,	6,	-200,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	96,	"Summon Element",		-1,	6,	-201,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
;	97,	"Summon Random",		-1,	6,	-202,0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
	98,	"Poltergeist",			-1,	6,	0,	0,	7,		4,		76,	11,	80,	100,	0,		0,		,			,			,			
	99,	"Ethereal Fog",			-1,	6,	0,	0,	109,	0,		56,	11, 120,100,	8,		0,		"",			"",				""
;						-8			DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-9			DISEASE_CHARM		(once charmed, monster fights for you)	
;						-10			DISEASE_TAME		(taming a charmed monster makes it a pet)	
;						-5			DISEASE_GAG			(cannot case spells)(* wears off)		
;						-21			DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-200		SUMMON_SAME			Summon a monster of same type as caster
;						-201		SUMMON_SAME_ELEMENT	Summon a monster of the same element as caster
;						-202		SUMMON_RANDOM		Summon a random monster at or below level of caster (within 10 levels)
;						-300		SUMMON_ITEM			Summon an item to be used on the target (item id in dotteg arg)
;
; Fire Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	101,"Match Burn",			-1,	5,	0,  0,	0,		4000,	0,	11, 80, 100,	0,		0,		"",			"",				""
	102,"Fry",				-1,	5,	0,	0,	1,		5000,	9,	11, 80, 100,	1,		0,		"",			"",				""
	103,"Ignite",				-1,	5,	0,	0,	2,		0,		18,	11, 120,100,	0,		0,		"",			"",				""
	104,"Fry All",				-1,	5,	0,  0,	103,	6000,	9,	11, 120,100,	2,		0,		"",			"",				""
	105,"Fire Bubble",			-1,	5,	0,	0,	4,		7000,	14,	11, 80, 100,	3,		0,		"",			"",				""
	106,"Lava",				-1,	5,	0,	0,	5,		10000,	55,	11, 80, 100,	4,		0,		"",			"",				""
	107,"Scorch",				-1,	5,	0,	0,	6,		0,		24,	11, 120,100,	0,		0,		"",			"",				""
	108,"Lava All",				-1,	5,	0,	0,	107,	0,		55,	11, 120,100,	0,		0,		"",			"",				""
	109,"Crisp",				-1,	5,	0,	0,	8,		0,		58,	11, 120,100,	0,		0,		"",			"",				""
	124,"Inferno",				-1,	5,	0,	0,	109,	0,		89,	11, 120,100,	0,		0,		"",			"",				""
;
; Water Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	151,"Rain Drop",			-1,	1,	0,  0,	0,		8000,	52,	11, 80, 100,	0,		0,		"",			"",				""
	152,"Drown",				-1,	1,	0,	0,	1,		9000,	11,	11, 80, 100,	1000,	0,		"",			"",				""
	153,"Drown All",			-1,	1,	0,  0,	102,	10000,	54,	11, 120,100,	1000,	0,		"",			"",				""
	154,"Deluge",				-1,	1,	0, 	0,	3,		11000,	10,	11, 80, 100,	2000,	0,		"",			"",				""
	155,"Deluge All",			-1,	1,	0,	0,	104,	0,		53,	11, 120,100,	2000,	0,		"",			"",				""
	156,"Freeze",				-1,	1,	0,	0,	5,		0,		67,	11, 80, 100,	0,		0,		"",			"",				""
	157,"Blizzard",				-1,	1,	0,	0,	6,		0,		50,	11, 80, 100,	9003000,0,		"",			"",				""
	158,"Freeze All",			-1,	1,	0,	0,	107,	0,		67,	11, 80, 100,	0,		0,		"",			"",				""
	159,"Blitz",				-1,	1,	0,	0,	8,		0,		51,	11, 80, 100,	9003000,0,		"",			"",				""
	160,"Ice Age",				-1,	1,	0,	0,	109,	0,		13,	11, 80, 100,	9003000,0,		"",			"",				""
;
; Nature Spells
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	175,"Gel",				-1,	2,	0,  0,	0,		4000,	15,	11, 80, 100,	0,		0,		"",			"",				""
	176,"Grease",				-1,	2,	-24,0,	1,		5000,	81,	11, 80, 100,	1,		0,		"",			"",				""
	177,"Scry",				-1,	2,	0,	0,	2,		5000,	88,	11, 80, 100,	1,		0,		"",			"",				""
	178,"Spiders",				-1,	2,	-23,0,	3,		5000,	1,	11, 80, 100,	1,		0,		"",			"",				""
	179,"Stampede",				-1,	2,	0,  0,	104,	6000,	22,	11, 120,100,	2,		0,		"",			"",				""
	180,"Screech",				-1,	2,	-22,0,	5,		5000,	81,	11, 80, 100,	1,		0,		"",			"",				""
	181,"Fertilize",			-1,	2,	0, 	0,	6,		10000,	60,	11, 80, 100,	4,		0,		"",			"",				""
	182,"Unravel",				-1,	2,	-20,0,	7,		5000,	81,	11, 80, 100,	1,		0,		"",			"",				""
	183,"Solar Flare",			-1,	2,	0,	0,	8,		7000,	8,	11, 80, 100,	3,		0,		"",			"",				""
	184,"Bear Hug",				-1,	2,	0,	0,	1,		5000,	84,	11,	80,	100,	1,		0,		,			,			,			
	185,"Locusts",				-1,	2,	0,	0,	3,		5000,	80,	11,	60,	100,	1,		0,		,			,			,			
	186,"Tiger Claw",			-1,	2,	0,	0,	105,	5000,	89,	11,	80,	100,	1,		0,		,			,			,			
	187,"Lightning",			-1,	2,	0,	0,	7,		5000,	56,	11,	80,	100,	1,		0,		,			,			,			
	199,"Nova",				-1,	2,	0,	0,	109,	0,		59,	11, 120,100,	0,		0,		"",			"",				""
;						-20			DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-22			DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-23			DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-24			DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	
;
; Health Spells
;
; Note: arg4 (DAMAGE) is VERY IMPORTANT HERE.  It is NOT set automatically. Do NOT use 0 (zero)
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15				16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP		summon.wav	travel.wav		strike.wav
;
	201,"Health Weave",			0,	0,	50,	0,	0,		4000000,16,	11, 80, 100,	0,		0,		"",			"",				""
	202,"Unparalyze",			0,	0,-111,	0,	0,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
	203,"Antidote",				0,	0,-102, 0,	1,		0,		17,	11, 80, 100,	0,		0,		"",			"",				""
	204,"Raise Dexterity",			0,	0,-124,	0,	1,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
	205,"Raise Agility",			0,	0,-123,	0,	2,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
	206,"Stop Leak",			0,	0,-103,	0,	2,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
	207,"Heal",				0,	0, 100, 0,	3,		0,		5,	11, 80, 100,	0,		0,		"",			"",				""
	208,"Raise Stamina",			0,	0,-122,	0,	3,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
	209,"Un Gag",				0,	0,-105,	0,	4,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
	210,"Numb away",			0,	0,-106,	0,	4,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
	211,"Heal All",				0,	0, 500,	0,	105,	0,		6,	11, 120,100,	0,		0,		"",			"",				""
	212,"Awaken",				0,	0,-107,	0,	5,		0,		72,	11, 80, 100,	0,		0,		"",			"",				""
	213,"Deconfuse",			0,	0,-108,	0,	6,		0,		72,	11, 80, 100,	0,		0,		"",			"",				""
	214,"Raise Strength",			0,		0,-120,	0,	6,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
	215,"Un Stun",				0,	0,-104,	0,	7,		0,		73,	11, 80, 100,	0,		0,		"",			"",				""
	216,"Restore",				0,	0,3000, 0,	7,		0,		5,	11, 120,100,	0,		0,		"",			"",				""
	217,"Raise Wisdom",			0,	0,-121,	0,	8,		0,		84,	11, 80, 100,	0,		0,		"",			"",				""
	223,"Resurrect",			0,	0,	-1, 0,	8,		3,		82,	11, 120,100,	0,		0,		"",			"",				""
	224,"Restore All",			0,	0,3000, 0,	109,	3,		5,	11, 120,100,	0,		0,		"",			"",				""
;						-102			CURE DISEASE_POISON		(saps a little HP every few seconds)		(NOT WORKING YET)
;						-103			CURE DISEASE_SAP		(saps a little MP every few seconds)		(NOT WORKING YET)
;						-104			CURE DISEASE_STUN		(cannot move, or attack)
;						-105			CURE DISEASE_GAG		(cannot case spells)(* wears off)		
;						-106			CURE DISEASE_NUMB		(cannot use right-hand)(* wears off)
;						-107			CURE DISEASE_SLEEP		(cannot move, or attack)(* wears off)	
;						-108			CURE DISEASE_CONFUSE		(attacks may hit friends)(* wears off)	
;						-111			CURE DISEASE_FREEZE		(cannot move)
;						-120			CURE DISEASE_STRENGTH	(lowers strenth til end of scene) (cure raises it)
;						-121			CURE DISEASE_WISDOM		(lowers wisdom til end of scene) (cure raises it)	
;						-122			CURE DISEASE_STAMINA		(lowers stamina til end of scene) (cure raises it)	
;						-123			CURE DISEASE_AGILITY		(lowers agility til end of scene) (cure raises it)
;						-124			CURE DISEASE_DEXTERITY	(lowers dexterity til end of scene) (cure raises it)	

; Special Element Spells
; These are generally for the use of special element monsters who can only cast within their own special element
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	 summon.wav	travel.wav	strike.wav
;
	600,"Wyrm Breath",		-1,	100,0,	0,	4,		3,		75,	11,	80,	100,	0,		0,		chant1.wav,	,			,			
	601,"More Eyes",		-1,	254,-203.132,0,6,	4,		76,	11,	80,	100,	0,		0,		,			,			,			
	
; Book Spells
; These are generally spellbound to book-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	 summon.wav	travel.wav	strike.wav
;
	700	"Book Ichi",		-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	701	"Book Ni",			-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	702	"Book San",			-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""
	703	"Book Shi",			-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"chant4.wav",	"",	""
	704	"Book Roku",		-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"chant5.wav",	"",	""
	705	"Book Hachi",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"chant6.wav",	"",	""
	706	"Book Mitsu",		-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"chant7.wav",	"",	""
	707	"Book Chi",			-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"chant1.wav",	"",	""
	708	"Book Ba",			-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"chant2.wav",	"",	""
	709	"Book Cho",			-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"chant3.wav",	"",	""

; Summoning Spells
; These are generally spellbound to spirit-class things, and cannot be learned otherwise
;
;   0	1					2	3	4	5	6		7		8	9	10	11		12		13		14			15			16
;	ID	Name				PP 	ELE	DAM	MP	LEV		PTH		ROW	COL	*s	ms		GRV		LOOP	summon.wav	travel.wav		strike.wav
;
	750	"Summon",			-1,	14,	0,	0,	0,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	751	"Summon Ash",		-1,	14,	0,	0,	1,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	752	"Summon Ember",		-1,	14,	0,	0,	2,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	753	"Summon Coral",		-1,	14,	0,	0,	3,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	754	"Summon Spirit",	-1,	14,	0,	0,	4,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	755	"Summon Atmos",		-1,	14,	0,	0,	5,		3,		28,	11,	80,	100,	0,		0,		"",			"",				""
	756	"Summon Chronos",	-1,	14,	0,	0,	6,		3,		29,	11,	80,	100,	0,		0,		"",			"",				""
	757	"Summon Pragma",	-1,	14,	0,	0,	7,		3,		30,	11,	80,	100,	0,		0,		"",			"",				""
	758	"Summon Arcana",	-1,	14,	0,	0,	8,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""
	759	"Summon Ethos",		-1,	14,	0,	0,	9,		3,		27,	11,	80,	100,	0,		0,		"",			"",				""


;
; Be sure to leave a blank line at the end of this file
