SCENE 1600, wauworldspinegate
#include banthing.txt
IF T601, @throughgate
IF C1, @gatefight2
IF C2, @gatefight2
IF C4, @gatefight2
IF C6, @gatefight2
IF C18, @gatefight2
IF C19, @gatefight2
IF C22, @gatefight2
IF C30, @gatefight2
IF C32, @gatefight2
@gatefight
ACTOR 1, "Gate Guard", skeletalwarrior, 1, 40, 95, 2
1: None shall pass to the lich king's territory!
H: I, %1, will kick your lord's old bones back to the grave he came out from!
1: I see, a tough one, then? Guards, protect the lich king!
MOVE 1, -50, 90, 1
THEME 53
FIGHT 420, 421, 421, 422, 422, 423, 423, 422, 423, 421,
IF WON, @gate
THEME 70
END
@gate
THEME 71
H: Yeah I AM tough. Atleast tougher than you.
HOST_GIVE T601
GOTO @throughgate
END
@gatefight2
ACTOR 1, "Gate Guard", skeletalwarrior, 1, 40, 95, 2
1: None shall pass to the lich king's territory!
H: Me, the in-infamous %1, will kick you lord's dusty old bones back to the-the grave!
1: Bold words.. For an imbecil.
H: Grr, bring it mr. bones!
MOVE 1, -50, 90, 1
THEME 53
FIGHT 420, 421, 421, 422, 422, 423, 423, 422, 423, 421,
IF WON, @gate2
THEME 70
END
@gate2
THEME 71
H: Yes, that teaches them to call me im-im-imbecil.
HOST_GIVE T601
GOTO @throughgate
END
@throughgate
GOTO LINK 62, 7
END

SCENE 1601, wauworldspinethrone
#include banthing.txt
IF T602, @nothi
ACTOR 1, "Lich King", lich, 1, 50, 70, 2
1: Come, join the dead..
H: Eek, fight me foul being and die!
WAIT 2
1: More souls to devourr...
1: Ssoon my army will be complete..
1: Sssome I'll leave as servantss but..
1: othersss soulss I will claim and gain their powerr..
1: You're powerful.. Your power will be of much use to me..
WAIT 1
MOVE 1, -30, 90
WAIT 10
THEME 52
FIGHT 424, 425, 425, 423, 423, 421, 421,
IF WON, @itsover 
THEME 70
N: Fwhahaha, join the dead..
END
@itsover
THEME 71
N: It is over. The Lich King is dead.
HOST_GIVE T602
GIVE I1282
END
@nothi
N: There's nothing left here anymore.
END

SCENE 1605, waucathedral
#include banthing.txt
;the hall of credits
	ACTOR	1,	"Queek da developer", queek, 1, 55, 88
	ACTOR 	2,  	"Lammy da tester", stormvermin, 1, 25, 85
	ACTOR	3,	"Madge da scripter", elf, 3, 55, 75
	ACTOR	4,	"Joodog da scripter", chaosorc, 1, 60, 100
	ACTOR	5,	"Rune da tester", birdman, 1, 35, 75, 13
	ACTOR	6,	"Rogue da tester", ratogre, 1, 20, 100
	ACTOR	7,	"Queek's pet", gnoll, 1, -10, 100
	ACTOR	8,	"Dragonmage da tester", dragon, 1, 68, 95
	ACTOR	9,	"Saberslash da tester", doomguard, 1, 63, 78
	FACE 1, 0	
	FACE 3, 0
	FACE 4, 0
	FACE 8, 0
	FACE 9, 0
	SEL 1
	WAIT	1.0
1: Challenge-challenge!
2: lol!
4: hey Queek!
5: What the heck is this? I'm trapped in the same room with HIM!?
8: ...Why am I a dragon?
2: lol
1: Errm.. DRAGONmage. See, perfectly logical.
8: ...
9: why am i looking like this, then..
1: Err. No be blaming me, it wasn't my idea!
9: bitch..
1: Blargh, I knew that was coming.
6: yay queek go you. you should've added houses to everyone!
1: I already did, STFU!
2: -.-
1: Lammy, you use lol way too much.
2: lol
1: See what I mean?
2: lol, no.
1: Fine. >.<
4: hey queek i think i found a couple more bugs.
1: What is it now?
6: GO HOUSES!!
1: Rogue. Shut up.
6: never.
4: as i was gonna say..
1: You know what? I'm TIRED of fixing bugs!
6: i want a house!
1: ...
5: Give me a free house!
1: No. I'm lazy.
2: lol
1: Madge, is there any way to make them disappear..
3: lemme check
1: Kay.
3: uhhh.. I don't think so.
2: lol
1: Goddamn you're annoying!
6: free houses for all!!
1: As I said already, I will not add Free houses!
6: queek you're unfair
1: I am? Yayyy.
1: Hey, I know what to do..
1: Onysablet, come!
WAIT 1
N: Queek summons pet.
MOVE 7, 43, 90
WAIT 10
1: Onysablet, bite #2!
WAIT 1
7: Wargh, grrbblll?
WAIT 1
MOVE 7, 48, 92
WAIT 5
N: Onysablet looks puzzled.
WAIT 2
SEL 7
POSE 2
SOUND footman2
WAIT 2
1: Oww what the heck!
WAIT 2
SEL 1
POSE 2
1: Dieee incompetent pet!
SOUND demonhunter5
WAIT 2
SOUND roarpain1
MOVE 7, -50, 100, 1
WAIT 1
SEL 1
POSE 1
2: lol!
5: Serves 'em right.
6: free houses!
1: ...
END

;comments are boring

