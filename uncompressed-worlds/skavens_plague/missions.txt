SCENE 46
;Skaven mission scene
#include banthing.txt
ACTOR 1, "Longwhisker the Crusher", skaven3, 1, 20, 98
ACTOR 2, "Soldier of Fortune", mercenary, 4, 56, 95
ACTOR 3, "Dark Elf", drowranger, 1, 33, 90
ACTOR 4, "Soldier of Fortune", elf, 3, 68, 95
ACTOR 5, "Campfire", danNoFloat, 1, 46, 95
ACTOR 6, "Guard", skaven1, 1, 15, 95
ACTOR 7, "Guard", fatgoblin, 1, 86, 90
FACE 4, 0
FACE 7, 0
SEL 5
POSE 1,2,3
1: You-you are seeking missions, right?
1: Click the QUEST-button to check what-what
1: missions are-are available to you.
MISSIONS 1, 2, 3, 4, 5,
2: Cheers, money!
2: I'm gonna head to Sturmwind and drink 
2: until I have nothing but a copper left!
4: ..But I need to buy some arrows!
4: ..Bah, drinks all around!
3: Humans are nothing but drunkards.
3: Useless, the whole lot of them.
2: Please go cry some more, elfling!
3: ...
7: Ho ho ho.. He he..
3: Shut up, goblin.
7: he.. Eh.
6: Boss, me thinks they are-are going to fight!
1: Not in my-my camp! Stop it at-at once!
7: Sure thing, boss. He he.
3: ..
3: ...
3: Imbecile!
WAIT 3
6: Tsk.. Tsk.. Hee hee!
FACE 3, 0
3: What is it now?!
6: Elf look funny when angry!
3: I'd toss you off a cliff were your boss not here!
6: Tee hee, right. I-I stick this sword to your belly first!
MOVE 1, 25, 105
WAIT 1
SEL 6
POSE 2
1: Heyy, watch what you-you are waving that thing at!
6: Oop, sorry boss.
POSE 1
MOVE 1, 20, 98
WAIT 2
1: Apology is-is accepted!
POSE 2
SOUND monsterpain5.wav
WAIT 2
POSE 1
FACE 1, 1
END