;----------MISCELLENEOUS + TRAVS JUNCTION + SOUTH DOWNS-------------------------- 
; Easthaven West Gate from Trav's J
SCENE 10 		gate2
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	COLOR	13
	COLOR	2014
	@day
	THEME		1
	WEATHER	0
	WAIT	1.5
	SOUND	"lockDoor1.wav"
	WAIT	1.5
	GOTO	LINK	26, 0
END
;------------------------------------------------
; Well Ticket day/night checker
SCENE 11 		weird9
	WAIT	0.1
	SOUND	"magic9.wav"
	BKGND "weird8.jpg"
	WAIT	0.1
	SOUND	"magic8.wav"
	WAIT	0.1
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	GOTO	LINK	9, 4, 1	
END
	@day
	GOTO	LINK	7, 4, 1
END
;------------------------------------------------------
; Horse Trader's Outpost
 SCENE 12	outpost1
	ACTOR	1,	"Horse Trader", actors4, 0, 10, 100
	POSE	0, 1
	ACTOR	2,	"Stallion", horse1, 1, 20, 90
	POSE	1, 4, 2
	ACTOR	3,	"Mere", horse2, 1, 35, 85
	POSE	1, 4
	IF	ALIVE, @alive
END
	@alive
	WAIT	1.5
	1: Hello, I sell horses for 10,000 coins.
	1: Click me for more info.
END
	@eventActorClick1
	IF	DEAD, @dead
	MENU	"Why are horses so special?=@special", "I'd like to buy a horse, please.=@buy",  
END
	@special
	H: Why are horses so special?
	1: Well, not only do they operate like any other pet
	1: ..you can also 'equip' your horse and ride it!
	1: This allows you to travel much faster.
	1: Be aware however: 
	1: If you RELEASE your pet horse you will lose the speed.
END
	@buy
	H: I'd like to buy a horse, please.
	1: Oh, ok.
	IF	G10000, @pay
	H: Oops, I don't have enough money.
	1: Well come back any time.
END
	@pay
	WAIT	3
	N: You pay 10,000 coins.
	HOST_TAKE 	G10000
	WAIT	3
	1: Would you like the brown or the white?
	ASK	20
	IF	Qbrown, @brown
	IF	Qwhite, @white
	1: Hmm.. sorry I'm rather busy.
	1: Here's your money back.
	HOST_GIVE	G10000
	SOUND	"bell2.wav"
	MOVE	1, -15, 100
END
	@brown
	1: No problem, #<g.sir>.
	HOST_GIVE	I1015
	HOST_GIVE	I719
	MOVE	2, -25, 100, 1
	SOUND	"bell2.wav"
END
	@white
	1: No problem, #<g.sir>.
	HOST_GIVE	I1016
	HOST_GIVE	I719
	MOVE	3, -25, 100, 1
	SOUND	"bell2.wav"
END
	@dead
END	
;------------------------------------------------
; The Big Smoke North Gate
SCENE 13  entrance2
	THEME		3
	WEATHER	0
	PARTY T200
	GOTO	SCENE 14
END
;-----------------------------------------------
; The Big Smoke North Gate Guards
SCENE 14  entrance2
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	COLOR	13
	COLOR	2014
	@day
	THEME		3
	WEATHER	0
	ACTOR	1,	"First Gate Guard", cop1, 1, 25, 98
	POSE	1, 2
	ACTOR	2,	"Second Gate Guard", cop1, 1, 71, 98
	FACE	2, 0
	POSE	1, 2
	WAIT	2
	IF	ALIVE, @alive
	1: No admittance to DEAD persons!
	2: Ha! Loser..
	GOTO	EXIT
END
	@alive
	IF	V20, @eventActorClick1
	1: Hmm, what do we have here then?
	H: Just a %C.
	1: Hahaha!
	2: Just a %L, more like!
	1: Shoo, little insect!
	1: The big city ain't the sort of place for blips like you!
	H: Hey!
	@eventActorClick1
	@eventActorClick2
	MENU	"Can I enter the city?=@enter", "You suck!=@insult" 
END
	@enter
	H: Can I enter the city?
	1: Entry costs 1000 coins.
	1: Comin' in?
	ASK	15
	IF	YES, @cash
	1: Stand there then.
END
	@cash
	WAIT	1.5
	IF	G1000, @entry
	H: Actually, I can't afford it..
	1: Then don't waste our time.
	2: Go away, sucker!
	WAIT	1.5
	GOTO EXIT
END
	@entry
	TAKE	G1000
	1: Have a nice day.
	@which
	COMPARE #<num.hostBravery>, "1000"
	IF< @coward
	IF	R25, @nuffin
	IF	R35, @yawn
	IF	R45, @ugly
	IF	R50, @strange
	IF	R55, @smell
	IF	R60, @wearing
	GOTO	@which
END
	@nuffin
	WAIT	1.5
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@yawn
	2: *yawn*	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@ugly
	2: Man, #<g.he> was an ugly one..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@strange
	2: There ain't half some strange looking people around..
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@smell
	2: Pfwer! Did you smell that #<g.guy>?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@wearing
	2: Blimey! What WAS #<g.he> wearing?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@coward
	2: Ya' know that #<g.guy> was %2, a well known coward who runs from monster fights..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 0
END
	@insult
	H: You suck!
	2: Grr..
	@eventActorAttack1
	@eventActorAttack2
	MOVE	1,	-25, 98, 1
	MOVE	2,	-71, 98, 1
	FIGHT	110, 110
	IF	WIN, @beatTheGuards
	WAIT	2
	MOVE	1,	25, 98, 1
	MOVE	2,	71, 98, 1
	FACE	2, 0
	1: Hahaha!
	2: Shouldn't have messed with the best!
END
	@beatTheGuards
	IF	R20, @jail
	WAIT	2
	N: You open the gate and enter the city.
	WAIT	2
	GOTO	LINK	10, 0
END
	@jail
	N: WHACK!
	N: You regain consciousness on a hard stone floor..
	GOTO	LINK	43, 0
END
;------------------------------------------------
; The Big Smoke South Gate
SCENE 15  entrance2
	THEME		3
	WEATHER	0
	PARTY T200
	GOTO	SCENE 16
END
;-----------------------------------------------
; The Big Smoke South Gate Guards
SCENE 16  entrance2
	THEME		3
	WEATHER	0
	ACTOR	1,	"First Gate Guard", cop1, 1, 25, 98
	POSE	1, 2
	ACTOR	2,	"Second Gate Guard", cop1, 1, 71, 98
	FACE	2, 0
	POSE	1, 2
	WAIT	2
	IF	ALIVE, @alive
	1: No admittance to DEAD persons!
	2: Ha! Loser..
	GOTO	EXIT
END
	@alive
	IF	V20, @eventActorClick1
	1: Hmm, what do we have here then?
	H: Just a %C.
	1: Hahaha!
	2: Just a %L, more like!
	1: Shoo, little insect!
	1: The big city ain't the sort of place for blips like you!
	H: Hey!
	@eventActorClick1
	@eventActorClick2
	MENU	"Can I enter the city?=@enter", "You suck!=@insult" 
END
	@enter
	H: Can I enter the city?
	1: Entry costs 1000 coins.
	1: Comin' in?
	ASK	15
	IF	YES, @cash
	1: Stand there then.
END
	@cash
	WAIT	1.5
	IF	G1000, @entry
	H: Actually, I can't afford it..
	1: Then don't waste our time.
	2: Go away, sucker!
	WAIT	1.5
	GOTO EXIT
END
	@entry
	TAKE	G1000
	1: Have a nice day.
	@which
	COMPARE #<num.hostBravery>, "1000"
	IF< @coward
	IF	R25, @nuffin
	IF	R35, @yawn
	IF	R45, @ugly
	IF	R50, @strange
	IF	R55, @smell
	IF	R60, @wearing
	GOTO	@which
END
	@nuffin
	WAIT	1.5
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@yawn
	2: *yawn*	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@ugly
	2: Man, #<g.he> was an ugly one..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@strange
	2: There ain't half some strange looking people around..
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@smell
	2: Pfwer! Did you smell that #<g.guy>?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@wearing
	2: Blimey! What WAS #<g.he> wearing?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@coward
	2: Ya' know that #<g.guy> was %2, a well known coward who runs from monster fights..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 24
END
	@insult
	H: You suck!
	2: Grr..
	@eventActorAttack1
	@eventActorAttack2
	MOVE	1,	-25, 98, 1
	MOVE	2,	-71, 98, 1
	FIGHT	110, 110
	IF	WIN, @beatTheGuards
	WAIT	2
	MOVE	1,	25, 98, 1
	MOVE	2,	71, 98, 1
	FACE	2, 0
	1: Hahaha!
	2: Shouldn't have messed with the best!
END
	@beatTheGuards
	IF	R20, @jail
	WAIT	2
	N: You open the gate and enter the city.
	WAIT	2
	GOTO	LINK	10, 24
END
	@jail
	N: WHACK!
	N: You regain consciousness on a hard stone floor..
	GOTO	LINK	43, 0
END
;------------------------------------------------
; The Big Smoke West Gate
SCENE 17  entrance2
	THEME		3
	WEATHER	0
	PARTY T200
	GOTO	SCENE 18
END
;-----------------------------------------------
; The Big Smoke West Gate Guards
SCENE 18  entrance2
	THEME		3
	WEATHER	0
	ACTOR	1,	"First Gate Guard", cop1, 1, 25, 98
	POSE	1, 2
	ACTOR	2,	"Second Gate Guard", cop1, 1, 71, 98
	FACE	2, 0
	POSE	1, 2
	WAIT	2
	IF	ALIVE, @alive
	1: No admittance to DEAD persons!
	2: Ha! Loser..
	GOTO	EXIT
END
	@alive
	IF	V20, @eventActorClick1
	1: Hmm, what do we have here then?
	H: Just a %C.
	1: Hahaha!
	2: Just a %L, more like!
	1: Shoo, little insect!
	1: The big city ain't the sort of place for blips like you!
	H: Hey!
	@eventActorClick1
	@eventActorClick2
	MENU	"Can I enter the city?=@enter", "You suck!=@insult" 
END
	@enter
	H: Can I enter the city?
	1: Entry costs 1000 coins.
	1: Comin' in?
	ASK	15
	IF	YES, @cash
	1: Stand there then.
END
	@cash
	WAIT	1.5
	IF	G1000, @entry
	H: Actually, I can't afford it..
	1: Then don't waste our time.
	2: Go away, sucker!
	WAIT	1.5
	GOTO EXIT
END
	@entry
	TAKE	G1000
	1: Have a nice day.
	@which
	COMPARE #<num.hostBravery>, "1000"
	IF< @coward
	IF	R25, @nuffin
	IF	R35, @yawn
	IF	R45, @ugly
	IF	R50, @strange
	IF	R55, @smell
	IF	R60, @wearing
	GOTO	@which
END
	@nuffin
	WAIT	1.5
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@yawn
	2: *yawn*	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@ugly
	2: Man, #<g.he> was an ugly one..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@strange
	2: There ain't half some strange looking people around..
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@smell
	2: Pfwer! Did you smell that #<g.guy>?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@wearing
	2: Blimey! What WAS #<g.he> wearing?	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@coward
	2: Ya' know that #<g.guy> was %2, a well known coward who runs from monster fights..	
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	10, 21
END
	@insult
	H: You suck!
	2: Grr..
	@eventActorAttack1
	@eventActorAttack2
	MOVE	1,	-25, 98, 1
	MOVE	2,	-71, 98, 1
	FIGHT	110, 110
	IF	WIN, @beatTheGuards
	WAIT	2
	MOVE	1,	25, 98, 1
	MOVE	2,	71, 98, 1
	FACE	2, 0
	1: Hahaha!
	2: Shouldn't have messed with the best!
END
	@beatTheGuards
	IF	R20, @jail
	WAIT	2
	N: You open the gate and enter the city.
	WAIT	2
	GOTO	LINK	10, 21
END
	@jail
	N: WHACK!
	N: You regain consciousness on a hard stone floor..
	GOTO	LINK	43, 0
END
;------------------------------------------------
; Cityport Main Gate
SCENE 19  entrance2
	THEME		3
	WEATHER	0
	PARTY T200
	GOTO	SCENE  20
END
;-----------------------------------------------
; Cityport Main Gate Guards
SCENE 20  entrance2
	THEME		3
	WEATHER	0
	ACTOR	1,	"First Bouncer", KoF2k-Seth,	1, 25, 100
	POSE	1, 4
	ACTOR	2,	"Second Bouncer", KoF2k-Seth,	1, 69, 101
	FACE	2, 0
	WAIT	2
	IF	ALIVE, @alive
	1: No admittance to DEAD persons!
	2: Ha! Loser..
END
	@alive
	IF	V30, @eventActorClick1
	1: What do we have here then, hmm?
	2: Heh, a %L I reckon!
	1: Go find some dung to play on, lil' fly!
	1: Cityport ain't for specks like you!
	H: Oh? 
	@eventActorClick1
	@eventActorClick2
	MENU	"Can I enter the city?=@enter", "Damn, your mothers must be ugly!=@insult" 
END
	@enter
	H: Can I enter the city?
	@loop
	IF	R50, @1000
	IF	R50, @800
	IF	R50, @700
	IF	R50, @500
	GOTO	@loop
END
	@1000
	1: Entry into Cityport is subject to a fee of 800 coins.
	2: Heh, make that 1000 coins!
	1: Are you willing to pay?
	ASK	15
	IF	YES, @cash1
	WAIT	1.5
	H: Actually, you can stick it.
	H: I won't be taken as a fool by you two %3's!
	1: Uh?
	2: Wha?
	H: YOU 'eard!
	WAIT	1.5
	GOTO	EXIT
END
	@cash1
	WAIT	1.5
	IF	G1000, @take1
	H: Oh, I can't afford it..
	1: Time wasting %3!
	2: Get away, %3!
	WAIT	1.5
	GOTO EXIT
END
	@take1
	TAKE	G1000
	GOTO	@comment
END
	@800
	1: Entry into Cityport is subject to a fee of 600 coins.
	2: Heh, make that 800 coins!
	1: Are you willing to pay?
	ASK	15
	IF	YES, @cash2
	2: LOSER!
	1: *kick*
	GOTO EXIT
END
	@cash2
	WAIT	1.5
	IF	G800, @take2
	H: Oh, I can't afford it..
	1: Time wasting %3!
	2: Get away, %3!
	WAIT	1.5
	GOTO EXIT
END
	@take2
	TAKE	G800
	GOTO	@comment
END
	@700
	1: Entry into Cityport is subject to a fee of 500 coins.
	2: Heh, make that 700 coins!
	1: Are you willing to pay?
	ASK	15
	IF	YES, @cash3
	WAIT	1.5
	H: Nah, shove it!
	WAIT	1.5
	GOTO	EXIT
END
	@cash3
	WAIT	1.5
	IF	G700, @take3
	H: Oh, I can't afford it..
	1: Time wasting %3!
	2: Get away, %3!
	WAIT	1.5
	GOTO EXIT
END
	@take3
	TAKE	G700
	GOTO	@comment
END
	@500
	1: Entry into Cityport is subject to a fee of 300 coins.
	2: Heh, make that 500 coins!
	1: Are you willing to pay?
	ASK	15
	IF	YES, @cash4
	1: Fair enough.
END
	@cash4
	WAIT	1.5
	IF	G500, @take4
	H: Oh, I can't afford it..
	1: Time wasting %3!
	2: Get away, %3!
	WAIT	1.5
	GOTO EXIT
END
	@take4
	TAKE	G500
	GOTO	@comment
END
	@comment
	1: Ooook..
	@which
	IF	R50, @nuffin
	IF	R50, @yawn
	IF	R50, @more
	IF	R50, @morris
	GOTO	@which
END
	@nuffin
	WAIT	1.5
	GOTO	@entry
END
	@yawn
	2: *yawn*
	GOTO	@entry
END
	@more
	2: Heh. What a pushover!
	2: Next time we should do #<g.him> for more..
	WAIT	1.5	
	GOTO	@entry
END

	@morris
	2: How long before Morris gets back from the clinic?
	1: Erm.. 
	GOTO	@entry
END
	@entry
	SOUND	"lockDoor1.wav"
	WAIT	1.0
	GOTO	LINK	24, 0
END
	@insult
	H: Damn, your mothers must be ugly!
	2: Grr..
	@eventActorAttack1
	@eventActorAttack2
	MOVE	1,	-25, 98, 1
	MOVE	2,	-71, 98, 1
	FIGHT	130, 130
	IF	WIN, @beatTheGuards
	WAIT	2
	MOVE	1,	25, 98, 1
	MOVE	2,	71, 98, 1
	FACE	2, 0
	1: Hahaha!
	2: Shouldn't have messed with the best!
END
	@beatTheGuards
	WAIT	2
	N: You open the gate and enter the city.
	WAIT	2
	GOTO	LINK	24, 0
END
;------------------------------------------------
; Macdonald's Farm
SCENE 21		entrance5
	THEME	8
	WEATHER 0
	MUSIC	"Happydays.mid"
	ACTOR	1,	"Slack Alice", joshTownsfolk, 23, 3, 100
	MOVE	1,	30, 90
	ACTOR 	2,	"Jumbo", Farmer~2, 1, 90, 98
	POSE	1, 2, 4
	ACTOR 	3,	"Mumbo", Farmer~2, 1, 80, 90
	POSE	2, 3, 4
	ACTOR 	4,	"", joshAnimals, 16, 35, 100
	POSE	16, 8
	ACTOR	5,	"", joshAnimals, 23, 5, 98
	POSE	23, 26
	ACTOR	6,	"", joshAnimals, 18, 15, 100
	ACTOR	7,	"",	petKDH, 1, 10, 93
	POSE	1,3,2
	@loop
	IF	R50, @chicks
	IF	R50, @pigs
	GOTO	@loop
END
	@chicks
	ACTOR	8,	"",	petKDH, 1, 20, 95
	POSE	1,3,2
	ACTOR	9,	"",	petKDH2, 1, 25, 98
	POSE	1,3,2
	IF	ALIVE, @alive
	1: Urg! You're dead!
END
	@pigs
	ACTOR	8,	"", joshAnimals, 24, 45, 98
	ACTOR	9,	"", joshAnimals, 10, 55, 100
	IF	ALIVE, @alive
	1: Urg! You're dead!
END
	@alive
	IF	T1	@shop
	5: WOOF! WOOF!
	1: Shuddup Einstein!
	1: Who the devil are YOU, standing there?
	1: Ugh! It's DISGUSTING!
	4: Baa-aa!
	1: Do you smell THAT?
	H: Erm..
	1: YOU should go to The Traveller's Rest near the well.. 
	5: WOOF! WOOF!
	1: Shuddup Einstein!
	WAIT	2
	GOTO	EXIT
END
	@shop
	5: rrrrrrrrr WOOF !
	5: woof WOOF !!
	1: QUIET Einstein!
	5: rrrr ...
	1: Old Macdonald ain't here.
	7: Cheap!
	1: He's gone to The Smoke to pick up supplies ..
	5: RR rrrrrrr!
	1: I'M looking after the place.
	1: I have a lot to do.
	4: Baahaaa !
	7: Cheap !
	5: rrrrr RRRRRR !!!
	1: QUIET EINSTEIN !!
	1: Well, I can't stand here all day..
	5: Wowowow ! rrrRR WOOF !!
	1: Einstein, I'm gonna KICK ya!
	1: Ok boys, look after the shop will ya?
	MOVE	1, -20, 105
	WAIT	3
	MOVE	4, -20, 105
	MOVE	5, -20, 105
	MOVE	7, -20, 105
	MOVE	8, -20, 105
	MOVE	9, -20, 105
	3: Sure Ma.
	OFFER	3, 10, 13, 14, 29, 1004
	2: Hyuk, hyuk..
END
;---------------------------------------------------
; Fairie Ring
SCENE 22	 stones8
	THEME	8
	ACTOR	1,	"Fairie", Sinful_Nina, 1, 70, 65
	POSE	1, 4
	ACTOR	2,	"Fairie", Sinful_Nina, 1, 25, 65
	POSE	1, 4
	ACTOR	3,	"Fairie", Sinful_Nina, 1, 50, 85
	POSE	1, 4
	ACTOR	4,	"Fairie", Sinful_Nina, 1, 50, 50
	POSE	1, 4
	MOVE	1, 45, 115
	MOVE	2, 75, 115
	MOVE	3, 55, -15
	MOVE	4, -15, 75
	WAIT	1
	MOVE	1, 115, 65, 1
	WAIT	1
	MOVE	2, -25, 65, 1
	WAIT 	0
	MOVE	3, -25, 65, 1
	WAIT 	0.5
	MOVE	4, -25, 65, 1
	SOUND	"thunder1.wav"
	GIVE	L1
	GIVE	H5000
	GIVE	M5000
END
;-----------------------------------------
; Hector's Ghost
SCENE 23	ruins1
	THEME	3
	IF	T38, @dead
	ACTOR	1,	"Hector's Ghost", PommasGhost, 22, -15, 85
	POSE	1, 2, 3
	ACTOR	2,	"", joshMiscellaneous, 22, 50, 100
END
	@eventActorClick2
	IF	DEAD, @dead
	MENU	"Examine..=@examine"
END
	@examine
	WAIT	2
	N: These bones look old..
	WAIT	3
	SOUND	"ghost2.wav"
	MOVE	1, 25, 90, 1
	1: Who disturbs my place of unrest?
	H: I'm.. %2.
	1: Ah, %C..
	1: Leave me be!
	H: I didn't mean to interfere with you.
	1: Then leave my place of unrest!
	H: But why so restless?
	1: Do you really want to hear my story?
	ASK	15
	IF	YES, @yes1
	SOUND	"ghost2.wav"
	1: Then leave me be I tell you!
	WAIT	7
	SOUND	"ghost2.wav"
	1: Go now!
	WAIT	6
	SOUND	"ghost2.wav"
	1: LEAVE before it's too late!
	WAIT	5
	SOUND	"ghost2.wav"
	@fight
	MOVE	1, -15, 90, 1
	FIGHT2	136, 136, 136, 136, 136, 136, 136, 136, 136, 136
END
	@yes1
	1: This, %2, is Hector's lamentable story:
	SOUND	"ghost2.wav"
	1: My Adromache.. she was heavy with child.
	1: We set up home in the city.. the new city.. 
	1: They wanted to prevent it from being built.
	H: Who?
	H: Who wanted to prevent the building of a new city?
	1: The western hordes.. they feared for the future.
	1: I was charged with the command of a thousand fighting men.
	1: Our mission: to build a fortress from where we could intercept and strike at the enemy.
	1: Before departing the new home I made a promise.
	1: Oh, how ill-judged it was!
	SOUND	"ghost2.wav"
	1: I promised to return or I should never rest..
	1: But, how we waged war!
	1: And the city walls grew day by day.
	1: A year and a day..
	1: Time just slips away.
	1: Alas..
	SOUND	"ghost2.wav"
	1: Some evil charm was cast.
	1: A hideous undead force came down on the gate..
	1: The north gate of the city!
	1: Our families!
	1: I sent eight hundred men to hold the gate.
	1: A mere two hundred men we were then!
	1: For two weeks we were beseiged
	1: ..from the mountains, of course.
	1: We were breached.. on they came..
	SOUND	"ghost2.wav"
	1: Ha! We were too few!
	1: Hacked and slewn to the bloody earth!
	1: I shall not say more.
	1: I saw a vision of Andromache..
	SOUND	"ghost2.wav"
	1: Then nothing.. drifting..
	1: Now, %2, do you see? 
	WAIT	3
	MENU	"That's a sad story.=@sadStory", "What if your bones were placed beside Andromache's?=@takeBones", "Well, it's your own stupid fault for promising to return=@stupidFault"
END
	@sadStory
	H: That's a sad story.
	1: Yes, so please understand I wish to be left alone.
	WAIT	3
	SOUND	"ghost2.wav"
	MOVE	1, -15, 90, 1
END
	@takeBones 
	H: What if your bones were placed beside Andromache's?
	H: Your soul.. may be set free.
	1: You are a smart #<g.guy> %2.
	1: However, how would you find her graveside?
	H: Confirm to me: which city was it you lived in?
	H: Tell me the name.
	1: At the time they called it Newhaven, named after Easthaven.
	1: It was going to be a BIG place.
	1: It was going to be bigger than Easthaven and Cityport.
	H: I know of no city called "Newhaven".
	1: Then the name must have changed for some reason.
	1: It was southwest of Easthaven, north of Cityport.
	1: Please, take my bones..
	1: Take them to her..
	1: Save my soul from this torment!
	1: Would you do that?
	ASK	15
	IF	YES, @yes2
	H: I think it's best I don't interfere.
	1: Then please, LEAVE ME BE!
	SOUND	"ghost2.wav"
	WAIT	3
	MOVE	1, -15, 90, 1
END
	@yes2
	H: I shall try my best, Hector.
	1: Then take my bones and good luck!
	1: Perhaps I CAN rest in peace..
	SOUND	"ghost2.wav"
	WAIT	3
	MOVE	1, -15, 90, 1
	WAIT	2
	N: You take Hector's bones.
	MOVE	2, -15, 90, 1
	SOUND	"bell2.wav"
	HOST_GIVE	I78
	HOST_GIVE	T38
END
	@stupidFault
	H: Well, it's your own stupid fault for promising to return.
	WAIT	3
	SOUND	"ghost2.wav"
	1: You, %2, are a disrespectful fool!
	WAIT	3
	GOTO	@fight
END
	@dead
END
;------------------------------------------------
; Foothills to Travs J day/night checker
SCENE 24	 trail1
	THEME	4
	IF	DEAD, @timeChecker
	IF	R20, @wasp
	IF	R20, @robbers
	GOTO	@timeChecker
END
	@robbers
	IF	R20, @1Robber
	IF	R20, @2Robbers
	IF	R20, @3Robbers
	GOTO	@robbers
END
	@1Robber
	FIGHT2	114
	IF	WIN, @beatRobbers
	@coinsTaker
	TAKE	G1000000
	N: You have been robbed of all your coins.
	GOTO	@timeChecker
END
	@2Robbers
	FIGHT2	114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@3Robbers
	FIGHT2	114, 114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@beatRobbers
	WAIT	2
	@whichRobberReward
	IF	R20, @brew
	IF	R20, @vip
	IF	R20, @largeSapphire	
	GOTO	@whichRobberReward
END
	@brew
	N: You found a witch's brew!
	SOUND	"bell2.wav"
	HOST_GIVE	I1 	
	GOTO	@timeChecker
END
	@vip
	N: You found a VIP pass!
	SOUND	"bell2.wav"
	HOST_GIVE	I97 	
	GOTO	@timeChecker
END
	@largeSapphire
	N: You found a large sapphire!
	SOUND	"bell2.wav"
	HOST_GIVE	I63 	
	GOTO	@timeChecker
END
	@wasp
	FIGHT	59
	GOTO	@timeChecker
END
	@timeChecker
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	WAIT	2
	N: Darkness falls..
	GOTO	LINK	9, 3	
END
	@day
	WAIT	2
	GOTO	LINK	7, 3
END
;------------------------------------------------------
; The Traveller's Rest
SCENE 25	room2
	WEATHER	0
	FX	0
	THEME	7
	ACTOR 1,	 "Honest John", joshTownsfolk, 17, -10, 100
	ACTOR 2,	 "Honest John", joshTownsfolk, 1, -10, 100
	WAIT	2
	MOVE	1, 15, 100
	1: Hang on, hold ya horses!
	IF ALIVE,	@alive
	1: Oh dear, what have we here?
	1: Appears to be a corpse.
	1: Well well well. Well.. well?
	1: Ahem, sorry, it's an old joke. 
	1: Enter the stone circle nearby here.
	WAIT	2
	MOVE	1, -15, 100
END
	@alive
	1: Who is it?
	WAIT	1
	H: %2 
	1: Please wait a moment..
	MOVE	1, -10, 100
	WAIT	6
	MOVE	2, 15, 100
 	MUSIC	"Midnight_Cowboy.mid"
	BKGND	shop5
	2: Welcome to The Traveller's Rest.
	IF	T1,	@beenAlready
	2: Have we met before?
	2: Since this is your first time here I shall give you a WELL TICKET.
	2: Do not use it unless you have to!
	2: It would normally cost 20,000 coins!
	HOST_GIVE	I99
	HOST_GIVE	T1
	SOUND	"bell2.wav"
	H: Thanks!
	2: ALWAYS carry a Well Ticket.
	2: Use them to instantly transport you back into the stone circle nearby here.
	2: Just remember me when you need firelighters, ok?
	H: Ok.
	@beenAlready
	2: Let me know if you want to buy a horse or a Well Ticket.
	WAIT	1.5
	OFFER	38, 39, 67, 204, 254, 706, 710, 711, 713, 720, 803,
END
	@eventActorClick2
	MENU	"Can I buy a horse around here?=@horse", "I'd like to buy a Well Ticket please..=@ticket"
END
	@horse
	H: Can I buy a horse around here?
	2: Yeah, just out back.
	2: Through there, %2..
	FACE	2, 0
	SEL	2
	POSE	7
	WAIT	2
	GOTO	SCENE	12
END
	@ticket
	H: I'd like to buy a Well Ticket please..
	2: You would like to purchase a Well Ticket for 20,000 coins?
	ASK	15
	IF	YES,	@yes2
	2: Hmm, ok.
END
	@yes2
	IF	G20000	@howMany
	H: But I don't have enough money.
	2: Nevermind then.
END
	@howMany
	IF	I99, @noMore
	HOST_TAKE	G20000
	HOST_GIVE	I99
	2: Into your items it goes..
	SOUND	"bell2.wav"
	2: Happy travelling!
END
	@noMore
	2: Sorry, one Well Ticket at a time please!
END
;------------------------------------------------
; Brackmire Bridge from Travs J
SCENE 26	 bridge1
	WEATHER	0
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	COLOR	13
	COLOR	2014
	@day
	THEME	8
	IF	DEAD, @goto
	IF	R20, @suckerFly
	IF	R20, @robbers
	@goto
	WAIT	2
	GOTO	LINK	6, 18
END
	@robbers
	IF	R20, @1Robber
	IF	R20, @2Robbers
	IF	R20, @3Robbers
	GOTO	@robbers
END
	@1Robber
	FIGHT2	114
	IF	WIN, @beatRobbers
	@coinsTaker
	TAKE	G1000000
	N: You have been robbed of all your coins.
	GOTO	@goto
END
	@2Robbers
	FIGHT2	114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@3Robbers
	FIGHT2	114, 114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@beatRobbers
	WAIT	2
	@whichRobberReward
	IF	R20, @brew
	IF	R20, @vip
	IF	R20, @largeSapphire	
	GOTO	@whichRobberReward
END
	@brew
	N: You found a witch's brew!
	SOUND	"bell2.wav"
	HOST_GIVE	I1 	
	GOTO	@goto
END
	@vip
	N: You found a VIP pass!
	SOUND	"bell2.wav"
	HOST_GIVE	I97 	
	GOTO	@goto
END
	@largeSapphire
	N: You found a large sapphire!
	SOUND	"bell2.wav"
	HOST_GIVE	I63 	
	GOTO	@goto
END
	@suckerFly
	FIGHT	29
	GOTO	@goto
END
;-----------------------------------------
; Travs J to Foothills
SCENE 27	 trail1
	THEME	0
	SET hour, #<num.serverTimeHour>
	COMPARE #<hour>, 0
	IF< @day	
	COMPARE #<hour>, 1
	IF< @night
	COMPARE #<hour>, 2
	IF< @day	
	COMPARE #<hour>, 3
	IF< @night		
	COMPARE #<hour>, 4
	IF< @day
	COMPARE #<hour>, 5
	IF< @night		
	COMPARE #<hour>, 6
	IF< @day
	COMPARE #<hour>, 7
	IF< @night
	COMPARE #<hour>, 8
	IF< @day
	COMPARE #<hour>, 9
	IF< @night
	COMPARE #<hour>, 10
	IF< @day
	COMPARE #<hour>, 11
	IF< @night
	COMPARE #<hour>, 12
	IF< @day
	COMPARE #<hour>, 13
	IF< @night
	COMPARE #<hour>, 14
	IF< @day
	COMPARE #<hour>, 15
	IF< @night
	COMPARE #<hour>, 16
	IF< @day
	COMPARE #<hour>, 17
	IF< @night
	COMPARE #<hour>, 18
	IF< @day
	COMPARE #<hour>, 19
	IF< @night
	COMPARE #<hour>, 20
	IF< @day
	COMPARE #<hour>, 21
	IF< @night
	COMPARE #<hour>, 22
	IF< @day
	COMPARE #<hour>, 23
	IF< @night
	GOTO	@day
END
	@night
	COLOR	13
	COLOR	2014
	@day
	THEME	8
	IF	DEAD, @goto
	IF	R20, @greyWolf
	IF	R20, @robbers
	@goto
	WAIT	2
	GOTO	LINK	4, 0
END
	@robbers
	IF	R20, @1Robber
	IF	R20, @2Robbers
	IF	R20, @3Robbers
	GOTO	@robbers
END
	@1Robber
	FIGHT2	114
	IF	WIN, @beatRobbers
	@coinsTaker
	TAKE	G1000000
	N: You have been robbed of all your coins.
	GOTO	@goto
END
	@2Robbers
	FIGHT2	114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@3Robbers
	FIGHT2	114, 114, 114
	IF	WIN, @beatRobbers
	GOTO	@coinsTaker
END
	@beatRobbers
	WAIT	2
	@whichRobberReward
	IF	R20, @brew
	IF	R20, @vip
	IF	R20, @largeSapphire	
	GOTO	@whichRobberReward
END
	@brew
	N: You found a witch's brew!
	SOUND	"bell2.wav"
	HOST_GIVE	I1 	
	GOTO	@goto
END
	@vip
	N: You found a VIP pass!
	SOUND	"bell2.wav"
	HOST_GIVE	I97 	
	GOTO	@goto
END
	@largeSapphire
	N: You found a large sapphire!
	SOUND	"bell2.wav"
	HOST_GIVE	I63 	
	GOTO	@goto
END
	@greyWolf
	FIGHT	60
	GOTO	@goto
END
;--------------------------------------------------------
; comment