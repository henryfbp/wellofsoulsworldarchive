;=========THE HOLLOWS============
; UNUSED!!!!!!!!!!!
	SCENE	770	room6
;	COLOR	13
;	COLOR	2013
	THEME		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive	
END
;-----------------------
; Well
	SCENE	771	rosewater
	COLOR	13
	COLOR	2013
	THEME	1
	FX	2
	ACTOR	1,	"Faerie", GothNymph, 1, -15, 80
	POSE	1, 4, 2
	WAIT	2
	MOVE	1, 	25, 80, 1
	WAIT	2
	WEATHER 	7249
	SOUND	"thunder1.wav"
	TAKE	H5000
	TAKE	M5000
	WAIT	1
	MOVE	1, -15, 115, 1
END
;-----------------------------------------------
; Tomb 
	SCENE	772	tomb1
	ACTOR	1,	"", actors2, 5, 15, 102
	ACTOR	2,	"", actors2, 5, -50, 102
	THEME		0
	FX		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive
END
	@eventActorClick1
	IF	DEAD, @end
	MENU	"Open casket=@open"
END
	@eventActorClick2
	N: There is nothing more of interest here.
END
	@open
	N: You prise open the casket..
	WAIT	2
	@loop
	IF	R5, @treasureSelection
	IF	R10, @fightSelection
	GOTO	@loop
END
	@treasureSelection
	MOVE	1, -50, 102, 1
	MOVE	2, 15, 102, 1
	SOUND	"bell2.wav"
	@whichTreasure
	IF	R10, @treasure1
	IF	R5, @treasure2
	IF	R5, @treasure3
	IF	R5, @treasure4
	GOTO	@whichTreasure
END
	@treasure1
	N: You find a silver ring!
	HOST_GIVE	I903
END
	@treasure2
	N: You find an ebony ring!
	HOST_GIVE	I901
END
	@treasure3
	N: You find a gold ring!
	HOST_GIVE	I900
END
	@treasure4
	N: You find a mithril ring!
	HOST_GIVE	I904
END
	@fightSelection
	IF	R5, @fight1
	IF	R5, @fight2
	IF	R5, @fight3
	IF	R5, @fight4
	GOTO	@fightSelection
END
	@fight1
	N: You disturbed an undead spirit!
	FIGHT2	148
	IF	WIN, @treasureSelection	
END
	@fight2
	N: You disturbed the undead and a cloud of sucker flies!
	FIGHT2	23, 29, 29, 29, 29, 29, 29, 29, 29
	IF	WIN, @treasureSelection		
END
	@fight3
	N: You disturbed the undead!
	FIGHT2	121	
	IF	WIN, @treasureSelection	
END
	@fight4
	N: You disturbed a mummified undead!
	FIGHT2	138
	IF	WIN, @treasureSelection		
END
	@end		
END
;-----------------------
; Tomb 
	SCENE	773	tomb1
	ACTOR	1,	"", actors2, 5, 15, 102
	ACTOR	2,	"", actors2, 5, -50, 102
	THEME		0
	FX		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive
END
	@eventActorClick1
	IF	DEAD, @end
	MENU	"Open casket=@open"
END
	@eventActorClick2
	N: There is nothing more of interest here.
END
	@open
	N: You prise open the casket..
	MOVE	1, -50, 102, 1
	MOVE	2, 15, 102, 1
	WAIT	2
	SOUND	"bell2.wav"
	@whichTreasure
	IF	R5, @treasure1
	IF	R5, @treasure2
	IF	R5, @treasure3
	IF	R5, @treasure4
	GOTO	@whichTreasure
END
	@treasure1
	N: You find a bone ring!
	HOST_GIVE	I902
END
	@treasure2
	N: You find a pair of leather boots!
	HOST_GIVE	I708
END
	@treasure3
	N: You find a robe!
	HOST_GIVE	I251
END
	@treasure4
	N: You find a wooden bow!
	HOST_GIVE	I502
END
	@end		
END
;-----------------------
; Tomb
	SCENE	774	tomb1
	THEME		0
;	COLOR		13
	ACTOR	1,	"", actors2, 0, 50, 80
	ACTOR	2,	"Ghostly voice", poltergeist, 1, -60, 75
	ACTOR	3,	"Disembodied voice", poltergeist, 1, -15, 100
	IF	ALIVE, @alive
END
	@alive
END
	@eventActorClick1
	IF	DEAD, @dead
	MENU	"Examine alter=@examine"
END
	@examine
	IF	-I6, @nothing
	IF	I7, @nothing
	IF	T18, @nothing
	N: You see a religious artifact.
	WAIT	3
	MENU	"Take artifact=@takeArtifact", "Search for a trap=@trap", "Shout: 'Is there anybody there?'=@shout", "Consider life's possibilities=@possibilities"
END
	@takeArtifact
	N: You reach for the artifact..
	WAIT	2
	MOVE	2,	60, 75, 1
	MOVE	3,	15, 100, 1
	SOUND	"ghost2.wav"
	2: Hhhhhhhhhhhhhhhh....
	2: Why have you come here, %2?
	3: Cththth.. Cththth.. 
	3: Did you think you could just TAKE our artifact?
	2: Ssssssssssss.....
	2: It belongs here with us..
	3: Kaafffth....
	3: And so now do YOU!
	SOUND	"laugh2.wav"
	WAIT	1.5
	MOVE	2, 115, 75, 1
	MOVE	3, -15, 100, 1
	FIGHT2	135, 135, 135, 135, 29
	IF	WIN,	@fight2
	SOUND	"laugh2.wav"
END
	@fight2
	FIGHT2	139, 139, 139, 139
	IF	WIN,	@fight3
	SOUND	"laugh2.wav"
END
	@fight3
	SOUND	"ghost2.wav"
	WAIT	3
	FIGHT2	136, 136, 136, 136, 140, 140
	IF	WIN,	@fight4
	SOUND	"laugh2.wav"
END
	@fight4
	FIGHT2	29, 29, 29, 29, 29, 29, 29, 29, 29
	SOUND	"ghost2.wav"
	FIGHT2	138, 168, 168, 148, 148 
	IF	WIN,	@fight5
	SOUND	"laugh2.wav"
END
	@fight5
	SOUND	"ghost2.wav"
	WAIT	3
	SOUND	"chant1.wav"
	HOST_GIVE	I7
	N: You found the artifact!	
END
	@trap
	N: You search the alter for traps.
	WAIT	3
	N: The alter appears to be safe.
END
	@shout
	H: ! Is there anybody there?
	WAIT	3
	N: You feel like a fool.
END
	@possibilities
	N: You attempt to consider life's possibilities.
	WAIT	3
	N: Since you are not omniscient, you cannot know all of life's possibilities.
	WAIT	4
	N: You do, however, consider some of them.
	WAIT	3
	N: You realize some of life's possibilities are irrelevant to certain situations.
	WAIT	4
	N: You conclude that considering life's possibilities at a time like this is a pointless waste of time. 
END
	@nothing
	N: There doesn't appear to be much of interest to you here.
END
	@dead
END
;-----------------------
; Tomb
	SCENE	775	tomb1
	ACTOR	1,	"", actors2, 5, 15, 102
	ACTOR	2,	"", actors2, 5, -50, 102
	THEME		0
	FX		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive
END
	@eventActorClick1
	IF	DEAD, @end
	MENU	"Open casket=@open"
END
	@eventActorClick2
	N: There is nothing more of interest here.
END
	@open
	N: You prise open the casket..
	WAIT	2
	@fightSelection
	IF	R5, @fight1
	IF	R5, @fight2
	IF	R5, @fight3
	IF	R5, @fight4
	GOTO	@fightSelection
END
	@fight1
	N: You disturbed a spider!
	FIGHT2	202
END
	@fight2
	N: You disturbed the undead!
	FIGHT2	136
END
	@fight3
	N: You disturbed the undead!
	FIGHT2	168	
END
	@fight4
	N: You disturbed eaters of the flesh!
	FIGHT2	27, 27, 27, 27, 27, 27, 27, 27, 27, 27
END
	@end		
END
;-----------------------
; UNUSED!!!!!!!!!!!!!!!
	SCENE	776	forest2
;	COLOR	13
	THEME		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive	
END
;-----------------------
; Tomb 
	SCENE	777	tomb1
	ACTOR	1,	"", actors2, 5, 15, 102
	ACTOR	2,	"", actors2, 5, -50, 102
	THEME		0
	FX		0
	WEATHER	0
	IF	ALIVE, @alive
END
	@alive
END
	@eventActorClick1
	IF	DEAD, @end
	MENU	"Open casket=@open"
END
	@eventActorClick2
	N: There is nothing more of interest here.
END
	@open
	N: You prise open the casket..
	MOVE	1, -50, 102, 1
	MOVE	2, 15, 102, 1
	WAIT	2
	SOUND	"bell2.wav"
	@whichTreasure
	IF	R15, @treasure1
	IF	R15, @treasure2
	IF	R5, @treasure3
	IF	R5, @treasure4
	GOTO	@whichTreasure
END
	@treasure1
	N: You find a gold coin!
	HOST_GIVE	G1
END
	@treasure2
	N: You find a leather waistcoat!
	HOST_GIVE	I252
END
	@treasure3
	N: You find a small emerald!
	HOST_GIVE	I58
END
	@treasure4
	N: You find a sapphire amulet!
	HOST_GIVE	I955
END
	@end		
END
;-----------------------
; comment